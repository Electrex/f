.class Landroid/support/v4/view/ViewCompat$Api21ViewCompatImpl;
.super Landroid/support/v4/view/ViewCompat$KitKatViewCompatImpl;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1147
    invoke-direct {p0}, Landroid/support/v4/view/ViewCompat$KitKatViewCompatImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1160
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatApi21;->a(Landroid/view/View;)V

    .line 1161
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 1165
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatApi21;->a(Landroid/view/View;F)V

    .line 1166
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/OnApplyWindowInsetsListener;)V
    .locals 0

    .prologue
    .line 1185
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatApi21;->a(Landroid/view/View;Landroid/support/v4/view/OnApplyWindowInsetsListener;)V

    .line 1186
    return-void
.end method
