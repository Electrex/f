.class Landroid/support/v4/view/ViewCompat$HCViewCompatImpl;
.super Landroid/support/v4/view/ViewCompat$GBViewCompatImpl;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0}, Landroid/support/v4/view/ViewCompat$GBViewCompatImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public a(III)I
    .locals 1

    .prologue
    .line 802
    invoke-static {p1, p2, p3}, Landroid/support/v4/view/ViewCompatHC;->a(III)I

    move-result v0

    return v0
.end method

.method a()J
    .locals 2

    .prologue
    .line 778
    invoke-static {}, Landroid/support/v4/view/ViewCompatHC;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 786
    invoke-static {p1, p2, p3}, Landroid/support/v4/view/ViewCompatHC;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 787
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 796
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewCompat$HCViewCompatImpl;->h(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v4/view/ViewCompat$HCViewCompatImpl;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 798
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 799
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 922
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->a(Landroid/view/View;Z)V

    .line 923
    return-void
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 826
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->a(Landroid/view/View;F)V

    .line 827
    return-void
.end method

.method public b(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 927
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->b(Landroid/view/View;Z)V

    .line 928
    return-void
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 830
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->b(Landroid/view/View;F)V

    .line 831
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 834
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->c(Landroid/view/View;F)V

    .line 835
    return-void
.end method

.method public e(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 858
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->d(Landroid/view/View;F)V

    .line 859
    return-void
.end method

.method public f(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 862
    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompatHC;->e(Landroid/view/View;F)V

    .line 863
    return-void
.end method

.method public g(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 782
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->a(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 790
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public l(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 806
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public m(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 814
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->d(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public n(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 818
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->e(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public o(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 822
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->f(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public p(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 899
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->g(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public v(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 917
    invoke-static {p1}, Landroid/support/v4/view/ViewCompatHC;->h(Landroid/view/View;)V

    .line 918
    return-void
.end method
