.class public final Landroid/support/v7/appcompat/R$color;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0a016c

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0a016d

.field public static final abc_input_method_navigation_guard:I = 0x7f0a0000

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0a016e

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0a016f

.field public static final abc_primary_text_material_dark:I = 0x7f0a0170

.field public static final abc_primary_text_material_light:I = 0x7f0a0171

.field public static final abc_search_url_text:I = 0x7f0a0172

.field public static final abc_search_url_text_normal:I = 0x7f0a0001

.field public static final abc_search_url_text_pressed:I = 0x7f0a0002

.field public static final abc_search_url_text_selected:I = 0x7f0a0003

.field public static final abc_secondary_text_material_dark:I = 0x7f0a0173

.field public static final abc_secondary_text_material_light:I = 0x7f0a0174

.field public static final accent_material_dark:I = 0x7f0a0004

.field public static final accent_material_light:I = 0x7f0a0005

.field public static final background_floating_material_dark:I = 0x7f0a0014

.field public static final background_floating_material_light:I = 0x7f0a0015

.field public static final background_material_dark:I = 0x7f0a0016

.field public static final background_material_light:I = 0x7f0a0017

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0a003e

.field public static final bright_foreground_disabled_material_light:I = 0x7f0a003f

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0a0040

.field public static final bright_foreground_inverse_material_light:I = 0x7f0a0041

.field public static final bright_foreground_material_dark:I = 0x7f0a0042

.field public static final bright_foreground_material_light:I = 0x7f0a0043

.field public static final button_material_dark:I = 0x7f0a004e

.field public static final button_material_light:I = 0x7f0a004f

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0a0089

.field public static final dim_foreground_disabled_material_light:I = 0x7f0a008a

.field public static final dim_foreground_material_dark:I = 0x7f0a008b

.field public static final dim_foreground_material_light:I = 0x7f0a008c

.field public static final highlighted_text_material_dark:I = 0x7f0a00a9

.field public static final highlighted_text_material_light:I = 0x7f0a00aa

.field public static final hint_foreground_material_dark:I = 0x7f0a00ab

.field public static final hint_foreground_material_light:I = 0x7f0a00ac

.field public static final link_text_material_dark:I = 0x7f0a00f3

.field public static final link_text_material_light:I = 0x7f0a00f4

.field public static final material_blue_grey_800:I = 0x7f0a00f5

.field public static final material_blue_grey_900:I = 0x7f0a00f6

.field public static final material_blue_grey_950:I = 0x7f0a00f7

.field public static final material_deep_teal_200:I = 0x7f0a00f8

.field public static final material_deep_teal_500:I = 0x7f0a00f9

.field public static final primary_dark_material_dark:I = 0x7f0a011d

.field public static final primary_dark_material_light:I = 0x7f0a011e

.field public static final primary_material_dark:I = 0x7f0a011f

.field public static final primary_material_light:I = 0x7f0a0120

.field public static final primary_text_default_material_dark:I = 0x7f0a0121

.field public static final primary_text_default_material_light:I = 0x7f0a0122

.field public static final primary_text_disabled_material_dark:I = 0x7f0a0123

.field public static final primary_text_disabled_material_light:I = 0x7f0a0124

.field public static final ripple_material_dark:I = 0x7f0a0144

.field public static final ripple_material_light:I = 0x7f0a0145

.field public static final secondary_text_default_material_dark:I = 0x7f0a0146

.field public static final secondary_text_default_material_light:I = 0x7f0a0147

.field public static final secondary_text_disabled_material_dark:I = 0x7f0a0148

.field public static final secondary_text_disabled_material_light:I = 0x7f0a0149

.field public static final switch_thumb_normal_material_dark:I = 0x7f0a014a

.field public static final switch_thumb_normal_material_light:I = 0x7f0a014b
