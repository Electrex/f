.class public Landroid/support/v7/internal/app/ToolbarActionBar;
.super Landroid/support/v7/app/ActionBar;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v7/internal/widget/DecorToolbar;

.field private b:Z

.field private c:Landroid/support/v7/internal/app/WindowCallback;

.field private d:Z

.field private e:Z

.field private f:Ljava/util/ArrayList;

.field private g:Landroid/view/Window;

.field private h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

.field private final i:Ljava/lang/Runnable;

.field private final j:Landroid/support/v7/widget/Toolbar$OnMenuItemClickListener;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window;Landroid/support/v7/internal/app/WindowCallback;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/support/v7/app/ActionBar;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->f:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Landroid/support/v7/internal/app/ToolbarActionBar$1;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/app/ToolbarActionBar$1;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;)V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->i:Ljava/lang/Runnable;

    .line 72
    new-instance v0, Landroid/support/v7/internal/app/ToolbarActionBar$2;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/app/ToolbarActionBar$2;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;)V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->j:Landroid/support/v7/widget/Toolbar$OnMenuItemClickListener;

    .line 82
    new-instance v0, Landroid/support/v7/internal/widget/ToolbarWidgetWrapper;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/support/v7/internal/widget/ToolbarWidgetWrapper;-><init>(Landroid/support/v7/widget/Toolbar;Z)V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    .line 83
    new-instance v0, Landroid/support/v7/internal/app/ToolbarActionBar$ToolbarCallbackWrapper;

    invoke-direct {v0, p0, p4}, Landroid/support/v7/internal/app/ToolbarActionBar$ToolbarCallbackWrapper;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;Landroid/support/v7/internal/app/WindowCallback;)V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    .line 84
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    invoke-interface {v0, v1}, Landroid/support/v7/internal/widget/DecorToolbar;->a(Landroid/support/v7/internal/app/WindowCallback;)V

    .line 85
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->j:Landroid/support/v7/widget/Toolbar$OnMenuItemClickListener;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/Toolbar;->setOnMenuItemClickListener(Landroid/support/v7/widget/Toolbar$OnMenuItemClickListener;)V

    .line 86
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0, p2}, Landroid/support/v7/internal/widget/DecorToolbar;->a(Ljava/lang/CharSequence;)V

    .line 88
    iput-object p3, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->g:Landroid/view/Window;

    .line 89
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/app/ToolbarActionBar;)Landroid/support/v7/internal/app/WindowCallback;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/internal/app/ToolbarActionBar;Landroid/view/Menu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/support/v7/internal/app/ToolbarActionBar;->a(Landroid/view/Menu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/Menu;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 503
    invoke-direct {p0, p1}, Landroid/support/v7/internal/app/ToolbarActionBar;->b(Landroid/view/Menu;)V

    .line 505
    if-eqz p1, :cond_0

    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    if-nez v1, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-object v0

    .line 509
    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/ListMenuPresenter;->a()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 510
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/DecorToolbar;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ListMenuPresenter;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/MenuView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/internal/app/ToolbarActionBar;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->b:Z

    return p1
.end method

.method private b(Landroid/view/Menu;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 516
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/support/v7/internal/view/menu/MenuBuilder;

    if-eqz v0, :cond_0

    .line 517
    check-cast p1, Landroid/support/v7/internal/view/menu/MenuBuilder;

    .line 519
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->b()Landroid/content/Context;

    move-result-object v0

    .line 520
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 521
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 522
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 525
    sget v3, Landroid/support/v7/appcompat/R$attr;->panelMenuListTheme:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 526
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_1

    .line 527
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 532
    :goto_0
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 533
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 536
    new-instance v0, Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    sget v2, Landroid/support/v7/appcompat/R$layout;->abc_list_menu_item_layout:I

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/view/menu/ListMenuPresenter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    .line 537
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    new-instance v1, Landroid/support/v7/internal/app/ToolbarActionBar$PanelMenuPresenterCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/v7/internal/app/ToolbarActionBar$PanelMenuPresenterCallback;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;Landroid/support/v7/internal/app/ToolbarActionBar$1;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ListMenuPresenter;->a(Landroid/support/v7/internal/view/menu/MenuPresenter$Callback;)V

    .line 538
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->h:Landroid/support/v7/internal/view/menu/ListMenuPresenter;

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/MenuBuilder;->a(Landroid/support/v7/internal/view/menu/MenuPresenter;)V

    .line 540
    :cond_0
    return-void

    .line 529
    :cond_1
    sget v1, Landroid/support/v7/appcompat/R$style;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/internal/app/ToolbarActionBar;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->b:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v7/internal/app/ToolbarActionBar;)Landroid/support/v7/internal/widget/DecorToolbar;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/internal/app/ToolbarActionBar;)Landroid/view/Window;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->g:Landroid/view/Window;

    return-object v0
.end method

.method private g()Landroid/view/Menu;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 574
    iget-boolean v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->d:Z

    if-nez v0, :cond_0

    .line 575
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    new-instance v1, Landroid/support/v7/internal/app/ToolbarActionBar$ActionMenuPresenterCallback;

    invoke-direct {v1, p0, v3}, Landroid/support/v7/internal/app/ToolbarActionBar$ActionMenuPresenterCallback;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;Landroid/support/v7/internal/app/ToolbarActionBar$1;)V

    new-instance v2, Landroid/support/v7/internal/app/ToolbarActionBar$MenuBuilderCallback;

    invoke-direct {v2, p0, v3}, Landroid/support/v7/internal/app/ToolbarActionBar$MenuBuilderCallback;-><init>(Landroid/support/v7/internal/app/ToolbarActionBar;Landroid/support/v7/internal/app/ToolbarActionBar$1;)V

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/DecorToolbar;->a(Landroid/support/v7/internal/view/menu/MenuPresenter$Callback;Landroid/support/v7/internal/view/menu/MenuBuilder$Callback;)V

    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->d:Z

    .line 579
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->r()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->p()I

    move-result v0

    return v0
.end method

.method public a(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/app/WindowCallback;->a(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 150
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 256
    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/DecorToolbar;->b(Ljava/lang/CharSequence;)V

    .line 257
    return-void

    .line 256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 266
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->p()I

    move-result v0

    .line 267
    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/DecorToolbar;->c(I)V

    .line 268
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBar;->a(Landroid/content/res/Configuration;)V

    .line 200
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/DecorToolbar;->c(Landroid/graphics/drawable/Drawable;)V

    .line 298
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/DecorToolbar;->a(Ljava/lang/CharSequence;)V

    .line 247
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 282
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/app/ToolbarActionBar;->a(II)V

    .line 283
    return-void

    .line 282
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 478
    invoke-direct {p0}, Landroid/support/v7/internal/app/ToolbarActionBar;->g()Landroid/view/Menu;

    move-result-object v1

    .line 479
    if-eqz v1, :cond_0

    invoke-interface {v1, p1, p2, v0}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->b()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/DecorToolbar;->e(I)V

    .line 190
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/DecorToolbar;->b(Landroid/graphics/drawable/Drawable;)V

    .line 170
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 436
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 437
    const/4 v0, 0x1

    return v0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->a:Landroid/support/v7/internal/widget/DecorToolbar;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/DecorToolbar;->e()V

    .line 444
    const/4 v0, 0x1

    .line 446
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Landroid/support/v7/internal/app/WindowCallback;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    return-object v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method f()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 450
    invoke-direct {p0}, Landroid/support/v7/internal/app/ToolbarActionBar;->g()Landroid/view/Menu;

    move-result-object v1

    .line 451
    instance-of v2, v1, Landroid/support/v7/internal/view/menu/MenuBuilder;

    if-eqz v2, :cond_4

    move-object v0, v1

    check-cast v0, Landroid/support/v7/internal/view/menu/MenuBuilder;

    move-object v2, v0

    .line 452
    :goto_0
    if-eqz v2, :cond_0

    .line 453
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/MenuBuilder;->g()V

    .line 456
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 457
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v1}, Landroid/support/v7/internal/app/WindowCallback;->a(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->c:Landroid/support/v7/internal/app/WindowCallback;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4, v1}, Landroid/support/v7/internal/app/WindowCallback;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 459
    :cond_1
    invoke-interface {v1}, Landroid/view/Menu;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    :cond_2
    if-eqz v2, :cond_3

    .line 463
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/MenuBuilder;->h()V

    .line 466
    :cond_3
    return-void

    :cond_4
    move-object v2, v0

    .line 451
    goto :goto_0

    .line 462
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 463
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/MenuBuilder;->h()V

    :cond_5
    throw v0
.end method

.method public f(Z)V
    .locals 3

    .prologue
    .line 491
    iget-boolean v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->e:Z

    if-ne p1, v0, :cond_1

    .line 500
    :cond_0
    return-void

    .line 494
    :cond_1
    iput-boolean p1, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->e:Z

    .line 496
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 497
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 498
    iget-object v0, p0, Landroid/support/v7/internal/app/ToolbarActionBar;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;

    invoke-interface {v0, p1}, Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;->a(Z)V

    .line 497
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
