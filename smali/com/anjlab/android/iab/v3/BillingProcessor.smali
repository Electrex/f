.class public Lcom/anjlab/android/iab/v3/BillingProcessor;
.super Lcom/anjlab/android/iab/v3/BillingBase;
.source "SourceFile"


# instance fields
.field private a:Lcom/android/vending/billing/IInAppBillingService;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/anjlab/android/iab/v3/BillingCache;

.field private e:Lcom/anjlab/android/iab/v3/BillingCache;

.field private f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

.field private g:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/anjlab/android/iab/v3/BillingBase;-><init>(Landroid/content/Context;)V

    .line 69
    new-instance v0, Lcom/anjlab/android/iab/v3/BillingProcessor$1;

    invoke-direct {v0, p0}, Lcom/anjlab/android/iab/v3/BillingProcessor$1;-><init>(Lcom/anjlab/android/iab/v3/BillingProcessor;)V

    iput-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->g:Landroid/content/ServiceConnection;

    .line 90
    iput-object p2, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->c:Ljava/lang/String;

    .line 91
    iput-object p3, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->b:Ljava/lang/String;

    .line 93
    new-instance v0, Lcom/anjlab/android/iab/v3/BillingCache;

    const-string v1, ".products.cache.v2_6"

    invoke-direct {v0, p1, v1}, Lcom/anjlab/android/iab/v3/BillingCache;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    .line 94
    new-instance v0, Lcom/anjlab/android/iab/v3/BillingCache;

    const-string v1, ".subscriptions.cache.v2_6"

    invoke-direct {v0, p1, v1}, Lcom/anjlab/android/iab/v3/BillingCache;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->e:Lcom/anjlab/android/iab/v3/BillingCache;

    .line 95
    invoke-direct {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->g()V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/anjlab/android/iab/v3/BillingProcessor;Lcom/android/vending/billing/IInAppBillingService;)Lcom/android/vending/billing/IInAppBillingService;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    return-object p1
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v7

    .line 212
    :goto_0
    return v0

    .line 185
    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 186
    invoke-direct {p0, v5}, Lcom/anjlab/android/iab/v3/BillingProcessor;->g(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->b:Ljava/lang/String;

    move-object v3, p2

    move-object v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/android/vending/billing/IInAppBillingService;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_2

    .line 189
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 190
    if-nez v1, :cond_4

    .line 191
    const-string v1, "BUY_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 192
    if-eqz p1, :cond_3

    .line 193
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const v2, 0x1f76a0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    .line 208
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v1, 0x67

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 209
    :catch_0
    move-exception v0

    .line 210
    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 212
    goto :goto_0

    .line 196
    :cond_4
    const/4 v0, 0x7

    if-ne v1, v0, :cond_7

    .line 197
    :try_start_1
    invoke-virtual {p0, p2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0, p2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 198
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->e()Z

    .line 199
    :cond_5
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_2

    .line 200
    invoke-virtual {p0, p2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->e(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/TransactionDetails;

    move-result-object v0

    .line 201
    if-nez v0, :cond_6

    .line 202
    invoke-virtual {p0, p2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->f(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/TransactionDetails;

    move-result-object v0

    .line 203
    :cond_6
    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    invoke-interface {v1, p2, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/TransactionDetails;)V

    goto :goto_1

    .line 205
    :cond_7
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v1, 0x65

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/anjlab/android/iab/v3/BillingProcessor;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->h()Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 164
    :goto_0
    return v0

    .line 146
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v0, v1, v4, p1, v5}, Lcom/android/vending/billing/IInAppBillingService;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 147
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 148
    invoke-virtual {p2}, Lcom/anjlab/android/iab/v3/BillingCache;->d()V

    .line 149
    const-string v1, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 150
    const-string v1, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    move v4, v2

    .line 151
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 152
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 154
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 155
    :goto_2
    const-string v8, "productId"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7, v0, v1}, Lcom/anjlab/android/iab/v3/BillingCache;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move-object v1, v3

    .line 154
    goto :goto_2

    .line 158
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v1, :cond_3

    .line 161
    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v3, 0x64

    invoke-interface {v1, v3, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V

    .line 162
    :cond_3
    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 164
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const/4 v0, 0x1

    .line 354
    :goto_0
    return v0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->c:Ljava/lang/String;

    invoke-static {p1, v0, p2, p3}, Lcom/anjlab/android/iab/v3/Security;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/anjlab/android/iab/v3/BillingProcessor;)Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    return-object v0
.end method

.method private b(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Lcom/anjlab/android/iab/v3/TransactionDetails;
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p2, p1}, Lcom/anjlab/android/iab/v3/BillingCache;->b(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/PurchaseInfo;

    move-result-object v1

    .line 217
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/anjlab/android/iab/v3/PurchaseInfo;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    :try_start_0
    new-instance v0, Lcom/anjlab/android/iab/v3/TransactionDetails;

    invoke-direct {v0, v1}, Lcom/anjlab/android/iab/v3/TransactionDetails;-><init>(Lcom/anjlab/android/iab/v3/PurchaseInfo;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-object v0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    const-string v0, "iabv3"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load saved purchase details for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 251
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    if-eqz v0, :cond_3

    .line 253
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 254
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 256
    const-string v3, "ITEM_ID_LIST"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 257
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->b:Ljava/lang/String;

    invoke-interface {v0, v3, v4, p2, v2}, Lcom/android/vending/billing/IInAppBillingService;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 258
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 259
    if-nez v2, :cond_1

    .line 260
    const-string v2, "DETAILS_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 261
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 262
    const-string v0, "productId"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Lcom/anjlab/android/iab/v3/SkuDetails;

    invoke-direct {v0, v3}, Lcom/anjlab/android/iab/v3/SkuDetails;-><init>(Lorg/json/JSONObject;)V

    .line 275
    :goto_0
    return-object v0

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_2

    .line 268
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V

    .line 269
    :cond_2
    const-string v0, "iabv3"

    const-string v3, "Failed to retrieve info for %s: error %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    move-object v0, v1

    .line 275
    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    .line 272
    const-string v2, "iabv3"

    const-string v3, "Failed to call getSkuDetails %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private g()V
    .locals 4

    .prologue
    .line 100
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".purchase.last.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 373
    return-void
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".products.restored.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".purchase.last.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/anjlab/android/iab/v3/BillingBase;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 295
    const v2, 0x1f76a0

    if-eq p1, v2, :cond_0

    .line 339
    :goto_0
    return v0

    .line 297
    :cond_0
    if-nez p3, :cond_1

    .line 298
    const-string v1, "iabv3"

    const-string v2, "handleActivityResult: data is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_1
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {p3, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 302
    const-string v3, "iabv3"

    const-string v4, "resultCode = %d, responseCode = %d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-direct {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->i()Ljava/lang/String;

    move-result-object v3

    .line 304
    const/4 v0, -0x1

    if-ne p2, v0, :cond_7

    if-nez v2, :cond_7

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 305
    const-string v0, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 306
    const-string v0, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 308
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 309
    const-string v5, "productId"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 310
    const-string v6, "developerPayload"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-nez v0, :cond_2

    .line 312
    const-string v0, ""

    .line 313
    :cond_2
    const-string v6, "subs"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    .line 314
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 315
    invoke-direct {p0, v5, v2, v4}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 316
    if-eqz v6, :cond_4

    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->e:Lcom/anjlab/android/iab/v3/BillingCache;

    .line 317
    :goto_1
    invoke-virtual {v0, v5, v2, v4}, Lcom/anjlab/android/iab/v3/BillingCache;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_3

    .line 319
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    new-instance v3, Lcom/anjlab/android/iab/v3/TransactionDetails;

    new-instance v6, Lcom/anjlab/android/iab/v3/PurchaseInfo;

    invoke-direct {v6, v2, v4}, Lcom/anjlab/android/iab/v3/PurchaseInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v6}, Lcom/anjlab/android/iab/v3/TransactionDetails;-><init>(Lcom/anjlab/android/iab/v3/PurchaseInfo;)V

    invoke-interface {v0, v5, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/TransactionDetails;)V

    :cond_3
    :goto_2
    move v0, v1

    .line 339
    goto/16 :goto_0

    .line 316
    :cond_4
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    goto :goto_1

    .line 321
    :cond_5
    const-string v0, "iabv3"

    const-string v2, "Public key signature doesn\'t match!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_3

    .line 323
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v2, 0x66

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 330
    :catch_0
    move-exception v0

    .line 331
    const-string v2, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_3

    .line 333
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v2, 0x6e

    invoke-interface {v0, v2, v8}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V

    goto :goto_2

    .line 326
    :cond_6
    :try_start_1
    const-string v2, "iabv3"

    const-string v4, "Payload mismatch: %s != %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_3

    .line 328
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/16 v2, 0x66

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 336
    :cond_7
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v0, :cond_3

    .line 337
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    invoke-interface {v0, v2, v8}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V

    goto :goto_2
.end method

.method public a(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 174
    const-string v0, "inapp"

    invoke-direct {p0, p1, p2, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-virtual {v0, p1}, Lcom/anjlab/android/iab/v3/BillingCache;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->e:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-virtual {v0, p1}, Lcom/anjlab/android/iab/v3/BillingCache;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->g:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    :try_start_0
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-virtual {v0}, Lcom/anjlab/android/iab/v3/BillingCache;->c()V

    .line 119
    invoke-super {p0}, Lcom/anjlab/android/iab/v3/BillingBase;->c()V

    .line 120
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    const-string v1, "iabv3"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d()Z

    move-result v2

    if-nez v2, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    .line 231
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-direct {p0, p1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Lcom/anjlab/android/iab/v3/TransactionDetails;

    move-result-object v2

    .line 232
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/anjlab/android/iab/v3/TransactionDetails;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 233
    iget-object v3, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/anjlab/android/iab/v3/TransactionDetails;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v2}, Lcom/android/vending/billing/IInAppBillingService;->b(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 234
    if-nez v2, :cond_2

    .line 235
    iget-object v2, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-virtual {v2, p1}, Lcom/anjlab/android/iab/v3/BillingCache;->c(Ljava/lang/String;)V

    .line 236
    const-string v2, "iabv3"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Successfully consumed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " purchase."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 237
    goto :goto_0

    .line 239
    :cond_2
    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    if-eqz v1, :cond_3

    .line 240
    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->f:Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;->a(ILjava/lang/Throwable;)V

    .line 241
    :cond_3
    const-string v1, "iabv3"

    const-string v3, "Failed to consume %s: error %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v1

    .line 245
    const-string v2, "iabv3"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;
    .locals 1

    .prologue
    .line 279
    const-string v0, "inapp"

    invoke-direct {p0, p1, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->a:Lcom/android/vending/billing/IInAppBillingService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/TransactionDetails;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-direct {p0, p1, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Lcom/anjlab/android/iab/v3/TransactionDetails;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "inapp"

    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->d:Lcom/anjlab/android/iab/v3/BillingCache;

    .line 169
    invoke-direct {p0, v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "subs"

    iget-object v1, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->e:Lcom/anjlab/android/iab/v3/BillingCache;

    .line 170
    invoke-direct {p0, v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/TransactionDetails;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/anjlab/android/iab/v3/BillingProcessor;->e:Lcom/anjlab/android/iab/v3/BillingCache;

    invoke-direct {p0, p1, v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b(Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingCache;)Lcom/anjlab/android/iab/v3/TransactionDetails;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".products.restored.v2_6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 369
    return-void
.end method
