.class public Lcom/cgollner/systemmonitor/BatteryCircleView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/RectF;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Paint;

.field private k:I

.field private l:I

.field private m:F

.field private n:Landroid/graphics/EmbossMaskFilter;

.field private o:Landroid/graphics/BlurMaskFilter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 23
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    .line 35
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->a()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    .line 40
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->a()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    .line 45
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->a()V

    .line 46
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/16 v2, 0xa

    const/16 v3, 0xd2

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 57
    const/16 v1, 0xd

    .line 58
    iput v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->k:I

    .line 59
    iput v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->l:I

    .line 60
    const/high16 v2, 0x3fe00000    # 1.75f

    iput v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->m:F

    .line 62
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    .line 63
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 67
    new-instance v2, Landroid/graphics/EmbossMaskFilter;

    const/4 v3, 0x3

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    const v4, 0x3ecccccd    # 0.4f

    const/high16 v5, 0x40c00000    # 6.0f

    const/high16 v6, 0x40600000    # 3.5f

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->n:Landroid/graphics/EmbossMaskFilter;

    .line 70
    new-instance v2, Landroid/graphics/BlurMaskFilter;

    const/high16 v3, 0x41800000    # 16.0f

    sget-object v4, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v2, v3, v4}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->o:Landroid/graphics/BlurMaskFilter;

    .line 72
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    .line 73
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 76
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    .line 77
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/cgollner/systemmonitor/lib/R$color;->holo_red_dark:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    .line 82
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/cgollner/systemmonitor/lib/R$color;->holo_orange_dark:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 84
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 86
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    .line 87
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    const/16 v3, 0xb6

    const/16 v4, 0xdb

    const/16 v5, 0x49

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 91
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    .line 92
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/cgollner/systemmonitor/lib/R$color;->holo_green_dark:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_0

    const-string v2, "theme"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 98
    :cond_0
    if-ne v0, v7, :cond_1

    const/4 v0, -0x1

    .line 99
    :goto_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    .line 100
    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 103
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->h:Landroid/graphics/RectF;

    .line 104
    return-void

    .line 98
    :cond_1
    const/high16 v0, -0x1000000

    goto :goto_0

    .line 67
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public getCircleWidthMultiplier()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->k:I

    return v0
.end method

.method public getCurrentColor()I
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v1, 0x41c80000    # 25.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0

    .line 157
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v1, 0x42480000    # 50.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto :goto_0

    .line 159
    :cond_1
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v1, 0x42960000    # 75.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public getInnerCircleBgColor()I
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getInnerCircleColor()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getOuterCircleWidth()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->l:I

    return v0
.end method

.method public getPercentageCircleWidth()I
    .locals 2

    .prologue
    .line 201
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->m:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const/high16 v4, 0x40200000    # 2.5f

    const-wide v12, 0x3f91df46a1fae711L    # 0.0174532925

    const/high16 v11, 0x41200000    # 10.0f

    .line 110
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    move v6, v1

    .line 113
    :goto_1
    const v1, 0x3c23d70a    # 0.01f

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 114
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->k:I

    int-to-float v3, v3

    div-float/2addr v3, v11

    mul-float/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->k:I

    int-to-float v3, v3

    div-float/2addr v3, v11

    mul-float/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 117
    div-int/lit8 v7, v0, 0x2

    div-int/lit8 v8, v6, 0x2

    .line 118
    int-to-float v0, v0

    div-float/2addr v0, v4

    int-to-float v2, v6

    div-float/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 119
    int-to-float v0, v7

    int-to-float v2, v8

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v9, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 120
    int-to-float v0, v7

    int-to-float v2, v8

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v9, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 122
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->h:Landroid/graphics/RectF;

    int-to-float v2, v7

    sub-float/2addr v2, v9

    int-to-float v3, v8

    sub-float/2addr v3, v9

    int-to-float v4, v7

    add-float/2addr v4, v9

    int-to-float v5, v8

    add-float/2addr v5, v9

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 127
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v0, v2

    float-to-int v10, v0

    .line 128
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v2, 0x41c80000    # 25.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->c:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    .line 137
    :goto_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->l:I

    int-to-float v2, v2

    div-float/2addr v2, v11

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 138
    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->h:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    int-to-float v3, v10

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 140
    int-to-double v0, v7

    add-int/lit8 v2, v10, -0x5a

    int-to-double v2, v2

    mul-double/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    float-to-double v4, v9

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    .line 141
    int-to-double v2, v8

    add-int/lit8 v1, v10, -0x5a

    int-to-double v4, v1

    mul-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    float-to-double v10, v9

    mul-double/2addr v4, v10

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 143
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->m:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 145
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 147
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    const v1, 0x3e4ccccd    # 0.2f

    int-to-float v2, v6

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 149
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(F)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 150
    int-to-float v1, v7

    int-to-float v2, v8

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    const/high16 v4, 0x40300000    # 2.75f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 152
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getHeight()I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 130
    :cond_2
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v2, 0x42480000    # 50.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->e:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 132
    :cond_3
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    const/high16 v2, 0x42960000    # 75.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    .line 133
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->f:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 135
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->g:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->i:Landroid/graphics/Paint;

    goto/16 :goto_2
.end method

.method public setBlurRadius(I)V
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/BlurMaskFilter;

    int-to-float v2, p1

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->INNER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 168
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->postInvalidate()V

    .line 169
    return-void
.end method

.method public setInnerCircleBgColor(I)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 209
    return-void
.end method

.method public setInnerCircleColor(I)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 188
    return-void
.end method

.method public setInnerCircleWidth(I)V
    .locals 0

    .prologue
    .line 180
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->k:I

    .line 181
    return-void
.end method

.method public setOuterCircleWidth(I)V
    .locals 0

    .prologue
    .line 191
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->l:I

    .line 192
    return-void
.end method

.method public setPercentageCircleWidth(I)V
    .locals 2

    .prologue
    .line 198
    int-to-float v0, p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->m:F

    .line 199
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 177
    return-void
.end method

.method public setValue(F)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryCircleView;->a:F

    .line 50
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->postInvalidate()V

    .line 51
    return-void
.end method
