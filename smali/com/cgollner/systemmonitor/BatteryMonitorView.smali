.class public Lcom/cgollner/systemmonitor/BatteryMonitorView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private A:J

.field private B:J

.field private C:Z

.field public a:Ljava/util/List;

.field public b:Z

.field public c:Z

.field public d:F

.field public e:Landroid/graphics/Rect;

.field public f:F

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Paint;

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/graphics/Paint;

.field private m:Landroid/graphics/Path;

.field private n:I

.field private o:F

.field private p:F

.field private q:Landroid/graphics/Canvas;

.field private r:F

.field private s:F

.field private t:I

.field private u:I

.field private v:J

.field private w:F

.field private x:Ljava/util/List;

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 193
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(Landroid/content/Context;)V

    .line 194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 188
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 175
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 176
    return-void
.end method

.method public static a(FLandroid/content/res/Resources;)F
    .locals 2

    .prologue
    .line 179
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x78

    const/4 v3, 0x1

    .line 55
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    .line 79
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->lineColor:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->fillColor:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 90
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-static {v1, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->k:Landroid/graphics/Paint;

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->k:Landroid/graphics/Paint;

    invoke-static {v4, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->gridColor:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 105
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 107
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 111
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 112
    const-string v1, "theme"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 113
    if-ne v0, v3, :cond_1

    .line 114
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    :goto_0
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    .line 120
    const/high16 v0, 0x42480000    # 50.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    .line 121
    iput v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->n:I

    .line 123
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    .line 124
    const/16 v0, 0x3e8

    invoke-static {v0, v5, p1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v5, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 126
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 127
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    .line 130
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(Landroid/content/Context;)V

    .line 171
    return-void
.end method

.method private a(ZLandroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 10

    .prologue
    .line 356
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    .line 358
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 359
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    long-to-float v0, v0

    const v1, 0x3c23d70a    # 0.01f

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-long v6, v0

    .line 360
    const/4 v2, 0x0

    .line 362
    const/4 v1, 0x0

    .line 363
    const/4 v0, 0x0

    .line 364
    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    move-object v2, v1

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 365
    iget v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v8, 0x3f800000    # 1.0f

    cmpg-float v4, v4, v8

    if-ltz v4, :cond_0

    .line 367
    iget-boolean v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-nez v4, :cond_1

    if-eqz v2, :cond_1

    iget v4, v2, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    float-to-int v4, v4

    iget v8, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    float-to-int v8, v8

    if-eq v4, v8, :cond_0

    .line 371
    :cond_1
    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    iget-wide v8, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v8, v6

    long-to-float v8, v8

    iget v9, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    div-float/2addr v8, v9

    iget v9, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v8, v9

    add-float/2addr v4, v8

    iput v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    .line 373
    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    const/4 v8, 0x0

    cmpl-float v4, v4, v8

    if-ltz v4, :cond_11

    if-nez v3, :cond_11

    .line 374
    if-eqz v2, :cond_4

    .line 375
    :goto_1
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    iget-wide v8, v2, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v8, v6

    long-to-float v4, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    div-float/2addr v4, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    .line 376
    iget-boolean v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->d:F

    div-float/2addr v2, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v2, v8

    sub-float v2, v4, v2

    move v4, v2

    .line 377
    :goto_2
    const/4 v2, 0x0

    cmpg-float v2, v3, v2

    if-gez v2, :cond_10

    const/4 v2, 0x0

    .line 378
    :goto_3
    if-eqz p1, :cond_6

    .line 379
    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    invoke-virtual {v3, v2, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 380
    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 385
    :goto_4
    const/4 v3, 0x1

    move v2, v3

    .line 387
    :goto_5
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_9

    .line 388
    iget-boolean v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-eqz v3, :cond_7

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->d:F

    div-float/2addr v4, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    .line 390
    :goto_6
    iget-object v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    invoke-virtual {v4, v8, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 392
    if-eqz v1, :cond_8

    .line 400
    :cond_2
    if-eqz p1, :cond_3

    .line 401
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 404
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->q:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0, v2, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 406
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    if-nez v0, :cond_a

    .line 435
    :goto_7
    return-void

    :cond_4
    move-object v2, v0

    .line 374
    goto :goto_1

    .line 376
    :cond_5
    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v2, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v2, v8

    sub-float v2, v4, v2

    move v4, v2

    goto :goto_2

    .line 383
    :cond_6
    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_4

    .line 388
    :cond_7
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v4, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    goto :goto_6

    .line 394
    :cond_8
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_9

    .line 395
    const/4 v1, 0x1

    :cond_9
    move v3, v2

    move-object v2, v0

    .line 399
    goto/16 :goto_0

    .line 409
    :cond_a
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 412
    if-eqz p1, :cond_e

    .line 413
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 414
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v0, v5

    iget v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 419
    :goto_8
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 420
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    iget v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    div-float/2addr v4, v5

    iget v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    .line 421
    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v0, v8

    iget v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v0, v8

    sub-float v0, v5, v0

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    if-eqz v1, :cond_f

    .line 428
    :cond_c
    if-eqz p1, :cond_d

    .line 429
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 430
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 432
    :cond_d
    const/16 v0, 0x32

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 433
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->q:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    invoke-virtual {v0, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 434
    const/16 v0, 0xff

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_7

    .line 417
    :cond_e
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->m:Landroid/graphics/Path;

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v0, v5

    iget v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_8

    .line 424
    :cond_f
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->y:F

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_b

    .line 425
    const/4 v1, 0x1

    goto/16 :goto_9

    :cond_10
    move v2, v3

    goto/16 :goto_3

    :cond_11
    move v2, v3

    goto/16 :goto_5
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 309
    const/16 v0, 0xbe

    invoke-static {v0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    const/16 v1, 0x80

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(III)V

    .line 313
    return-void
.end method

.method private getDefaultVisible()J
    .locals 3

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->battery_visible_range_key:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$color;->fillColorDark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->gridColorDark:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->lineColor:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(III)V

    .line 320
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 324
    :goto_0
    const/16 v1, 0x19

    if-ge v0, v1, :cond_1

    .line 325
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ1Fill:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 326
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ1Line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 340
    :goto_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 341
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    float-to-int v0, v0

    goto :goto_0

    .line 328
    :cond_1
    const/16 v1, 0x32

    if-ge v0, v1, :cond_2

    .line 329
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ2Fill:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ2Line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 332
    :cond_2
    const/16 v1, 0x4b

    if-ge v0, v1, :cond_3

    .line 333
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ3Fill:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 334
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ3Line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 337
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ4Fill:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 338
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ4Line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1
.end method

.method private j()V
    .locals 3

    .prologue
    .line 351
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->k:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(ZLandroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 352
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->l:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(ZLandroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 353
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 52
    return-void
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 447
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 448
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 449
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->C:Z

    .line 450
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x41200000    # 10.0f

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 200
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->C:Z

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->q:Landroid/graphics/Canvas;

    .line 215
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->n:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 217
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getHeight()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getHeight()I

    move-result v0

    :goto_1
    int-to-float v0, v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    const/high16 v3, 0x40200000    # 2.5f

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    .line 218
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getWidth()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getWidth()I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    .line 220
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    div-float v7, v0, v10

    .line 221
    iput v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->f:F

    .line 224
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-eqz v0, :cond_6

    .line 225
    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v3

    .line 226
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move v2, v9

    .line 227
    :goto_3
    const/16 v0, 0xa

    if-gt v2, v0, :cond_6

    .line 228
    rsub-int/lit8 v0, v2, 0xa

    int-to-float v0, v0

    div-float/2addr v0, v10

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->d:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v0, v9, v4}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 229
    iget-object v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v9, v5, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 230
    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->f:F

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    mul-float v6, v3, v12

    add-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->f:F

    .line 231
    int-to-float v4, v2

    mul-float/2addr v4, v7

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 227
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 202
    :cond_1
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-nez v0, :cond_0

    .line 203
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->u:I

    if-nez v0, :cond_2

    .line 204
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i()V

    goto/16 :goto_0

    .line 206
    :cond_2
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->u:I

    if-ne v0, v8, :cond_3

    .line 207
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->h()V

    goto/16 :goto_0

    .line 210
    :cond_3
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g()V

    goto/16 :goto_0

    .line 217
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    goto/16 :goto_1

    .line 218
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    goto :goto_2

    .line 235
    :cond_6
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v0, v0

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->f:F

    sub-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    .line 236
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 306
    :cond_7
    :goto_4
    return-void

    .line 238
    :cond_8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 239
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->f:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 241
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 242
    :cond_9
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    .line 247
    :goto_5
    const-wide/32 v2, 0x36ee80

    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getDefaultVisible()J

    move-result-wide v4

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->A:J

    .line 249
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    div-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    .line 250
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    iget-wide v10, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->A:J

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x5

    div-long/2addr v2, v4

    long-to-float v0, v2

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    .line 252
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v2, v4

    .line 253
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    if-eqz v0, :cond_c

    .line 254
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    .line 259
    :goto_6
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 260
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v2, v4

    .line 261
    long-to-float v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    div-float/2addr v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v0, v2

    .line 262
    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_a

    .line 263
    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    sub-float v0, v3, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    :cond_a
    move v6, v9

    .line 267
    :goto_7
    const/16 v0, 0xa

    if-gt v6, v0, :cond_d

    .line 268
    int-to-float v0, v6

    mul-float v2, v0, v7

    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v3, v0

    int-to-float v0, v6

    mul-float v4, v0, v7

    iget-object v5, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 267
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_7

    .line 245
    :cond_b
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    goto/16 :goto_5

    .line 257
    :cond_c
    long-to-float v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    div-float/2addr v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v2, v2

    div-float/2addr v2, v12

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    goto/16 :goto_6

    .line 272
    :cond_d
    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->p:F

    :goto_8
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v0, v0

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    add-float/2addr v0, v2

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_e

    .line 273
    iget v6, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget-object v7, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 272
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    add-float/2addr v3, v0

    goto :goto_8

    .line 276
    :cond_e
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j()V

    .line 278
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    .line 280
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 282
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->z:F

    iget v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    sub-float v1, v0, v1

    .line 284
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    long-to-float v0, v2

    const v2, 0x3c23d70a    # 0.01f

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-long v0, v0

    move-wide v2, v0

    move v0, v8

    .line 286
    :goto_9
    const/4 v1, 0x5

    if-ge v0, v1, :cond_f

    .line 287
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 288
    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    int-to-float v6, v0

    iget v7, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget-object v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v8}, Landroid/graphics/Paint;->getTextSize()F

    move-result v8

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 289
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    int-to-float v5, v0

    iget v6, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    iget-object v7, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->getTextSize()F

    move-result v7

    const v8, 0x40133333    # 2.3f

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 291
    long-to-float v1, v2

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    add-float/2addr v1, v2

    float-to-long v2, v1

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 293
    :cond_f
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 295
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->b:Z

    if-eqz v0, :cond_7

    .line 296
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 297
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->unlicensed_dialog_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 299
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    int-to-float v3, v3

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const v4, 0x3e4ccccd    # 0.2f

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 301
    :cond_10
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    const v4, 0x3dcccccd    # 0.1f

    sub-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 302
    iget-object v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v0, v9, v3, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 303
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    if-ge v2, v3, :cond_10

    .line 304
    iget v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->t:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->s:F

    div-float/2addr v2, v12

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    div-float/2addr v3, v12

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4
.end method

.method public b()V
    .locals 0

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    .line 469
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    .line 480
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    const-wide v2, 0x3fb999999999999aL    # 0.1

    iget v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    const/high16 v5, 0x40a00000    # 5.0f

    mul-float/2addr v4, v5

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    long-to-float v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    .line 481
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    .line 482
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 484
    const-wide v0, 0x3fb999999999999aL    # 0.1

    iget v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->r:F

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v2, v3

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    long-to-float v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 485
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    .line 486
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->B:J

    sub-long/2addr v2, v0

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 487
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->v:J

    .line 489
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    .line 491
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 493
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    const/high16 v1, 0x42480000    # 50.0f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    .line 494
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    .line 496
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 498
    iget v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    const/high16 v1, 0x42480000    # 50.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->w:F

    .line 499
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    .line 500
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(Landroid/graphics/Canvas;)V

    .line 439
    return-void
.end method

.method public setBgColor(I)V
    .locals 0

    .prologue
    .line 454
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->n:I

    .line 455
    return-void
.end method

.method public setColorScheme(I)V
    .locals 0

    .prologue
    .line 503
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->u:I

    .line 504
    return-void
.end method

.method public setColors([I)V
    .locals 3

    .prologue
    .line 442
    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(III)V

    .line 443
    return-void
.end method

.method public setGridWidth(F)V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 510
    return-void
.end method

.method public setLabelColor(I)V
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 507
    return-void
.end method

.method public setLineWidth(F)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 157
    return-void
.end method

.method public setMargin(F)V
    .locals 0

    .prologue
    .line 458
    iput p1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->o:F

    .line 459
    return-void
.end method

.method public setPredictionArray(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    if-nez v0, :cond_0

    .line 472
    iput-object p1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    .line 478
    :goto_0
    return-void

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 475
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 198
    return-void
.end method

.method public setTextSize(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 160
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 161
    const/16 v0, 0x3e8

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 163
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 164
    return-void
.end method

.method public setValues(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a()V

    .line 465
    iget-object v0, p0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 466
    return-void
.end method
