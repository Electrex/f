.class public Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public a(Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;)I
    .locals 4

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 43
    check-cast p1, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->a(Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;)I

    move-result v0

    return v0
.end method
