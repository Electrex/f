.class public Lcom/cgollner/systemmonitor/FrequenciesPieChart;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field public static a:[[I


# instance fields
.field private b:F

.field private c:F

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/RectF;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/RectF;

.field private j:F

.field private k:I

.field private l:Z

.field private m:Landroid/graphics/Paint;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/16 v10, 0xff

    const/16 v9, 0xcc

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 162
    const/4 v0, 0x6

    new-array v0, v0, [[I

    new-array v1, v8, [I

    const/16 v2, 0x33

    const/16 v3, 0xb5

    const/16 v4, 0xe5

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v6

    const/16 v2, 0x99

    invoke-static {v6, v2, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    aput-object v1, v0, v6

    new-array v1, v8, [I

    const/16 v2, 0x99

    invoke-static {v2, v9, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v6

    const/16 v2, 0x66

    const/16 v3, 0x99

    invoke-static {v2, v3, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    aput-object v1, v0, v7

    new-array v1, v8, [I

    const/16 v2, 0xbb

    const/16 v3, 0x33

    invoke-static {v10, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v6

    const/16 v2, 0x88

    invoke-static {v10, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v8, [I

    const/16 v3, 0x44

    const/16 v4, 0x44

    invoke-static {v10, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v6

    invoke-static {v9, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v8, [I

    const/16 v3, 0xef

    const/16 v4, 0x67

    const/16 v5, 0xf1

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v6

    invoke-static {v10, v6, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v8, [I

    const/16 v3, 0xaa

    const/16 v4, 0x66

    invoke-static {v3, v4, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v6

    const/16 v3, 0x99

    const/16 v4, 0x33

    invoke-static {v3, v4, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    aput-object v2, v0, v1

    sput-object v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Landroid/content/Context;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 154
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 160
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/high16 v6, 0x41a00000    # 20.0f

    const/16 v5, 0xff

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 105
    iput-boolean v3, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->q:Z

    .line 106
    iput v4, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->r:I

    .line 108
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 111
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/GraphUtil;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->e:Landroid/graphics/Paint;

    .line 116
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 117
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->e:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/cgollner/systemmonitor/GraphUtil;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 119
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    .line 120
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/GraphUtil;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 123
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    .line 124
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/cgollner/systemmonitor/GraphUtil;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 126
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 127
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    const-string v1, "sans-serif-thin"

    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 128
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v3, [I

    const v2, 0x1010036

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    .line 132
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 133
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    invoke-static {v5, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    const-string v0, "sans-serif-thin"

    iput-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->n:Ljava/lang/String;

    .line 136
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Landroid/content/Context;)V

    .line 150
    return-void
.end method

.method private getValuesSum()J
    .locals 5

    .prologue
    .line 93
    const-wide/16 v0, 0x0

    .line 94
    iget-object v2, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    .line 95
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 96
    goto :goto_0

    .line 97
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public a(Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Ljava/util/List;IZ)V

    .line 79
    return-void
.end method

.method public a(Ljava/util/List;IZ)V
    .locals 4

    .prologue
    .line 81
    iput-object p1, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->j:F

    .line 88
    :goto_0
    if-eqz p3, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->postInvalidate()V

    .line 90
    :cond_0
    return-void

    .line 85
    :cond_1
    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    iget v0, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->d:F

    const v3, 0x40666666    # 3.6f

    mul-float/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->j:F

    .line 86
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, p2, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->k:I

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 24

    .prologue
    .line 173
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 261
    :cond_0
    return-void

    .line 176
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->getWidth()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->getWidth()I

    move-result v3

    int-to-float v3, v3

    :goto_0
    move-object/from16 v0, p0

    iput v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->b:F

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->getHeight()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->getHeight()I

    move-result v3

    int-to-float v3, v3

    :goto_1
    move-object/from16 v0, p0

    iput v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->c:F

    .line 179
    const v3, 0x3ef5c28f    # 0.48f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->b:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->c:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float v14, v3, v4

    .line 182
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->b:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v15, v3, v4

    .line 183
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->c:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v16, v3, v4

    .line 184
    const/high16 v4, -0x3d4c0000    # -90.0f

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    .line 187
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    if-nez v5, :cond_2

    .line 188
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    .line 189
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    sub-float v6, v14, v3

    sub-float v6, v16, v6

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 190
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    sub-float v6, v14, v3

    add-float v6, v6, v16

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 191
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    sub-float v6, v14, v3

    sub-float v6, v15, v6

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 192
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    sub-float v6, v14, v3

    add-float/2addr v6, v15

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 194
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v3, v5

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual/range {p0 .. p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cgollner/systemmonitor/GraphUtil;->a(FLandroid/content/res/Resources;)F

    move-result v5

    add-float/2addr v3, v5

    .line 196
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    if-nez v5, :cond_3

    .line 197
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    .line 198
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    sub-float v6, v16, v14

    add-float/2addr v6, v3

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 199
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    add-float v6, v16, v14

    sub-float/2addr v6, v3

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    sub-float v6, v15, v14

    add-float/2addr v6, v3

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 201
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    add-float v6, v15, v14

    sub-float v3, v6, v3

    iput v3, v5, Landroid/graphics/RectF;->right:F

    .line 203
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->r:I

    if-eqz v3, :cond_4

    .line 204
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->r:I

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->q:Z

    if-eqz v3, :cond_9

    .line 206
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1, v14, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 212
    :cond_4
    :goto_2
    const/4 v3, 0x0

    .line 213
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move v11, v3

    move v12, v4

    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    .line 214
    iget v3, v9, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->d:F

    const v4, 0x40666666    # 3.6f

    mul-float v18, v3, v4

    .line 216
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->p:Z

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->o:I

    .line 217
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->p:Z

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->o:I

    move v13, v4

    .line 218
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->e:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 223
    const/high16 v3, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->j:F

    mul-float v19, v3, v4

    .line 224
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v18, v3

    if-lez v3, :cond_5

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->i:Landroid/graphics/RectF;

    add-float v5, v12, v19

    sub-float v6, v18, v19

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->d:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 227
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->k:I

    if-ne v3, v11, :cond_d

    .line 228
    iget v3, v9, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->d:F

    invoke-static {v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(F)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Ljava/lang/String;

    .line 229
    iget-object v0, v9, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->b:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 231
    const v3, 0x3e99999a    # 0.3f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->c:F

    mul-float v21, v3, v4

    .line 232
    const v3, 0x3dcccccd    # 0.1f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->c:F

    mul-float v22, v3, v4

    .line 234
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v22

    add-float v3, v3, v21

    .line 235
    const v4, 0x40133333    # 2.3f

    div-float/2addr v3, v4

    add-float v23, v16, v3

    .line 236
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v18, v3

    if-lez v3, :cond_6

    .line 237
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    add-float v5, v12, v19

    sub-float v6, v18, v19

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->f:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 238
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    invoke-virtual {v3, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 240
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 241
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 242
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    cmpl-float v3, v3, v14

    if-lez v3, :cond_c

    .line 243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    const v5, 0x3dcccccd    # 0.1f

    sub-float/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_6

    .line 176
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    int-to-float v3, v3

    goto/16 :goto_0

    .line 177
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    goto/16 :goto_1

    .line 209
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->g:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->m:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 216
    :cond_a
    sget-object v3, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    sget-object v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    array-length v4, v4

    rem-int v4, v11, v4

    aget-object v3, v3, v4

    const/4 v4, 0x0

    aget v3, v3, v4

    goto/16 :goto_4

    .line 217
    :cond_b
    sget-object v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    sget-object v5, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    array-length v5, v5

    rem-int v5, v11, v5

    aget-object v4, v4, v5

    const/4 v5, 0x1

    aget v4, v4, v5

    move v13, v4

    goto/16 :goto_5

    .line 245
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 247
    const v3, 0x3f99999a    # 1.2f

    mul-float v3, v3, v22

    sub-float v3, v23, v3

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    const-string v5, "sans-serif-thin"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 250
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v15, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 252
    const v4, 0x3f666666    # 0.9f

    mul-float v4, v4, v21

    sub-float/2addr v3, v4

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->n:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 254
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 255
    iget-object v4, v9, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 257
    :cond_d
    add-int/lit8 v3, v11, 0x1

    .line 258
    add-float v4, v12, v18

    move v11, v3

    move v12, v4

    .line 260
    goto/16 :goto_3
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 140
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 141
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 143
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 144
    invoke-virtual {p0, v0, v0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setMeasuredDimension(II)V

    .line 145
    return-void
.end method

.method public setPaintBackground(Z)V
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->l:Z

    .line 41
    return-void
.end method

.method public setSelected(I)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, p1, v0

    :goto_0
    iput v0, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->k:I

    .line 102
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->postInvalidate()V

    .line 103
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSmallFont(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->n:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setValues(Ljava/util/List;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0, p1, v0, v0}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Ljava/util/List;IZ)V

    .line 76
    return-void
.end method
