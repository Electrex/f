.class Lcom/cgollner/systemmonitor/MainActivityAbstract$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/MainActivityAbstract;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/MainActivityAbstract;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "pos"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;I)I

    move-result v0

    .line 120
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move v1, v0

    .line 129
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    .line 130
    iget v2, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->a:I

    .line 131
    iget-object v3, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    const-class v4, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 132
    iget-object v3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "android:switcher:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/cgollner/systemmonitor/lib/R$id;->viewPager:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    .line 135
    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a()I

    move-result v1

    .line 139
    :goto_1
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->b(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    move-result-object v2

    iget v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 140
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 141
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->c(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/PagerTitleStrip;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    .line 142
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 124
    const-string v1, "pos"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 126
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_1
    move v1, v0

    goto/16 :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method
