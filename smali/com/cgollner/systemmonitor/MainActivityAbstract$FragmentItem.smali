.class public Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/Class;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    .line 69
    return-void
.end method

.method public constructor <init>(IIILjava/lang/Class;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput p1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    .line 62
    iput p2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->a:I

    .line 63
    iput p3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->b:I

    .line 64
    iput-object p4, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    .line 65
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p0, p1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 84
    goto :goto_0

    .line 85
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 86
    goto :goto_0

    .line 87
    :cond_3
    check-cast p1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    .line 88
    iget v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    iget v3, p1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 73
    .line 75
    iget v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    add-int/lit8 v0, v0, 0x1f

    .line 76
    return v0
.end method
