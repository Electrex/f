.class Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/MainActivityAbstract;


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/MainActivityAbstract;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    .line 315
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 316
    return-void
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 321
    :try_start_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 322
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 323
    move-object v0, v1

    check-cast v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    move-object v2, v0

    invoke-virtual {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 329
    :cond_0
    :goto_0
    return-object v1

    .line 326
    :catch_0
    move-exception v1

    .line 327
    :goto_1
    invoke-virtual {v1}, Ljava/lang/ReflectiveOperationException;->printStackTrace()V

    .line 329
    const/4 v1, 0x0

    goto :goto_0

    .line 326
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public a(IFI)V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    .line 353
    return-void
.end method

.method public a_(I)V
    .locals 5

    .prologue
    .line 357
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    .line 358
    iget v2, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->a:I

    .line 359
    iget-object v1, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android:switcher:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/cgollner/systemmonitor/lib/R$id;->viewPager:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    .line 363
    if-eqz v1, :cond_0

    .line 364
    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a()I

    move-result v1

    .line 367
    :goto_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget-object v4, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v4}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 368
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->c(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/PagerTitleStrip;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    .line 369
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->b(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    move-result-object v2

    iget v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 370
    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b_(I)V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    .line 376
    return-void
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 338
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->b:I

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    const-class v1, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->c(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/PagerTitleStrip;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    .line 345
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 346
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->b(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    move-result-object v1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/MainActivityAbstract;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 348
    :cond_0
    return-void
.end method
