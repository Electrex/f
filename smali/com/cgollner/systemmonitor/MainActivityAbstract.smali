.class public abstract Lcom/cgollner/systemmonitor/MainActivityAbstract;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;


# static fields
.field public static n:Z


# instance fields
.field protected o:Ljava/util/List;

.field private p:Landroid/support/v4/view/ViewPager;

.field private q:Landroid/support/v4/view/PagerTitleStrip;

.field private r:I

.field private s:Landroid/support/v4/widget/DrawerLayout;

.field private t:Landroid/support/v7/app/ActionBarDrawerToggle;

.field private u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 312
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/MainActivityAbstract;I)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private b(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 419
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    iget v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->c:I

    if-ne v0, p1, :cond_0

    .line 423
    :goto_1
    return v1

    .line 419
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 423
    goto :goto_1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Lcom/cgollner/systemmonitor/NavigationDrawerFragment;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/MainActivityAbstract;)Landroid/support/v4/view/PagerTitleStrip;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->q:Landroid/support/v4/view/PagerTitleStrip;

    return-object v0
.end method

.method private s()V
    .locals 6

    .prologue
    .line 201
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    .line 202
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_cpu:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->cpu_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->q()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_cpu_temperature:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->cpu_temperature:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->l()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_0
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a()Lcom/cgollner/systemmonitor/backend/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_gpu:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->gpu:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->k()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_ram:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_purple_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->ram_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_io:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_green_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->io_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_net:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_orange_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->network_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_top_apps:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_red_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->top_apps_title:I

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_cpu_freqs:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_light:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->cpu_frequencies:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_storage_stats:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->storage_stats:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_apps_cache:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->cache_stats:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_battery_history:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_red_light:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->battery_title:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_battery_stats:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->battery_stats:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tab_battery_temperature:I

    sget v3, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_light:I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->battery_temperature:I

    const-class v5, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(IIILjava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 256
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 257
    const-string v3, "theme"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    .line 258
    iget v3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    packed-switch v3, :pswitch_data_0

    .line 271
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.franco.kernel"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 272
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    .line 273
    iget v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    if-ne v0, v1, :cond_2

    sget v0, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDark:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->setTheme(I)V

    .line 274
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "theme"

    iget v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 276
    :goto_1
    return-void

    .line 260
    :pswitch_0
    sget v0, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDefault:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->setTheme(I)V

    goto :goto_1

    .line 263
    :pswitch_1
    sget v0, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDark:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->setTheme(I)V

    goto :goto_1

    .line 266
    :pswitch_2
    sget v1, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDefault:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->setTheme(I)V

    .line 267
    iput v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    .line 268
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "theme"

    iget v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 273
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDefault:I

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 400
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->b(I)I

    move-result v2

    .line 401
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    .line 402
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v2, v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 405
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->e(I)V

    .line 408
    :cond_1
    iget v1, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->a:I

    .line 409
    iget-object v0, v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;->d:Ljava/lang/Class;

    const-class v3, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android:switcher:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->viewPager:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;

    .line 413
    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a()I

    move-result v0

    .line 415
    :goto_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/lang/Integer;I)V

    .line 416
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected abstract k()Ljava/lang/Class;
.end method

.method protected abstract l()Ljava/lang/Class;
.end method

.method protected abstract m()V
.end method

.method protected abstract n()Ljava/lang/Class;
.end method

.method protected abstract o()Ljava/lang/Class;
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 248
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t:Landroid/support/v7/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->a(Landroid/content/res/Configuration;)V

    .line 251
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 102
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t()V

    .line 103
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->nav_drawer:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->setContentView(I)V

    .line 106
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->_toolbar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 108
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s()V

    .line 110
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->drawerFragment:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->o:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a(Ljava/util/List;)V

    .line 112
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->viewPager:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;

    invoke-direct {v1}, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;-><init>()V

    invoke-virtual {v0, v6, v1}, Landroid/support/v4/view/ViewPager;->a(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 114
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->pager_title_strip:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTitleStrip;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->q:Landroid/support/v4/view/PagerTitleStrip;

    .line 116
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 117
    new-instance v1, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract$1;-><init>(Lcom/cgollner/systemmonitor/MainActivityAbstract;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 146
    new-instance v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract$MonitorAdapter;-><init>(Lcom/cgollner/systemmonitor/MainActivityAbstract;Landroid/support/v4/app/FragmentManager;)V

    .line 147
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 148
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 149
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 150
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "settings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 151
    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->m()V

    .line 155
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->r:I

    if-ne v0, v6, :cond_1

    .line 159
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_history_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 170
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->drawer_layout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    .line 171
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_3

    .line 172
    new-instance v0, Lcom/cgollner/systemmonitor/MainActivityAbstract$2;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->drawer_open:I

    sget v5, Lcom/cgollner/systemmonitor/lib/R$string;->drawer_close:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/cgollner/systemmonitor/MainActivityAbstract$2;-><init>(Lcom/cgollner/systemmonitor/MainActivityAbstract;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 187
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 189
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 190
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/app/ActionBar;->b(Z)V

    .line 198
    :cond_3
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 281
    sget v1, Lcom/cgollner/systemmonitor/lib/R$menu;->activity_main:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 282
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x3

    .line 289
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->f(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->e(I)V

    .line 302
    :goto_0
    return v0

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/DrawerLayout;->d(I)V

    goto :goto_0

    .line 298
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->menu_settings:I

    if-ne v1, v2, :cond_2

    .line 299
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->m()V

    goto :goto_0

    .line 302
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 383
    :try_start_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 384
    const-string v1, "pos"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 385
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 389
    return-void

    .line 386
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 240
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->t:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->a()V

    .line 243
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 224
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 225
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->s:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->u:Lcom/cgollner/systemmonitor/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->t()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 227
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 229
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 230
    sub-int v0, v1, v0

    .line 231
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 232
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 233
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->p:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    :cond_0
    return-void
.end method

.method protected abstract p()Ljava/lang/Class;
.end method

.method protected abstract q()Ljava/lang/Class;
.end method

.method protected abstract r()Ljava/lang/Class;
.end method
