.class public final enum Lcom/cgollner/systemmonitor/MonitorView$LabelType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

.field public static final enum b:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

.field public static final enum c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

.field public static final enum d:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

.field private static final synthetic e:[Lcom/cgollner/systemmonitor/MonitorView$LabelType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    const-string v1, "PERCENTAGE"

    invoke-direct {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView$LabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->a:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    new-instance v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    const-string v1, "TEMPERATURE"

    invoke-direct {v0, v1, v3}, Lcom/cgollner/systemmonitor/MonitorView$LabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->b:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    new-instance v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    const-string v1, "SPEED"

    invoke-direct {v0, v1, v4}, Lcom/cgollner/systemmonitor/MonitorView$LabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    new-instance v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    const-string v1, "MEMORY"

    invoke-direct {v0, v1, v5}, Lcom/cgollner/systemmonitor/MonitorView$LabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->d:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v1, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->a:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->b:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->d:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->e:[Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cgollner/systemmonitor/MonitorView$LabelType;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    return-object v0
.end method

.method public static values()[Lcom/cgollner/systemmonitor/MonitorView$LabelType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->e:[Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    invoke-virtual {v0}, [Lcom/cgollner/systemmonitor/MonitorView$LabelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    return-object v0
.end method
