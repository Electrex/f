.class public Lcom/cgollner/systemmonitor/MonitorView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public b:D

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Landroid/graphics/Rect;

.field public h:Z

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Paint;

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/graphics/Paint;

.field private m:Landroid/graphics/Paint;

.field private n:Landroid/graphics/Path;

.field private o:I

.field private p:I

.field private q:F

.field private r:F

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:F

.field private x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 153
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 40
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->s:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->t:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->u:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->v:Z

    .line 154
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/MonitorView;->a(Landroid/content/Context;)V

    .line 155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 148
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->s:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->t:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->u:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->v:Z

    .line 149
    invoke-direct {p0, p2, p1}, Lcom/cgollner/systemmonitor/MonitorView;->a(Landroid/util/AttributeSet;Landroid/content/Context;)V

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 136
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->s:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->t:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->u:Z

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->v:Z

    .line 137
    invoke-direct {p0, p2, p1}, Lcom/cgollner/systemmonitor/MonitorView;->a(Landroid/util/AttributeSet;Landroid/content/Context;)V

    .line 138
    return-void
.end method

.method public static a(FLandroid/content/res/Resources;)F
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/16 v8, 0x11

    const/4 v7, -0x1

    const/high16 v6, 0x41200000    # 10.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    .line 49
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    const/16 v1, 0xc8

    const/16 v2, 0x7d

    const/16 v3, 0xbb

    invoke-static {v1, v8, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 55
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    .line 56
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    const/16 v1, 0x32

    const/16 v2, 0x7d

    const/16 v3, 0xbb

    invoke-static {v1, v8, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    .line 60
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 61
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    const/16 v1, 0xd9

    const/16 v2, 0xea

    const/16 v3, 0xf4

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 74
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 75
    const-string v1, "theme"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 76
    if-ne v0, v4, :cond_1

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    :goto_0
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    .line 83
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    .line 84
    iput v7, p0, Lcom/cgollner/systemmonitor/MonitorView;->p:I

    .line 86
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 88
    iput-boolean v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->e:Z

    .line 91
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    .line 92
    const/16 v0, 0x3e8

    invoke-static {v0, v5, p1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v5, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 95
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-direct {p0, p2}, Lcom/cgollner/systemmonitor/MonitorView;->a(Landroid/content/Context;)V

    .line 100
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_lineWidth:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_lineColor:I

    const/16 v3, 0xc8

    const/16 v4, 0x11

    const/16 v5, 0x7d

    const/16 v6, 0xbb

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_fillColor:I

    const/16 v3, 0x32

    const/16 v4, 0x11

    const/16 v5, 0x7d

    const/16 v6, 0xbb

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_backgroundColor:I

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->p:I

    .line 118
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_gridColor:I

    const/16 v3, 0xd9

    const/16 v4, 0xea

    const/16 v5, 0xf4

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_gridWidth:I

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 124
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_valuesMargin:I

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    .line 126
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_drawBackground:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    if-eqz v1, :cond_0

    .line 130
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 133
    :cond_0
    return-void

    .line 129
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 130
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public a(FZ)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->o:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->o:I

    if-lez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 161
    :cond_0
    if-eqz p2, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->postInvalidate()V

    .line 164
    :cond_1
    return-void
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 328
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 329
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 177
    if-nez p1, :cond_1

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getHeight()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getHeight()I

    move-result v0

    int-to-float v8, v0

    .line 180
    :goto_1
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getWidth()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    .line 181
    :goto_2
    const/high16 v0, 0x41200000    # 10.0f

    div-float v7, v8, v0

    .line 183
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->o:I

    iget v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    div-float v1, v3, v1

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->o:I

    .line 185
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->h:Z

    if-eqz v0, :cond_8

    .line 186
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->p:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 192
    :cond_2
    :goto_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->w:F

    .line 194
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    if-eqz v0, :cond_e

    .line 195
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x41100000    # 9.0f

    mul-float/2addr v0, v1

    cmpl-float v0, v0, v8

    if-lez v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    .line 196
    :goto_4
    const/4 v0, 0x1

    .line 197
    if-eqz v1, :cond_1b

    .line 198
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    cmpl-float v0, v0, v8

    if-lez v0, :cond_a

    .line 199
    const/4 v0, 0x3

    move v1, v0

    .line 203
    :goto_5
    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v5

    .line 204
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 205
    const/4 v0, 0x1

    move v4, v0

    :goto_6
    const/16 v0, 0x9

    if-gt v4, v0, :cond_d

    .line 206
    rsub-int/lit8 v0, v4, 0xa

    int-to-float v0, v0

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v0, v2

    float-to-double v10, v0

    iget-wide v12, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    mul-double/2addr v10, v12

    double-to-int v0, v10

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v0, v2, v6}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v6, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    if-ne v2, v6, :cond_b

    .line 208
    const/4 v0, 0x0

    .line 209
    iget-wide v10, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    const-wide/16 v12, 0x0

    cmpg-double v2, v10, v12

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-le v2, v6, :cond_3

    .line 210
    const/4 v2, 0x1

    .line 211
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v10, v0

    iput-wide v10, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    move v0, v2

    .line 213
    :cond_3
    const/high16 v2, 0x41200000    # 10.0f

    int-to-float v6, v4

    sub-float/2addr v2, v6

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v2, v6

    float-to-double v10, v2

    iget-wide v12, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    mul-double/2addr v10, v12

    .line 214
    const-wide/16 v12, 0x3e8

    invoke-static {v10, v11, v12, v13}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v2

    .line 215
    if-eqz v0, :cond_4

    .line 216
    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    iput-wide v10, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    :cond_4
    move-object v0, v2

    .line 222
    :cond_5
    :goto_7
    const/4 v2, 0x3

    if-ge v1, v2, :cond_c

    .line 223
    int-to-float v2, v4

    mul-float/2addr v2, v7

    iget-object v6, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    move-result v6

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v6, v9

    add-float/2addr v2, v6

    iget-object v6, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 227
    :goto_8
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    iget-object v10, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v6, v9, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 228
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->w:F

    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v5

    add-float/2addr v2, v6

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->w:F

    .line 205
    add-int v0, v4, v1

    move v4, v0

    goto/16 :goto_6

    .line 179
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v8, v0

    goto/16 :goto_1

    .line 180
    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    goto/16 :goto_2

    .line 188
    :cond_8
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->e:Z

    if-eqz v0, :cond_2

    .line 189
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->p:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_3

    .line 195
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_4

    .line 201
    :cond_a
    const/4 v0, 0x2

    move v1, v0

    goto/16 :goto_5

    .line 218
    :cond_b
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v6, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->a:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    if-ne v2, v6, :cond_5

    .line 219
    const/high16 v0, 0x41200000    # 10.0f

    int-to-float v2, v4

    sub-float/2addr v0, v2

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v0, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 225
    :cond_c
    int-to-float v2, v4

    mul-float/2addr v2, v7

    iget-object v6, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    move-result v6

    const/high16 v9, 0x3fc00000    # 1.5f

    div-float/2addr v6, v9

    add-float/2addr v2, v6

    iget-object v6, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_8

    .line 231
    :cond_d
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    sub-float v2, v8, v0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    sub-float v4, v8, v0

    iget-object v5, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 234
    :cond_e
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->w:F

    sub-float/2addr v3, v0

    .line 235
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 236
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->w:F

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 237
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v3, v8}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 240
    const/4 v0, 0x0

    move v6, v0

    :goto_9
    const/16 v0, 0xa

    if-ge v6, v0, :cond_f

    .line 241
    const/4 v1, 0x0

    int-to-float v0, v6

    mul-float v2, v0, v7

    int-to-float v0, v6

    mul-float v4, v0, v7

    iget-object v5, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 240
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_9

    .line 245
    :cond_f
    iget v5, p0, Lcom/cgollner/systemmonitor/MonitorView;->r:F

    :goto_a
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    add-float/2addr v0, v3

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_10

    .line 246
    const/4 v6, 0x0

    iget-object v9, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    move-object v4, p1

    move v7, v5

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 245
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    add-float/2addr v5, v0

    goto :goto_a

    .line 249
    :cond_10
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->r:F

    iget v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->r:F

    .line 250
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->r:F

    iget v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v1, v2

    neg-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_11

    .line 251
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->r:F

    .line 254
    :cond_11
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 256
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v0, v3, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 258
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    move v2, v3

    :goto_b
    if-ltz v1, :cond_13

    const/high16 v0, -0x3db80000    # -50.0f

    cmpl-float v0, v2, v0

    if-ltz v0, :cond_13

    .line 259
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    const-wide/16 v6, 0x0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_12

    .line 260
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v0, v4

    mul-float/2addr v0, v8

    sub-float v0, v8, v0

    .line 261
    iget-object v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v4, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    :goto_c
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    sub-float/2addr v2, v0

    .line 258
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_b

    .line 265
    :cond_12
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    div-double/2addr v4, v6

    .line 266
    float-to-double v6, v8

    float-to-double v10, v8

    mul-double/2addr v4, v10

    sub-double v4, v6, v4

    double-to-float v0, v4

    .line 267
    iget-object v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v4, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_c

    .line 271
    :cond_13
    iget v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    add-float/2addr v0, v2

    .line 272
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v1, v0, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 273
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 275
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 276
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->n:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 278
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->s:Z

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    if-eqz v0, :cond_15

    .line 279
    :cond_14
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v9, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 281
    :cond_15
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->t:Z

    if-eqz v0, :cond_16

    .line 282
    const/4 v4, 0x0

    iget-object v7, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    move-object v2, p1

    move v5, v3

    move v6, v8

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 284
    :cond_16
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->u:Z

    if-eqz v0, :cond_17

    .line 285
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 287
    :cond_17
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->v:Z

    if-eqz v0, :cond_18

    .line 288
    const/4 v7, 0x0

    iget-object v11, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    move-object v6, p1

    move v9, v3

    move v10, v8

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 291
    :cond_18
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->d:Z

    if-eqz v0, :cond_1a

    .line 296
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 297
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->unlicensed_dialog_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 299
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v4

    const v5, 0x3e4ccccd    # 0.2f

    mul-float/2addr v4, v5

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 301
    :cond_19
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    const v5, 0x3dcccccd    # 0.1f

    sub-float/2addr v4, v5

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 302
    iget-object v2, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v0, v4, v5, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 303
    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    cmpl-float v2, v2, v3

    if-gez v2, :cond_19

    .line 304
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v3, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v8, v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 306
    :cond_1a
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->c:Z

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 308
    const-string v0, "OFFLINE"

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v3, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v8, v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 292
    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_1b
    move v1, v0

    goto/16 :goto_5
.end method

.method public a(ZZZZ)V
    .locals 0

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/cgollner/systemmonitor/MonitorView;->s:Z

    .line 345
    iput-boolean p2, p0, Lcom/cgollner/systemmonitor/MonitorView;->t:Z

    .line 346
    iput-boolean p3, p0, Lcom/cgollner/systemmonitor/MonitorView;->u:Z

    .line 347
    iput-boolean p4, p0, Lcom/cgollner/systemmonitor/MonitorView;->v:Z

    .line 348
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/MonitorView;->a(Landroid/graphics/Canvas;)V

    .line 320
    return-void
.end method

.method public setBgColor(I)V
    .locals 1

    .prologue
    .line 334
    iput p1, p0, Lcom/cgollner/systemmonitor/MonitorView;->p:I

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->e:Z

    .line 336
    return-void
.end method

.method public setColors([I)V
    .locals 3

    .prologue
    .line 323
    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(III)V

    .line 324
    return-void
.end method

.method public setFillColor(I)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 398
    return-void
.end method

.method public setGridColor(I)V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 388
    return-void
.end method

.method public setGridLineWidth(F)V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 403
    return-void
.end method

.method public setGridWidth(F)V
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->k:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 383
    return-void
.end method

.method public setLabelColor(I)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 378
    return-void
.end method

.method public setLabelType(Lcom/cgollner/systemmonitor/MonitorView$LabelType;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 351
    iput-object p1, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    .line 352
    if-nez p1, :cond_0

    .line 353
    iput-boolean v4, p0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    .line 368
    :goto_0
    return-void

    .line 356
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    .line 358
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    .line 359
    const-string v0, ""

    .line 360
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v2, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->b:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    if-ne v1, v2, :cond_2

    .line 361
    const/16 v0, 0x3e8

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 366
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 367
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 362
    :cond_2
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v2, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    if-ne v1, v2, :cond_3

    .line 363
    const-wide v0, 0x412c200000000000L    # 921600.0

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 364
    :cond_3
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->x:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    sget-object v2, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->a:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    if-ne v1, v2, :cond_1

    .line 365
    const-string v0, "100%"

    goto :goto_1
.end method

.method public setLineColor(I)V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    return-void
.end method

.method public setLineWidth(F)V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->i:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 373
    return-void
.end method

.method public setMargin(F)V
    .locals 0

    .prologue
    .line 339
    iput p1, p0, Lcom/cgollner/systemmonitor/MonitorView;->q:F

    .line 340
    return-void
.end method

.method public setTextSize(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 170
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 171
    const/16 v0, 0x3e8

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    iget-object v1, p0, Lcom/cgollner/systemmonitor/MonitorView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 173
    iget-object v0, p0, Lcom/cgollner/systemmonitor/MonitorView;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/MonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 174
    return-void
.end method
