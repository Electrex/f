.class public Lcom/cgollner/systemmonitor/NavigationDrawerFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Z

.field private aj:Landroid/widget/TextView;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/content/Context;

.field private h:Landroid/view/ViewGroup;

.field private i:Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->g:Landroid/content/Context;

    .line 50
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->drawer_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 51
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    move v1, v2

    .line 52
    :goto_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 53
    iget-object v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 54
    invoke-virtual {v2}, Landroid/view/View;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_1
    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 40
    :try_start_0
    check-cast p1, Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;

    iput-object p1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->i:Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Activity Must implement MenuItemSelectedListener"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/Integer;I)V
    .locals 6

    .prologue
    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 69
    if-nez v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-boolean v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a:Z

    if-nez v1, :cond_1

    .line 72
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->b:I

    .line 73
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v1

    iput v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->f:I

    .line 74
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    iput v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->c:I

    .line 75
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    sget v4, Lcom/cgollner/systemmonitor/lib/R$attr;->selectableItemBackground:I

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 76
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->d:I

    .line 77
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    iput v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->e:I

    .line 78
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 79
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->a:Z

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 82
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    iget v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->d:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 83
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    iget v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->b:I

    iget v3, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->c:I

    iget v4, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->f:I

    iget v5, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->c:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 84
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    iget v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->e:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    :cond_2
    iput-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->k()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->aj:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 96
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Landroid/view/View;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/cgollner/systemmonitor/MainActivityAbstract$FragmentItem;-><init>(I)V

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 100
    add-int/lit8 v0, v0, -0x1

    .line 96
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/NavigationDrawerFragment;->i:Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/NavigationDrawerFragment$MenuItemSelectedListener;->a(I)V

    .line 64
    return-void
.end method
