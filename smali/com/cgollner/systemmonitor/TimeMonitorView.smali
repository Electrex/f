.class public Lcom/cgollner/systemmonitor/TimeMonitorView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public b:D

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Path;

.field private j:I

.field private k:F

.field private l:F

.field private m:Ljava/lang/String;

.field private n:Landroid/graphics/Canvas;

.field private o:J

.field private p:F

.field private q:I

.field private r:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public static a(FLandroid/content/res/Resources;)F
    .locals 2

    .prologue
    .line 179
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 13

    .prologue
    const/16 v12, 0x11

    const/16 v11, 0xd2

    const/4 v10, 0x0

    const/16 v9, 0x78

    const/4 v1, 0x1

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 69
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    move v0, v1

    .line 72
    :goto_0
    const/16 v4, 0x3c

    if-gt v0, v4, :cond_0

    .line 73
    iget-object v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    new-instance v5, Lcom/cgollner/systemmonitor/battery/TimeValue;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const/16 v8, 0x64

    invoke-virtual {v3, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v5, v6, v7, v8}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    const/16 v4, 0xd

    const/4 v5, 0x5

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    const/16 v2, 0xff

    const/16 v3, 0x7d

    const/16 v4, 0xbb

    invoke-static {v2, v12, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 84
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    const/16 v2, 0xb4

    const/16 v3, 0x7d

    const/16 v4, 0xbb

    invoke-static {v2, v12, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->h:Landroid/graphics/Paint;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->h:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-static {v2, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->h:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->g:Landroid/graphics/Paint;

    .line 95
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->g:Landroid/graphics/Paint;

    invoke-static {v9, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    invoke-static {v11, v11, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    .line 103
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 104
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x1060000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 115
    new-array v0, v1, [I

    const v1, 0x1010036

    aput v1, v0, v10

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v10, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    .line 123
    const/high16 v0, 0x42480000    # 50.0f

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    .line 124
    iput v10, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->j:I

    .line 126
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    .line 127
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Landroid/content/Context;)V

    .line 131
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_lineWidth:I

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 140
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_lineColor:I

    const/16 v3, 0xc8

    const/16 v4, 0x11

    const/16 v5, 0x7d

    const/16 v6, 0xbb

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_fillColor:I

    const/16 v3, 0x32

    const/16 v4, 0x11

    const/16 v5, 0x7d

    const/16 v6, 0xbb

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_backgroundColor:I

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->j:I

    .line 149
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_gridColor:I

    const/16 v3, 0xd9

    const/16 v4, 0xea

    const/16 v5, 0xf4

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_gridWidth:I

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 155
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_valuesMargin:I

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    .line 158
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->MonitorView_titleMonitor:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->m:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 163
    return-void

    .line 161
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(ZLandroid/graphics/Paint;)V
    .locals 10

    .prologue
    .line 245
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 246
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 247
    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    neg-float v2, v2

    iget v3, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 248
    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 249
    iget-wide v4, v1, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-wide v6, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->r:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    iget v3, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    int-to-double v6, v3

    mul-double/2addr v4, v6

    double-to-float v3, v4

    .line 250
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    const-wide/16 v6, 0x0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 251
    iget v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v1, v5

    iget v5, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    .line 252
    iget-object v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {v4, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 255
    :cond_0
    iget v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    float-to-double v4, v4

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    float-to-double v6, v1

    iget-wide v8, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    div-double/2addr v6, v8

    iget v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    float-to-double v8, v1

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-float v1, v4

    .line 256
    iget-object v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {v4, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 260
    :cond_1
    if-eqz p1, :cond_2

    .line 261
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    iget v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    int-to-float v1, v1

    iget v2, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->n:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->i:Landroid/graphics/Path;

    invoke-virtual {v0, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 266
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 239
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(ZLandroid/graphics/Paint;)V

    .line 240
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(ZLandroid/graphics/Paint;)V

    .line 241
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 65
    return-void
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 278
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 279
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 280
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/high16 v9, 0x41200000    # 10.0f

    .line 192
    iput-object p1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->n:Landroid/graphics/Canvas;

    .line 194
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getHeight()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    .line 195
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getWidth()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    .line 197
    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    div-float v7, v0, v9

    .line 199
    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->j:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 200
    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    div-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    move v6, v8

    .line 203
    :goto_2
    const/16 v0, 0xa

    if-gt v6, v0, :cond_2

    .line 204
    int-to-float v0, v6

    mul-float v2, v0, v7

    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    int-to-float v3, v0

    int-to-float v0, v6

    mul-float v4, v0, v7

    iget-object v5, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 203
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    .line 194
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    goto :goto_1

    .line 208
    :cond_2
    iget v3, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->l:F

    :goto_3
    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->q:I

    int-to-float v0, v0

    iget v2, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    add-float/2addr v0, v2

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_3

    .line 209
    iget v6, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    iget-object v7, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->e:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 208
    iget v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    add-float/2addr v3, v0

    goto :goto_3

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 236
    :cond_4
    return-void

    .line 217
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long v0, v2, v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->r:J

    .line 219
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->r:J

    const-wide/16 v2, 0x5

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->o:J

    .line 221
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->b()V

    .line 223
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->m:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 224
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 225
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->m:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    add-float/2addr v2, v9

    iget-object v3, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 226
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 228
    :cond_6
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 230
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 231
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 232
    const/4 v0, 0x1

    :goto_4
    const/4 v3, 0x5

    if-ge v0, v3, :cond_4

    .line 233
    const/16 v3, 0xe

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->o:J

    long-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 234
    new-instance v3, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    int-to-float v4, v0

    iget v5, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->p:F

    iget-object v6, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    move-result v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method public a(Lcom/cgollner/systemmonitor/battery/TimeValue;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 269
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Landroid/graphics/Canvas;)V

    .line 270
    return-void
.end method

.method public setBgColor(I)V
    .locals 0

    .prologue
    .line 284
    iput p1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->j:I

    .line 285
    return-void
.end method

.method public setColors([I)V
    .locals 3

    .prologue
    .line 273
    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(III)V

    .line 274
    return-void
.end method

.method public setLineWidth(I)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->c:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 169
    return-void
.end method

.method public setMargin(F)V
    .locals 0

    .prologue
    .line 288
    iput p1, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->k:F

    .line 289
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 190
    return-void
.end method

.method public setTextSize(I)V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->f:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(FLandroid/content/res/Resources;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 173
    return-void
.end method

.method public setValues(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 295
    iget-object v0, p0, Lcom/cgollner/systemmonitor/TimeMonitorView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 296
    return-void
.end method
