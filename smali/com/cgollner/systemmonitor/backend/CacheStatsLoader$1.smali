.class Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 47
    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 50
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 51
    iget-object v2, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    new-instance v3, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 52
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 53
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "du -ks "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Android/data/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cache"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "du -ks /data/data/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_1
    invoke-static {v1}, Leu/chainfire/libsuperuser/Shell$SU;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_4

    .line 60
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 61
    iget-object v2, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-virtual {v2, v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->b(Ljava/lang/String;)Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    move-result-object v2

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    .line 63
    if-eqz v0, :cond_2

    .line 64
    iget-wide v4, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    goto :goto_1

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 69
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 70
    new-instance v1, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$1;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 75
    iget-object v1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->c(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$2;

    invoke-direct {v2, p0, v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$2;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 91
    :goto_2
    return-void

    .line 83
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->c(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$3;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1$3;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method
