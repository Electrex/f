.class final Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

.field final synthetic b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

.field final synthetic c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    iput-object p3, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->c:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 130
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rm -rf /data/data/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    iget-object v3, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rm -rf "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Android/data/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    iget-object v3, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$SU;->a([Ljava/lang/String;)Ljava/util/List;

    .line 134
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;->c:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2$1;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 142
    :cond_0
    return-void
.end method
