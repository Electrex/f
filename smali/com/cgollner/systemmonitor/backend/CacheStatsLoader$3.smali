.class final Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

.field final synthetic c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    iput-object p3, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->c:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 152
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 153
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    .line 154
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rm -rf /data/data/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cache"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rm -rf "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Android/data/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_0
    invoke-static {v1}, Leu/chainfire/libsuperuser/Shell$SU;->a(Ljava/util/List;)Ljava/util/List;

    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;->c:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3$1;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 166
    :cond_1
    return-void
.end method
