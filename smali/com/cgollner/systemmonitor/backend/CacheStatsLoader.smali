.class public Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;

.field private c:Ljava/util/HashMap;

.field private d:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/String;

    const-string v1, "/data/data"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->d:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->c:Ljava/util/HashMap;

    return-object p1
.end method

.method public static a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;)V
    .locals 3

    .prologue
    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 127
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$2;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 144
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;)V
    .locals 3

    .prologue
    .line 148
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 149
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;

    invoke-direct {v2, p0, p1, v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$3;-><init>(Ljava/util/List;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 168
    return-void
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ls "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$SU;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;)V
    .locals 2

    .prologue
    .line 43
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->b:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;

    .line 44
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$1;-><init>(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 93
    return-void
.end method

.method protected b(Ljava/lang/String;)Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;
    .locals 6

    .prologue
    .line 100
    new-instance v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-direct {v0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;-><init>()V

    .line 101
    const-string v1, "(.*/data/)(.*)(/cache)"

    const-string v2, "$2"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    .line 102
    const-wide/16 v2, 0x400

    const-string v1, "(\\d+)(.*)"

    const-string v4, "$1"

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    mul-long/2addr v2, v4

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    .line 103
    return-object v0
.end method
