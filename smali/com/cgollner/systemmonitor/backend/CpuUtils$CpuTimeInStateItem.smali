.class public Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:I

.field public b:J

.field public c:F

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(IJ)V
    .locals 2

    .prologue
    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    iput p1, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a:I

    .line 426
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    .line 427
    if-nez p1, :cond_0

    const-string v0, "Deep sleep"

    :goto_0
    iput-object v0, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->d:Ljava/lang/String;

    .line 428
    return-void

    .line 427
    :cond_0
    int-to-double v0, p1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;)I
    .locals 4

    .prologue
    .line 438
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 417
    check-cast p1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a(Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
