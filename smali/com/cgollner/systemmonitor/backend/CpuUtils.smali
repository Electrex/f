.class public final Lcom/cgollner/systemmonitor/backend/CpuUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field static final a:Ljava/io/File;

.field public static b:Ljava/lang/String;

.field static c:Ljava/util/HashMap;

.field static d:Ljava/io/File;

.field private static e:I

.field private static final f:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc/stat"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a:Ljava/io/File;

    .line 275
    const/4 v0, 0x4

    sput v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->e:I

    .line 294
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f:Ljava/io/File;

    .line 414
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    sput-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b:Ljava/lang/String;

    .line 461
    sput-object v2, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    .line 463
    sput-object v2, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)F
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    .line 92
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 93
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 110
    :cond_1
    :goto_0
    return v0

    .line 94
    :cond_2
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c([Ljava/lang/String;)J

    move-result-wide v2

    .line 96
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b([Ljava/lang/String;)J

    move-result-wide v4

    .line 98
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c([Ljava/lang/String;)J

    move-result-wide v6

    .line 100
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b([Ljava/lang/String;)J

    move-result-wide v8

    .line 102
    const/4 v0, 0x0

    .line 103
    cmp-long v1, v2, v10

    if-ltz v1, :cond_1

    cmp-long v1, v4, v10

    if-ltz v1, :cond_1

    cmp-long v1, v6, v10

    if-ltz v1, :cond_1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_1

    .line 104
    add-long v10, v8, v6

    add-long v12, v4, v2

    cmp-long v1, v10, v12

    if-lez v1, :cond_1

    cmp-long v1, v8, v4

    if-ltz v1, :cond_1

    .line 105
    sub-long v0, v8, v4

    long-to-float v0, v0

    add-long/2addr v6, v8

    add-long/2addr v2, v4

    sub-long v2, v6, v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 106
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;J)F
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 184
    :try_start_0
    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d([Ljava/lang/String;)J

    move-result-wide v2

    .line 187
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 188
    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d([Ljava/lang/String;)J

    move-result-wide v4

    .line 191
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    long-to-double v6, p2

    const-wide/16 v8, 0x0

    cmpl-double v1, v6, v8

    if-lez v1, :cond_0

    .line 192
    const/high16 v1, 0x42c80000    # 100.0f

    sub-long v2, v4, v2

    long-to-float v2, v2

    mul-float/2addr v1, v2

    long-to-float v2, p2

    div-float/2addr v1, v2

    .line 195
    :goto_0
    const/4 v2, 0x0

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c(I)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    mul-float/2addr v0, v1

    .line 197
    :goto_1
    return v0

    .line 196
    :catch_0
    move-exception v1

    goto :goto_1

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JF)F
    .locals 2

    .prologue
    .line 328
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, p4, v0

    invoke-static {p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(Ljava/lang/String;Ljava/lang/String;J)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public static a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 278
    new-instance v0, Ljava/io/File;

    const-string v2, "/sys/devices/system/cpu"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 280
    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    .line 281
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v7, 0x4

    if-lt v0, v7, :cond_0

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 282
    :goto_1
    const/16 v7, 0x30

    if-lt v0, v7, :cond_1

    const/16 v7, 0x39

    if-gt v0, v7, :cond_1

    const/4 v0, 0x1

    .line 283
    :goto_2
    const-string v7, "cpu"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    .line 284
    add-int/lit8 v0, v2, 0x1

    .line 287
    :goto_3
    sget v2, Lcom/cgollner/systemmonitor/backend/CpuUtils;->e:I

    if-ne v0, v2, :cond_2

    .line 291
    :goto_4
    return v0

    .line 281
    :cond_0
    const/16 v0, 0x6b

    goto :goto_1

    :cond_1
    move v0, v1

    .line 282
    goto :goto_2

    .line 280
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4
.end method

.method public static a(I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 66
    .line 68
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/io/File;

    const-string v4, "/proc/stat"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 69
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 70
    :goto_0
    if-eqz v2, :cond_4

    .line 71
    if-nez p0, :cond_2

    .line 72
    const-string v3, "cpu "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    .line 84
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 88
    :goto_2
    if-eqz v2, :cond_1

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    .line 77
    :cond_2
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cpu"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, p0, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 82
    :cond_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 86
    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 85
    :catch_1
    move-exception v1

    goto :goto_3

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 332
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->g()Ljava/util/List;

    move-result-object v0

    .line 333
    const-string v1, "time_in_state"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 334
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    .line 335
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    invoke-interface {v1, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 337
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 338
    return-void
.end method

.method public static a([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 34
    .line 38
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v1, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 39
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 40
    :goto_0
    if-eqz v3, :cond_0

    .line 41
    const-string v1, "cpu"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 43
    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v1

    .line 45
    add-int/lit8 v1, v1, 0x1

    .line 49
    :goto_1
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p0, v1

    .line 50
    add-int/lit8 v0, v0, 0x1

    .line 52
    array-length v1, p0

    if-lt v0, v1, :cond_2

    .line 57
    :cond_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 61
    :goto_2
    return-void

    :cond_1
    move v1, v2

    .line 47
    goto :goto_1

    .line 55
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static b([Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 114
    const-wide/16 v2, 0x0

    .line 115
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 116
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 118
    :try_start_0
    aget-object v1, p0, v0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    add-long/2addr v2, v4

    .line 115
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    const-wide/16 v2, -0x1

    .line 126
    :cond_1
    return-wide v2
.end method

.method public static b(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 141
    .line 142
    const/4 v1, 0x0

    .line 145
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/proc/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/stat"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "r"

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 147
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 152
    :goto_0
    return-object v0

    .line 148
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 149
    :goto_1
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 148
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static b()Ljava/util/List;
    .locals 5

    .prologue
    .line 297
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 298
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 300
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 301
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 305
    :cond_0
    return-object v1

    .line 302
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Ljava/util/List;
    .locals 18

    .prologue
    .line 358
    const-string v2, "time_in_state"

    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 360
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 362
    new-instance v8, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    sget-object v3, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 364
    const-string v2, "0"

    const-wide/16 v4, 0x0

    invoke-interface {v6, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 365
    const-wide/16 v4, 0x0

    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d()J

    move-result-wide v10

    sub-long v2, v10, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 366
    new-instance v4, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v2, v3}, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;-><init>(IJ)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v4, v2

    .line 370
    :goto_0
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 371
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 372
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 373
    const/4 v9, 0x1

    aget-object v2, v2, v9

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v10, v2

    .line 374
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ""

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v12, 0x0

    invoke-interface {v6, v2, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 375
    const-wide/16 v14, 0x0

    const-wide/16 v16, 0xa

    mul-long v10, v10, v16

    sub-long/2addr v10, v12

    invoke-static {v14, v15, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .line 376
    new-instance v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    invoke-direct {v2, v3, v10, v11}, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;-><init>(IJ)V

    .line 377
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    iget-wide v2, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    add-long/2addr v2, v4

    move-wide v4, v2

    .line 379
    goto :goto_0

    .line 382
    :cond_0
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 383
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 384
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    .line 385
    iget-wide v10, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    long-to-double v10, v10

    long-to-double v12, v4

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double/2addr v10, v12

    double-to-float v6, v10

    iput v6, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    .line 386
    iget v2, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v6

    if-gez v2, :cond_1

    .line 387
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 390
    :cond_2
    const-wide/16 v2, 0x0

    .line 391
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v4, v2

    .line 392
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 393
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    add-long/2addr v2, v4

    move-wide v4, v2

    goto :goto_2

    .line 397
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 398
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 399
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    .line 400
    iget-wide v10, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    long-to-double v10, v10

    long-to-double v12, v4

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double/2addr v10, v12

    double-to-float v6, v10

    iput v6, v2, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    goto :goto_3

    .line 403
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 404
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V

    .line 405
    return-object v7
.end method

.method public static c(I)F
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 156
    if-nez p0, :cond_3

    .line 157
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a()I

    move-result v1

    .line 158
    new-array v3, v1, [I

    .line 159
    new-array v4, v1, [I

    .line 161
    :goto_0
    if-ge p0, v1, :cond_0

    .line 162
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->h(I)I

    move-result v2

    aput v2, v3, v0

    .line 163
    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->g(I)I

    move-result v2

    aput v2, v4, v0

    .line 161
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 166
    :cond_0
    array-length v5, v3

    move v1, v0

    move v2, v0

    :goto_1
    if-ge v1, v5, :cond_1

    aget v6, v3, v1

    .line 167
    add-int/2addr v2, v6

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 170
    :cond_1
    array-length v3, v4

    move v1, v0

    :goto_2
    if-ge v0, v3, :cond_2

    aget v5, v4, v0

    .line 171
    add-int/2addr v1, v5

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 173
    :cond_2
    int-to-float v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 178
    :goto_3
    return v0

    .line 175
    :cond_3
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->h(I)I

    move-result v0

    .line 176
    add-int/lit8 v1, p0, -0x1

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->g(I)I

    move-result v1

    .line 178
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_3
.end method

.method public static c([Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x3

    :try_start_0
    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 136
    :goto_0
    return-wide v0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 136
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static c()Ljava/util/Set;
    .locals 5

    .prologue
    .line 309
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 310
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 312
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 313
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_0
    return-object v1

    .line 314
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static d()J
    .locals 4

    .prologue
    .line 409
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 411
    return-wide v0
.end method

.method public static d([Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 202
    const/16 v0, 0xd

    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const/16 v2, 0xe

    aget-object v2, p0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static d(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/proc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/stat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 211
    array-length v1, v0

    if-le v1, v2, :cond_2

    .line 212
    aget-object v2, v0, v2

    .line 213
    const/16 v0, 0x28

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 214
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 215
    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    .line 216
    :cond_0
    if-ne v0, v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 217
    :cond_1
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static e()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 466
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f()Ljava/io/File;

    move-result-object v1

    sput-object v1, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    .line 467
    sget-object v1, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 470
    :cond_1
    :try_start_0
    sget-object v1, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    invoke-static {v1}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 471
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 472
    :catch_0
    move-exception v1

    .line 473
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static e(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 253
    const-string v2, "sys/devices/system/cpu/cpu%d/online"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static f(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 258
    if-lez v0, :cond_0

    .line 260
    :goto_0
    return v0

    :cond_0
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static f()Ljava/io/File;
    .locals 4

    .prologue
    .line 479
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 480
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d:Ljava/io/File;

    .line 485
    :goto_0
    return-object v0

    .line 481
    :cond_0
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->h()V

    .line 483
    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 484
    :catch_0
    move-exception v0

    .line 485
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 264
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 265
    if-lez v0, :cond_0

    .line 268
    :goto_0
    return v0

    :cond_0
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static g()Ljava/util/List;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    .line 341
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 343
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    sget-object v3, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 345
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d()J

    move-result-wide v2

    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 346
    new-instance v4, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    invoke-direct {v4, v10, v2, v3}, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;-><init>(IJ)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 348
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 349
    new-instance v3, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    aget-object v4, v2, v10

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0xa

    mul-long/2addr v6, v8

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;-><init>(IJ)V

    .line 350
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 352
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 353
    return-object v0
.end method

.method public static h(I)I
    .locals 4

    .prologue
    .line 272
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/FileUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static h()V
    .locals 3

    .prologue
    .line 491
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 493
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    .line 494
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "tuna"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "maguro"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "toro"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "toroplus"

    const-string v2, "/sys/devices/platform/omap/omap_temp_sensor.0/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "mako"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone7/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "grouper"

    const-string v2, "/sys/devices/platform/tegra-i2c.4/i2c-4/4-004c/temperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "flo"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone0/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "hammerhead"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone7/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    sget-object v0, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c:Ljava/util/HashMap;

    const-string v1, "m7"

    const-string v2, "/sys/devices/virtual/thermal/thermal_zone1/temp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
