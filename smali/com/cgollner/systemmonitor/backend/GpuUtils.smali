.class public abstract Lcom/cgollner/systemmonitor/backend/GpuUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/Class;

.field private static b:Lcom/cgollner/systemmonitor/backend/GpuUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a:[Ljava/lang/Class;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static a()Lcom/cgollner/systemmonitor/backend/GpuUtils;
    .locals 4

    .prologue
    .line 18
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b:Lcom/cgollner/systemmonitor/backend/GpuUtils;

    if-nez v0, :cond_1

    .line 19
    sget-object v2, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a:[Ljava/lang/Class;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 21
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b:Lcom/cgollner/systemmonitor/backend/GpuUtils;

    .line 22
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b:Lcom/cgollner/systemmonitor/backend/GpuUtils;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b:Lcom/cgollner/systemmonitor/backend/GpuUtils;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_1
    return-object v0

    .line 25
    :catch_0
    move-exception v0

    .line 19
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 29
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtils;->b:Lcom/cgollner/systemmonitor/backend/GpuUtils;

    goto :goto_1
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public abstract c()F
.end method

.method public abstract d()I
.end method

.method public abstract e()I
.end method

.method public abstract f()I
.end method
