.class public Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;
.super Lcom/cgollner/systemmonitor/backend/GpuUtils;
.source "SourceFile"


# static fields
.field static final a:[Ljava/io/File;

.field static final b:[Ljava/io/File;

.field static final c:[Ljava/io/File;

.field static final d:[Ljava/io/File;

.field private static e:[Ljava/lang/Integer;

.field private static f:I

.field private static g:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-array v0, v4, [Ljava/io/File;

    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/gpubusy"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->a:[Ljava/io/File;

    .line 14
    new-array v0, v4, [Ljava/io/File;

    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/gpuclk"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->b:[Ljava/io/File;

    .line 18
    new-array v0, v4, [Ljava/io/File;

    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->c:[Ljava/io/File;

    .line 22
    new-array v0, v4, [Ljava/io/File;

    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->d:[Ljava/io/File;

    .line 28
    sput v5, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->f:I

    .line 29
    sput v5, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;-><init>()V

    return-void
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->j()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->h()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->i()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()F
    .locals 3

    .prologue
    .line 69
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 70
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 71
    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 72
    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    .line 75
    :cond_0
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    .line 76
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->l()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 77
    mul-float/2addr v0, v1

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 81
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 86
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->h()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 91
    :goto_0
    return v0

    .line 89
    :catch_0
    move-exception v0

    .line 91
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 114
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->f:I

    .line 118
    :goto_0
    return v0

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->k()[Ljava/lang/Integer;

    move-result-object v0

    .line 116
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 117
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->f:I

    .line 118
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->f:I

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->i()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 137
    :goto_0
    return v0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 137
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public g()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 32
    sget-object v3, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->a:[Ljava/io/File;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 33
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v0, v5, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 37
    :cond_0
    :goto_1
    return-object v0

    .line 32
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 37
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 41
    sget-object v3, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->b:[Ljava/io/File;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 42
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v0, v5, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 46
    :cond_0
    :goto_1
    return-object v0

    .line 41
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public i()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 50
    sget-object v3, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->d:[Ljava/io/File;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 51
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v0, v5, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 55
    :cond_0
    :goto_1
    return-object v0

    .line 50
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 55
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public j()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 59
    sget-object v3, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->c:[Ljava/io/File;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 60
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v0, v5, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 64
    :cond_0
    :goto_1
    return-object v0

    .line 59
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 64
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public k()[Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 96
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->e:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->e:[Ljava/lang/Integer;

    .line 109
    :goto_0
    return-object v0

    .line 100
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->j()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 101
    const-string v1, "\\s+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 102
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->e:[Ljava/lang/Integer;

    .line 103
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 104
    sget-object v2, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->e:[Ljava/lang/Integer;

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 106
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->e:[Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 109
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()I
    .locals 2

    .prologue
    .line 122
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 123
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g:I

    .line 127
    :goto_0
    return v0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->k()[Ljava/lang/Integer;

    move-result-object v0

    .line 125
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 126
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g:I

    .line 127
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsMako;->g:I

    goto :goto_0
.end method
