.class public Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;
.super Lcom/cgollner/systemmonitor/backend/GpuUtils;
.source "SourceFile"


# static fields
.field private static final a:Ljava/io/File;

.field private static final b:Ljava/io/File;

.field private static final c:Ljava/io/File;

.field private static d:[Ljava/lang/Integer;

.field private static e:I

.field private static f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 14
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/clock"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a:Ljava/io/File;

    .line 15
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/clock"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->b:Ljava/io/File;

    .line 16
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mali.0/dvfs"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->c:Ljava/io/File;

    .line 19
    sput v2, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->e:I

    .line 20
    sput v2, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;-><init>()V

    return-void
.end method

.method private a(Ljava/io/File;)Ljava/io/File;
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 68
    const-string v1, "\\d+"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 69
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 70
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_0
    return-object v0
.end method

.method private b(Ljava/lang/String;)[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 118
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->j()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->k()[Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->k()[Ljava/lang/Integer;

    move-result-object v1

    array-length v1, v1

    if-le v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->g()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->h()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->i()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()F
    .locals 3

    .prologue
    .line 44
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->g()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\r"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\t"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "(.*utilisation:)(\\d{1,3})"

    const-string v2, "$2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    .line 47
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->l()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 48
    mul-float/2addr v0, v1

    .line 51
    :goto_0
    return v0

    .line 49
    :catch_0
    move-exception v0

    .line 51
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public d()I
    .locals 3

    .prologue
    .line 56
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->h()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 57
    const-string v1, "\\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 58
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, "(.*)(\\d{3,})(.*)"

    const-string v2, "$2"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    .line 63
    :goto_0
    return v0

    .line 61
    :catch_0
    move-exception v0

    .line 63
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 95
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 96
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->e:I

    .line 100
    :goto_0
    return v0

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->k()[Ljava/lang/Integer;

    move-result-object v0

    .line 98
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 99
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->e:I

    .line 100
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->e:I

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->l()I

    move-result v0

    return v0
.end method

.method public g()Ljava/io/File;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->c:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/io/File;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/io/File;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->b:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/io/File;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->b:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public k()[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 82
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->d:[Ljava/lang/Integer;

    .line 91
    :goto_0
    return-object v0

    .line 86
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->j()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 87
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->b(Ljava/lang/String;)[Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->d:[Ljava/lang/Integer;

    .line 88
    sget-object v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->d:[Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 91
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()I
    .locals 2

    .prologue
    .line 104
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 105
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->f:I

    .line 109
    :goto_0
    return v0

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->k()[Ljava/lang/Integer;

    move-result-object v0

    .line 107
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 108
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->f:I

    .line 109
    sget v0, Lcom/cgollner/systemmonitor/backend/GpuUtilsManta;->f:I

    goto :goto_0
.end method
