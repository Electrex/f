.class Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field public a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

.field public b:Ljava/io/File;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/io/File;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->b:Ljava/io/File;

    .line 106
    iput-object p2, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->c:Landroid/content/Context;

    .line 107
    return-void
.end method

.method private a(Ljava/io/File;)J
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    .line 119
    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "-new"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 120
    invoke-static {p1}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a(Ljava/io/File;)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 121
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 124
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileWithSize;

    .line 125
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 126
    iget-wide v4, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileWithSize;->b:J

    cmp-long v1, v8, v4

    if-gtz v1, :cond_0

    .line 127
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileWithSize;->a:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-wide v0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 133
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    :try_start_1
    invoke-static {p1}, Lorg/apache/commons/io/FileUtils;->h(Ljava/io/File;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    move-wide v0, v2

    .line 137
    goto :goto_0

    .line 142
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 143
    if-eqz v7, :cond_2

    .line 144
    array-length v10, v7

    const/4 v0, 0x0

    move v11, v0

    move-wide v0, v2

    move v2, v11

    :goto_1
    if-ge v2, v10, :cond_3

    aget-object v3, v7, v2

    .line 145
    invoke-direct {p0, v3}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 144
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_1

    :cond_2
    move-wide v0, v2

    .line 151
    :cond_3
    :try_start_2
    new-instance v2, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 152
    new-instance v3, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileWithSize;

    invoke-direct {v3, v0, v1, v8, v9}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileWithSize;-><init>(JJ)V

    invoke-virtual {v2, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 153
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 154
    :catch_2
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->b:Ljava/io/File;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->b:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a(Ljava/io/File;)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;-><init>(Ljava/io/File;J)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    .line 111
    return-void
.end method
