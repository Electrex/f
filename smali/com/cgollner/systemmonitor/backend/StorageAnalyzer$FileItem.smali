.class public Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/io/File;

.field public b:J

.field public c:F


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    iput-wide p2, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;)I
    .locals 4

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a(Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 32
    const-string v1, "%s: %s = %s"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "File: "

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-static {v3}, Lorg/apache/commons/io/FileUtils;->h(Ljava/io/File;)J

    move-result-wide v4

    long-to-double v4, v4

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "Dir: "

    goto :goto_0
.end method
