.class public Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 162
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "%.0f bytes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "%.0f KB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "%.1f MB"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "%.2f GB"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "%.3f TB"

    aput-object v2, v0, v1

    sput-object v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a:[Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Ljava/io/File;)J
    .locals 2

    .prologue
    .line 18
    invoke-static {p0}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->b(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(D)Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    invoke-static {p0, p1}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(ID)Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 170
    cmpg-double v0, p1, v2

    if-gez v0, :cond_0

    .line 171
    sget-object v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a:[Ljava/lang/String;

    aget-object v0, v0, p0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v0, p0, 0x1

    div-double v2, p1, v2

    invoke-static {v0, v2, v3}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a(ID)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Landroid/content/Context;)Ljava/util/List;
    .locals 10

    .prologue
    .line 38
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 39
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 40
    if-nez v1, :cond_0

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 64
    :goto_0
    return-object v0

    .line 42
    :cond_0
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 43
    new-instance v5, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;

    invoke-direct {v5, v4, p1}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;-><init>(Ljava/io/File;Landroid/content/Context;)V

    .line 44
    invoke-virtual {v5}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->start()V

    .line 45
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 47
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    .line 49
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;

    .line 51
    :try_start_0
    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->join()V

    .line 52
    iget-object v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    if-eqz v1, :cond_2

    .line 53
    iget-object v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$Analyzer;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    add-long/2addr v2, v0

    :cond_2
    move-wide v0, v2

    :goto_3
    move-wide v2, v0

    .line 59
    goto :goto_2

    .line 56
    :catch_0
    move-exception v0

    .line 57
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move-wide v0, v2

    goto :goto_3

    .line 60
    :cond_3
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 61
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    .line 62
    iget-wide v6, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    long-to-double v6, v6

    long-to-double v8, v2

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->c:F

    goto :goto_4

    :cond_4
    move-object v0, v4

    .line 64
    goto :goto_0
.end method

.method private static b(Ljava/io/File;)J
    .locals 8

    .prologue
    .line 82
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .line 96
    :goto_0
    return-wide v0

    .line 85
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 86
    if-eqz v2, :cond_1

    array-length v0, v2

    if-nez v0, :cond_2

    .line 87
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 89
    :cond_2
    const/4 v0, 0x0

    aget-object v1, v2, v0

    .line 90
    const/4 v0, 0x1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_4

    .line 91
    aget-object v3, v2, v0

    invoke-static {v3}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->b(Ljava/io/File;)J

    move-result-wide v4

    .line 92
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-gez v3, :cond_3

    .line 93
    aget-object v1, v2, v0

    .line 90
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 96
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static b(D)Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a(ID)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
