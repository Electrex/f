.class public Lcom/cgollner/systemmonitor/backend/StringUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field static a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 238
    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->sunday:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->monday:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->tuesday:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->wednesday:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->thursday:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->friday:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->saturday:I

    aput v2, v0, v1

    sput-object v0, Lcom/cgollner/systemmonitor/backend/StringUtils;->a:[I

    return-void
.end method

.method public static a(IILandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    .line 164
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 165
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->temperature_units_key:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    if-nez v0, :cond_0

    .line 167
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 168
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    if-ne v0, v2, :cond_1

    const-string v0, "1"

    .line 169
    :goto_0
    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->temperature_units_key:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 170
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 172
    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    int-to-float v0, p0

    div-float/2addr v0, v3

    const v1, 0x3fe66666    # 1.8f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42000000    # 32.0f

    add-float/2addr v0, v1

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "f\u00baF"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_1
    return-object v0

    .line 168
    :cond_1
    const-string v0, "0"

    goto :goto_0

    .line 177
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "f\u00baC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    int-to-float v2, p0

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(D)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 104
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    .line 105
    const-string v0, "-"

    .line 114
    :goto_0
    return-object v0

    .line 107
    :cond_0
    div-double v2, p0, v4

    .line 108
    const-string v0, "%.0fMHz"

    .line 109
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 111
    div-double/2addr v2, v4

    .line 112
    const-string v0, "%.1fGHz"

    .line 114
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(DJ)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 34
    long-to-double v0, p2

    div-double v0, v4, v0

    .line 35
    mul-double v2, p0, v0

    .line 36
    const-string v0, "%.0fB/s"

    .line 38
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 39
    div-double/2addr v2, v6

    .line 40
    const-string v0, "%.0fKB/s"

    .line 43
    :cond_0
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 44
    div-double/2addr v2, v6

    .line 45
    const-string v0, "%.1fMB/s"

    .line 48
    :cond_1
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 49
    div-double/2addr v2, v6

    .line 50
    const-string v0, "%.1GB/s"

    .line 52
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(F)Ljava/lang/String;
    .locals 4

    .prologue
    .line 24
    const v0, -0x42333333    # -0.1f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const-string v0, "0%"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%.0f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 100
    const-string v0, "%.0fMHz"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-double v4, p0

    div-double/2addr v4, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 137
    long-to-double v2, p0

    .line 138
    const-string v0, "%.0fB"

    .line 140
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 141
    div-double/2addr v2, v6

    .line 142
    const-string v0, "%.0fKB"

    .line 145
    :cond_0
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 146
    div-double/2addr v2, v6

    .line 147
    const-string v0, "%.1fMB"

    .line 150
    :cond_1
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 151
    div-double/2addr v2, v6

    .line 152
    const-string v0, "%.1fGB"

    .line 154
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 16

    .prologue
    .line 182
    const-wide/16 v2, 0x3e8

    div-long v2, p0, v2

    .line 183
    const-wide/16 v4, 0x3c

    div-long v4, v2, v4

    const-wide/16 v6, 0x3c

    rem-long/2addr v4, v6

    .line 184
    const-wide/16 v6, 0xe10

    div-long v6, v2, v6

    const-wide/16 v8, 0x18

    rem-long/2addr v6, v8

    .line 185
    const-wide/16 v8, 0xe10

    div-long v8, v2, v8

    const-wide/16 v10, 0x18

    div-long/2addr v8, v10

    .line 186
    const-wide/16 v10, 0x3c

    rem-long v10, v2, v10

    .line 188
    const-wide/16 v2, 0x1

    cmp-long v2, v8, v2

    if-nez v2, :cond_0

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->day:I

    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 189
    const-wide/16 v12, 0x1

    cmp-long v2, v6, v12

    if-nez v2, :cond_1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->hour:I

    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 190
    const-wide/16 v14, 0x1

    cmp-long v2, v4, v14

    if-nez v2, :cond_2

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->minute:I

    :goto_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 191
    const-wide/16 v14, 0x1

    cmp-long v2, v10, v14

    if-nez v2, :cond_3

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->second:I

    :goto_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 193
    const-wide/16 v14, 0x1

    cmp-long v14, v8, v14

    if-ltz v14, :cond_4

    .line 194
    const-string v2, "%d %s %d %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    aput-object v3, v4, v5

    const/4 v3, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x3

    aput-object v12, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 206
    :goto_4
    return-object v2

    .line 188
    :cond_0
    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->days:I

    goto :goto_0

    .line 189
    :cond_1
    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->hours:I

    goto :goto_1

    .line 190
    :cond_2
    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->minutes:I

    goto :goto_2

    .line 191
    :cond_3
    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->seconds:I

    goto :goto_3

    .line 196
    :cond_4
    const-wide/16 v8, 0x1

    cmp-long v3, v6, v8

    if-ltz v3, :cond_5

    .line 197
    const-string v2, "%d %s %d %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v3, v8

    const/4 v6, 0x1

    aput-object v12, v3, v6

    const/4 v6, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    aput-object v13, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 199
    :cond_5
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-lez v3, :cond_6

    .line 200
    const-string v2, "%d %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    aput-object v13, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 202
    :cond_6
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-ltz v3, :cond_7

    .line 203
    const-string v3, "%d %s %d %s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v13, v6, v4

    const/4 v4, 0x2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v6, v4

    const/4 v4, 0x3

    aput-object v2, v6, v4

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 206
    :cond_7
    const-string v3, "%d %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4
.end method

.method public static a(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 253
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 254
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 255
    invoke-virtual {v1, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 257
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    .line 259
    if-nez v0, :cond_0

    .line 260
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->today:I

    .line 267
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 261
    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 262
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->yesterday:I

    goto :goto_0

    .line 263
    :cond_1
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 264
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->tomorrow:I

    goto :goto_0

    .line 266
    :cond_2
    sget-object v0, Lcom/cgollner/systemmonitor/backend/StringUtils;->a:[I

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 57
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 58
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$array;->chaves:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 59
    array-length v5, v3

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v1, v3, v2

    .line 61
    invoke-virtual {v1}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v6

    .line 62
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 63
    array-length v7, v4

    move v1, v0

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v8, v4, v1

    .line 64
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_0

    .line 72
    :goto_2
    return v0

    .line 63
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 72
    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static b(I)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 118
    const-string v0, "%.0fMHz"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(D)Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    .line 122
    const-string v0, "%.0fMB"

    .line 124
    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v1, p0, v2

    if-ltz v1, :cond_0

    .line 125
    const-wide/high16 v0, 0x4090000000000000L    # 1024.0

    div-double/2addr p0, v0

    .line 126
    const-string v0, "%.1fGB"

    .line 128
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(F)Ljava/lang/String;
    .locals 4

    .prologue
    .line 28
    const v0, -0x42333333    # -0.1f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    .line 29
    const-string v0, "Offline"

    .line 31
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%.0f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    const-wide/32 v0, 0x100000

    div-long v0, p0, v0

    long-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(JLandroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 210
    invoke-static {p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 211
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(F)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 161
    const-string v0, "%.0f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(JLandroid/content/Context;)Ljava/lang/String;
    .locals 16

    .prologue
    .line 215
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 216
    const-wide/16 v2, 0x3c

    div-long v2, v0, v2

    const-wide/16 v4, 0x3c

    rem-long/2addr v2, v4

    .line 217
    const-wide/16 v4, 0xe10

    div-long v4, v0, v4

    const-wide/16 v6, 0x18

    rem-long/2addr v4, v6

    .line 218
    const-wide/16 v6, 0xe10

    div-long v6, v0, v6

    const-wide/16 v8, 0x18

    div-long/2addr v6, v8

    .line 219
    const-wide/16 v8, 0x3c

    rem-long/2addr v0, v8

    .line 221
    const-string v8, "%02d%s%02d%s"

    .line 222
    const-string v9, "d"

    .line 223
    const-string v10, "h"

    .line 224
    const-string v11, "m"

    .line 225
    const-string v12, "s"

    .line 227
    const-wide/16 v14, 0x1

    cmp-long v13, v6, v14

    if-ltz v13, :cond_0

    .line 228
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v9, v0, v1

    const/4 v1, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v10, v0, v1

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 234
    :goto_0
    return-object v0

    .line 230
    :cond_0
    const-wide/16 v6, 0x1

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    .line 231
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v10, v0, v1

    const/4 v1, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v11, v0, v1

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_1
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v11, v4, v2

    const/4 v2, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x3

    aput-object v12, v4, v0

    invoke-static {v8, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(JLandroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0, p2}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
