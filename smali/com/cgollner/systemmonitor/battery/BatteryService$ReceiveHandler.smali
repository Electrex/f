.class Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/battery/BatteryService;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/Intent;

.field private d:J

.field private e:I


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 142
    iput-object p2, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    .line 143
    iput-object p3, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    .line 144
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 147
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    const-string v1, "clear"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 148
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 149
    :cond_0
    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    .line 151
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    .line 152
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 153
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 154
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 155
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->d(Landroid/content/Context;)V

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->g(Landroid/content/Context;)V

    .line 157
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    .line 160
    :cond_1
    new-instance v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/battery/BatteryInfo;-><init>()V

    .line 161
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    const-string v1, "status"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 162
    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    .line 165
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    const-string v1, "level"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 166
    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    const-string v3, "scale"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 168
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    .line 169
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    const-string v1, "temperature"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->b:I

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    .line 172
    const/4 v0, 0x0

    .line 173
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v1, :cond_7

    .line 174
    iget v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    if-ge v0, v1, :cond_5

    iget-boolean v0, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 175
    :goto_1
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    iget v3, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sub-int/2addr v1, v3

    if-nez v1, :cond_6

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    iget-boolean v3, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-ne v1, v3, :cond_6

    .line 265
    :cond_3
    :goto_2
    return-void

    .line 162
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 179
    :cond_6
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    sput-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    :cond_7
    move v1, v0

    .line 181
    sput-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    .line 183
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    if-nez v0, :cond_8

    .line 184
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->e(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    .line 186
    :cond_8
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    if-nez v0, :cond_9

    .line 187
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    .line 190
    :cond_9
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->m(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 191
    sget-object v3, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    monitor-enter v3

    .line 192
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sget-object v4, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v4, v4, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sub-int/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-eqz v0, :cond_b

    .line 193
    :cond_a
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/TimeValue;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 195
    :try_start_1
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 196
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 197
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 198
    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    iget-wide v8, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v6, v8

    .line 199
    int-to-long v8, v2

    const-wide/16 v10, 0xe10

    mul-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v0, v6, v8

    if-lez v0, :cond_b

    .line 200
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 205
    :catch_0
    move-exception v0

    .line 206
    :try_start_2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    .line 207
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/TimeValue;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_b
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212
    sget-object v3, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    monitor-enter v3

    .line 213
    :try_start_3
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    new-instance v4, Lcom/cgollner/systemmonitor/battery/TimeValue;

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v5, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->b:I

    int-to-float v5, v5

    invoke-direct {v4, v6, v7, v5}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 215
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 216
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 217
    sget-object v5, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    iget-wide v8, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    sub-long/2addr v6, v8

    .line 218
    int-to-long v8, v2

    const-wide/16 v10, 0xe10

    mul-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v0, v6, v8

    if-lez v0, :cond_c

    .line 219
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 210
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 224
    :cond_c
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 226
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->b(Landroid/content/Context;)V

    .line 227
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->g(Landroid/content/Context;)V

    .line 228
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->d(Landroid/content/Context;)V

    .line 230
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v2, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-ne v0, v2, :cond_12

    if-nez v1, :cond_12

    const/4 v0, 0x1

    .line 233
    :goto_5
    if-eqz v0, :cond_f

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->d:J

    .line 235
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->e:I

    .line 237
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_e

    :cond_d
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-nez v0, :cond_f

    iget v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_f

    .line 240
    :cond_e
    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v3, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->d:J

    iget v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->e:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static/range {v1 .. v6}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;ZIJI)V

    .line 243
    :cond_f
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    if-eq v0, v1, :cond_11

    .line 244
    :cond_10
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    .line 245
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;)V

    .line 248
    :cond_11
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_3

    .line 249
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;ZI)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    .line 250
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 251
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;)V

    goto/16 :goto_2

    .line 230
    :cond_12
    const/4 v0, 0x0

    goto :goto_5

    .line 254
    :cond_13
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 255
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget-boolean v1, v1, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    iget v2, v2, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;ZI)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    .line 256
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->h(Landroid/content/Context;)V

    .line 257
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;)V

    goto/16 :goto_2

    .line 259
    :cond_14
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    iget-object v0, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->a:Lcom/cgollner/systemmonitor/battery/BatteryService;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/battery/BatteryService$ReceiveHandler;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;)V

    goto/16 :goto_2
.end method
