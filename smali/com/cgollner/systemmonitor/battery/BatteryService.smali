.class public Lcom/cgollner/systemmonitor/battery/BatteryService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

.field public static b:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

.field public static c:Ljava/util/List;

.field public static d:Ljava/util/List;

.field public static e:Ljava/util/List;

.field private static f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 135
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 123
    const/4 v0, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 124
    new-instance v3, Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    invoke-direct {v3}, Lcom/cgollner/systemmonitor/battery/BatteryInfo;-><init>()V

    .line 125
    const-string v0, "status"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 126
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    const/4 v4, 0x5

    if-ne v0, v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v3, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    .line 128
    const-string v0, "level"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 129
    const-string v4, "scale"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 130
    int-to-float v0, v0

    int-to-float v4, v4

    div-float/2addr v0, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, v3, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    .line 131
    const-string v0, "temperature"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v3, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->b:I

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v3, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->c:J

    .line 133
    return-object v3

    :cond_1
    move v0, v1

    .line 126
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 430
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rws"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 431
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 432
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    .line 433
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v2, v2, [B

    .line 434
    invoke-virtual {v0, v2}, Ljava/io/RandomAccessFile;->read([B)I

    .line 435
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 436
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 437
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 438
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 439
    invoke-interface {v2}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 440
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 441
    invoke-interface {v2}, Ljava/io/ObjectInput;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :goto_0
    return-object v0

    .line 444
    :catch_0
    move-exception v0

    .line 446
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 443
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 425
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILandroid/content/Context;)V
    .locals 2

    .prologue
    .line 334
    const-string v0, "STRATEGY_FILE"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 335
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 336
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 280
    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 281
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 367
    :try_start_0
    monitor-enter p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 369
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 370
    invoke-interface {v1, p2}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 371
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 372
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V

    .line 373
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 374
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rws"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 375
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 376
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    .line 377
    invoke-virtual {v0, v2}, Ljava/io/RandomAccessFile;->write([B)V

    .line 378
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 379
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 380
    monitor-exit p2

    .line 384
    :goto_0
    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 381
    :catch_0
    move-exception v0

    .line 382
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/Object;)V

    .line 363
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 449
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 450
    invoke-static {p0, v0, p3}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/Object;)V

    .line 451
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 404
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 406
    :try_start_0
    monitor-enter p3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    :try_start_1
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rws"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 408
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v0

    .line 410
    invoke-virtual {v1, p3}, Ljava/io/RandomAccessFile;->write([B)V

    .line 411
    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V

    .line 412
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 413
    monitor-exit p3

    .line 417
    :goto_0
    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 414
    :catch_0
    move-exception v0

    .line 415
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 351
    invoke-static {p0, p1}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->b(Landroid/content/Context;Z)V

    .line 352
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 353
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/battery/BatteryService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->n(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 6

    .prologue
    .line 387
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 389
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rws"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 390
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v2

    .line 392
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B

    .line 393
    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 394
    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->release()V

    .line 395
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :goto_0
    return-object v0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 400
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 421
    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 284
    const-string v0, "CURRENT_BATTERY_INFO"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->a:Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 285
    return-void
.end method

.method public static c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;
    .locals 1

    .prologue
    .line 288
    const-string v0, "CURRENT_BATTERY_INFO"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    return-object v0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 291
    const-string v0, "BATTERY_HISTORY_FILE"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->c:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    return-void
.end method

.method public static e(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    .prologue
    .line 296
    const-string v0, "BATTERY_HISTORY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 297
    if-nez v0, :cond_0

    .line 298
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 301
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    .prologue
    .line 307
    const-string v0, "TEMPERATURE_HISTORY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 308
    if-nez v0, :cond_0

    .line 309
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 312
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 318
    :try_start_0
    const-string v0, "TEMPERATURE_HISTORY_FILE"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->d:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    return-void

    .line 319
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 325
    const-string v0, "PREDICTION_ARRAY"

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryService;->e:Ljava/util/List;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 326
    return-void
.end method

.method public static i(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    .prologue
    .line 330
    const-string v0, "PREDICTION_ARRAY"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static j(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338
    const-string v0, "STRATEGY_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 339
    if-nez v0, :cond_0

    .line 340
    invoke-static {v1, p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(ILandroid/content/Context;)V

    .line 342
    :cond_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_1
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static k(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 346
    invoke-static {p0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c(Landroid/content/Context;)V

    .line 347
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 348
    return-void
.end method

.method public static l(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 356
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 357
    const-string v1, "clear"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 358
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 359
    return-void
.end method

.method static synthetic m(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->o(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private n(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 101
    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetProvider;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetProvider;

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v2, "appWidgetIds"

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    return-void
.end method

.method private static o(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/16 v2, 0x30

    .line 269
    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 270
    if-nez v0, :cond_0

    .line 271
    const-string v0, "BATT_HIST_SIZE_FILE"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 272
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 84
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 85
    const/4 v0, 0x0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    .line 86
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 72
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;-><init>(Lcom/cgollner/systemmonitor/battery/BatteryService;)V

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    .line 74
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 75
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 76
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryService;->f:Lcom/cgollner/systemmonitor/battery/BatteryService$BatteryReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 63
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 64
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 66
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 67
    return-void
.end method
