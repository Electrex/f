.class public Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:J

.field public static b:J

.field public static c:Ljava/util/List;

.field public static d:Ljava/lang/Long;

.field private static e:Ljava/lang/Boolean;

.field private static f:Ljava/lang/Integer;

.field private static g:Ljava/util/regex/Pattern;

.field private static h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-wide/32 v0, 0xbf110

    sput-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a:J

    .line 17
    const-wide/32 v0, 0x34f23

    sput-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->b:J

    .line 20
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    return-void
.end method

.method private static a(Ljava/lang/String;C)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 231
    .line 232
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-char v4, v2, v1

    .line 233
    if-ne v4, p1, :cond_0

    .line 234
    add-int/lit8 v0, v0, 0x1

    .line 232
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 236
    :cond_1
    return v0
.end method

.method private static a(Landroid/content/Context;IZ)J
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 172
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 173
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 174
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 225
    :cond_0
    :goto_0
    return-wide v0

    .line 175
    :cond_1
    if-eqz p2, :cond_2

    sget-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->b:J

    goto :goto_0

    :cond_2
    sget-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a:J

    goto :goto_0

    .line 177
    :cond_3
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    move v2, v0

    .line 179
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, "charging"

    .line 180
    :goto_2
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 182
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 183
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 185
    sget v3, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->h:I

    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-double v6, v3

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double/2addr v6, v8

    .line 186
    const-wide/16 v8, 0x0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    sub-double v6, v10, v6

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    .line 187
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v8, v6

    .line 190
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 192
    new-instance v3, Ljava/lang/String;

    invoke-static {p0, v0, v4}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    .line 194
    if-eqz v3, :cond_9

    .line 195
    const/16 v0, 0x2c

    invoke-static {v3, v0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Ljava/lang/String;C)I

    move-result v0

    .line 196
    new-array v4, v0, [J

    .line 198
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    if-nez v0, :cond_4

    .line 199
    const-string v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    .line 201
    :cond_4
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    move v0, v1

    .line 203
    :goto_3
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 204
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    aput-wide v10, v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_3

    :cond_5
    move v2, v1

    .line 177
    goto :goto_1

    .line 179
    :cond_6
    const-string v0, "discharging"

    goto :goto_2

    .line 206
    :cond_7
    :try_start_1
    invoke-static {v4}, Ljava/util/Arrays;->sort([J)V

    .line 208
    if-eqz v2, :cond_8

    .line 209
    invoke-static {v4}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a([J)J

    move-result-wide v0

    long-to-double v0, v0

    mul-double/2addr v0, v8

    sget-object v3, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    double-to-long v0, v0

    goto/16 :goto_0

    .line 212
    :cond_8
    invoke-static {v4}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a([J)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    goto/16 :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 220
    :cond_9
    if-eqz p2, :cond_a

    sget-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->b:J

    .line 221
    :goto_4
    if-eqz v2, :cond_0

    .line 222
    long-to-double v0, v0

    mul-double/2addr v0, v8

    sget-object v2, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    double-to-long v0, v0

    goto/16 :goto_0

    .line 220
    :cond_a
    sget-wide v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a:J

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Z)J
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 72
    .line 73
    if-eqz p1, :cond_1

    .line 74
    const-string v0, "charging"

    .line 77
    :goto_0
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    .line 79
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v4, v2

    .line 81
    :goto_1
    const/16 v1, 0x64

    if-gt v4, v1, :cond_4

    .line 82
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ""

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 84
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 85
    if-eqz v3, :cond_3

    .line 86
    const/16 v1, 0x2c

    invoke-static {v3, v1}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Ljava/lang/String;C)I

    move-result v1

    .line 87
    new-array v7, v1, [Ljava/lang/Long;

    .line 89
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    if-nez v1, :cond_0

    .line 90
    const-string v1, "\\d+"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    .line 92
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    move v1, v2

    .line 94
    :goto_2
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v1

    move v1, v3

    goto :goto_2

    .line 76
    :cond_1
    const-string v0, "discharging"

    goto :goto_0

    .line 98
    :cond_2
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 81
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 102
    :cond_4
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 103
    if-eqz p1, :cond_5

    const/high16 v0, 0x3f000000    # 0.5f

    .line 104
    :goto_3
    invoke-static {v6, v0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Ljava/util/List;F)J

    move-result-wide v0

    return-wide v0

    .line 103
    :cond_5
    const v0, 0x3f333333    # 0.7f

    goto :goto_3
.end method

.method private static a(Ljava/util/List;)J
    .locals 4

    .prologue
    .line 252
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 253
    :cond_0
    const-wide/16 v0, -0x1

    .line 261
    :goto_0
    return-wide v0

    .line 255
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 256
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 259
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 260
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 261
    add-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method private static a(Ljava/util/List;F)J
    .locals 2

    .prologue
    .line 108
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 109
    :cond_0
    const-wide/16 v0, -0x1

    .line 112
    :goto_0
    return-wide v0

    .line 111
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 112
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a([J)J
    .locals 4

    .prologue
    .line 240
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 241
    :cond_0
    const-wide/16 v0, -0x1

    .line 248
    :goto_0
    return-wide v0

    .line 242
    :cond_1
    array-length v0, p0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 243
    array-length v0, p0

    div-int/lit8 v0, v0, 0x2

    aget-wide v0, p0, v0

    goto :goto_0

    .line 246
    :cond_2
    array-length v0, p0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, p0, v0

    .line 247
    array-length v2, p0

    div-int/lit8 v2, v2, 0x2

    aget-wide v2, p0, v2

    .line 248
    add-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ZI)Ljava/util/List;
    .locals 6

    .prologue
    .line 116
    invoke-static {p0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->j(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->f:Ljava/lang/Integer;

    .line 117
    sput p2, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->h:I

    .line 118
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    .line 121
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_2

    .line 122
    :cond_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    .line 123
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 126
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 127
    new-instance v1, Ljava/util/ArrayList;

    const/16 v0, 0x65

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 128
    new-instance v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    int-to-float v4, p2

    invoke-direct {v0, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    if-nez p1, :cond_3

    .line 130
    add-int/lit8 v0, p2, -0x1

    :goto_0
    if-ltz v0, :cond_4

    .line 131
    invoke-static {p0, v0, p1}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;IZ)J

    move-result-wide v4

    .line 132
    add-long/2addr v2, v4

    .line 133
    new-instance v4, Lcom/cgollner/systemmonitor/battery/TimeValue;

    int-to-float v5, v0

    invoke-direct {v4, v2, v3, v5}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 137
    :cond_3
    add-int/lit8 v0, p2, 0x1

    :goto_1
    const/16 v4, 0x64

    if-gt v0, v4, :cond_4

    .line 138
    invoke-static {p0, v0, p1}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;IZ)J

    move-result-wide v4

    .line 139
    add-long/2addr v2, v4

    .line 140
    new-instance v4, Lcom/cgollner/systemmonitor/battery/TimeValue;

    int-to-float v5, v0

    invoke-direct {v4, v2, v3, v5}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 143
    :cond_4
    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 61
    :goto_0
    const-string v2, "LAST_FIVE"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p0, v2, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    return-void

    .line 60
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ZIJI)V
    .locals 5

    .prologue
    .line 33
    const/4 v0, 0x5

    .line 34
    sput p2, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->h:I

    .line 36
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    if-nez v1, :cond_0

    .line 37
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    sput-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    .line 40
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_2

    .line 41
    :cond_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->e:Ljava/lang/Boolean;

    .line 42
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 45
    :cond_2
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v1, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_3

    .line 47
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 49
    :cond_3
    sget-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Ljava/util/List;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->d:Ljava/lang/Long;

    .line 50
    invoke-static {p0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;)V

    .line 52
    if-eqz p1, :cond_4

    const-string v0, "charging"

    .line 53
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    new-instance v2, Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 57
    return-void

    .line 52
    :cond_4
    const-string v0, "discharging"

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)J
    .locals 6

    .prologue
    .line 65
    const-string v0, "LAST_FIVE"

    invoke-static {p0, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 66
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 67
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 68
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 158
    if-eqz p1, :cond_0

    .line 159
    const-string v1, "charging"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 160
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 161
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_0
    const-string v1, "discharging"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 167
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 170
    :cond_1
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 147
    const-string v1, "charging"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 149
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    :cond_0
    const-string v1, "discharging"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 152
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 153
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_1
    return-void
.end method
