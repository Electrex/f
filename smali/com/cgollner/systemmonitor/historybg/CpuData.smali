.class public Lcom/cgollner/systemmonitor/historybg/CpuData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:[I

.field public b:[I

.field public c:[I

.field public d:[F

.field public e:J


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-array v0, v1, [I

    aput v2, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->a:[I

    .line 26
    new-array v0, v1, [I

    aput v2, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->b:[I

    .line 27
    new-array v0, v1, [I

    aput v2, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->c:[I

    .line 28
    new-array v0, v1, [F

    const/4 v1, 0x0

    aput v1, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->d:[F

    .line 29
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    .line 30
    return-void
.end method

.method public constructor <init>([I[I[I[F)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->a:[I

    .line 16
    iput-object p2, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->b:[I

    .line 17
    iput-object p3, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->c:[I

    .line 18
    iput-object p4, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->d:[F

    .line 19
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 20
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 21
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    .line 22
    return-void
.end method
