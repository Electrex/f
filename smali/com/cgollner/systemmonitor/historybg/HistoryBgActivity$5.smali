.class Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;->b:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 181
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 182
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->a:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/CpuData;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    move-wide v2, v0

    .line 183
    :goto_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->a:Ljava/util/List;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/historybg/BgData;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/CpuData;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    .line 184
    :goto_1
    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;->b:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-virtual {v5}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "history"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "\""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    invoke-static {v5, v6, v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;->b:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "File saved"

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 189
    return-void

    .line 182
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_0

    .line 183
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_1
.end method
