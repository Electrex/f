.class Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    .line 222
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 223
    return-void
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 228
    :try_start_0
    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->k()[Ljava/lang/Class;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 229
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 230
    const-string v2, "SEE_ONLY"

    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->g(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 238
    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    .line 238
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 235
    :catch_1
    move-exception v0

    .line 236
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 260
    return-void
.end method

.method public a_(I)V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Landroid/support/v4/view/PagerTitleStrip;

    move-result-object v0

    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->m()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerTitleStrip;->setBackgroundResource(I)V

    .line 256
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->k()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public b_(I)V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-static {}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->l()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
