.class public Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# static fields
.field public static n:Lcom/cgollner/systemmonitor/historybg/BgData;

.field private static final o:[I

.field private static final p:[I

.field private static final w:[Ljava/lang/Class;


# instance fields
.field private q:Landroid/support/v4/view/ViewPager;

.field private r:Landroid/support/v4/view/PagerTitleStrip;

.field private s:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v6, [I

    sget v1, Lcom/cgollner/systemmonitor/lib/R$color;->holo_blue_dark:I

    aput v1, v0, v2

    sget v1, Lcom/cgollner/systemmonitor/lib/R$color;->holo_purple_dark:I

    aput v1, v0, v3

    sget v1, Lcom/cgollner/systemmonitor/lib/R$color;->holo_green_dark:I

    aput v1, v0, v4

    sget v1, Lcom/cgollner/systemmonitor/lib/R$color;->holo_orange_dark:I

    aput v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->o:[I

    .line 37
    new-array v0, v6, [I

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->cpu_title:I

    aput v1, v0, v2

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->ram_title:I

    aput v1, v0, v3

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->io_title:I

    aput v1, v0, v4

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->network_title:I

    aput v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->p:[I

    .line 212
    new-array v0, v6, [Ljava/lang/Class;

    const-class v1, Lcom/cgollner/systemmonitor/historybg/CpuFragmentHistoryBg;

    aput-object v1, v0, v2

    const-class v1, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;

    aput-object v1, v0, v3

    const-class v1, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;

    aput-object v1, v0, v4

    const-class v1, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;

    aput-object v1, v0, v5

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->w:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 219
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)Landroid/support/v4/view/PagerTitleStrip;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->r:Landroid/support/v4/view/PagerTitleStrip;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 103
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n:Lcom/cgollner/systemmonitor/historybg/BgData;

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "history"

    invoke-static {v0, v1, p1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/BgData;

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n:Lcom/cgollner/systemmonitor/historybg/BgData;

    goto :goto_0
.end method

.method static synthetic k()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->w:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic l()[I
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->p:[I

    return-object v0
.end method

.method static synthetic m()[I
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->o:[I

    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 172
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 173
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_history_enter_name:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 175
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_save_menu:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_save_menu:I

    new-instance v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;

    invoke-direct {v3, p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$5;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->dialog_cancel:I

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$4;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$4;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 197
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "theme"

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-ge v0, v5, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 57
    packed-switch v0, :pswitch_data_0

    .line 68
    sget v3, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDark:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    .line 71
    :goto_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    sget v3, Lcom/cgollner/systemmonitor/lib/R$layout;->activity_main:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setContentView(I)V

    .line 74
    :try_start_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->background_history_title:I

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->a(I)V

    .line 75
    if-ne v0, v1, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v3, 0x106000d

    invoke-static {p0, v3}, Landroid/support/v4/content/ContextCompat;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->a(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v3, v1, [I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$attr;->logoIcon:I

    aput v4, v3, v2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 83
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    .line 84
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 87
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->viewPager:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->q:Landroid/support/v4/view/ViewPager;

    .line 88
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->pager_title_strip:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTitleStrip;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->r:Landroid/support/v4/view/PagerTitleStrip;

    .line 90
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SEE_ONLY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->u:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->u:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->b(Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->f()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->s:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->q:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->s:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 95
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->q:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->s:Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$MonitorAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 96
    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->t:Z

    .line 98
    invoke-static {p0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->v:Z

    .line 99
    return-void

    :cond_1
    move v0, v2

    .line 56
    goto/16 :goto_0

    .line 59
    :pswitch_0
    sget v3, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDefault:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 62
    :pswitch_1
    sget v3, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDark:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 65
    :pswitch_2
    sget v3, Lcom/cgollner/systemmonitor/lib/R$style;->AppThemeDefault:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->setTheme(I)V

    goto/16 :goto_1

    .line 78
    :catch_0
    move-exception v0

    goto :goto_2

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$menu;->bg_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 113
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->save:I

    if-ne v0, v1, :cond_4

    .line 120
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_title:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Play Store"

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$2;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$1;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 165
    :cond_0
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 166
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->invalidateOptionsMenu()V

    .line 168
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 140
    :cond_2
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->v:Z

    if-eqz v0, :cond_3

    .line 141
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->unlicensed_dialog_title:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->unlicensed_dialog_body:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$3;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity$3;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 152
    :cond_3
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n()V

    goto :goto_0

    .line 154
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->stop_start:I

    if-ne v0, v1, :cond_0

    .line 155
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->t:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->t:Z

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    iget-boolean v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->t:Z

    if-eqz v1, :cond_6

    .line 158
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 159
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->finish()V

    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 155
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 162
    :cond_6
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 201
    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->u:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [I

    sget v4, Lcom/cgollner/systemmonitor/lib/R$attr;->playIcon:I

    aput v4, v3, v1

    sget v4, Lcom/cgollner/systemmonitor/lib/R$attr;->stopIcon:I

    aput v4, v3, v0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 205
    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->stop_start:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v4, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->t:Z

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 206
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 208
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 205
    goto :goto_0
.end method
