.class Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    .line 152
    new-instance v5, Lcom/cgollner/systemmonitor/historybg/CpuData;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    move-result-object v0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    move-result-object v1

    iget-object v1, v1, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->c:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    move-result-object v2

    iget-object v2, v2, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    move-result-object v3

    iget-object v3, v3, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    invoke-virtual {v3}, [F->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [F

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/historybg/CpuData;-><init>([I[I[I[F)V

    .line 158
    const/4 v1, 0x0

    .line 159
    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    monitor-enter v6

    .line 160
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 161
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/CpuData;

    .line 163
    iget-wide v2, v0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    .line 164
    iget-wide v8, v5, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    iget-wide v10, v0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    sub-long/2addr v8, v10

    .line 165
    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v8, v10

    move v0, v1

    move v1, v4

    .line 166
    :goto_0
    int-to-long v10, v1

    cmp-long v7, v10, v8

    if-gez v7, :cond_0

    .line 167
    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v2, v10

    .line 168
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/CpuData;

    invoke-direct {v7, v2, v3}, Lcom/cgollner/systemmonitor/historybg/CpuData;-><init>(J)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_0

    :cond_0
    move v1, v0

    .line 172
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;

    .line 176
    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;->a(Z)V

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 178
    :cond_2
    return-void
.end method
