.class Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 182
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/RamData;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    move-result-object v1

    iget v1, v1, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->a:F

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    move-result-object v2

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->b:J

    iget-object v4, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v4}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    move-result-object v4

    iget-wide v4, v4, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->c:J

    iget-object v6, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v6}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    move-result-object v6

    iget-wide v6, v6, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->d:J

    invoke-direct/range {v0 .. v7}, Lcom/cgollner/systemmonitor/historybg/RamData;-><init>(FJJJ)V

    .line 189
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    monitor-enter v1

    .line 190
    :try_start_0
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;

    .line 194
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;->a(Z)V

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 196
    :cond_0
    return-void
.end method
