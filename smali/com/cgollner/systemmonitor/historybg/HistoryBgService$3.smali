.class Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 200
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/IoData;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v1

    iget v1, v1, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->c:F

    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v2

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->d:J

    iget-object v4, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v4}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v4

    iget-wide v4, v4, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->e:J

    sget-wide v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct/range {v0 .. v7}, Lcom/cgollner/systemmonitor/historybg/IoData;-><init>(FJJJ)V

    .line 206
    const/4 v2, 0x0

    .line 207
    sget-object v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    monitor-enter v3

    .line 208
    :try_start_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 209
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    sget-object v4, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/historybg/IoData;

    .line 211
    iget-wide v4, v1, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    .line 212
    iget-wide v6, v0, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    iget-wide v10, v1, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    sub-long/2addr v6, v10

    .line 213
    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v6, v10

    move v1, v2

    move v2, v8

    .line 214
    :goto_0
    int-to-long v10, v2

    cmp-long v9, v10, v6

    if-gez v9, :cond_1

    .line 215
    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v4, v10

    .line 216
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    new-instance v9, Lcom/cgollner/systemmonitor/historybg/IoData;

    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v9, v4, v5, v10, v11}, Lcom/cgollner/systemmonitor/historybg/IoData;-><init>(JJ)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v8

    goto :goto_0

    :cond_0
    move v1, v2

    .line 220
    :cond_1
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;

    .line 224
    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;->a(Z)V

    goto :goto_1

    .line 221
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 226
    :cond_2
    return-void
.end method
