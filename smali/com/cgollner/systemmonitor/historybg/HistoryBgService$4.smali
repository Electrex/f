.class Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 19

    .prologue
    .line 230
    new-instance v3, Lcom/cgollner/systemmonitor/historybg/NetData;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v4, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->c:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v6, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->a:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v8, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->b:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v10, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->j:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v12, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->d:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;->a:Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    move-result-object v2

    iget-wide v14, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->e:J

    sget-wide v16, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct/range {v3 .. v17}, Lcom/cgollner/systemmonitor/historybg/NetData;-><init>(JJJJJJJ)V

    .line 241
    const/4 v4, 0x0

    .line 242
    sget-object v8, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    monitor-enter v8

    .line 243
    :try_start_0
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 244
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    sget-object v5, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/historybg/NetData;

    .line 246
    iget-wide v6, v2, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    .line 247
    iget-wide v10, v3, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    iget-wide v12, v2, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    sub-long/2addr v10, v12

    .line 248
    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v10, v12

    .line 249
    const/4 v2, 0x1

    move/from16 v18, v2

    move v2, v4

    move/from16 v4, v18

    :goto_0
    int-to-long v12, v4

    cmp-long v5, v12, v10

    if-gez v5, :cond_0

    .line 250
    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v6, v12

    .line 251
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    new-instance v5, Lcom/cgollner/systemmonitor/historybg/NetData;

    sget-wide v12, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v5, v6, v7, v12, v13}, Lcom/cgollner/systemmonitor/historybg/NetData;-><init>(JJ)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    const/4 v5, 0x1

    .line 249
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_0

    :cond_0
    move v4, v2

    .line 255
    :cond_1
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;

    .line 259
    invoke-interface {v2, v4}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;->a(Z)V

    goto :goto_1

    .line 256
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 261
    :cond_2
    return-void
.end method
