.class public Lcom/cgollner/systemmonitor/historybg/HistoryBgService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static a:Ljava/util/List;

.field public static b:Ljava/util/List;

.field public static c:Ljava/util/List;

.field public static d:Ljava/util/List;

.field public static e:Lcom/cgollner/systemmonitor/historybg/BgData;

.field public static f:Ljava/util/List;

.field public static g:Ljava/util/List;

.field public static h:Ljava/util/List;

.field public static i:Ljava/util/List;

.field public static j:J


# instance fields
.field private k:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

.field private l:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

.field private m:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

.field private n:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

.field private o:Landroid/app/Notification;

.field private p:Landroid/support/v4/app/NotificationCompat$Builder;

.field private q:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

.field private r:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

.field private s:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

.field private t:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    const-wide/16 v0, 0x1388

    sput-wide v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 150
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$1;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->q:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    .line 180
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$2;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->r:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    .line 198
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$3;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->s:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    .line 228
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService$4;-><init>(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->t:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/CpuMonitor;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 103
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->q:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    .line 104
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->r:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    move v7, v4

    invoke-direct/range {v1 .. v7}, Lcom/cgollner/systemmonitor/monitor/RamMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    .line 105
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->s:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    .line 106
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    sget-wide v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->t:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/NetMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    .line 107
    return-void
.end method

.method public static a(JJLandroid/content/Context;)V
    .locals 6

    .prologue
    const/high16 v5, 0x8000000

    .line 313
    const-string v0, "alarm"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 314
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v1, p4, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    long-to-int v2, p0

    invoke-static {p4, v2, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 317
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v2, p4, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    const-string v3, "EXTRA_FINISH"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 319
    long-to-int v3, p2

    invoke-static {p4, v3, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 321
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 322
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 323
    return-void
.end method

.method public static a(JJLjava/lang/String;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/high16 v6, 0x8000000

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 295
    .line 296
    const-string v0, "schedules"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p5, v0, v1, v2}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 298
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, p5, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    long-to-int v1, p0

    invoke-static {p5, v1, v0, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 301
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v0, p5, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 302
    const-string v2, "EXTRA_FINISH"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 303
    const-string v2, "SCHEDULE_NAME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    const-string v2, "EXTRA_FINISH_TIME"

    invoke-virtual {v0, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 305
    long-to-int v2, p2

    invoke-static {p5, v2, v0, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 307
    const-string v0, "alarm"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 308
    invoke-virtual {v0, v5, p0, p1, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 309
    invoke-virtual {v0, v5, p2, p3, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 310
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 270
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v1, :cond_2

    .line 271
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/CpuData;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/historybg/CpuData;->e:J

    .line 272
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 273
    sub-long v4, v8, v2

    .line 274
    sget-wide v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    div-long/2addr v4, v6

    .line 275
    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    .line 276
    :goto_0
    int-to-long v6, v1

    cmp-long v6, v6, v4

    if-gez v6, :cond_1

    .line 277
    sget-wide v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    add-long/2addr v2, v6

    .line 278
    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/CpuData;

    invoke-direct {v7, v2, v3}, Lcom/cgollner/systemmonitor/historybg/CpuData;-><init>(J)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/IoData;

    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v7, v2, v3, v10, v11}, Lcom/cgollner/systemmonitor/historybg/IoData;-><init>(JJ)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    sget-object v6, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    new-instance v7, Lcom/cgollner/systemmonitor/historybg/NetData;

    sget-wide v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    invoke-direct {v7, v2, v3, v10, v11}, Lcom/cgollner/systemmonitor/historybg/NetData;-><init>(JJ)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 275
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 282
    :cond_1
    if-eqz v0, :cond_2

    .line 283
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/cgollner/systemmonitor/historybg/RamData;

    .line 284
    sget-object v10, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    new-instance v0, Lcom/cgollner/systemmonitor/historybg/RamData;

    iget v1, v6, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    iget-wide v2, v6, Lcom/cgollner/systemmonitor/historybg/RamData;->c:J

    iget-wide v4, v6, Lcom/cgollner/systemmonitor/historybg/RamData;->b:J

    iget-wide v6, v6, Lcom/cgollner/systemmonitor/historybg/RamData;->d:J

    invoke-direct/range {v0 .. v9}, Lcom/cgollner/systemmonitor/historybg/RamData;-><init>(FJJJJ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_2
    const-string v0, "history"

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    invoke-static {p0, v0, p1, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 11

    .prologue
    const/16 v10, 0x2d

    const/4 v0, 0x0

    .line 326
    const-string v1, "schedules"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 327
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 328
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 329
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 330
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/16 v7, 0x22

    invoke-virtual {v5, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    invoke-virtual {v6, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 331
    new-instance v7, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 332
    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v6, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 333
    new-instance v6, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 334
    invoke-virtual {v7, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v6, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 337
    :goto_1
    return v0

    .line 328
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 337
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->k:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->h()V

    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->l:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->h()V

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->h()V

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->h()V

    .line 114
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->m:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/HistoryBgService;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 68
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 70
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_update_interval_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sput-wide v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->j:J

    .line 72
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->f:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->i:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    .line 79
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    .line 82
    new-instance v0, Lcom/cgollner/systemmonitor/historybg/BgData;

    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    sget-object v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    sget-object v4, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/historybg/BgData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    sput-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->e:Lcom/cgollner/systemmonitor/historybg/BgData;

    .line 83
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a()V

    .line 85
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {v1, v5, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 88
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_service_is_running:I

    invoke-virtual {p0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_service_see_progress:I

    invoke-virtual {p0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_service_is_running:I

    invoke-virtual {p0, v2}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$drawable;->ic_stat_iconstatus:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->o:Landroid/app/Notification;

    .line 99
    const/16 v0, 0x26c0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->o:Landroid/app/Notification;

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->startForeground(ILandroid/app/Notification;)V

    .line 100
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->stopForeground(Z)V

    .line 61
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b()V

    .line 62
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 63
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 118
    if-eqz p1, :cond_1

    const-string v0, "EXTRA_FINISH"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    const-string v0, "SCHEDULE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->stopSelf()V

    .line 124
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    const-string v2, "SEE_ONLY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "EXTRA_FINISH_TIME"

    const-wide/16 v4, 0x1

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v3, v4

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 133
    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 134
    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_service_is_finished:I

    invoke-virtual {p0, v5}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->history_bg_service_is_finished:I

    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 142
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->p:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v2

    .line 143
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 144
    const/16 v3, 0xc

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 147
    :cond_1
    return v6
.end method
