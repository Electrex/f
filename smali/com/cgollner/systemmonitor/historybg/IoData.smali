.class public Lcom/cgollner/systemmonitor/historybg/IoData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:F

.field public b:J

.field public c:J

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>(FJJJ)V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->a:F

    .line 14
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->b:J

    .line 15
    iput-wide p4, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->c:J

    .line 16
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    .line 17
    iput-wide p6, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->e:J

    .line 18
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    .line 21
    iput-wide p3, p0, Lcom/cgollner/systemmonitor/historybg/IoData;->e:J

    .line 22
    return-void
.end method
