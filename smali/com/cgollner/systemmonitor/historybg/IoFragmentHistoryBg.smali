.class public Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;


# instance fields
.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private aA:Landroid/widget/TextView;

.field private aB:Landroid/widget/TextView;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/TextView;

.field private aF:Z

.field private aG:Landroid/os/Handler;

.field private aj:J

.field private ak:J

.field private al:J

.field private am:J

.field private an:J

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Landroid/widget/TextView;

.field private ar:Lcom/cgollner/systemmonitor/historybg/IoData;

.field private as:I

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/TextView;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/widget/TextView;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F

.field private e:F

.field private f:J

.field private g:J

.field private h:J

.field private i:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic A(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aD:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic B(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->an:J

    return-wide v0
.end method

.method static synthetic C(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aE:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic D(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->c:F

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aG:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg$1;-><init>(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/IoData;)V
    .locals 6

    .prologue
    .line 168
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->c:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->a:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->c:F

    .line 169
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->e:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->e:F

    .line 170
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->d:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->d:F

    .line 171
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v1, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->d:J

    iget v4, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->a:F

    invoke-direct {v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/TimeValue;)V

    .line 173
    const-wide v0, 0x408f400000000000L    # 1000.0

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->e:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 174
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->b:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 175
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->f:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->f:J

    .line 176
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->h:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->h:J

    .line 177
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->g:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->g:J

    .line 179
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->c:J

    long-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 180
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i:J

    .line 181
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ak:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ak:J

    .line 182
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aj:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aj:J

    .line 184
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    .line 185
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->am:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->am:J

    .line 186
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->an:J

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->b:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/IoData;->c:J

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->an:J

    .line 188
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 127
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    .line 128
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 130
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/IoData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ar:Lcom/cgollner/systemmonitor/historybg/IoData;

    .line 132
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/IoData;

    .line 133
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/IoData;)V

    goto :goto_1

    .line 135
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ao:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->d:F

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aq:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->e:F

    return v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ap:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Lcom/cgollner/systemmonitor/historybg/IoData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ar:Lcom/cgollner/systemmonitor/historybg/IoData;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->at:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->f:J

    return-wide v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->au:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->h:J

    return-wide v0
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->g:J

    return-wide v0
.end method

.method static synthetic o(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->av:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ax:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i:J

    return-wide v0
.end method

.method static synthetic r(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ay:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ak:J

    return-wide v0
.end method

.method static synthetic t(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aA:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic u(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aj:J

    return-wide v0
.end method

.method static synthetic v(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->az:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic w(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aB:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic x(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    return-wide v0
.end method

.method static synthetic y(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic z(Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->am:J

    return-wide v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aG:Landroid/os/Handler;

    .line 76
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->io_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ao:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ap:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aq:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->at:Landroid/widget/TextView;

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->readAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->au:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->readMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->av:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->readMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aw:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->readLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ax:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->writeAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ay:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->writeMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->az:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->writeMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aA:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->writeLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aB:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->totalRead:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aC:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->totalWrite:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aD:Landroid/widget/TextView;

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->totalTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aE:Landroid/widget/TextView;

    .line 100
    iput v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->c:F

    .line 101
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->e:F

    .line 102
    iput v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->d:F

    .line 104
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->f:J

    .line 105
    iput-wide v6, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->h:J

    .line 106
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->g:J

    .line 108
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i:J

    .line 109
    iput-wide v6, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ak:J

    .line 110
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aj:J

    .line 112
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    .line 113
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    .line 114
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->an:J

    .line 116
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aF:Z

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->b:Landroid/view/View;

    return-object v0

    .line 120
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    monitor-enter v1

    .line 121
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 122
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aF:Z

    .line 56
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aF:Z

    if-eqz v0, :cond_1

    .line 61
    :goto_1
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 140
    if-eqz p1, :cond_0

    .line 141
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->c:F

    .line 142
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->e:F

    .line 143
    iput v1, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->d:F

    .line 145
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->f:J

    .line 146
    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->h:J

    .line 147
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->g:J

    .line 149
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->i:J

    .line 150
    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ak:J

    .line 151
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aj:J

    .line 153
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    .line 154
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->al:J

    .line 155
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->an:J

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 157
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 165
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    .line 161
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->c:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->as:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/IoData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ar:Lcom/cgollner/systemmonitor/historybg/IoData;

    .line 162
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->ar:Lcom/cgollner/systemmonitor/historybg/IoData;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/IoData;)V

    .line 163
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->a()V

    goto :goto_0
.end method

.method public w()V
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/IoFragmentHistoryBg;->aF:Z

    if-nez v0, :cond_0

    .line 66
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 69
    return-void
.end method
