.class public Lcom/cgollner/systemmonitor/historybg/NetData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    .line 28
    iput-wide p3, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->h:J

    .line 29
    return-void
.end method

.method public constructor <init>(JJJJJJJ)V
    .locals 5

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->a:J

    .line 17
    iput-wide p3, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->b:J

    .line 18
    iput-wide p5, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->c:J

    .line 19
    iput-wide p7, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->d:J

    .line 20
    iput-wide p9, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->e:J

    .line 21
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->f:J

    .line 22
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    .line 23
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetData;->h:J

    .line 24
    return-void
.end method
