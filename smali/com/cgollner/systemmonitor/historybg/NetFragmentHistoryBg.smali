.class public Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;


# instance fields
.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private aA:Landroid/widget/TextView;

.field private aB:Landroid/widget/TextView;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/TextView;

.field private aF:Landroid/widget/TextView;

.field private aG:Landroid/widget/TextView;

.field private aH:Z

.field private aI:Landroid/os/Handler;

.field private aj:J

.field private ak:J

.field private al:J

.field private am:J

.field private an:J

.field private ao:J

.field private ap:Lcom/cgollner/systemmonitor/historybg/NetData;

.field private aq:I

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/TextView;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/widget/TextView;

.field private b:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private c:Landroid/view/View;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic A(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aE:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic B(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ao:J

    return-wide v0
.end method

.method static synthetic C(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aG:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic D(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aF:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic E(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method

.method static synthetic F(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->d:J

    return-wide v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aI:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg$1;-><init>(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 240
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/NetData;)V
    .locals 10

    .prologue
    .line 180
    const-wide v0, 0x408f400000000000L    # 1000.0

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->h:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 181
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->b:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 182
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->d:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->d:J

    .line 183
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->e:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->e:J

    .line 184
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->f:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->b:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->f:J

    .line 186
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->c:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 187
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->g:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->g:J

    .line 188
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->h:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->h:J

    .line 189
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->c:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i:J

    .line 191
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->e:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 192
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aj:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aj:J

    .line 193
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ak:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ak:J

    .line 194
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->al:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->e:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->al:J

    .line 196
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->f:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    .line 197
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->am:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->am:J

    .line 198
    iget-wide v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->an:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->an:J

    .line 199
    iget-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ao:J

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->f:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ao:J

    .line 201
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->b:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->c:J

    long-to-double v4, v4

    mul-double/2addr v4, v0

    add-double/2addr v2, v4

    double-to-long v2, v2

    .line 202
    iget-object v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iget-wide v6, v5, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    long-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iput-wide v6, v4, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    .line 203
    iget-object v4, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v5, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v6, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    long-to-float v2, v2

    invoke-direct {v5, v6, v7, v2}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-virtual {v4, v5}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/TimeValue;)V

    .line 205
    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->e:J

    long-to-double v2, v2

    mul-double/2addr v2, v0

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->f:J

    long-to-double v4, v4

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-long v0, v0

    .line 206
    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iget-wide v4, v3, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    long-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    .line 207
    iget-object v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v3, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v4, p1, Lcom/cgollner/systemmonitor/historybg/NetData;->g:J

    long-to-float v0, v0

    invoke-direct {v3, v4, v5, v0}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-virtual {v2, v3}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/TimeValue;)V

    .line 209
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 136
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    .line 137
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 139
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/NetData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ap:Lcom/cgollner/systemmonitor/historybg/NetData;

    .line 141
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/NetData;

    .line 142
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/NetData;)V

    goto :goto_1

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ar:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->e:J

    return-wide v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->as:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->f:J

    return-wide v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->au:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Lcom/cgollner/systemmonitor/historybg/NetData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ap:Lcom/cgollner/systemmonitor/historybg/NetData;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->at:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->g:J

    return-wide v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->av:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->h:J

    return-wide v0
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i:J

    return-wide v0
.end method

.method static synthetic o(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ay:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ax:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aj:J

    return-wide v0
.end method

.method static synthetic r(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->az:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ak:J

    return-wide v0
.end method

.method static synthetic t(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aA:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic u(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->al:J

    return-wide v0
.end method

.method static synthetic v(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic w(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aB:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic x(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->am:J

    return-wide v0
.end method

.method static synthetic y(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aD:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic z(Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->an:J

    return-wide v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v2, 0x0

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aI:Landroid/os/Handler;

    .line 79
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->net_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->monitorviewWifi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-wide v4, v0, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->monitorviewMobile:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-wide v4, v0, Lcom/cgollner/systemmonitor/TimeMonitorView;->b:D

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiRecvAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ar:Landroid/widget/TextView;

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiRecvMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->as:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiRecvLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->at:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiRecvTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->au:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiSendAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->av:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiSendMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aw:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiSendLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ax:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->wifiSendTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ay:Landroid/widget/TextView;

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileRecvAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->az:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileRecvMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aA:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileRecvLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aB:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileRecvTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aC:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileSendAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aD:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileSendMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aE:Landroid/widget/TextView;

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileSendLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aF:Landroid/widget/TextView;

    .line 107
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->mobileSendTotal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aG:Landroid/widget/TextView;

    .line 109
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->d:J

    .line 110
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->e:J

    .line 111
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->f:J

    .line 113
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->g:J

    .line 114
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->h:J

    .line 115
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i:J

    .line 117
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aj:J

    .line 118
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ak:J

    .line 119
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->al:J

    .line 121
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->am:J

    .line 122
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->an:J

    .line 123
    iput-wide v2, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ao:J

    .line 125
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aH:Z

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->c:Landroid/view/View;

    return-object v0

    .line 129
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    monitor-enter v1

    .line 130
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 131
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aH:Z

    .line 58
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aH:Z

    if-eqz v0, :cond_1

    .line 63
    :goto_1
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->i:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 149
    if-eqz p1, :cond_0

    .line 150
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->d:J

    .line 151
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->e:J

    .line 152
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->f:J

    .line 154
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->g:J

    .line 155
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->h:J

    .line 156
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->i:J

    .line 158
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aj:J

    .line 159
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ak:J

    .line 160
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->al:J

    .line 162
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->am:J

    .line 163
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->an:J

    .line 164
    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ao:J

    .line 166
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 167
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->b:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 169
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 177
    :goto_0
    return-void

    .line 172
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    .line 173
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->d:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aq:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/NetData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ap:Lcom/cgollner/systemmonitor/historybg/NetData;

    .line 174
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->ap:Lcom/cgollner/systemmonitor/historybg/NetData;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/NetData;)V

    .line 175
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->a()V

    goto :goto_0
.end method

.method public w()V
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/NetFragmentHistoryBg;->aH:Z

    if-nez v0, :cond_0

    .line 68
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->i:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 71
    return-void
.end method
