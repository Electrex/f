.class public Lcom/cgollner/systemmonitor/historybg/RamData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:F

.field public b:J

.field public c:J

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>(FJJJ)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    .line 16
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->b:J

    .line 17
    iput-wide p4, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->c:J

    .line 18
    iput-wide p6, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->d:J

    .line 19
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 20
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 21
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->e:J

    .line 22
    return-void
.end method

.method public constructor <init>(FJJJJ)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    .line 26
    iput-wide p4, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->b:J

    .line 27
    iput-wide p2, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->c:J

    .line 28
    iput-wide p6, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->d:J

    .line 29
    iput-wide p8, p0, Lcom/cgollner/systemmonitor/historybg/RamData;->e:J

    .line 30
    return-void
.end method
