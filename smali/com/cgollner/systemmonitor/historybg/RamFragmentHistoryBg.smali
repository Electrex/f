.class public Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/historybg/HistoryBgService$HistoryBgListener;


# instance fields
.field private a:Lcom/cgollner/systemmonitor/TimeMonitorView;

.field private aj:I

.field private ak:Landroid/widget/TextView;

.field private al:Z

.field private am:Landroid/os/Handler;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F

.field private e:F

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/cgollner/systemmonitor/historybg/RamData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->c:F

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->am:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg$1;-><init>(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    return-void
.end method

.method private a(Lcom/cgollner/systemmonitor/historybg/RamData;)V
    .locals 5

    .prologue
    .line 105
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->c:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->c:F

    .line 106
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->e:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->e:F

    .line 107
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->d:F

    iget v1, p1, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->d:F

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    new-instance v1, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, p1, Lcom/cgollner/systemmonitor/historybg/RamData;->e:J

    iget v4, p1, Lcom/cgollner/systemmonitor/historybg/RamData;->a:F

    invoke-direct {v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/battery/TimeValue;-><init>(JF)V

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a(Lcom/cgollner/systemmonitor/battery/TimeValue;)V

    .line 110
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 85
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    .line 86
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 88
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/RamData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->i:Lcom/cgollner/systemmonitor/historybg/RamData;

    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/RamData;

    .line 91
    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/RamData;)V

    goto :goto_1

    .line 93
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    return v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->d:F

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->e:F

    return v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Lcom/cgollner/systemmonitor/historybg/RamData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->i:Lcom/cgollner/systemmonitor/historybg/RamData;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->ak:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;)Lcom/cgollner/systemmonitor/TimeMonitorView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->am:Landroid/os/Handler;

    .line 60
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->ram_fragment_history_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/TimeMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a:Lcom/cgollner/systemmonitor/TimeMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/TimeMonitorView;->a()V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->f:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageMin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->g:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageMax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->h:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageLast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->ak:Landroid/widget/TextView;

    .line 70
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->c:F

    .line 71
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->e:F

    .line 72
    iput v2, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->d:F

    .line 74
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->al:Z

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgActivity;->n:Lcom/cgollner/systemmonitor/historybg/BgData;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/historybg/BgData;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->b:Landroid/view/View;

    return-object v0

    .line 78
    :cond_0
    sget-object v1, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    monitor-enter v1

    .line 79
    :try_start_0
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a(Ljava/util/List;)V

    .line 80
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->i()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SEE_ONLY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->al:Z

    .line 40
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->al:Z

    if-eqz v0, :cond_1

    .line 45
    :goto_1
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :cond_1
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    .line 99
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->b:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->aj:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/historybg/RamData;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->i:Lcom/cgollner/systemmonitor/historybg/RamData;

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->i:Lcom/cgollner/systemmonitor/historybg/RamData;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a(Lcom/cgollner/systemmonitor/historybg/RamData;)V

    .line 101
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->a()V

    .line 102
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/historybg/RamFragmentHistoryBg;->al:Z

    if-nez v0, :cond_0

    .line 50
    sget-object v0, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 53
    return-void
.end method
