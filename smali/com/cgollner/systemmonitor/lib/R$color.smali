.class public final Lcom/cgollner/systemmonitor/lib/R$color;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0a016c

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0a016d

.field public static final abc_input_method_navigation_guard:I = 0x7f0a0000

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0a016e

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0a016f

.field public static final abc_primary_text_material_dark:I = 0x7f0a0170

.field public static final abc_primary_text_material_light:I = 0x7f0a0171

.field public static final abc_search_url_text:I = 0x7f0a0172

.field public static final abc_search_url_text_normal:I = 0x7f0a0001

.field public static final abc_search_url_text_pressed:I = 0x7f0a0002

.field public static final abc_search_url_text_selected:I = 0x7f0a0003

.field public static final abc_secondary_text_material_dark:I = 0x7f0a0173

.field public static final abc_secondary_text_material_light:I = 0x7f0a0174

.field public static final accent_material_dark:I = 0x7f0a0004

.field public static final accent_material_light:I = 0x7f0a0005

.field public static final background_floating_material_dark:I = 0x7f0a0014

.field public static final background_floating_material_light:I = 0x7f0a0015

.field public static final background_material_dark:I = 0x7f0a0016

.field public static final background_material_light:I = 0x7f0a0017

.field public static final batteryQ1Fill:I = 0x7f0a0018

.field public static final batteryQ1Line:I = 0x7f0a0019

.field public static final batteryQ1Preview:I = 0x7f0a001a

.field public static final batteryQ2Fill:I = 0x7f0a001b

.field public static final batteryQ2Line:I = 0x7f0a001c

.field public static final batteryQ2Preview:I = 0x7f0a001d

.field public static final batteryQ3Fill:I = 0x7f0a001e

.field public static final batteryQ3Line:I = 0x7f0a001f

.field public static final batteryQ3Preview:I = 0x7f0a0020

.field public static final batteryQ4Fill:I = 0x7f0a0021

.field public static final batteryQ4Line:I = 0x7f0a0022

.field public static final batteryQ4Preview:I = 0x7f0a0023

.field public static final black_overlay:I = 0x7f0a0025

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0a003e

.field public static final bright_foreground_disabled_material_light:I = 0x7f0a003f

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0a0040

.field public static final bright_foreground_inverse_material_light:I = 0x7f0a0041

.field public static final bright_foreground_material_dark:I = 0x7f0a0042

.field public static final bright_foreground_material_light:I = 0x7f0a0043

.field public static final button_material_dark:I = 0x7f0a004e

.field public static final button_material_light:I = 0x7f0a004f

.field public static final darkgray:I = 0x7f0a006c

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0a0089

.field public static final dim_foreground_disabled_material_light:I = 0x7f0a008a

.field public static final dim_foreground_material_dark:I = 0x7f0a008b

.field public static final dim_foreground_material_light:I = 0x7f0a008c

.field public static final fillColor:I = 0x7f0a008d

.field public static final fillColorDark:I = 0x7f0a008e

.field public static final gridColor:I = 0x7f0a00a7

.field public static final gridColorDark:I = 0x7f0a00a8

.field public static final highlighted_text_material_dark:I = 0x7f0a00a9

.field public static final highlighted_text_material_light:I = 0x7f0a00aa

.field public static final hint_foreground_material_dark:I = 0x7f0a00ab

.field public static final hint_foreground_material_light:I = 0x7f0a00ac

.field public static final holo_blue_dark:I = 0x7f0a00ad

.field public static final holo_blue_light:I = 0x7f0a00ae

.field public static final holo_green_dark:I = 0x7f0a00af

.field public static final holo_green_light:I = 0x7f0a00b0

.field public static final holo_orange_dark:I = 0x7f0a00b1

.field public static final holo_orange_light:I = 0x7f0a00b2

.field public static final holo_purple_dark:I = 0x7f0a00b3

.field public static final holo_purple_light:I = 0x7f0a00b4

.field public static final holo_red_dark:I = 0x7f0a00b5

.field public static final holo_red_light:I = 0x7f0a00b6

.field public static final ioFillColor:I = 0x7f0a00c5

.field public static final ioGridColor:I = 0x7f0a00c6

.field public static final ioLineColor:I = 0x7f0a00c7

.field public static final lineColor:I = 0x7f0a00f2

.field public static final link_text_material_dark:I = 0x7f0a00f3

.field public static final link_text_material_light:I = 0x7f0a00f4

.field public static final material_blue_grey_800:I = 0x7f0a00f5

.field public static final material_blue_grey_900:I = 0x7f0a00f6

.field public static final material_blue_grey_950:I = 0x7f0a00f7

.field public static final material_deep_teal_200:I = 0x7f0a00f8

.field public static final material_deep_teal_500:I = 0x7f0a00f9

.field public static final networkFillColor:I = 0x7f0a00fa

.field public static final networkGridColor:I = 0x7f0a00fb

.field public static final networkLineColor:I = 0x7f0a00fc

.field public static final notificationBG:I = 0x7f0a00fd

.field public static final notificationIconBg:I = 0x7f0a00fe

.field public static final notificationLine:I = 0x7f0a00ff

.field public static final pressed_greenaction:I = 0x7f0a011c

.field public static final primary_dark_material_dark:I = 0x7f0a011d

.field public static final primary_dark_material_light:I = 0x7f0a011e

.field public static final primary_material_dark:I = 0x7f0a011f

.field public static final primary_material_light:I = 0x7f0a0120

.field public static final primary_text_default_material_dark:I = 0x7f0a0121

.field public static final primary_text_default_material_light:I = 0x7f0a0122

.field public static final primary_text_disabled_material_dark:I = 0x7f0a0123

.field public static final primary_text_disabled_material_light:I = 0x7f0a0124

.field public static final ramFillColor:I = 0x7f0a0133

.field public static final ramGridColor:I = 0x7f0a0134

.field public static final ramLineColor:I = 0x7f0a0135

.field public static final ripple_material_dark:I = 0x7f0a0144

.field public static final ripple_material_light:I = 0x7f0a0145

.field public static final secondary_text_default_material_dark:I = 0x7f0a0146

.field public static final secondary_text_default_material_light:I = 0x7f0a0147

.field public static final secondary_text_disabled_material_dark:I = 0x7f0a0148

.field public static final secondary_text_disabled_material_light:I = 0x7f0a0149

.field public static final switch_thumb_normal_material_dark:I = 0x7f0a014a

.field public static final switch_thumb_normal_material_light:I = 0x7f0a014b

.field public static final whiteFillColor:I = 0x7f0a015a

.field public static final whiteGridColor:I = 0x7f0a015b

.field public static final whiteLineColor:I = 0x7f0a015c
