.class public final Lcom/cgollner/systemmonitor/lib/R$id;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final _toolbar:I = 0x7f0c0197

.field public static final action_bar:I = 0x7f0c0040

.field public static final action_bar_activity_content:I = 0x7f0c0000

.field public static final action_bar_container:I = 0x7f0c003f

.field public static final action_bar_root:I = 0x7f0c003b

.field public static final action_bar_spinner:I = 0x7f0c0001

.field public static final action_bar_subtitle:I = 0x7f0c002e

.field public static final action_bar_title:I = 0x7f0c002d

.field public static final action_context_bar:I = 0x7f0c0041

.field public static final action_menu_divider:I = 0x7f0c0002

.field public static final action_menu_presenter:I = 0x7f0c0003

.field public static final action_mode_bar:I = 0x7f0c003d

.field public static final action_mode_bar_stub:I = 0x7f0c003c

.field public static final action_mode_close_button:I = 0x7f0c002f

.field public static final activity_chooser_view_content:I = 0x7f0c0030

.field public static final always:I = 0x7f0c0025

.field public static final askRootPermissions:I = 0x7f0c008b

.field public static final askRootStart:I = 0x7f0c008c

.field public static final back:I = 0x7f0c007b

.field public static final batteryHistoryView:I = 0x7f0c0079

.field public static final batteryStats:I = 0x7f0c007e

.field public static final batteryView:I = 0x7f0c0064

.field public static final batteryWidgetImage:I = 0x7f0c0089

.field public static final beginning:I = 0x7f0c0020

.field public static final buttonCPU:I = 0x7f0c0199

.field public static final buttonCPUTIME:I = 0x7f0c019b

.field public static final buttonName:I = 0x7f0c0198

.field public static final buttonRAM:I = 0x7f0c019a

.field public static final cacheItemLayout:I = 0x7f0c0091

.field public static final cacheNumApps:I = 0x7f0c008d

.field public static final cacheTotalSize:I = 0x7f0c008e

.field public static final checkbox:I = 0x7f0c0038

.field public static final choose:I = 0x7f0c016d

.field public static final circleWidthLayout:I = 0x7f0c006c

.field public static final circleWidthSeekBar:I = 0x7f0c006e

.field public static final circleWidthText:I = 0x7f0c006d

.field public static final collapseActionView:I = 0x7f0c0026

.field public static final colorInnerCircle:I = 0x7f0c0070

.field public static final colorInnerCircleBg:I = 0x7f0c0072

.field public static final colorSquare:I = 0x7f0c006b

.field public static final color_picker_view:I = 0x7f0c00c3

.field public static final contentFrame:I = 0x7f0c0050

.field public static final cpu0:I = 0x7f0c00ba

.field public static final cpuStatTitle0:I = 0x7f0c0146

.field public static final cpuStatTitle1:I = 0x7f0c0149

.field public static final cpuStatTitle2:I = 0x7f0c014c

.field public static final cpuStatTitle3:I = 0x7f0c014f

.field public static final cpuStatVal0:I = 0x7f0c0147

.field public static final cpuStatVal1:I = 0x7f0c014a

.field public static final cpuStatVal2:I = 0x7f0c014d

.field public static final cpuStatVal3:I = 0x7f0c0150

.field public static final cpuTempAvgTitle:I = 0x7f0c00c1

.field public static final cpuTempAvgValue:I = 0x7f0c00c2

.field public static final cpuTempCurrentTitle:I = 0x7f0c00bb

.field public static final cpuTempCurrentValue:I = 0x7f0c00bc

.field public static final cpuTempMaxTitle:I = 0x7f0c00bf

.field public static final cpuTempMaxValue:I = 0x7f0c00c0

.field public static final cpuTempMinTitle:I = 0x7f0c00bd

.field public static final cpuTempMinValue:I = 0x7f0c00be

.field public static final cpuTitle0:I = 0x7f0c0145

.field public static final cpuTitle1:I = 0x7f0c0148

.field public static final cpuTitle2:I = 0x7f0c014b

.field public static final cpuTitle3:I = 0x7f0c014e

.field public static final decor_content_parent:I = 0x7f0c003e

.field public static final decrement:I = 0x7f0c0187

.field public static final default_activity_button:I = 0x7f0c0033

.field public static final delete:I = 0x7f0c0093

.field public static final deleteAll:I = 0x7f0c0090

.field public static final deleteAllSeparator:I = 0x7f0c008f

.field public static final dialog:I = 0x7f0c002a

.field public static final disableHome:I = 0x7f0c0019

.field public static final display_all_cores:I = 0x7f0c01ad

.field public static final drawerContent:I = 0x7f0c0102

.field public static final drawerFragment:I = 0x7f0c016f

.field public static final drawer_layout:I = 0x7f0c016e

.field public static final dropdown:I = 0x7f0c002b

.field public static final edit_query:I = 0x7f0c0042

.field public static final empty:I = 0x7f0c008a

.field public static final end:I = 0x7f0c0021

.field public static final expand_activities_button:I = 0x7f0c0031

.field public static final expanded_menu:I = 0x7f0c0037

.field public static final forward:I = 0x7f0c007a

.field public static final freeRamTitle:I = 0x7f0c0189

.field public static final freeRamValue:I = 0x7f0c018a

.field public static final freeStorage:I = 0x7f0c0194

.field public static final freqAvg:I = 0x7f0c00ab

.field public static final freqLast:I = 0x7f0c00ae

.field public static final freqMax:I = 0x7f0c00ac

.field public static final freqMin:I = 0x7f0c00ad

.field public static final freqsFileNotFound:I = 0x7f0c00b8

.field public static final home:I = 0x7f0c0004

.field public static final homeAsUp:I = 0x7f0c001a

.field public static final icon:I = 0x7f0c0035

.field public static final iconLayout:I = 0x7f0c00fe

.field public static final ifRoom:I = 0x7f0c0027

.field public static final image:I = 0x7f0c0032

.field public static final increment:I = 0x7f0c0185

.field public static final infoTextView:I = 0x7f0c0190

.field public static final innerCircleBgColorLayout:I = 0x7f0c0071

.field public static final innerCircleColorLayout:I = 0x7f0c006f

.field public static final itemLayout:I = 0x7f0c00fd

.field public static final left_drawer:I = 0x7f0c0101

.field public static final listMode:I = 0x7f0c0016

.field public static final list_item:I = 0x7f0c0034

.field public static final mainLayout:I = 0x7f0c0063

.field public static final maxSpeedTitle:I = 0x7f0c0143

.field public static final maxSpeedValue:I = 0x7f0c0144

.field public static final menu_accept:I = 0x7f0c01a0

.field public static final menu_settings:I = 0x7f0c01a1

.field public static final menu_show_all_processes:I = 0x7f0c01b4

.field public static final menu_storage_folder:I = 0x7f0c01b1

.field public static final middle:I = 0x7f0c0022

.field public static final minSpeedTitle:I = 0x7f0c0140

.field public static final minSpeedValue:I = 0x7f0c0141

.field public static final minus:I = 0x7f0c007d

.field public static final mobileRecvAvg:I = 0x7f0c0177

.field public static final mobileRecvLast:I = 0x7f0c0179

.field public static final mobileRecvMax:I = 0x7f0c0178

.field public static final mobileRecvTotal:I = 0x7f0c017a

.field public static final mobileSendAvg:I = 0x7f0c017f

.field public static final mobileSendLast:I = 0x7f0c0181

.field public static final mobileSendMax:I = 0x7f0c0180

.field public static final mobileSendTotal:I = 0x7f0c0182

.field public static final monitorview:I = 0x7f0c00a6

.field public static final monitorview2:I = 0x7f0c00b1

.field public static final monitorview3:I = 0x7f0c00af

.field public static final monitorview4:I = 0x7f0c00b2

.field public static final monitorviewMobile:I = 0x7f0c0172

.field public static final monitorviewWifi:I = 0x7f0c0171

.field public static final name:I = 0x7f0c015b

.field public static final never:I = 0x7f0c0028

.field public static final newFolder:I = 0x7f0c01ac

.field public static final new_color_panel:I = 0x7f0c00c5

.field public static final nextButton:I = 0x7f0c0065

.field public static final none:I = 0x7f0c001b

.field public static final normal:I = 0x7f0c0017

.field public static final num_picker:I = 0x7f0c00f3

.field public static final numpicker_input:I = 0x7f0c0186

.field public static final old_color_panel:I = 0x7f0c00c4

.field public static final outerCircleWidthLayout:I = 0x7f0c0073

.field public static final outerCircleWidthSeekBar:I = 0x7f0c0075

.field public static final outerCircleWidthText:I = 0x7f0c0074

.field public static final pager_title_strip:I = 0x7f0c0056

.field public static final percentageCircleWidthLayout:I = 0x7f0c0076

.field public static final percentageCircleWidthSeekBar:I = 0x7f0c0078

.field public static final percentageCircleWidthText:I = 0x7f0c0077

.field public static final percentageLayout:I = 0x7f0c0067

.field public static final percentageSeekBar:I = 0x7f0c0069

.field public static final percentageText:I = 0x7f0c0068

.field public static final pie:I = 0x7f0c00b9

.field public static final plus:I = 0x7f0c007c

.field public static final pref_num_picker:I = 0x7f0c0188

.field public static final prevButton:I = 0x7f0c0066

.field public static final progressBar:I = 0x7f0c0192

.field public static final progress_circular:I = 0x7f0c0005

.field public static final progress_horizontal:I = 0x7f0c0006

.field public static final quadCoreLayout:I = 0x7f0c00b0

.field public static final radio:I = 0x7f0c003a

.field public static final readAvg:I = 0x7f0c015e

.field public static final readLast:I = 0x7f0c0161

.field public static final readMax:I = 0x7f0c015f

.field public static final readMin:I = 0x7f0c0160

.field public static final readSpeedTitle:I = 0x7f0c0169

.field public static final readSpeedValue:I = 0x7f0c016a

.field public static final replay:I = 0x7f0c01a3

.field public static final resetFrequencies:I = 0x7f0c01ae

.field public static final resetStats:I = 0x7f0c01a9

.field public static final save:I = 0x7f0c01ab

.field public static final scroll:I = 0x7f0c0195

.field public static final search_badge:I = 0x7f0c0044

.field public static final search_bar:I = 0x7f0c0043

.field public static final search_button:I = 0x7f0c0045

.field public static final search_close_btn:I = 0x7f0c004a

.field public static final search_edit_frame:I = 0x7f0c0046

.field public static final search_go_btn:I = 0x7f0c004c

.field public static final search_mag_icon:I = 0x7f0c0047

.field public static final search_plate:I = 0x7f0c0048

.field public static final search_src_text:I = 0x7f0c0049

.field public static final search_voice_btn:I = 0x7f0c004d

.field public static final seekBar:I = 0x7f0c018f

.field public static final separator:I = 0x7f0c0092

.field public static final shadow:I = 0x7f0c0170

.field public static final shortcut:I = 0x7f0c0039

.field public static final showCustom:I = 0x7f0c001c

.field public static final showHome:I = 0x7f0c001d

.field public static final showTitle:I = 0x7f0c001e

.field public static final speedTitle:I = 0x7f0c013d

.field public static final speedValue:I = 0x7f0c013e

.field public static final split_action_bar:I = 0x7f0c0007

.field public static final statLayout1:I = 0x7f0c013a

.field public static final statLayout2:I = 0x7f0c013c

.field public static final statLayout3:I = 0x7f0c013f

.field public static final statLayout4:I = 0x7f0c0142

.field public static final statTitle0:I = 0x7f0c0152

.field public static final statTitle1:I = 0x7f0c0154

.field public static final statTitle2:I = 0x7f0c0156

.field public static final statTitle3:I = 0x7f0c0158

.field public static final statValue0:I = 0x7f0c0153

.field public static final statValue1:I = 0x7f0c0155

.field public static final statValue2:I = 0x7f0c0157

.field public static final statValue3:I = 0x7f0c0159

.field public static final stats:I = 0x7f0c0193

.field public static final stats_layout:I = 0x7f0c00b3

.field public static final stats_layout_cpu:I = 0x7f0c00b4

.field public static final stop_start:I = 0x7f0c01aa

.field public static final submit_area:I = 0x7f0c004b

.field public static final tabMode:I = 0x7f0c0018

.field public static final tab_apps_cache:I = 0x7f0c0008

.field public static final tab_battery_history:I = 0x7f0c0009

.field public static final tab_battery_stats:I = 0x7f0c000a

.field public static final tab_battery_temperature:I = 0x7f0c000b

.field public static final tab_cpu:I = 0x7f0c000c

.field public static final tab_cpu_freqs:I = 0x7f0c000d

.field public static final tab_cpu_temperature:I = 0x7f0c000e

.field public static final tab_gpu:I = 0x7f0c000f

.field public static final tab_io:I = 0x7f0c0010

.field public static final tab_net:I = 0x7f0c0011

.field public static final tab_ram:I = 0x7f0c0012

.field public static final tab_storage_stats:I = 0x7f0c0013

.field public static final tab_top_apps:I = 0x7f0c0014

.field public static final text:I = 0x7f0c00ff

.field public static final text2:I = 0x7f0c0100

.field public static final textColorLayout:I = 0x7f0c006a

.field public static final textViewCycleRateHour:I = 0x7f0c0082

.field public static final textViewCycleRateTitle:I = 0x7f0c0081

.field public static final textViewCycleSpeed:I = 0x7f0c0080

.field public static final textViewCycleSpeedTitle:I = 0x7f0c007f

.field public static final textViewStatsChargeRateHour:I = 0x7f0c0084

.field public static final textViewStatsChargeSpeed:I = 0x7f0c0083

.field public static final textViewStatsDisChargeRateHour:I = 0x7f0c0087

.field public static final textViewStatsDisChargeSpeed:I = 0x7f0c0086

.field public static final textViewStatsTimeToCharge:I = 0x7f0c0085

.field public static final textViewStatsTimeToDisCharge:I = 0x7f0c0088

.field public static final throughputTitle:I = 0x7f0c0183

.field public static final throughputValue:I = 0x7f0c0184

.field public static final timePickerEnd:I = 0x7f0c015d

.field public static final timePickerStart:I = 0x7f0c015c

.field public static final title:I = 0x7f0c0036

.field public static final topAppIcon:I = 0x7f0c005c

.field public static final topAppName:I = 0x7f0c005d

.field public static final topAppProcessName:I = 0x7f0c005e

.field public static final topAppUsageCpu:I = 0x7f0c005f

.field public static final topAppUsageNet:I = 0x7f0c0061

.field public static final topAppUsageRam:I = 0x7f0c0060

.field public static final totalRamTitle:I = 0x7f0c018d

.field public static final totalRamValue:I = 0x7f0c018e

.field public static final totalRead:I = 0x7f0c0166

.field public static final totalTotal:I = 0x7f0c0168

.field public static final totalWrite:I = 0x7f0c0167

.field public static final tvFreq:I = 0x7f0c00b5

.field public static final tvFreqPercentage:I = 0x7f0c00b7

.field public static final tvFreqTime:I = 0x7f0c00b6

.field public static final up:I = 0x7f0c0015

.field public static final usageAvg:I = 0x7f0c00a7

.field public static final usageLast:I = 0x7f0c00aa

.field public static final usageMax:I = 0x7f0c00a8

.field public static final usageMin:I = 0x7f0c00a9

.field public static final useLogo:I = 0x7f0c001f

.field public static final usedRamTitle:I = 0x7f0c018b

.field public static final usedRamValue:I = 0x7f0c018c

.field public static final utilizationTitle:I = 0x7f0c013b

.field public static final viewPager:I = 0x7f0c0055

.field public static final widgetConfigFragment:I = 0x7f0c019c

.field public static final wifiRecvAvg:I = 0x7f0c0173

.field public static final wifiRecvLast:I = 0x7f0c0175

.field public static final wifiRecvMax:I = 0x7f0c0174

.field public static final wifiRecvTotal:I = 0x7f0c0176

.field public static final wifiSendAvg:I = 0x7f0c017b

.field public static final wifiSendLast:I = 0x7f0c017d

.field public static final wifiSendMax:I = 0x7f0c017c

.field public static final wifiSendTotal:I = 0x7f0c017e

.field public static final withText:I = 0x7f0c0029

.field public static final wrap_content:I = 0x7f0c002c

.field public static final writeAvg:I = 0x7f0c0162

.field public static final writeLast:I = 0x7f0c0165

.field public static final writeMax:I = 0x7f0c0163

.field public static final writeMin:I = 0x7f0c0164

.field public static final writeSpeedTitle:I = 0x7f0c016b

.field public static final writeSpeedValue:I = 0x7f0c016c
