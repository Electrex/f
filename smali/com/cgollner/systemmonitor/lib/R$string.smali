.class public final Lcom/cgollner/systemmonitor/lib/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final No:I = 0x7f070001

.field public static final abc_action_bar_home_description:I = 0x7f070002

.field public static final abc_action_bar_home_description_format:I = 0x7f070003

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070004

.field public static final abc_action_bar_up_description:I = 0x7f070005

.field public static final abc_action_menu_overflow_description:I = 0x7f070006

.field public static final abc_action_mode_done:I = 0x7f070007

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070008

.field public static final abc_activitychooserview_choose_application:I = 0x7f070009

.field public static final abc_searchview_description_clear:I = 0x7f07000a

.field public static final abc_searchview_description_query:I = 0x7f07000b

.field public static final abc_searchview_description_search:I = 0x7f07000c

.field public static final abc_searchview_description_submit:I = 0x7f07000d

.field public static final abc_searchview_description_voice:I = 0x7f07000e

.field public static final abc_shareactionprovider_share_with:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070010

.field public static final abc_toolbar_collapse_description:I = 0x7f070011

.field public static final action_open_app:I = 0x7f070015

.field public static final action_open_settings:I = 0x7f070016

.field public static final all_apps:I = 0x7f070019

.field public static final allow:I = 0x7f07001a

.field public static final and:I = 0x7f07001b

.field public static final app:I = 0x7f07001c

.field public static final app_description:I = 0x7f07001d

.field public static final app_description_lite:I = 0x7f07001e

.field public static final app_name:I = 0x7f070021

.field public static final app_name_lite:I = 0x7f070023

.field public static final application_error:I = 0x7f070026

.field public static final apps:I = 0x7f07002b

.field public static final apps_monitor:I = 0x7f07002c

.field public static final are_you_sure_delete:I = 0x7f07002d

.field public static final are_you_sure_message:I = 0x7f07002e

.field public static final are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I = 0x7f07002f

.field public static final availableRam:I = 0x7f07003c

.field public static final background_history_summay:I = 0x7f07003d

.field public static final background_history_title:I = 0x7f07003e

.field public static final background_history_title_key:I = 0x7f07003f

.field public static final battery_calculation_strategy:I = 0x7f070051

.field public static final battery_charged_at:I = 0x7f070052

.field public static final battery_charged_in:I = 0x7f070053

.field public static final battery_clear_stats:I = 0x7f070054

.field public static final battery_clear_stats_key:I = 0x7f070055

.field public static final battery_clear_stats_message:I = 0x7f070056

.field public static final battery_clear_stats_summary:I = 0x7f070057

.field public static final battery_default_visible_range:I = 0x7f070058

.field public static final battery_empty_at:I = 0x7f070059

.field public static final battery_empty_in:I = 0x7f07005a

.field public static final battery_enable_history:I = 0x7f07005b

.field public static final battery_extension_description:I = 0x7f07005c

.field public static final battery_health:I = 0x7f07005d

.field public static final battery_history:I = 0x7f07005e

.field public static final battery_history_key:I = 0x7f07005f

.field public static final battery_history_size:I = 0x7f070060

.field public static final battery_history_size_key:I = 0x7f070061

.field public static final battery_percentage:I = 0x7f070062

.field public static final battery_service:I = 0x7f070064

.field public static final battery_settings_summary:I = 0x7f070066

.field public static final battery_stats:I = 0x7f070067

.field public static final battery_strategy_cycle:I = 0x7f070068

.field public static final battery_strategy_key:I = 0x7f070069

.field public static final battery_strategy_stats:I = 0x7f07006a

.field public static final battery_strategy_stats_and_cycle:I = 0x7f07006b

.field public static final battery_temperature:I = 0x7f07006c

.field public static final battery_time_to_empty:I = 0x7f07006d

.field public static final battery_time_to_full:I = 0x7f07006e

.field public static final battery_title:I = 0x7f07006f

.field public static final battery_visible_range_key:I = 0x7f070070

.field public static final battery_voltage:I = 0x7f070071

.field public static final batteryhistoryclearkey:I = 0x7f070072

.field public static final buy_button:I = 0x7f07007f

.field public static final cache_stats:I = 0x7f070080

.field public static final charge_rate_per_hour:I = 0x7f070088

.field public static final charge_speed_1_:I = 0x7f070089

.field public static final charge_statistics:I = 0x7f07008a

.field public static final check_license:I = 0x7f07008b

.field public static final checking_license:I = 0x7f07008c

.field public static final choose_current_folder:I = 0x7f07008f

.field public static final clear_battery_history:I = 0x7f070092

.field public static final clear_cache:I = 0x7f070093

.field public static final clearing_cache_:I = 0x7f070094

.field public static final cpuFrequency:I = 0x7f0700bb

.field public static final cpuUsage:I = 0x7f0700bc

.field public static final cpu_frequencies:I = 0x7f0700bf

.field public static final cpu_temperature:I = 0x7f0700c8

.field public static final cpu_title:I = 0x7f0700cc

.field public static final current:I = 0x7f0700cf

.field public static final current_cycle:I = 0x7f0700d0

.field public static final dashclock_desc:I = 0x7f0700d3

.field public static final day:I = 0x7f0700d4

.field public static final days:I = 0x7f0700d5

.field public static final delete:I = 0x7f0700d6

.field public static final dialog_cancel:I = 0x7f0700db

.field public static final dialog_color_picker:I = 0x7f0700dc

.field public static final dialog_picker_title:I = 0x7f0700dd

.field public static final dialog_set_number:I = 0x7f0700de

.field public static final discharge_rate_per_hour:I = 0x7f0700e6

.field public static final discharge_speed_1_:I = 0x7f0700e7

.field public static final discharge_statistics:I = 0x7f0700e8

.field public static final display_all_cores:I = 0x7f0700e9

.field public static final dont_allow:I = 0x7f0700ef

.field public static final drawer_close:I = 0x7f0700f9

.field public static final drawer_open:I = 0x7f0700fa

.field public static final dynamic:I = 0x7f0700fb

.field public static final email_key:I = 0x7f0700fd

.field public static final empty:I = 0x7f0700fe

.field public static final extension_cpu_description:I = 0x7f070103

.field public static final extension_io_description:I = 0x7f070104

.field public static final extension_net_description:I = 0x7f070105

.field public static final extension_ram_description:I = 0x7f070106

.field public static final features:I = 0x7f070109

.field public static final flying_monitor_is_minimized:I = 0x7f070118

.field public static final folder:I = 0x7f070121

.field public static final friday:I = 0x7f070123

.field public static final full_version_details:I = 0x7f070127

.field public static final full_version_feature_message:I = 0x7f070128

.field public static final full_version_feature_title:I = 0x7f070129

.field public static final get_full:I = 0x7f070131

.field public static final gpu:I = 0x7f070134

.field public static final graph_zoom:I = 0x7f07013b

.field public static final history:I = 0x7f070147

.field public static final history_bg_click_to_schedule:I = 0x7f070148

.field public static final history_bg_components:I = 0x7f070149

.field public static final history_bg_configure:I = 0x7f07014a

.field public static final history_bg_cpu_key:I = 0x7f07014b

.field public static final history_bg_dialog_choose_action:I = 0x7f07014c

.field public static final history_bg_frquency_title:I = 0x7f07014d

.field public static final history_bg_history_click:I = 0x7f07014e

.field public static final history_bg_history_enter_name:I = 0x7f07014f

.field public static final history_bg_io_key:I = 0x7f070150

.field public static final history_bg_last:I = 0x7f070151

.field public static final history_bg_mobile_recv_title:I = 0x7f070152

.field public static final history_bg_mobile_send_title:I = 0x7f070153

.field public static final history_bg_net_key:I = 0x7f070154

.field public static final history_bg_not_running:I = 0x7f070155

.field public static final history_bg_not_scheduled:I = 0x7f070156

.field public static final history_bg_ram_key:I = 0x7f070157

.field public static final history_bg_readspeed_title:I = 0x7f070158

.field public static final history_bg_restart:I = 0x7f070159

.field public static final history_bg_running:I = 0x7f07015a

.field public static final history_bg_save_history:I = 0x7f07015b

.field public static final history_bg_save_history_desc:I = 0x7f07015c

.field public static final history_bg_save_menu:I = 0x7f07015d

.field public static final history_bg_saved_history:I = 0x7f07015e

.field public static final history_bg_schedule:I = 0x7f07015f

.field public static final history_bg_schedule_end:I = 0x7f070160

.field public static final history_bg_schedule_name:I = 0x7f070161

.field public static final history_bg_schedule_start:I = 0x7f070162

.field public static final history_bg_scheduled:I = 0x7f070163

.field public static final history_bg_scheduled_description:I = 0x7f070164

.field public static final history_bg_scheduled_key:I = 0x7f070165

.field public static final history_bg_scheduled_monitoring:I = 0x7f070166

.field public static final history_bg_see_report:I = 0x7f070167

.field public static final history_bg_service:I = 0x7f070168

.field public static final history_bg_service_is_finished:I = 0x7f070169

.field public static final history_bg_service_is_running:I = 0x7f07016a

.field public static final history_bg_service_is_stopped:I = 0x7f07016b

.field public static final history_bg_service_see_progress:I = 0x7f07016c

.field public static final history_bg_service_see_report:I = 0x7f07016d

.field public static final history_bg_status:I = 0x7f07016e

.field public static final history_bg_status_key:I = 0x7f07016f

.field public static final history_bg_stop_start:I = 0x7f070170

.field public static final history_bg_total_title:I = 0x7f070171

.field public static final history_bg_update_interval_key:I = 0x7f070172

.field public static final history_bg_usage_title:I = 0x7f070173

.field public static final history_bg_wifi_receive_title:I = 0x7f070174

.field public static final history_bg_wifi_send:I = 0x7f070175

.field public static final history_bg_writespeed_title:I = 0x7f070176

.field public static final holo_blue:I = 0x7f070177

.field public static final hour:I = 0x7f07017a

.field public static final hours:I = 0x7f07017b

.field public static final hwCode:I = 0x7f07017d

.field public static final io_title:I = 0x7f070182

.field public static final live_monitor:I = 0x7f070196

.field public static final look_and_feel:I = 0x7f07019b

.field public static final max:I = 0x7f0701a1

.field public static final max_speed:I = 0x7f0701a6

.field public static final menu_settings:I = 0x7f0701ab

.field public static final menu_show_all_processes:I = 0x7f0701ac

.field public static final min:I = 0x7f0701b0

.field public static final min_speed:I = 0x7f0701b3

.field public static final minute:I = 0x7f0701b6

.field public static final minutes:I = 0x7f0701b7

.field public static final monday:I = 0x7f0701b9

.field public static final net:I = 0x7f0701cc

.field public static final network_title:I = 0x7f0701cd

.field public static final notif_graph_color:I = 0x7f0701e4

.field public static final notif_icon_color:I = 0x7f0701e5

.field public static final notif_priority_battery_key:I = 0x7f0701e6

.field public static final notif_priority_cpu_key:I = 0x7f0701e7

.field public static final notif_priority_def:I = 0x7f0701e8

.field public static final notif_priority_high:I = 0x7f0701e9

.field public static final notif_priority_io_key:I = 0x7f0701ea

.field public static final notif_priority_low:I = 0x7f0701eb

.field public static final notif_priority_max:I = 0x7f0701ec

.field public static final notif_priority_min:I = 0x7f0701ed

.field public static final notif_priority_net_key:I = 0x7f0701ee

.field public static final notif_priority_ram_key:I = 0x7f0701ef

.field public static final notif_priority_title:I = 0x7f0701f0

.field public static final pref_screen_app:I = 0x7f07020b

.field public static final prefscreen_title_floating:I = 0x7f07020c

.field public static final prefscreen_title_floating_full:I = 0x7f07020d

.field public static final press_color_to_apply:I = 0x7f07020e

.field public static final quit_button:I = 0x7f070214

.field public static final ram_title:I = 0x7f070216

.field public static final read:I = 0x7f070217

.field public static final read_speed:I = 0x7f070218

.field public static final receive:I = 0x7f07021a

.field public static final receive_speed:I = 0x7f07021b

.field public static final refresh:I = 0x7f07021c

.field public static final reset:I = 0x7f07021e

.field public static final reset_all_statistics:I = 0x7f07021f

.field public static final reset_battery_statistics:I = 0x7f070220

.field public static final reset_charging_statistics:I = 0x7f070221

.field public static final reset_discharging_statistics:I = 0x7f070222

.field public static final reset_timers:I = 0x7f070223

.field public static final retry_button:I = 0x7f070224

.field public static final saturday:I = 0x7f07022b

.field public static final saved_history_key:I = 0x7f07022c

.field public static final second:I = 0x7f07022e

.field public static final seconds:I = 0x7f07022f

.field public static final send:I = 0x7f070230

.field public static final send_speed:I = 0x7f070231

.field public static final settings:I = 0x7f070238

.field public static final settings_about:I = 0x7f070239

.field public static final settings_actions:I = 0x7f07023a

.field public static final settings_app_cpu_updatefreq_key:I = 0x7f07023b

.field public static final settings_app_io_updatefreq_key:I = 0x7f07023c

.field public static final settings_app_net_updatefreq_key:I = 0x7f07023d

.field public static final settings_app_ram_updatefreq_key:I = 0x7f07023e

.field public static final settings_app_screen_summary:I = 0x7f07023f

.field public static final settings_app_topapps_updatefreq_key:I = 0x7f070240

.field public static final settings_contact:I = 0x7f070241

.field public static final settings_extension_all_cores:I = 0x7f070242

.field public static final settings_extension_all_cores_key:I = 0x7f070243

.field public static final settings_extension_cpu_updatefreq_key:I = 0x7f070244

.field public static final settings_extension_io_updatefreq_key:I = 0x7f070245

.field public static final settings_extension_net_updatefreq_key:I = 0x7f070246

.field public static final settings_extension_ram_updatefreq_key:I = 0x7f070247

.field public static final settings_floating_cpu_cat:I = 0x7f070248

.field public static final settings_floating_cpu_height_key:I = 0x7f070249

.field public static final settings_floating_cpu_key:I = 0x7f07024a

.field public static final settings_floating_cpu_showgraph_key:I = 0x7f07024b

.field public static final settings_floating_cpu_updatefreq_key:I = 0x7f07024c

.field public static final settings_floating_cpu_updatefreq_key_new:I = 0x7f07024d

.field public static final settings_floating_cpu_width_key:I = 0x7f07024e

.field public static final settings_floating_display_all_cores_key:I = 0x7f07024f

.field public static final settings_floating_height_key:I = 0x7f070250

.field public static final settings_floating_io_cat:I = 0x7f070251

.field public static final settings_floating_io_height_key:I = 0x7f070252

.field public static final settings_floating_io_key:I = 0x7f070253

.field public static final settings_floating_io_showgraph_key:I = 0x7f070254

.field public static final settings_floating_io_updatefreq_key:I = 0x7f070255

.field public static final settings_floating_io_width_key:I = 0x7f070256

.field public static final settings_floating_monitor:I = 0x7f070257

.field public static final settings_floating_net_cat:I = 0x7f070258

.field public static final settings_floating_net_height_key:I = 0x7f070259

.field public static final settings_floating_net_key:I = 0x7f07025a

.field public static final settings_floating_net_showgraph_key:I = 0x7f07025b

.field public static final settings_floating_net_updatefreq_key:I = 0x7f07025c

.field public static final settings_floating_net_width_key:I = 0x7f07025d

.field public static final settings_floating_ram_cat:I = 0x7f07025e

.field public static final settings_floating_ram_height_key:I = 0x7f07025f

.field public static final settings_floating_ram_key:I = 0x7f070260

.field public static final settings_floating_ram_showgraph_key:I = 0x7f070261

.field public static final settings_floating_ram_updatefreq_key:I = 0x7f070262

.field public static final settings_floating_ram_width_key:I = 0x7f070263

.field public static final settings_floating_screen_summary:I = 0x7f070264

.field public static final settings_floating_showgraph:I = 0x7f070265

.field public static final settings_floating_top_apps_updatefreq_key:I = 0x7f070266

.field public static final settings_floating_topapps_key:I = 0x7f070267

.field public static final settings_floating_transparency:I = 0x7f070268

.field public static final settings_floating_ui_category:I = 0x7f070269

.field public static final settings_floating_width_key:I = 0x7f07026a

.field public static final settings_follow_me:I = 0x7f07026b

.field public static final settings_help_translating:I = 0x7f07026c

.field public static final settings_notification_display_all_cores_key:I = 0x7f07026d

.field public static final settings_notification_net_category:I = 0x7f07026e

.field public static final settings_notification_screen_summary:I = 0x7f07026f

.field public static final settings_notifications_battery_key:I = 0x7f070270

.field public static final settings_notifications_click_action:I = 0x7f070271

.field public static final settings_notifications_cpu_category:I = 0x7f070272

.field public static final settings_notifications_cpu_key:I = 0x7f070273

.field public static final settings_notifications_cpu_updatefreq:I = 0x7f070274

.field public static final settings_notifications_cpu_updatefreq_key:I = 0x7f070275

.field public static final settings_notifications_io_category:I = 0x7f070276

.field public static final settings_notifications_io_key:I = 0x7f070277

.field public static final settings_notifications_io_updatefreq_key:I = 0x7f070278

.field public static final settings_notifications_net_key:I = 0x7f070279

.field public static final settings_notifications_net_updatefreq_key:I = 0x7f07027a

.field public static final settings_notifications_ram_category:I = 0x7f07027b

.field public static final settings_notifications_ram_key:I = 0x7f07027c

.field public static final settings_notifications_ram_updatefreq_key:I = 0x7f07027d

.field public static final settings_notifications_show:I = 0x7f07027e

.field public static final settings_notifications_show_expanded:I = 0x7f07027f

.field public static final settings_notifications_show_expanded_battery_key:I = 0x7f070280

.field public static final settings_notifications_show_expanded_cpu_key:I = 0x7f070281

.field public static final settings_notifications_show_expanded_io_key:I = 0x7f070282

.field public static final settings_notifications_show_expanded_net_key:I = 0x7f070283

.field public static final settings_notifications_show_expanded_ram_key:I = 0x7f070284

.field public static final settings_notifications_title:I = 0x7f070285

.field public static final settings_notifications_title_full:I = 0x7f070286

.field public static final settings_on_click_action:I = 0x7f070287

.field public static final settings_others:I = 0x7f070288

.field public static final settings_theme_cat:I = 0x7f070289

.field public static final settings_theme_choose:I = 0x7f07028a

.field public static final settings_theme_choose_key:I = 0x7f07028b

.field public static final settings_translations:I = 0x7f07028d

.field public static final statistics_average:I = 0x7f07029a

.field public static final statistics_mode:I = 0x7f07029b

.field public static final storage_stats:I = 0x7f07029c

.field public static final sunday:I = 0x7f07029d

.field public static final system_monitor:I = 0x7f0702a1

.field public static final system_monitor_battery:I = 0x7f0702a2

.field public static final system_stats:I = 0x7f0702a5

.field public static final tap_to_delete_battery_history:I = 0x7f0702a6

.field public static final tap_to_restore:I = 0x7f0702a7

.field public static final temperature_units_key:I = 0x7f0702ac

.field public static final through:I = 0x7f0702b3

.field public static final throughput:I = 0x7f0702b4

.field public static final thursday:I = 0x7f0702b5

.field public static final time_to_fully_charge:I = 0x7f0702b6

.field public static final time_to_fully_discharge:I = 0x7f0702b7

.field public static final today:I = 0x7f0702b9

.field public static final tomorrow:I = 0x7f0702ba

.field public static final top_apps_app_name:I = 0x7f0702bb

.field public static final top_apps_cpu_time:I = 0x7f0702bc

.field public static final top_apps_title:I = 0x7f0702bd

.field public static final total:I = 0x7f0702be

.field public static final totalRam:I = 0x7f0702bf

.field public static final tuesday:I = 0x7f0702c0

.field public static final unlicensed_dialog_body:I = 0x7f0702c8

.field public static final unlicensed_dialog_retry_body:I = 0x7f0702c9

.field public static final unlicensed_dialog_title:I = 0x7f0702ca

.field public static final usedRam:I = 0x7f0702ce

.field public static final wednesday:I = 0x7f0702d9

.field public static final whats_new:I = 0x7f0702da

.field public static final white:I = 0x7f0702db

.field public static final widget_configuration:I = 0x7f0702dc

.field public static final widget_name_cpu:I = 0x7f0702dd

.field public static final widget_name_io:I = 0x7f0702de

.field public static final widget_name_network:I = 0x7f0702df

.field public static final widget_name_ram:I = 0x7f0702e0

.field public static final widgets:I = 0x7f0702e1

.field public static final widgets_desc:I = 0x7f0702e2

.field public static final write:I = 0x7f0702e7

.field public static final write_speed:I = 0x7f0702e8

.field public static final written:I = 0x7f0702e9

.field public static final x_app_apps_containing_cache:I = 0x7f0702ea

.field public static final x_mb_can_be_cleaned:I = 0x7f0702eb

.field public static final yes:I = 0x7f0702ee

.field public static final yesterday:I = 0x7f0702ef
