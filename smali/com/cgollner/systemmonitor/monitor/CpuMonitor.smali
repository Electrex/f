.class public Lcom/cgollner/systemmonitor/monitor/CpuMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field public a:[F

.field public b:[I

.field public c:[I

.field public d:[I

.field private e:I

.field private j:[[Ljava/lang/String;

.field private k:[Ljava/lang/String;

.field private l:[Ljava/lang/String;


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 21
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    .line 23
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    .line 24
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->j:[[Ljava/lang/String;

    .line 26
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->k:[Ljava/lang/String;

    .line 27
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->l:[Ljava/lang/String;

    .line 29
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    .line 30
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->c:[I

    .line 31
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    .line 33
    if-eqz p5, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->start()V

    .line 36
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    aget v0, v0, p1

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->k:[Ljava/lang/String;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a([Ljava/lang/String;)V

    move v0, v1

    .line 41
    :goto_0
    iget v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    if-gt v0, v2, :cond_0

    .line 42
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->j:[[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->k:[Ljava/lang/String;

    aget-object v3, v3, v0

    aput-object v3, v2, v1

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    aget v0, v0, p1

    int-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 48
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    if-ge v0, v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->h(I)I

    move-result v3

    aput v3, v2, v0

    .line 50
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->c:[I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->f(I)I

    move-result v3

    aput v3, v2, v0

    .line 51
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->g(I)I

    move-result v3

    aput v3, v2, v0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->l:[Ljava/lang/String;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a([Ljava/lang/String;)V

    move v0, v1

    .line 55
    :goto_1
    iget v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->e:I

    if-gt v0, v2, :cond_5

    .line 56
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->j:[[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->l:[Ljava/lang/String;

    aget-object v3, v3, v0

    aput-object v3, v2, v8

    .line 57
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    iget-object v3, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->j:[[Ljava/lang/String;

    aget-object v3, v3, v0

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->j:[[Ljava/lang/String;

    aget-object v4, v4, v0

    aget-object v4, v4, v8

    invoke-static {v3, v4}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(Ljava/lang/String;Ljava/lang/String;)F

    move-result v3

    aput v3, v2, v0

    .line 58
    if-nez v0, :cond_3

    .line 60
    iget-object v5, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    array-length v6, v5

    move v2, v1

    move v4, v1

    :goto_2
    if-ge v2, v6, :cond_1

    aget v3, v5, v2

    .line 61
    add-int/2addr v3, v4

    .line 60
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_2

    .line 64
    :cond_1
    iget-object v5, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    array-length v6, v5

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v6, :cond_2

    aget v7, v5, v2

    .line 65
    add-int/2addr v3, v7

    .line 64
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 67
    :cond_2
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    iget-object v5, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    aget v5, v5, v1

    int-to-float v4, v4

    int-to-float v3, v3

    div-float v3, v4, v3

    mul-float/2addr v3, v5

    aput v3, v2, v1

    .line 55
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :cond_3
    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->e(I)Z

    move-result v2

    .line 71
    if-eqz v2, :cond_4

    .line 72
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    add-int/lit8 v3, v0, -0x1

    const/4 v4, -0x1

    aput v4, v2, v3

    .line 73
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v2, v0

    goto :goto_4

    .line 76
    :cond_4
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    iget-object v3, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    aget v3, v3, v0

    iget-object v4, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b:[I

    add-int/lit8 v5, v0, -0x1

    aget v4, v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    add-int/lit8 v6, v0, -0x1

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    aput v3, v2, v0

    goto :goto_4

    .line 80
    :cond_5
    return-void
.end method

.method public c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->c:[I

    aget v0, v0, p1

    int-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d:[I

    aget v0, v0, p1

    int-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
