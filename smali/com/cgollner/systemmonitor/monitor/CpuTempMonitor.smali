.class public Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/util/List;

.field private j:Landroid/content/Context;


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;ZLandroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 23
    invoke-virtual {p6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->j:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->e:Ljava/util/List;

    .line 25
    if-eqz p5, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->start()V

    .line 28
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 42
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 43
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    div-int v0, v1, v0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->e()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->a:I

    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->e:Ljava/util/List;

    iget v1, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->c:I

    .line 35
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->b:I

    .line 36
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->d:I

    .line 37
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->a:I

    return v0
.end method

.method public d()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 68
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->a:I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->j:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 72
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->c:I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->j:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 76
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->b:I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->j:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 80
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->d:I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->j:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
