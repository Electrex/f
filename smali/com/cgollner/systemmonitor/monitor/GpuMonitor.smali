.class public Lcom/cgollner/systemmonitor/monitor/GpuMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:F


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 13
    if-eqz p5, :cond_0

    .line 14
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->start()V

    .line 16
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a()Lcom/cgollner/systemmonitor/backend/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->c()F

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->d:F

    .line 25
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a()Lcom/cgollner/systemmonitor/backend/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->f()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->b:I

    .line 26
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a()Lcom/cgollner/systemmonitor/backend/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->e()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->c:I

    .line 27
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->a()Lcom/cgollner/systemmonitor/backend/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/GpuUtils;->d()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->a:I

    .line 28
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->d:F

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->a:I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->b:I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->c:I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->d:F

    return v0
.end method
