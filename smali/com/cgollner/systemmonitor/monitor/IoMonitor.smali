.class public Lcom/cgollner/systemmonitor/monitor/IoMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field a:J

.field b:J

.field public c:F

.field public d:J

.field public e:J

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Ljava/util/List;

.field private o:Ljava/util/List;


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 22
    if-eqz p5, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->start()V

    .line 25
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 30
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->j:Ljava/util/List;

    .line 31
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->k:Ljava/util/List;

    .line 32
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->l:Ljava/util/List;

    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->a:J

    .line 34
    return-void
.end method

.method protected b()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x200

    .line 38
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->m:Ljava/util/List;

    .line 39
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->n:Ljava/util/List;

    .line 40
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->o:Ljava/util/List;

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->b:J

    .line 43
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->j:Ljava/util/List;

    iget-wide v1, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->a:J

    iget-object v3, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->m:Ljava/util/List;

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->b:J

    invoke-static/range {v0 .. v5}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(Ljava/util/List;JLjava/util/List;J)D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->c:F

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->k:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->n:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    mul-long/2addr v0, v6

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->d:J

    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->o:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/IoUtils;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    mul-long/2addr v0, v6

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->e:J

    .line 46
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 49
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%.0f%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->c:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->d:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->e:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
