.class public abstract Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private a:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

.field public f:J

.field public g:Z

.field public h:Ljava/lang/Boolean;

.field public i:Z


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 13
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 14
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->f:J

    .line 15
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    .line 16
    iput-boolean v1, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->g:Z

    .line 17
    iput-boolean p3, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->i:Z

    .line 18
    iput-object p4, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->a:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract b()V
.end method

.method public h()V
    .locals 2

    .prologue
    .line 47
    iget-object v1, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    monitor-enter v1

    .line 48
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    .line 49
    monitor-exit v1

    .line 50
    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->a:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    if-eqz v0, :cond_0

    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->a:Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;

    invoke-interface {v0}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;->a()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x64

    .line 23
    :goto_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->a()V

    .line 26
    :try_start_0
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->i:Z

    if-eqz v0, :cond_1

    move-wide v0, v2

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->g:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 29
    :goto_2
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->b()V

    .line 31
    iget-object v1, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    monitor-enter v1

    .line 32
    :try_start_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->i()V

    .line 35
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    const-wide/16 v0, 0x64

    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 26
    :cond_1
    :try_start_3
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;->f:J
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 35
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 44
    :cond_2
    return-void

    .line 28
    :catch_1
    move-exception v0

    goto :goto_2
.end method
