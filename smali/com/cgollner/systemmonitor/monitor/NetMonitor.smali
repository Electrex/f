.class public Lcom/cgollner/systemmonitor/monitor/NetMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field private n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 20
    new-instance v0, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;-><init>(Lcom/cgollner/systemmonitor/monitor/NetMonitor;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    .line 21
    if-eqz p5, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->start()V

    .line 24
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->b:J

    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->a:J

    .line 30
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->d:J

    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->c:J

    .line 32
    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 36
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->a:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->l:J

    .line 37
    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->m:J

    .line 38
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->l:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->m:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->k:J

    .line 40
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->d:J

    .line 41
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->n:Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/monitor/NetMonitor$NetworkStats;->d:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->e:J

    .line 42
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->d:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->j:J

    .line 44
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->l:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->d:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->a:J

    .line 45
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->m:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->b:J

    .line 46
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->a:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->c:J

    .line 47
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->k:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->l:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->m:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
