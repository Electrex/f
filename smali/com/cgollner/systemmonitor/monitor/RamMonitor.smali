.class public Lcom/cgollner/systemmonitor/monitor/RamMonitor;
.super Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:J

.field public c:J

.field public d:J

.field private e:Landroid/content/Context;

.field private j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

.field private k:J

.field private l:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cgollner/systemmonitor/monitor/MonitorAbstract;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;)V

    .line 28
    iput-object p5, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->e:Landroid/content/Context;

    .line 29
    const-string v0, "activity"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->l:Landroid/app/ActivityManager;

    .line 31
    new-instance v0, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    invoke-direct {v0}, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    .line 32
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 33
    iget-object v1, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->l:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 34
    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->k:J

    .line 36
    if-eqz p6, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->start()V

    .line 40
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x0

    const-wide/16 v8, 0x400

    .line 53
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;->a()V

    .line 54
    iget-object v2, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;->c()J

    move-result-wide v2

    iget-object v5, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    invoke-virtual {v5}, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;->d()J

    move-result-wide v6

    add-long/2addr v2, v6

    iget-wide v6, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->k:J

    sub-long/2addr v2, v6

    .line 56
    cmp-long v5, v2, v0

    if-gez v5, :cond_0

    move-wide v2, v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->l:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 60
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 61
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 63
    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v7, 0x190

    if-ne v6, v7, :cond_1

    .line 65
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_2
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 70
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 71
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v6, v1

    move v1, v5

    .line 72
    goto :goto_1

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->l:Landroid/app/ActivityManager;

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 75
    array-length v1, v0

    :goto_2
    if-ge v4, v1, :cond_4

    aget-object v5, v0, v4

    .line 76
    iget v6, v5, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    iget v7, v5, Landroid/os/Debug$MemoryInfo;->nativePss:I

    add-int/2addr v6, v7

    iget v5, v5, Landroid/os/Debug$MemoryInfo;->otherPss:I

    add-int/2addr v5, v6

    .line 78
    mul-int/lit16 v5, v5, 0x400

    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 75
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 81
    :cond_4
    div-long v0, v2, v8

    div-long/2addr v0, v8

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->c:J

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->j:Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/backend/google/MemInfoReader;->b()J

    move-result-wide v0

    div-long/2addr v0, v8

    div-long/2addr v0, v8

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->d:J

    .line 83
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->d:J

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->b:J

    .line 84
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->b:J

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->d:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->a:F

    .line 87
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 95
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%.0f%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->a:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->b:J

    long-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->c:J

    long-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->d:J

    long-to-double v0, v0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
