.class public Lcom/cgollner/systemmonitor/pref/SchedulePreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/TimePicker;

.field private b:Landroid/widget/TimePicker;

.field private c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->a(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->a(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->history_bg_schedule:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->setDialogLayoutResource(I)V

    .line 31
    const v0, 0x104000a

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->setPositiveButtonText(I)V

    .line 32
    const/high16 v0, 0x1040000

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->setNegativeButtonText(I)V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->setDialogIcon(Landroid/graphics/drawable/Drawable;)V

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/pref/SchedulePreference;)V
    .locals 0

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->onClick()V

    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->timePickerStart:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->a:Landroid/widget/TimePicker;

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->timePickerEnd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->b:Landroid/widget/TimePicker;

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->c:Landroid/widget/EditText;

    .line 47
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 48
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0xb

    const/4 v5, 0x0

    const/4 v4, 0x6

    const/4 v6, 0x1

    .line 51
    if-eqz p1, :cond_1

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Name missing"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Please enter a name"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/cgollner/systemmonitor/pref/SchedulePreference$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference$1;-><init>(Lcom/cgollner/systemmonitor/pref/SchedulePreference;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 91
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->a:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 66
    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->a:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 67
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 68
    invoke-virtual {v2, v7, v0}, Ljava/util/Calendar;->set(II)V

    .line 69
    invoke-virtual {v2, v8, v1}, Ljava/util/Calendar;->set(II)V

    .line 70
    const/16 v0, 0xd

    invoke-virtual {v2, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->b:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->b:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 74
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 75
    invoke-virtual {v3, v7, v0}, Ljava/util/Calendar;->set(II)V

    .line 76
    invoke-virtual {v3, v8, v1}, Ljava/util/Calendar;->set(II)V

    .line 77
    const/16 v0, 0xd

    invoke-virtual {v3, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 79
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    invoke-virtual {v2, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 81
    invoke-virtual {v3, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 84
    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    invoke-virtual {v3, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 88
    :cond_4
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;->a(JJLjava/lang/String;Landroid/content/Context;)V

    .line 89
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SchedulePreference;->callChangeListener(Ljava/lang/Object;)Z

    goto :goto_0
.end method
