.class public Lcom/cgollner/systemmonitor/pref/SeekbarPreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/cgollner/systemmonitor/pref/SeekbarPreference$SeekbarPreferenceChangeListener;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a()V

    .line 33
    return-void
.end method

.method private a()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 67
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->seekBar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    .line 69
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->infoTextView:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->b:Landroid/widget/TextView;

    .line 71
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 72
    iget v1, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->d:I

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->d:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->e:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transparency: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 78
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 79
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3

    .prologue
    .line 48
    if-eqz p1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 50
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 52
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    :cond_0
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->c:Lcom/cgollner/systemmonitor/pref/SeekbarPreference$SeekbarPreferenceChangeListener;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/pref/SeekbarPreference;->c:Lcom/cgollner/systemmonitor/pref/SeekbarPreference$SeekbarPreferenceChangeListener;

    invoke-interface {v0, p2, p0}, Lcom/cgollner/systemmonitor/pref/SeekbarPreference$SeekbarPreferenceChangeListener;->a(ILcom/cgollner/systemmonitor/pref/SeekbarPreference;)V

    .line 90
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
