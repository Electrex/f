.class public Lcom/cgollner/systemmonitor/settings/SeekBarPreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected onCreateDialogView()Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, -0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x6

    .line 37
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 39
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 41
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->b:Landroid/widget/TextView;

    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 46
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    new-array v4, v4, [I

    const v5, 0x1010040

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 51
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 55
    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    new-instance v0, Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->d:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->a:Landroid/widget/SeekBar;

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 59
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->a:Landroid/widget/SeekBar;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->g:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->getPersistedInt(I)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    .line 65
    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->a:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->h:I

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->a:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 68
    return-object v1

    .line 65
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDialogClosed(Z)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 74
    if-eqz p1, :cond_0

    .line 75
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 76
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->persistInt(I)Z

    .line 77
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->notifyChanged()V

    .line 79
    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->g:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 90
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 94
    if-eqz p3, :cond_0

    .line 95
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->f:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iput p2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    .line 99
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 100
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->persistInt(I)Z

    .line 101
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->notifyChanged()V

    .line 103
    :cond_0
    return-void

    .line 96
    :cond_1
    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 83
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->g:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->getPersistedInt(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    .line 84
    iget v0, p0, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->i:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SeekBarPreference;->persistInt(I)Z

    .line 85
    return-void

    .line 83
    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method
