.class Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v1, "android.intent.extra.EMAIL"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "christian.goellner88@gmail.com"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v1, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->app_name:I

    invoke-virtual {v2, v3}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;->a:Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->startActivity(Landroid/content/Intent;)V

    .line 51
    return v5
.end method
