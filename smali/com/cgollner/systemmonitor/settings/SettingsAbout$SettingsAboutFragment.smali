.class public Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;
.super Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;-><init>()V

    .line 42
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment$1;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 17
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 57
    sget v0, Lcom/cgollner/systemmonitor/lib/R$xml;->settings_about:I

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 22
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->email_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 25
    const-string v0, "versionKey"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 26
    const-string v1, "System Monitor Lite"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 27
    const-string v1, "1.3.3"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 28
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$drawable;->icon:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 30
    const-string v0, "facebookKey"

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsAbout$SettingsAboutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 31
    if-eqz v6, :cond_0

    .line 32
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 33
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 34
    const/16 v1, 0x7dd

    const/4 v2, 0x4

    const/4 v3, 0x5

    const/16 v4, 0x14

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 35
    invoke-virtual {v7, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "Open contest for you to win 50$ with System Monitor! Visit the Facebook page for more details."

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    return-void
.end method
