.class Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;->a:Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 67
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;->a:Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_clear_stats:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_clear_stats_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->yes:I

    new-instance v2, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2$2;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->No:I

    new-instance v2, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2$1;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 81
    const/4 v0, 0x1

    return v0
.end method
