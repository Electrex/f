.class public Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;
.super Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/preference/Preference$OnPreferenceClickListener;

.field private b:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;-><init>()V

    .line 43
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$1;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 64
    new-instance v0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment$2;-><init>(Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->b:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 32
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 87
    sget v0, Lcom/cgollner/systemmonitor/lib/R$xml;->settings_app_prefs:I

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_clear_stats_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->b:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 40
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->batteryhistoryclearkey:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->a:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 41
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 94
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-super {p0, p1, p2}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 97
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_theme_choose_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {p0, p2}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 99
    const-string v1, ""

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 100
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 101
    const-string v2, "theme"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 104
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 105
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 106
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 107
    const-string v1, "MONITOR_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->startActivity(Landroid/content/Intent;)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_cpu_updatefreq_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 111
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a:I

    goto :goto_0

    .line 112
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_ram_updatefreq_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 114
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->a:I

    goto :goto_0

    .line 115
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_io_updatefreq_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 117
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a:I

    goto :goto_0

    .line 118
    :cond_4
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_net_updatefreq_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 119
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 120
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->a:I

    goto :goto_0

    .line 121
    :cond_5
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_topapps_updatefreq_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 122
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 123
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a:I

    goto :goto_0

    .line 127
    :cond_6
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_strategy_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 128
    invoke-virtual {p0, p2}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 129
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 130
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 131
    :cond_7
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_history_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 132
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 133
    if-eqz v0, :cond_8

    .line 134
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->a(Landroid/app/Activity;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 136
    :cond_8
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/cgollner/systemmonitor/battery/BatteryService;

    invoke-virtual {p0, v0, v1}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->b(Landroid/app/Activity;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 138
    :cond_9
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_history_size_key:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/16 v0, 0x30

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 140
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/settings/SettingsApp$SettingsAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method
