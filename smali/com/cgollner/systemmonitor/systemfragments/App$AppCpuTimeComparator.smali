.class public Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuTimeComparator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/cgollner/systemmonitor/systemfragments/App;Lcom/cgollner/systemmonitor/systemfragments/App;)I
    .locals 4

    .prologue
    .line 43
    iget-wide v0, p1, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    iget-wide v2, p2, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    iget-wide v2, p2, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/cgollner/systemmonitor/systemfragments/App;

    check-cast p2, Lcom/cgollner/systemmonitor/systemfragments/App;

    invoke-virtual {p0, p1, p2}, Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuTimeComparator;->a(Lcom/cgollner/systemmonitor/systemfragments/App;Lcom/cgollner/systemmonitor/systemfragments/App;)I

    move-result v0

    return v0
.end method
