.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

.field final synthetic b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 314
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    :goto_0
    return-void

    .line 316
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->clear_cache:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I

    invoke-virtual {v2, v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    iget-object v5, v5, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
