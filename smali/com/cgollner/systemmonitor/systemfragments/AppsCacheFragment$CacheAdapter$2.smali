.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

.field final synthetic b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5

    .prologue
    .line 349
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 350
    const/4 v0, 0x0

    .line 355
    :goto_0
    return v0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v3, v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$1;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 352
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->notifyDataSetChanged()V

    .line 355
    const/4 v0, 0x1

    goto :goto_0
.end method
