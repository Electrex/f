.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

.field final synthetic b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 361
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 362
    :goto_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 363
    if-eqz v0, :cond_3

    .line 364
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    :goto_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->notifyDataSetChanged()V

    .line 373
    :cond_1
    return-void

    .line 361
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->b:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;->a:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method
