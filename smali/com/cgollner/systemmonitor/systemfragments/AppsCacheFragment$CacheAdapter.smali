.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 277
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 282
    .line 283
    if-nez p2, :cond_0

    .line 284
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$layout;->cache_item:I

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 286
    :cond_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    const v1, 0x1020015

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 288
    const v2, 0x1020006

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 289
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    .line 290
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v4}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 291
    iget-object v5, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 293
    :try_start_0
    iget-object v5, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 294
    invoke-virtual {v5, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    .line 295
    invoke-virtual {v5, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->b:Landroid/graphics/drawable/Drawable;

    .line 296
    iget-object v4, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :goto_0
    iget-object v0, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 306
    iget-object v0, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 309
    :goto_1
    iget-wide v4, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;

    invoke-direct {v1, p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 335
    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->cacheItemLayout:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 336
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 337
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 338
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->separator:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 344
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 345
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;

    invoke-direct {v0, p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 358
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;

    invoke-direct {v0, p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    return-object p2

    .line 298
    :catch_0
    move-exception v4

    .line 299
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 300
    iget-object v4, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 303
    :cond_1
    iget-object v4, v3, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 308
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 340
    :cond_3
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 341
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->separator:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
