.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 153
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v3, v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    sget v4, Lcom/cgollner/systemmonitor/lib/R$string;->clearing_cache_:I

    invoke-virtual {v3, v4}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a:Landroid/app/ProgressDialog;

    .line 154
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;

    iget-object v1, v1, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->f(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Ljava/util/List;Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;)V

    .line 156
    return-void
.end method
