.class Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;


# direct methods
.method private constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$1;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 135
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    if-ne v0, v1, :cond_0

    .line 136
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v4

    .line 165
    :cond_0
    :goto_0
    return v2

    .line 138
    :cond_1
    const-string v0, ""

    move v1, v2

    .line 139
    :goto_1
    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 140
    if-lez v1, :cond_3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_3

    .line 141
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    :cond_2
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    .line 142
    :cond_3
    if-lez v1, :cond_2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_2

    .line 143
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x2

    if-le v0, v5, :cond_4

    const-string v0, ","

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    const-string v0, ""

    goto :goto_3

    .line 147
    :cond_5
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v3}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->clear_cache:I

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    sget v6, Lcom/cgollner/systemmonitor/lib/R$string;->are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_:I

    invoke-virtual {v5, v6}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-static {v3, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$1;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 122
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$menu;->apps_cache_contextual_menu:I

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 123
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->d(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->d(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 173
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 174
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$ContextualAction;->a:Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 175
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method
