.class public Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;
.implements Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;


# instance fields
.field protected a:Landroid/app/ProgressDialog;

.field private aj:Ljava/util/List;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Landroid/widget/ListView;

.field private an:Landroid/view/View$OnClickListener;

.field private ao:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

.field private b:Ljava/util/List;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/ActionMode;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 183
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->an:Landroid/view/View$OnClickListener;

    .line 379
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$5;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->ao:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    return-void
.end method

.method private Q()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->am:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->al:Landroid/view/View;

    return-object v0
.end method

.method private b(Ljava/util/List;)J
    .locals 5

    .prologue
    .line 257
    const-wide/16 v0, 0x0

    .line 258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;

    .line 259
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$AppCache;->d:J

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    .line 260
    :cond_0
    return-wide v2
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c()V

    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e:Landroid/view/View;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    new-instance v0, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, p0}, Lcom/cgollner/systemmonitor/backend/CacheStatsLoader;->a(Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnStatsLoadedListener;)V

    .line 103
    return-void
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->ak:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->aj:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->ao:Lcom/cgollner/systemmonitor/backend/CacheStatsLoader$OnDeleteListener;

    return-object v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->Q()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->i:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->f:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->cache_fragment:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 60
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->aj:Ljava/util/List;

    .line 61
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cacheNumApps:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c:Landroid/widget/TextView;

    .line 62
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cacheTotalSize:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->d:Landroid/widget/TextView;

    .line 63
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->deleteAll:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e:Landroid/view/View;

    .line 64
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->deleteAllSeparator:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->ak:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->an:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->empty:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h:Landroid/view/View;

    .line 67
    const v0, 0x102000d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->f:Landroid/view/View;

    .line 68
    const v0, 0x1020002

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g:Landroid/view/View;

    .line 69
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->askRootPermissions:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->al:Landroid/view/View;

    .line 71
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 72
    const-string v2, "save_start_root"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c()V

    .line 91
    :goto_0
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->am:Landroid/widget/ListView;

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->am:Landroid/widget/ListView;

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$CacheAdapter;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 93
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->d(Z)V

    .line 95
    return-object v1

    .line 75
    :cond_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->al:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->al:Landroid/view/View;

    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->askRootStart:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;Landroid/content/SharedPreferences;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c()V

    .line 395
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$menu;->apps_cache_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 107
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 108
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v1, 0x8

    const/4 v5, 0x0

    .line 212
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    :goto_1
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->b:Ljava/util/List;

    .line 223
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c:Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->x_app_apps_containing_cache:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v6, :cond_3

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->apps:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->b(Ljava/util/List;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->x_mb_can_be_cleaned:I

    invoke-virtual {p0, v2}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->Q()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 223
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->app:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 112
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->replay:I

    if-ne v0, v1, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->c()V

    .line 115
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 229
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.cgollner.systemmonitor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    .line 230
    :goto_0
    if-nez v2, :cond_1

    .line 253
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 229
    goto :goto_0

    .line 233
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_title:I

    invoke-virtual {p0, v2}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_message:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "Play Store"

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$4;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$4;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$3;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/AppsCacheFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v1

    .line 253
    goto :goto_1
.end method
