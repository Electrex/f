.class public Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private aj:Landroid/widget/TextView;

.field private ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

.field private al:Landroid/content/Context;

.field private am:F

.field private an:Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;

.field private ao:Landroid/view/View$OnClickListener;

.field private ap:Landroid/view/View$OnClickListener;

.field private aq:Landroid/view/View$OnClickListener;

.field private ar:Landroid/view/View$OnClickListener;

.field private as:Landroid/content/BroadcastReceiver;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 96
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ao:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ap:Landroid/view/View$OnClickListener;

    .line 106
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->aq:Landroid/view/View$OnClickListener;

    .line 111
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$4;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ar:Landroid/view/View$OnClickListener;

    .line 191
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$5;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->as:Landroid/content/BroadcastReceiver;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)Lcom/cgollner/systemmonitor/BatteryMonitorView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    .line 143
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_history_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 144
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    move-result-object v0

    .line 145
    :goto_0
    if-nez v0, :cond_2

    .line 183
    :cond_0
    :goto_1
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_2
    iget v2, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->a:I

    int-to-float v2, v2

    iput v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    .line 149
    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    .line 151
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->g:Landroid/widget/TextView;

    iget v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    invoke-static {v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(F)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->d:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_charged_at:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->f:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_charged_in:I

    :goto_3
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    if-nez v1, :cond_5

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->h:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->i:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 152
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_empty_at:I

    goto :goto_2

    .line 153
    :cond_4
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->battery_empty_in:I

    goto :goto_3

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->i(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    if-nez v2, :cond_6

    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_7

    :cond_6
    if-eqz v2, :cond_9

    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    const/high16 v2, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_9

    .line 167
    :cond_7
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 172
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 173
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->h:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v2, v3, v6}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v2, v3, v6}, Lcom/cgollner/systemmonitor/backend/StringUtils;->d(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v4, v5, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->e(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    if-eqz v1, :cond_8

    .line 179
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v2, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setPredictionArray(Ljava/util/List;)V

    .line 180
    :cond_8
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setValues(Ljava/util/List;)V

    .line 181
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->b()V

    goto/16 :goto_1

    .line 170
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    iget-wide v2, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b()V

    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;)Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->an:Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    const/high16 v1, 0x41c80000    # 25.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 119
    sget v0, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ1Line:I

    .line 128
    :goto_0
    return v0

    .line 121
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    const/high16 v1, 0x42480000    # 50.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 122
    sget v0, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ2Line:I

    goto :goto_0

    .line 124
    :cond_1
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->am:F

    const/high16 v1, 0x42960000    # 75.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 125
    sget v0, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ3Line:I

    goto :goto_0

    .line 128
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$color;->batteryQ4Line:I

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    .line 62
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->battery_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ao:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ap:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->aq:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->forward:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ar:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->batteryStats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->batteryHistoryView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setTextSize(I)V

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->utilizationTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->c:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->speedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->d:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->f:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->maxSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->e:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->g:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->speedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->h:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->i:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->maxSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->aj:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->c:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_percentage:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->d:Landroid/widget/TextView;

    const-string v1, "Complete at"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->f:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_time_to_empty:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->aj:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->an:Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;

    .line 52
    return-void
.end method

.method public u()V
    .locals 3

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 134
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->as:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->b()V

    .line 137
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->an:Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->an:Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment$OnColorChangedListener;->e(I)V

    .line 140
    :cond_0
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 188
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->al:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryFragment;->as:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 189
    return-void
.end method
