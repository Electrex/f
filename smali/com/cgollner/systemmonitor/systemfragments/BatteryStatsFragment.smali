.class public Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private aj:Landroid/widget/TextView;

.field private ak:Landroid/widget/TextView;

.field private al:Landroid/content/Context;

.field private am:Landroid/content/BroadcastReceiver;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 106
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->am:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 12

    .prologue
    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    move-result-object v0

    .line 115
    iget-boolean v1, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->d:Z

    .line 116
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;Z)J

    move-result-wide v2

    .line 117
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->a(Landroid/content/Context;Z)J

    move-result-wide v4

    .line 118
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryStatsCalculator;->b(Landroid/content/Context;)J

    move-result-wide v6

    .line 120
    iget-object v8, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->aj:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->charge_speed_1_:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v8, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->h:Landroid/widget/TextView;

    const-wide/16 v10, -0x1

    cmp-long v0, v6, v10

    if-nez v0, :cond_1

    const-string v0, "N/A"

    :goto_1
    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v8, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->ak:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->charge_rate_per_hour:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->i:Landroid/widget/TextView;

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    const-string v0, "N/A"

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->b:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_4

    const-string v0, "N/A"

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->c:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_5

    const-string v0, "N/A"

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->d:Landroid/widget/TextView;

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-nez v0, :cond_6

    const-string v0, "N/A"

    :goto_6
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->e:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_7

    const-string v0, "N/A"

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->f:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_8

    const-string v0, "N/A"

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->g:Landroid/widget/TextView;

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-nez v0, :cond_9

    const-string v0, "N/A"

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void

    .line 120
    :cond_0
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->discharge_speed_1_:I

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v6, v7, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 123
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->discharge_rate_per_hour:I

    goto :goto_2

    .line 124
    :cond_3
    const-string v0, "%.1f%%"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-wide v10, 0x414b774000000000L    # 3600000.0

    long-to-double v6, v6

    div-double v6, v10, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-static {v0, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v2, v3, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 127
    :cond_5
    const-string v0, "%.1f%%"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide v8, 0x414b774000000000L    # 3600000.0

    long-to-double v10, v2

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 128
    :cond_6
    const-wide/16 v6, 0x64

    mul-long/2addr v2, v6

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v2, v3, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 130
    :cond_7
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v4, v5, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 131
    :cond_8
    const-string v0, "%.1f%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-wide v6, 0x414b774000000000L    # 3600000.0

    long-to-double v8, v4

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 132
    :cond_9
    const-wide/16 v2, 0x64

    mul-long/2addr v2, v4

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    invoke-static {v2, v3, v0}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_9
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    .line 42
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->battery_stats_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewCycleSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->h:Landroid/widget/TextView;

    .line 45
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewCycleRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->i:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewCycleSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->aj:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewCycleRateTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->ak:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsChargeSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->b:Landroid/widget/TextView;

    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsChargeRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->c:Landroid/widget/TextView;

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsTimeToCharge:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->d:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsDisChargeSpeed:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->e:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsDisChargeRateHour:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->f:Landroid/widget/TextView;

    .line 55
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->textViewStatsTimeToDisCharge:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->g:Landroid/widget/TextView;

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->d(Z)V

    .line 59
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a()V

    .line 61
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 90
    sget v0, Lcom/cgollner/systemmonitor/lib/R$menu;->battery_stats_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 91
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 92
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 66
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->resetStats:I

    if-ne v0, v1, :cond_0

    .line 67
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->reset_battery_statistics:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->reset_charging_statistics:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->reset_discharging_statistics:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->reset_all_statistics:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 86
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public u()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->am:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 99
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 103
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->al:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryStatsFragment;->am:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 104
    return-void
.end method
