.class public Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private aj:Landroid/widget/TextView;

.field private ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

.field private al:Landroid/content/Context;

.field private am:Landroid/view/View$OnClickListener;

.field private an:Landroid/view/View$OnClickListener;

.field private ao:Landroid/view/View$OnClickListener;

.field private ap:Landroid/view/View$OnClickListener;

.field private aq:Landroid/content/BroadcastReceiver;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 94
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->am:Landroid/view/View$OnClickListener;

    .line 99
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->an:Landroid/view/View$OnClickListener;

    .line 104
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ao:Landroid/view/View$OnClickListener;

    .line 109
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$4;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ap:Landroid/view/View$OnClickListener;

    .line 182
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment$5;-><init>(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aq:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 4

    .prologue
    .line 151
    const/high16 v0, 0x4f000000

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 153
    iget v3, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    cmpg-float v3, v3, v1

    if-gez v3, :cond_1

    .line 154
    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    :goto_1
    move v1, v0

    .line 155
    goto :goto_0

    .line 156
    :cond_0
    float-to-int v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)Lcom/cgollner/systemmonitor/BatteryMonitorView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 124
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_history_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 125
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->c(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    move-result-object v0

    .line 127
    :goto_0
    if-nez v0, :cond_2

    .line 148
    :cond_0
    :goto_1
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->a(Landroid/content/Context;)Lcom/cgollner/systemmonitor/battery/BatteryInfo;

    move-result-object v0

    goto :goto_0

    .line 130
    :cond_2
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->g:Landroid/widget/TextView;

    iget v0, v0, Lcom/cgollner/systemmonitor/battery/BatteryInfo;->b:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v0, v4, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    if-nez v1, :cond_3

    .line 133
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->h:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->i:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aj:Landroid/widget/TextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/battery/BatteryService;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 142
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->h:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->c(Ljava/util/List;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v2, v4, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->i:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b(Ljava/util/List;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v2, v4, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aj:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a(Ljava/util/List;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    invoke-static {v2, v4, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(IILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setValues(Ljava/util/List;)V

    .line 147
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->postInvalidate()V

    goto :goto_1
.end method

.method private b(Ljava/util/List;)I
    .locals 4

    .prologue
    .line 160
    const/high16 v0, -0x31000000

    .line 161
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 162
    iget v3, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    cmpl-float v3, v3, v1

    if-lez v3, :cond_1

    .line 163
    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    :goto_1
    move v1, v0

    .line 164
    goto :goto_0

    .line 165
    :cond_0
    float-to-int v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a()V

    return-void
.end method

.method private c(Ljava/util/List;)I
    .locals 3

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/battery/TimeValue;

    .line 171
    iget v0, v0, Lcom/cgollner/systemmonitor/battery/TimeValue;->b:F

    add-float/2addr v0, v1

    move v1, v0

    .line 172
    goto :goto_0

    .line 173
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    .line 47
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->battery_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->am:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->an:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ao:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->forward:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ap:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->batteryStats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->batteryHistoryView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    .line 56
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->setTextSize(I)V

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->c:Z

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    const/high16 v1, 0x447a0000    # 1000.0f

    iput v1, v0, Lcom/cgollner/systemmonitor/BatteryMonitorView;->d:F

    .line 59
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryMonitorView;->invalidate()V

    .line 61
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->utilizationTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->c:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->speedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->d:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->f:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->maxSpeedTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->e:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->g:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->speedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->h:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->i:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->b:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->maxSpeedValue:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aj:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->c:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->battery_temperature:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->d:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->statistics_average:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->f:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->max:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->e:Landroid/widget/TextView;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->min:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aj:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->plus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 83
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 84
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->minus:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 88
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->ak:Lcom/cgollner/systemmonitor/BatteryMonitorView;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/BatteryMonitorView;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 89
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public u()V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 118
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aq:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 120
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->a()V

    .line 121
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 179
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->al:Landroid/content/Context;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/BatteryTemperatureFragment;->aq:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 180
    return-void
.end method
