.class Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 106
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->c(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 116
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 118
    :goto_0
    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->c(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)[Ljava/io/File;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->c(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)[Ljava/io/File;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 124
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 130
    if-nez p2, :cond_0

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->d(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$layout;->simple_list_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 136
    :goto_0
    invoke-virtual {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 137
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;->a:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "../"

    :goto_1
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter$1;

    invoke-direct {v1, p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;Ljava/io/File;)V

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    return-object p2

    .line 134
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    goto :goto_0

    .line 137
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
