.class public Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SourceFile"


# instance fields
.field private aj:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

.field private ak:Landroid/widget/ListView;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private an:Ljava/io/File;

.field private ao:[Ljava/io/File;

.field private ap:Landroid/content/Context;

.field private aq:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 96
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->aq:Landroid/view/View$OnClickListener;

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->an:Ljava/io/File;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Ljava/io/File;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 72
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->an:Ljava/io/File;

    .line 74
    const-string v0, "/"

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->an:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->am:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    :goto_1
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$1;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)V

    .line 84
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->an:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ao:[Ljava/io/File;

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ao:[Ljava/io/File;

    if-nez v0, :cond_3

    .line 86
    new-array v0, v2, [Ljava/io/File;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ao:[Ljava/io/File;

    goto :goto_0

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->am:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ao:[Ljava/io/File;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$2;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->aj:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ao:[Ljava/io/File;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ap:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ap:Landroid/content/Context;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->list_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->al:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->al:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ak:Landroid/widget/ListView;

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->choose:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->am:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->am:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->aq:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    new-instance v0, Ljava/io/File;

    const-string v1, "/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Ljava/io/File;)V

    .line 62
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ak:Landroid/widget/ListView;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->ap:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$ExplorerAdapter;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->al:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a(Landroid/app/Activity;)V

    .line 47
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->aj:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->aj:Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;

    goto :goto_0
.end method
