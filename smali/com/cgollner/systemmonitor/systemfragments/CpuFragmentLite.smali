.class public Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static aj:I

.field private static an:Ljava/util/List;

.field private static ao:Ljava/util/List;

.field private static ap:Ljava/util/List;

.field private static aq:Ljava/util/List;

.field private static av:Z


# instance fields
.field private aA:Landroid/content/Context;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private ar:[Landroid/widget/TextView;

.field private as:[Landroid/widget/TextView;

.field private at:[Landroid/widget/TextView;

.field private au:Landroid/view/View;

.field private aw:Landroid/os/Handler;

.field private ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

.field private ay:[Lcom/cgollner/systemmonitor/MonitorView;

.field private az:I

.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/MonitorView;

.field private d:Lcom/cgollner/systemmonitor/MonitorView;

.field private e:Lcom/cgollner/systemmonitor/MonitorView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 107
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->az:I

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 109
    const-string v1, "displayAllCores"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->av:Z

    .line 110
    sget-boolean v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->av:Z

    if-nez v0, :cond_0

    .line 111
    sput v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->az:I

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    goto :goto_0
.end method

.method private R()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->Q()V

    .line 117
    const/4 v0, 0x3

    :goto_0
    sget v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-lt v0, v2, :cond_0

    .line 118
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Lcom/cgollner/systemmonitor/MonitorView;->setVisibility(I)V

    .line 119
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->as:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->at:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ar:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 123
    :goto_1
    sget v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-ge v0, v2, :cond_1

    .line 124
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/cgollner/systemmonitor/MonitorView;->setVisibility(I)V

    .line 125
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->as:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->at:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ar:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->k()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_3

    .line 130
    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ak:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 144
    :goto_2
    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    .line 145
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->al:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->am:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    :goto_3
    return-void

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ak:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 137
    :cond_3
    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-ge v0, v4, :cond_4

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ak:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ak:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 149
    :cond_5
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->al:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->am:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method private S()V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 281
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_cpu_updatefreq_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 282
    sput v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a:I

    .line 283
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aw:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 225
    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aw:Landroid/os/Handler;

    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 237
    return-void
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->as:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->at:[Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x4

    .line 54
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    .line 55
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->S()V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aw:Landroid/os/Handler;

    .line 57
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->cpu_fragment_layout:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 59
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->an:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->an:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview2:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c:Lcom/cgollner/systemmonitor/MonitorView;

    .line 63
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ao:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ao:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview3:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->d:Lcom/cgollner/systemmonitor/MonitorView;

    .line 67
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ap:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->d:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ap:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview4:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->e:Lcom/cgollner/systemmonitor/MonitorView;

    .line 71
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aq:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aq:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 74
    :cond_3
    new-array v0, v7, [Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v2, v0, v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v2, v0, v8

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->d:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->e:Lcom/cgollner/systemmonitor/MonitorView;

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->f:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->speedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->g:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->minSpeedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->h:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->maxSpeedValue:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->i:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->quadCoreLayout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ak:Landroid/view/View;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->stats_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->al:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->stats_layout_cpu:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->am:Landroid/view/View;

    .line 85
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->as:[Landroid/widget/TextView;

    .line 86
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->at:[Landroid/widget/TextView;

    .line 87
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ar:[Landroid/widget/TextView;

    .line 89
    :goto_0
    if-ge v1, v7, :cond_4

    .line 90
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->k()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpuStatTitle"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 91
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->k()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cpuStatVal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 92
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->k()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cpuTitle"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "id"

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 94
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->as:[Landroid/widget/TextView;

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 95
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->at:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 96
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ar:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 99
    :cond_4
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->R()V

    .line 101
    invoke-virtual {p0, v8}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->d(Z)V

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->au:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 187
    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    move v3, v1

    .line 189
    :goto_1
    sget v4, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-ge v3, v4, :cond_8

    .line 191
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    invoke-static {v3}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->e(I)Z

    move-result v5

    iput-boolean v5, v4, Lcom/cgollner/systemmonitor/MonitorView;->c:Z

    .line 192
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    iget-object v5, v5, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a:[F

    add-int v6, v3, v0

    aget v5, v5, v6

    sget-boolean v6, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v4, v5, v6}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 194
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->k()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_3

    .line 195
    if-eq v3, v2, :cond_0

    if-ne v3, v9, :cond_2

    .line 196
    :cond_0
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v2, v1, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 207
    :goto_2
    sget-boolean v4, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-nez v4, :cond_6

    .line 189
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 187
    goto :goto_0

    .line 198
    :cond_2
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v1, v1, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    goto :goto_2

    .line 201
    :cond_3
    if-eq v3, v8, :cond_4

    if-ne v3, v9, :cond_5

    .line 202
    :cond_4
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v2, v1, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    goto :goto_2

    .line 204
    :cond_5
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ay:[Lcom/cgollner/systemmonitor/MonitorView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v1, v1, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    goto :goto_2

    .line 210
    :cond_6
    sget v4, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aj:I

    if-le v4, v2, :cond_7

    .line 211
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    add-int v5, v3, v0

    invoke-virtual {v4, v5}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v5, v3}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 214
    :cond_7
    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    add-int v5, v3, v0

    invoke-virtual {v4, v5}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->a(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v5, v1}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->b(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v6, v1}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->c(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v7, v1}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->d(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 222
    :cond_8
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 157
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a()I

    move-result v0

    .line 158
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 159
    sget v0, Lcom/cgollner/systemmonitor/lib/R$menu;->cpu_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 160
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 164
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->display_all_cores:I

    if-ne v2, v3, :cond_1

    .line 165
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aA:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 166
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 167
    const-string v4, "displayAllCores"

    const-string v5, "displayAllCores"

    invoke-interface {v2, v5, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 168
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 169
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->R()V

    .line 172
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;->h()V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    .line 257
    :cond_0
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 260
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b()V

    .line 263
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->a:I

    int-to-long v2, v0

    move-object v5, p0

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/CpuMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ax:Lcom/cgollner/systemmonitor/monitor/CpuMonitor;

    .line 264
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->an:Ljava/util/List;

    .line 177
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ao:Ljava/util/List;

    .line 178
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->d:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->ap:Ljava/util/List;

    .line 179
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->e:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->aq:Ljava/util/List;

    .line 181
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 182
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 273
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 275
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->c()V

    .line 277
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 267
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 268
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;->b()V

    .line 269
    return-void
.end method
