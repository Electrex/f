.class public Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

.field private c:I

.field private d:Landroid/view/View;

.field private e:Landroid/content/Context;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 164
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->f:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;I)I
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    return p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->e:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a(Landroid/view/LayoutInflater;)V

    .line 95
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->k()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "cpu0"

    const-string v3, "id"

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 102
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 104
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object v7

    .line 108
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v6

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    .line 109
    sget v2, Lcom/cgollner/systemmonitor/lib/R$layout;->cpu_freq_state_item:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 111
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreq:I

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 112
    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreqTime:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 113
    sget v4, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreqPercentage:I

    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 115
    iget-object v10, v1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->d:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-wide v10, v1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget v1, v1, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(F)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    sget-object v1, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    .line 121
    iget v10, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    if-ne v5, v10, :cond_0

    .line 122
    iput v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    .line 123
    array-length v10, v1

    rem-int v10, v5, v10

    aget-object v10, v1, v10

    const/4 v11, 0x0

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundColor(I)V

    .line 126
    :cond_0
    iget v10, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    if-ne v5, v10, :cond_1

    const/4 v1, -0x1

    .line 127
    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 129
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    invoke-virtual {v9, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    invoke-virtual {v9, v5}, Landroid/view/View;->setId(I)V

    .line 134
    add-int/lit8 v1, v5, 0x1

    .line 135
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v5, v1

    .line 136
    goto :goto_0

    .line 126
    :cond_1
    array-length v10, v1

    rem-int v10, v5, v10

    aget-object v1, v1, v10

    const/4 v10, 0x1

    aget v1, v1, v10

    goto :goto_1

    .line 138
    :cond_2
    iput v5, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a:I

    .line 140
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->pie:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    .line 141
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 142
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;

    .line 143
    new-instance v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    invoke-direct {v4}, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;-><init>()V

    .line 144
    iget v1, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a:I

    if-gtz v1, :cond_3

    .line 145
    const-string v1, "Deep sleep"

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->b:Ljava/lang/String;

    .line 149
    :goto_3
    iget-wide v8, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->e:Landroid/content/Context;

    invoke-static {v8, v9, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->a:Ljava/lang/String;

    .line 150
    iget v1, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->c:F

    iput v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->d:F

    .line 151
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->b:J

    iput-wide v0, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    .line 152
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 158
    :catch_0
    move-exception v0

    .line 159
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->freqsFileNotFound:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 162
    :goto_4
    return-void

    .line 147
    :cond_3
    :try_start_1
    iget v1, v0, Lcom/cgollner/systemmonitor/backend/CpuUtils$CpuTimeInStateItem;->a:I

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->b:Ljava/lang/String;

    goto :goto_3

    .line 154
    :cond_4
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    invoke-virtual {v0, v2, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Ljava/util/List;I)V

    .line 155
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setClickable(Z)V

    .line 156
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a()V

    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    return v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a:I

    return v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d(Z)V

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->cpu_freq_state_layout:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d:Landroid/view/View;

    .line 46
    iput v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    .line 47
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->e:Landroid/content/Context;

    .line 48
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a(Landroid/view/LayoutInflater;)V

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/cgollner/systemmonitor/lib/R$menu;->cpu_freqs_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 60
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 64
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->replay:I

    if-ne v1, v2, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a()V

    .line 90
    :goto_0
    return v0

    .line 68
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->resetFrequencies:I

    if-ne v1, v2, :cond_1

    .line 69
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->reset_timers:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->are_you_sure_message:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->yes:I

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$2;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->No:I

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$1;

    invoke-direct {v3, p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 90
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->c:I

    .line 175
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuFreqStatsFragment;->a()V

    .line 176
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 55
    return-void
.end method
