.class public Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->a:I

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->h:Landroid/os/Handler;

    .line 42
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->Q()V

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->cpu_temp_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    const-wide v2, 0x408f400000000000L    # 1000.0

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iput-boolean v4, v0, Lcom/cgollner/systemmonitor/MonitorView;->f:Z

    .line 48
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v4, v5, v5, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cpuTempCurrentValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->d:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cpuTempMinValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->e:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cpuTempMaxValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->f:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->cpuTempAvgValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->g:Landroid/widget/TextView;

    .line 57
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->d(Z)V

    .line 59
    return-object v1
.end method

.method public a()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->c()I

    move-result v1

    int-to-float v1, v1

    sget-boolean v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 97
    sget-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;->h()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    .line 85
    :cond_0
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b()V

    .line 91
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->a:I

    int-to-long v2, v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    move-object v5, p0

    move v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;ZLandroid/content/Context;)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c:Lcom/cgollner/systemmonitor/monitor/CpuTempMonitor;

    .line 92
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->i:Ljava/util/List;

    .line 77
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 78
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 65
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->c()V

    .line 66
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 71
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;->b()V

    .line 72
    return-void
.end method
