.class public Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x3e8

    sput v0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->a:I

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)Lcom/cgollner/systemmonitor/monitor/GpuMonitor;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->e:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->h:Landroid/os/Handler;

    .line 42
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->Q()V

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->gpu_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 46
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v3, v3, v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 51
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->statValue0:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->d:Landroid/widget/TextView;

    .line 52
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->statValue1:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->g:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->statValue2:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->f:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->statValue3:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->e:Landroid/widget/TextView;

    .line 55
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->d(Z)V

    .line 57
    return-object v1
.end method

.method public a()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->g()F

    move-result v1

    sget-boolean v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 95
    sget-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;->h()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    .line 83
    :cond_0
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 86
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b()V

    .line 89
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->a:I

    int-to-long v2, v0

    move-object v5, p0

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/GpuMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c:Lcom/cgollner/systemmonitor/monitor/GpuMonitor;

    .line 90
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->i:Ljava/util/List;

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 76
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 63
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->c()V

    .line 64
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 69
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;->b()V

    .line 70
    return-void
.end method
