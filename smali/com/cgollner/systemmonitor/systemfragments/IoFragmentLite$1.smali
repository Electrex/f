.class Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/MonitorView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v1

    iget v1, v1, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->c:F

    sget-boolean v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 101
    sget-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->d(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->e(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;->a:Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    goto :goto_0
.end method
