.class public Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static i:Ljava/util/List;


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/content/Context;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 34
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_io_updatefreq_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 35
    sput v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a:I

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/monitor/IoMonitor;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Lcom/cgollner/systemmonitor/MonitorView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->g:Landroid/content/Context;

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->h:Landroid/os/Handler;

    .line 43
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->Q()V

    .line 44
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->io_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 46
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v3, v3, v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 51
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->d:Landroid/widget/TextView;

    .line 52
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->readSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->e:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->writeSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->f:Landroid/widget/TextView;

    .line 55
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->d(Z)V

    .line 56
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;->h()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    .line 82
    :cond_0
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b()V

    .line 88
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->a:I

    int-to-long v2, v0

    move-object v5, p0

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/IoMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/IoMonitor;

    .line 89
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->i:Ljava/util/List;

    .line 62
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 63
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 68
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->c()V

    .line 69
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 74
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;->b()V

    .line 75
    return-void
.end method
