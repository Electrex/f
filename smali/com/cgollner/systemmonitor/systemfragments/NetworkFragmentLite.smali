.class public Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static h:Ljava/util/List;

.field private static i:D


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const-wide/high16 v0, 0x40f9000000000000L    # 102400.0

    sput-wide v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->i:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 33
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_net_updatefreq_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 34
    sput v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->a:I

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;)Lcom/cgollner/systemmonitor/monitor/NetMonitor;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->g:Landroid/os/Handler;

    .line 41
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->Q()V

    .line 42
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->network_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 43
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    sget-object v2, Lcom/cgollner/systemmonitor/MonitorView$LabelType;->c:Lcom/cgollner/systemmonitor/MonitorView$LabelType;

    invoke-virtual {v0, v2}, Lcom/cgollner/systemmonitor/MonitorView;->setLabelType(Lcom/cgollner/systemmonitor/MonitorView$LabelType;)V

    .line 45
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v3, v3, v3, v6}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 50
    sget-wide v2, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->i:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    sget-wide v2, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->i:D

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 53
    :cond_1
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->throughputValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->d:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->readSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->e:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->writeSpeedValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->f:Landroid/widget/TextView;

    .line 56
    invoke-virtual {p0, v6}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->d(Z)V

    .line 58
    return-object v1
.end method

.method public a()V
    .locals 6

    .prologue
    .line 72
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    iget-wide v4, v1, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->k:J

    long-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    iput-wide v2, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    .line 73
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    iget-wide v2, v1, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->k:J

    long-to-float v1, v2

    sget-boolean v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 74
    sget-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->g:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/NetMonitor;->h()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    .line 110
    :cond_0
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b()V

    .line 116
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->a:I

    int-to-long v2, v0

    move-object v5, p0

    move v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/cgollner/systemmonitor/monitor/NetMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/NetMonitor;

    .line 117
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->h:Ljava/util/List;

    .line 64
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-wide v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->b:D

    sput-wide v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->i:D

    .line 66
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 67
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 95
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->c()V

    .line 96
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 101
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 102
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;->b()V

    .line 103
    return-void
.end method
