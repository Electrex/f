.class public Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;
.super Landroid/support/v4/app/ListFragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# instance fields
.field private aj:Landroid/content/Context;

.field private ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private an:Ljava/util/HashMap;

.field private ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

.field private ap:Landroid/widget/TextView;

.field private i:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->aj:Landroid/content/Context;

    .line 79
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->top_apps_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->progressBar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->am:Landroid/view/View;

    .line 82
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonRAM:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPUTIME:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonName:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    .line 88
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonName:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/App$AppNameComparator;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/systemfragments/App$AppNameComparator;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPU:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuComparator;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuComparator;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonRAM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/App$AppRamComparator;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/systemfragments/App$AppRamComparator;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPUTIME:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuTimeComparator;

    invoke-direct {v2}, Lcom/cgollner/systemmonitor/systemfragments/App$AppCpuTimeComparator;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->a(Landroid/os/Bundle;)V

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->i:Landroid/os/Handler;

    .line 47
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/cgollner/systemmonitor/lib/R$layout;->apps_top_entry:I

    invoke-direct {v0, v1, v2}, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    .line 48
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->a(Landroid/widget/ListAdapter;)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->d(Z)V

    .line 51
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 55
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->menu_show_all_processes:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "showAllProcesses"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->a(Landroid/view/Menu;)V

    .line 58
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/cgollner/systemmonitor/lib/R$menu;->topapps_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 62
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 66
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->menu_show_all_processes:I

    if-ne v0, v2, :cond_1

    .line 67
    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-boolean v0, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 69
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->aj:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "showAllProcesses"

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-boolean v3, v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    :goto_1
    return v1

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->a(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 119
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "<u>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "</u>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 124
    check-cast v0, Landroid/widget/TextView;

    .line 125
    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 127
    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->l:Ljava/util/Comparator;

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    iget-object v1, v0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    monitor-enter v1

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->l:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->notifyDataSetChanged()V

    .line 139
    return-void

    .line 137
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 134
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public u()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 105
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iput-boolean v5, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->g:Z

    .line 107
    :cond_0
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ao:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->i:Landroid/os/Handler;

    iget-object v4, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->am:Landroid/view/View;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;-><init>(Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;Landroid/content/Context;Landroid/os/Handler;Landroid/view/View;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    .line 108
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->an:Ljava/util/HashMap;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, v1, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->l:Ljava/util/Comparator;

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->al:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->buttonCPU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->aj:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "showAllProcesses"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    .line 112
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->start()V

    .line 113
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->u()V

    .line 114
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;->ak:Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->g:Z

    .line 100
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->v()V

    .line 101
    return-void
.end method
