.class public Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# static fields
.field public static a:I


# instance fields
.field public b:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

.field public c:Ljava/util/Map;

.field public d:Ljava/util/Map;

.field public e:Ljava/util/Map;

.field public f:Ljava/util/List;

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/util/Comparator;

.field private m:Landroid/app/ActivityManager;

.field private n:Landroid/content/pm/PackageManager;

.field private o:Landroid/os/Handler;

.field private p:Landroid/view/View;

.field private q:F

.field private r:[Ljava/lang/String;

.field private s:[I


# direct methods
.method public constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;Landroid/content/Context;Landroid/os/Handler;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 66
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->r:[Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->c:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->d:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->e:Ljava/util/Map;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    .line 73
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->b:Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;

    .line 75
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 76
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_topapps_updatefreq_key:I

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 77
    sput v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a:I

    .line 79
    const-string v0, "activity"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->m:Landroid/app/ActivityManager;

    .line 80
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->n:Landroid/content/pm/PackageManager;

    .line 81
    iput-object p3, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->o:Landroid/os/Handler;

    .line 82
    iput-object p4, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->p:Landroid/view/View;

    .line 83
    invoke-virtual {p4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 84
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->p:Landroid/view/View;

    return-object v0
.end method

.method private a(Ljava/util/Map;)V
    .locals 8

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    if-eqz v0, :cond_1

    .line 220
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 221
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 222
    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    const-wide/16 v4, 0x0

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v4, v5, v3}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;JLjava/lang/String;)V

    .line 224
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 226
    :cond_0
    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    goto :goto_0

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->m:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 231
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 232
    if-nez v1, :cond_2

    .line 233
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v1}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v4

    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v1}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 234
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v1}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 235
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;JLjava/lang/String;)V

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 237
    :cond_2
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v3}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v4

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v3}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->a:J

    .line 238
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    goto :goto_1

    .line 241
    :cond_3
    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 90
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->g:Z

    .line 91
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->h:Z

    .line 92
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->g:Z

    if-eqz v2, :cond_9

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->c:Ljava/util/Map;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a(Ljava/util/Map;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->r:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 95
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->i:Ljava/lang/String;

    .line 97
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->h:Z

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x64

    :goto_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 98
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->h:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 101
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->r:[Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->r:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->r:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(Ljava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->q:F

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->d:Ljava/util/Map;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a(Ljava/util/Map;)V

    .line 106
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->j:Ljava/lang/String;

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->i:Ljava/lang/String;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b([Ljava/lang/String;)J

    move-result-wide v2

    .line 108
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->j:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->b([Ljava/lang/String;)J

    move-result-wide v4

    .line 109
    sub-long v6, v4, v2

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 111
    invoke-static {}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->c()Ljava/util/Set;

    move-result-object v8

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->m:Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->c:Ljava/util/Map;

    iget v4, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->d:Ljava/util/Map;

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 116
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v10, 0x190

    if-eq v5, v10, :cond_0

    .line 119
    iget-object v5, v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    .line 120
    iget-object v10, v4, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    .line 122
    iget-wide v12, v4, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->a:J

    iget-wide v14, v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->a:J

    sub-long/2addr v12, v14

    .line 124
    move-object/from16 v0, p0

    iget v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->q:F

    invoke-static {v5, v10, v6, v7, v3}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(Ljava/lang/String;Ljava/lang/String;JF)F

    move-result v10

    .line 125
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->s:[I

    if-nez v3, :cond_1

    .line 126
    const/4 v3, 0x1

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->s:[I

    .line 127
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->s:[I

    const/4 v4, 0x0

    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    aput v5, v3, v4

    .line 129
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->m:Landroid/app/ActivityManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->s:[I

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v3

    .line 130
    const-wide/16 v4, 0x0

    .line 132
    const/4 v11, 0x0

    aget-object v11, v3, v11

    iget v11, v11, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    int-to-long v14, v11

    add-long/2addr v4, v14

    .line 133
    const/4 v11, 0x0

    aget-object v11, v3, v11

    iget v11, v11, Landroid/os/Debug$MemoryInfo;->nativePss:I

    int-to-long v14, v11

    add-long/2addr v4, v14

    .line 134
    const/4 v11, 0x0

    aget-object v3, v3, v11

    iget v3, v3, Landroid/os/Debug$MemoryInfo;->otherPss:I

    int-to-long v14, v3

    add-long/2addr v4, v14

    .line 135
    const-wide/16 v14, 0x400

    mul-long/2addr v14, v4

    .line 137
    iget-object v11, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v0, v11

    move/from16 v16, v0

    const/4 v3, 0x0

    move v5, v3

    :goto_3
    move/from16 v0, v16

    if-ge v5, v0, :cond_0

    aget-object v17, v11, v5

    .line 139
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->n:Landroid/content/pm/PackageManager;

    const/16 v4, 0x81

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 141
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->n:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 142
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->e:Ljava/util/Map;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cgollner/systemmonitor/systemfragments/App;

    .line 143
    if-nez v4, :cond_2

    new-instance v4, Lcom/cgollner/systemmonitor/systemfragments/App;

    invoke-direct {v4}, Lcom/cgollner/systemmonitor/systemfragments/App;-><init>()V

    .line 145
    :cond_2
    monitor-enter v4
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 146
    :try_start_2
    iput-object v3, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->a:Ljava/lang/String;

    .line 147
    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->b:Ljava/lang/String;

    .line 148
    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 150
    iget-object v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "(.*:)(.*)"

    const-string v19, "$2"

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->g:Ljava/lang/String;

    .line 154
    :cond_3
    iput v10, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->c:F

    .line 155
    iput-wide v14, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->e:J

    .line 156
    iput-wide v12, v4, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    .line 157
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->e:Ljava/util/Map;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "-"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    iget v3, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    .line 137
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_3

    .line 97
    :cond_4
    :try_start_4
    sget v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a:I
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    int-to-long v2, v2

    goto/16 :goto_1

    .line 157
    :catchall_0
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v3
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_0

    .line 161
    :catch_0
    move-exception v3

    goto :goto_4

    .line 166
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->k:Z

    if-eqz v2, :cond_8

    .line 167
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->c:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 169
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->d:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;

    .line 171
    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    .line 175
    iget-object v8, v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    .line 176
    iget-object v9, v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->b:Ljava/lang/String;

    .line 178
    iget-wide v10, v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->a:J

    iget-wide v2, v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$AppState;->a:J

    sub-long/2addr v10, v2

    .line 180
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->q:F

    invoke-static {v8, v9, v6, v7, v2}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->a(Ljava/lang/String;Ljava/lang/String;JF)F

    move-result v8

    .line 182
    invoke-static {v5}, Lcom/cgollner/systemmonitor/backend/CpuUtils;->d(I)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->e:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cgollner/systemmonitor/systemfragments/App;

    .line 184
    if-nez v3, :cond_7

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/App;

    invoke-direct {v3}, Lcom/cgollner/systemmonitor/systemfragments/App;-><init>()V

    .line 186
    :cond_7
    monitor-enter v3

    .line 187
    :try_start_7
    iput-object v2, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->a:Ljava/lang/String;

    .line 188
    const-string v2, "systemprocess"

    iput-object v2, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->b:Ljava/lang/String;

    .line 189
    const/4 v2, 0x0

    iput-object v2, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->d:Landroid/graphics/drawable/Drawable;

    .line 190
    iput v8, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->c:F

    .line 191
    invoke-static {v5}, Lcom/cgollner/systemmonitor/backend/RamUtils;->a(I)J

    move-result-wide v8

    iput-wide v8, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->e:J

    .line 192
    iput-wide v10, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    .line 193
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->e:Ljava/util/Map;

    iget-object v5, v3, Lcom/cgollner/systemmonitor/systemfragments/App;->a:Ljava/lang/String;

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 193
    :catchall_1
    move-exception v2

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v2

    .line 198
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    monitor-enter v3

    .line 200
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->l:Ljava/util/Comparator;

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 204
    :goto_6
    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->o:Landroid/os/Handler;

    new-instance v3, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 204
    :catchall_2
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v2

    .line 217
    :cond_9
    return-void

    .line 201
    :catch_1
    move-exception v2

    goto :goto_6

    .line 99
    :catch_2
    move-exception v2

    goto/16 :goto_2
.end method
