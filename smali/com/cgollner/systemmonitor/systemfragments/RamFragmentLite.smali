.class public Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;


# static fields
.field public static a:I

.field private static aj:Ljava/util/List;


# instance fields
.field private b:Lcom/cgollner/systemmonitor/MonitorView;

.field private c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/os/Handler;

.field private i:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 35
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->settings_app_ram_updatefreq_key:I

    invoke-virtual {p0, v1}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 36
    sput v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->a:I

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)Lcom/cgollner/systemmonitor/monitor/RamMonitor;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    return-object v0
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->h:Landroid/os/Handler;

    .line 44
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->i:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->Q()V

    .line 47
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->ram_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 48
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->monitorview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/MonitorView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    .line 49
    sget-object v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->aj:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sget-object v2, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->aj:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    invoke-virtual {v0, v3, v3, v3, v4}, Lcom/cgollner/systemmonitor/MonitorView;->a(ZZZZ)V

    .line 54
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->usageAvg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->d:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->freeRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->e:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->totalRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->f:Landroid/widget/TextView;

    .line 57
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->usedRamValue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->g:Landroid/widget/TextView;

    .line 58
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->d(Z)V

    .line 60
    return-object v1
.end method

.method public a()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    iget v1, v1, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->a:F

    sget-boolean v2, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/cgollner/systemmonitor/MonitorView;->a(FZ)V

    .line 72
    sget-boolean v0, Lcom/cgollner/systemmonitor/MainActivityAbstract;->n:Z

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->h:Landroid/os/Handler;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/monitor/RamMonitor;->h()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    .line 105
    :cond_0
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b()V

    .line 111
    :cond_0
    new-instance v1, Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    sget v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->a:I

    int-to-long v2, v0

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->i:Landroid/content/Context;

    move-object v5, p0

    move v7, v4

    invoke-direct/range {v1 .. v7}, Lcom/cgollner/systemmonitor/monitor/RamMonitor;-><init>(JZLcom/cgollner/systemmonitor/monitor/MonitorAbstract$MonitorListener;Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c:Lcom/cgollner/systemmonitor/monitor/RamMonitor;

    .line 112
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b:Lcom/cgollner/systemmonitor/MonitorView;

    iget-object v0, v0, Lcom/cgollner/systemmonitor/MonitorView;->a:Ljava/util/List;

    sput-object v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->aj:Ljava/util/List;

    .line 66
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 67
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 91
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->c()V

    .line 92
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->v()V

    .line 97
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;->b()V

    .line 98
    return-void
.end method
