.class Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 103
    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;J)J

    .line 107
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;F)F

    .line 108
    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;J)J

    .line 109
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v2}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer;->a(Ljava/io/File;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;Ljava/util/List;)Ljava/util/List;

    .line 110
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;->a:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    return-void
.end method
