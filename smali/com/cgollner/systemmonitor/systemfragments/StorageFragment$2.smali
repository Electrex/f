.class Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

.field final synthetic b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    iput-object p2, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.cgollner.systemmonitor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_title:I

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->full_version_feature_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Play Store"

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$2;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$1;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 232
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->b:Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;

    invoke-static {v1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Delete"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;->a:Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    iget-object v2, v2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Yes"

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$4;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$4;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "No"

    new-instance v2, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$3;

    invoke-direct {v2, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
