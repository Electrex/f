.class public Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment$OnChooseFolderListener;


# instance fields
.field private a:I

.field private aj:Landroid/view/ViewGroup;

.field private ak:Landroid/content/Context;

.field private al:Landroid/os/Handler;

.field private am:Landroid/view/View$OnClickListener;

.field private an:Ljava/io/File;

.field private b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

.field private c:I

.field private d:Landroid/view/View;

.field private e:Ljava/util/List;

.field private f:Ljava/io/File;

.field private g:J

.field private h:F

.field private i:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 239
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$3;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->am:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;F)F
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->h:F

    return p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    return p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;J)J
    .locals 1

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->g:J

    return-wide p1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    return-object v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->e:Ljava/util/List;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Landroid/view/LayoutInflater;)V

    .line 122
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->progressBar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->stats:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->freeStorage:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/cgollner/systemmonitor/lib/R$string;->availableRam:I

    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%.1f%%"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->h:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->g:J

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->i:J

    invoke-static {v4, v5}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->k()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "cpu0"

    const-string v4, "id"

    iget-object v5, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->aj:Landroid/view/ViewGroup;

    .line 134
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->aj:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 137
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->aj:Landroid/view/ViewGroup;

    new-instance v2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;-><init>(Ljava/io/File;J)V

    sget-object v3, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    const-string v4, "../"

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;ILjava/lang/String;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    .line 140
    iget-object v3, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->aj:Landroid/view/ViewGroup;

    iget-object v4, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v0, v1, v4}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;ILjava/lang/String;)V

    .line 141
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 142
    goto :goto_0

    .line 144
    :cond_1
    iput v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a:I

    .line 146
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->pie:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    .line 147
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 148
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    .line 149
    new-instance v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;

    invoke-direct {v4}, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;-><init>()V

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "/"

    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->b:Ljava/lang/String;

    .line 151
    iget-wide v6, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    invoke-static {v6, v7}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->a:Ljava/lang/String;

    .line 152
    iget v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->c:F

    iput v1, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->d:F

    .line 153
    iget-wide v0, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    iput-wide v0, v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart$PieItem;->c:J

    .line 154
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 150
    :cond_2
    const-string v1, ""

    goto :goto_2

    .line 156
    :cond_3
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    invoke-virtual {v0, v2, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a(Ljava/util/List;I)V

    .line 157
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    invoke-virtual {v0, v8}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setClickable(Z)V

    .line 158
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b:Lcom/cgollner/systemmonitor/FrequenciesPieChart;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->am:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;ILjava/lang/String;)V
    .locals 10

    .prologue
    .line 162
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$layout;->storage_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 164
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreq:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 165
    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreqTime:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 166
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->tvFreqPercentage:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 167
    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 169
    iget-object v4, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    const-string v4, "../"

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    sget v4, Lcom/cgollner/systemmonitor/lib/R$id;->delete:I

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 175
    :goto_0
    iget-wide v6, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    const-wide/16 v8, -0x1

    cmp-long v4, v6, v8

    if-eqz v4, :cond_0

    .line 176
    iget-wide v6, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->b:J

    invoke-static {v6, v7}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget v4, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->c:F

    invoke-static {v4}, Lcom/cgollner/systemmonitor/backend/StringUtils;->c(F)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :cond_0
    sget-object v4, Lcom/cgollner/systemmonitor/FrequenciesPieChart;->a:[[I

    .line 181
    iget v6, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    if-ne p3, v6, :cond_1

    .line 182
    array-length v6, v4

    rem-int v6, p3, v6

    aget-object v6, v4, v6

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 183
    sget v6, Lcom/cgollner/systemmonitor/lib/R$drawable;->ic_ac_content_discard_holo_dark:I

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    :cond_1
    iget v6, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    if-ne p3, v6, :cond_4

    const/4 v4, -0x1

    .line 187
    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 188
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 191
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    invoke-virtual {v5, p3}, Landroid/view/View;->setId(I)V

    .line 193
    invoke-virtual {v5, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 194
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;

    invoke-direct {v0, p0, p2}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$2;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    add-int/lit8 v0, p3, 0x1

    .line 235
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 237
    return-void

    .line 173
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p2, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "/"

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    const-string v4, ""

    goto :goto_2

    .line 186
    :cond_4
    array-length v6, v4

    rem-int v6, p3, v6

    aget-object v4, v4, v6

    const/4 v6, 0x1

    aget v4, v4, v6

    goto :goto_1
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    return-void
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;J)J
    .locals 1

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->i:J

    return-wide p1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    return-object v0
.end method

.method private b(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 96
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->progressBar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->stats:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 118
    return-void
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a()V

    return-void
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->al:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    return v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a:I

    return v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d(Z)V

    .line 62
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->al:Landroid/os/Handler;

    .line 64
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->storage_stats_layout:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    .line 65
    iput v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    .line 67
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "ROOT_FOLDER"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    .line 73
    invoke-virtual {p0, v4}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d(Z)V

    .line 74
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 79
    sget v0, Lcom/cgollner/systemmonitor/lib/R$menu;->menu_storage:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 80
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 81
    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 266
    const/4 v0, 0x0

    iput v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    .line 267
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    .line 268
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->ak:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ROOT_FOLDER"

    iget-object v2, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->an:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 269
    invoke-direct {p0, p1}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    .line 270
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 85
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->menu_storage_folder:I

    if-ne v0, v1, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->m()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;

    invoke-direct {v1}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;-><init>()V

    .line 88
    const-string v2, "tag"

    invoke-virtual {v1, v0, v2}, Lcom/cgollner/systemmonitor/systemfragments/ChooseFolderFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 93
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 90
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->replay:I

    if-ne v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    .line 251
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;

    .line 252
    iget-object v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "../"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    iput v3, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    .line 254
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    .line 262
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v1, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    iput v3, p0, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->c:I

    .line 258
    iget-object v0, v0, Lcom/cgollner/systemmonitor/backend/StorageAnalyzer$FileItem;->a:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->b(Ljava/io/File;)V

    goto :goto_0

    .line 260
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/systemfragments/StorageFragment;->a()V

    goto :goto_0
.end method
