.class public Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field private final b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->c:Landroid/view/LayoutInflater;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    .line 32
    iput p2, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->d:I

    .line 33
    iput-object p1, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->b:Landroid/content/Context;

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 47
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    .line 52
    .line 53
    if-nez p2, :cond_0

    .line 54
    iget-object v0, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 60
    :cond_0
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->topAppIcon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->topAppName:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 62
    sget v2, Lcom/cgollner/systemmonitor/lib/R$id;->topAppUsageCpu:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 63
    sget v3, Lcom/cgollner/systemmonitor/lib/R$id;->topAppUsageRam:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 64
    sget v4, Lcom/cgollner/systemmonitor/lib/R$id;->topAppUsageNet:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 65
    sget v5, Lcom/cgollner/systemmonitor/lib/R$id;->topAppProcessName:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 67
    iget-object v6, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cgollner/systemmonitor/systemfragments/App;

    .line 68
    iget-object v7, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->a:Ljava/util/List;

    monitor-enter v7

    .line 69
    if-eqz v0, :cond_1

    .line 70
    :try_start_0
    iget-object v8, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v8, :cond_7

    .line 71
    iget-object v8, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 84
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    iget-object v0, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_2
    if-eqz v2, :cond_3

    const-string v0, "%.1f%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->c:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :cond_3
    if-eqz v3, :cond_4

    iget-wide v0, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->e:J

    invoke-static {v0, v1}, Lcom/cgollner/systemmonitor/backend/StringUtils;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :cond_4
    if-eqz v4, :cond_5

    iget-wide v0, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->f:J

    long-to-double v0, v0

    sget v2, Lcom/cgollner/systemmonitor/systemfragments/ProcessesMonitor;->a:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/cgollner/systemmonitor/backend/StringUtils;->a(DJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_5
    if-eqz v5, :cond_8

    iget-object v0, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->g:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 89
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 91
    iget-object v0, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->g:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :cond_6
    :goto_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    new-instance v0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter$1;

    invoke-direct {v0, p0, v6}, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter$1;-><init>(Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;Lcom/cgollner/systemmonitor/systemfragments/App;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-object p2

    .line 74
    :cond_7
    :try_start_1
    iget-object v8, p0, Lcom/cgollner/systemmonitor/systemfragments/TopAppsAdapter;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 76
    :try_start_2
    iget-object v9, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->b:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 77
    invoke-virtual {v9, v8}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->d:Landroid/graphics/drawable/Drawable;

    .line 78
    iget-object v8, v6, Lcom/cgollner/systemmonitor/systemfragments/App;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 79
    :catch_0
    move-exception v8

    .line 80
    const v8, 0x1080093

    :try_start_3
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 92
    :cond_8
    if-eqz v5, :cond_6

    .line 93
    const/16 v0, 0x8

    :try_start_4
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
