.class public Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ViewPager$PageTransformer;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static a:F

.field private static b:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const v0, 0x3f59999a    # 0.85f

    sput v0, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->a:F

    .line 12
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->b:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;F)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 15
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 48
    :goto_0
    return-void

    .line 17
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 18
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 20
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v2, p2, v2

    if-gez v2, :cond_1

    .line 22
    invoke-virtual {p1, v6}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 24
    :cond_1
    cmpg-float v2, p2, v4

    if-gtz v2, :cond_3

    .line 26
    sget v2, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->a:F

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float v3, v4, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 27
    int-to-float v1, v1

    sub-float v3, v4, v2

    mul-float/2addr v1, v3

    div-float/2addr v1, v5

    .line 28
    int-to-float v0, v0

    sub-float v3, v4, v2

    mul-float/2addr v0, v3

    div-float/2addr v0, v5

    .line 29
    cmpg-float v3, p2, v6

    if-gez v3, :cond_2

    .line 30
    div-float/2addr v1, v5

    sub-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 36
    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 37
    invoke-virtual {p1, v2}, Landroid/view/View;->setScaleY(F)V

    .line 40
    sget v0, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->b:F

    sget v1, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->a:F

    sub-float v1, v2, v1

    sget v2, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->a:F

    sub-float v2, v4, v2

    div-float/2addr v1, v2

    sget v2, Lcom/cgollner/systemmonitor/transformers/ZoomOutPageTransformer;->b:F

    sub-float v2, v4, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 32
    :cond_2
    neg-float v0, v0

    div-float/2addr v1, v5

    add-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 46
    :cond_3
    invoke-virtual {p1, v6}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
