.class Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$1;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 209
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$1;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 210
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetProvider;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetProvider;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 213
    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v3, "appWidgetIds"

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 216
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 217
    return-void
.end method
