.class Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;


# direct methods
.method constructor <init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->e(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lcom/cgollner/systemmonitor/BatteryCircleView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setOuterCircleWidth(I)V

    .line 255
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->e(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lcom/cgollner/systemmonitor/BatteryCircleView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->invalidate()V

    .line 256
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;->a:Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;

    invoke-static {v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->f(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Outer circle width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 250
    return-void
.end method
