.class public Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/View;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:[Landroid/view/View;

.field private H:I

.field private I:Landroid/view/View;

.field private J:I

.field private K:Landroid/view/View$OnClickListener;

.field private L:Landroid/view/View$OnClickListener;

.field private M:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private N:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private O:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private P:Landroid/view/View$OnClickListener;

.field private Q:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private R:Landroid/view/View$OnClickListener;

.field private S:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private T:Landroid/view/View$OnClickListener;

.field private U:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

.field private n:Lcom/cgollner/systemmonitor/BatteryCircleView;

.field private o:Landroid/widget/SeekBar;

.field private p:Landroid/widget/SeekBar;

.field private q:Landroid/widget/SeekBar;

.field private r:Landroid/widget/SeekBar;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 231
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$2;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$2;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->K:Landroid/view/View$OnClickListener;

    .line 239
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$3;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$3;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->L:Landroid/view/View$OnClickListener;

    .line 249
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$4;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->M:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 260
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$5;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$5;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->N:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 270
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$6;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$6;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->O:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 280
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$7;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$7;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->P:Landroid/view/View$OnClickListener;

    .line 291
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$8;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$8;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->Q:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    .line 301
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$9;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$9;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->R:Landroid/view/View$OnClickListener;

    .line 310
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$10;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$10;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->S:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    .line 320
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$11;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$11;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->T:Landroid/view/View$OnClickListener;

    .line 330
    new-instance v0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$12;

    invoke-direct {v0, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$12;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->U:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-void
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    return v0
.end method

.method static synthetic a(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    return p1
.end method

.method static synthetic b(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)[Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->m()V

    return-void
.end method

.method static synthetic d(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    return v0
.end method

.method static synthetic e(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lcom/cgollner/systemmonitor/BatteryCircleView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    return-object v0
.end method

.method static synthetic f(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->s:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->u:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->Q:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method static synthetic i(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->E:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->S:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method static synthetic k(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->F:Landroid/view/View;

    return-object v0
.end method

.method private k()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    .line 164
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-textColor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setTextColor(I)V

    .line 166
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleWidth(I)V

    .line 167
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleColor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, -0x1000000

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleColor(I)V

    .line 168
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-outerCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setOuterCircleWidth(I)V

    .line 169
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-percentageCircleWidth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setPercentageCircleWidth(I)V

    .line 170
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-innerCircleBG"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setInnerCircleBgColor(I)V

    .line 173
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->E:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 174
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->u:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Inner circle width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->F:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 176
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->s:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Outer circle width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getOuterCircleWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->w:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleBgColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 178
    return-void
.end method

.method static synthetic l(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->U:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 204
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$1;

    invoke-direct {v1, p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration$1;-><init>(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 219
    return-void
.end method

.method static synthetic m(Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->w:Landroid/view/View;

    return-object v0
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 221
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 222
    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    if-ne v0, v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    aget-object v2, v2, v0

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 230
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->battery_circle_layout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->setContentView(I)V

    .line 53
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->batteryView:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cgollner/systemmonitor/BatteryCircleView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    .line 56
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->percentageSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->q:Landroid/widget/SeekBar;

    .line 57
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->q:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 60
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1, v0}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->percentageText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->t:Landroid/widget/TextView;

    .line 65
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->percentageLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->z:Landroid/view/View;

    .line 67
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->nextButton:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->C:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->C:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->prevButton:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->D:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->D:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->L:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->textColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->A:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->A:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->colorSquare:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->E:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->E:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 84
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->E:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->circleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->B:Landroid/view/View;

    .line 88
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->circleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->r:Landroid/widget/SeekBar;

    .line 89
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->r:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 90
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->r:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->O:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 91
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->circleWidthText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->u:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->innerCircleColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->y:Landroid/view/View;

    .line 95
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->colorInnerCircle:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->F:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->F:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 97
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->y:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->F:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->innerCircleBgColorLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->v:Landroid/view/View;

    .line 100
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->colorInnerCircleBg:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->w:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->w:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->v:Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->outerCircleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->x:Landroid/view/View;

    .line 106
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->outerCircleWidthText:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->s:Landroid/widget/TextView;

    .line 107
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->outerCircleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->p:Landroid/widget/SeekBar;

    .line 108
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->p:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->M:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 111
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->percentageCircleWidthLayout:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->I:Landroid/view/View;

    .line 112
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->percentageCircleWidthSeekBar:I

    invoke-virtual {p0, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->o:Landroid/widget/SeekBar;

    .line 113
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->o:Landroid/widget/SeekBar;

    const/16 v1, 0xaf

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 114
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->o:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->N:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 116
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    iget-object v1, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->z:Landroid/view/View;

    aput-object v1, v0, v3

    .line 118
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->A:Landroid/view/View;

    aput-object v2, v0, v1

    .line 119
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->B:Landroid/view/View;

    aput-object v2, v0, v1

    .line 120
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->y:Landroid/view/View;

    aput-object v2, v0, v1

    .line 121
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->v:Landroid/view/View;

    aput-object v2, v0, v1

    .line 122
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->x:Landroid/view/View;

    aput-object v2, v0, v1

    .line 123
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->G:[Landroid/view/View;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->I:Landroid/view/View;

    aput-object v2, v0, v1

    .line 125
    iput v3, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->H:I

    .line 127
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->m()V

    .line 129
    invoke-virtual {p0, v3}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->setResult(I)V

    .line 132
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 135
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    .line 141
    :cond_0
    iget v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    if-nez v0, :cond_1

    .line 142
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->finish()V

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->k()V

    .line 145
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 150
    sget v1, Lcom/cgollner/systemmonitor/lib/R$menu;->activity_configure_battery_widget:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 151
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-textColor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getTextColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getCircleWidthMultiplier()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleColor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-outerCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getOuterCircleWidth()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-percentageCircleWidth"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getPercentageCircleWidth()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-innerCircleBG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    invoke-virtual {v2}, Lcom/cgollner/systemmonitor/BatteryCircleView;->getInnerCircleBgColor()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 189
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 192
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 193
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->J:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->setResult(ILandroid/content/Intent;)V

    .line 198
    invoke-virtual {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->finish()V

    .line 200
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->l()V

    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->n:Lcom/cgollner/systemmonitor/BatteryCircleView;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/cgollner/systemmonitor/BatteryCircleView;->setValue(F)V

    .line 341
    iget-object v0, p0, Lcom/cgollner/systemmonitor/widgets/BatteryWidgetConfiguration;->t:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Percentage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method
