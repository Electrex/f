.class public Lcom/franco/kernel/App;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field public static a:Landroid/content/Context;

.field public static b:Lcom/franco/kernel/abstracts/Device;

.field public static c:Landroid/content/res/Resources;

.field public static d:Landroid/os/Handler;

.field public static e:Z

.field public static f:Lcom/franco/kernel/helpers/ColorHelpers;

.field public static g:I

.field public static h:Lcom/anjlab/android/iab/v3/BillingProcessor;

.field private static i:Ljava/lang/String;

.field private static j:Lde/halfbit/tinybus/TinyBus;

.field private static k:Lcom/path/android/jobqueue/JobManager;

.field private static l:Ljava/lang/String;

.field private static m:Landroid/content/SharedPreferences;

.field private static n:Lcom/loopj/android/http/AsyncHttpClient;

.field private static o:Ljava/util/HashMap;

.field private static p:Lcom/google/example/games/basegameutils/GameHelper;

.field private static q:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lcom/franco/kernel/App$1;

    invoke-direct {v0}, Lcom/franco/kernel/App$1;-><init>()V

    sput-object v0, Lcom/franco/kernel/App;->q:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static a()Lcom/loopj/android/http/AsyncHttpClient;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/franco/kernel/App;->n:Lcom/loopj/android/http/AsyncHttpClient;

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/example/games/basegameutils/GameHelper;)V
    .locals 2

    .prologue
    .line 101
    sput-object p0, Lcom/franco/kernel/App;->p:Lcom/google/example/games/basegameutils/GameHelper;

    .line 102
    sget-object v0, Lcom/franco/kernel/App;->p:Lcom/google/example/games/basegameutils/GameHelper;

    sget-object v1, Lcom/franco/kernel/App;->q:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    invoke-virtual {v0, v1}, Lcom/google/example/games/basegameutils/GameHelper;->a(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V

    .line 103
    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static b(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Landroid/support/v4/content/ContextCompat;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/franco/kernel/App;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static c(I)V
    .locals 0

    .prologue
    .line 134
    sput p0, Lcom/franco/kernel/App;->g:I

    .line 135
    return-void
.end method

.method public static d()Lde/halfbit/tinybus/Bus;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    return-object v0
.end method

.method public static e()Lcom/path/android/jobqueue/JobManager;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/franco/kernel/App;->k:Lcom/path/android/jobqueue/JobManager;

    return-object v0
.end method

.method public static f()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    return-object v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/franco/kernel/App;->l:Ljava/lang/String;

    return-object v0
.end method

.method public static h()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 89
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 90
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_preferences"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    .line 93
    :cond_0
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public static i()Lcom/google/example/games/basegameutils/GameHelper;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/franco/kernel/App;->p:Lcom/google/example/games/basegameutils/GameHelper;

    return-object v0
.end method

.method public static j()Ljava/util/HashMap;
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/franco/kernel/App;->o:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/franco/kernel/helpers/TunableInfoMap;

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, Lcom/franco/kernel/helpers/TunableInfoMap;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/franco/kernel/App;->o:Ljava/util/HashMap;

    .line 110
    :cond_0
    sget-object v0, Lcom/franco/kernel/App;->o:Ljava/util/HashMap;

    return-object v0
.end method

.method public static k()I
    .locals 1

    .prologue
    .line 130
    sget v0, Lcom/franco/kernel/App;->g:I

    return v0
.end method

.method public static l()V
    .locals 3

    .prologue
    .line 162
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    const-string v1, "battery_saver_level"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->a(Ljava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    const-class v1, Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    new-instance v1, Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-direct {v1}, Lde/halfbit/tinybus/wires/BatteryWire;-><init>()V

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->a(Lde/halfbit/tinybus/TinyBus$Wireable;)Lde/halfbit/tinybus/TinyBus;

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->b(Ljava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    const-class v1, Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    sget-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    const-class v1, Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/TinyBus;->a(Ljava/lang/Class;)Lde/halfbit/tinybus/TinyBus$Wireable;

    goto :goto_0
.end method


# virtual methods
.method public a(Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;)V
    .locals 6
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 183
    iget v0, p1, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;->b:I

    .line 184
    sget-object v1, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    const-string v2, "battery_saver_level"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 185
    sget-object v2, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    const-string v3, "low_power_mode_event"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 188
    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    if-lez v0, :cond_2

    if-gt v0, v1, :cond_2

    .line 194
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    const-string v1, "fku.perf.profile"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    if-nez v2, :cond_0

    .line 199
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.franco.kernel.POWER_SAVING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/App;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "low_power_mode_event"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 202
    :cond_2
    if-le v0, v1, :cond_0

    .line 203
    if-eqz v2, :cond_0

    .line 204
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.franco.kernel.BALANCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/App;->sendBroadcast(Landroid/content/Intent;)V

    .line 205
    sget-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "low_power_mode_event"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 225
    invoke-static {p1}, Lcom/franco/kernel/helpers/SecurityHelpers;->a(Landroid/app/Activity;)V

    .line 226
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 140
    invoke-virtual {p0}, Lcom/franco/kernel/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    .line 141
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    .line 142
    invoke-static {}, Lcom/franco/kernel/abstracts/Device;->j()Lcom/franco/kernel/abstracts/Device;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    .line 143
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/franco/kernel/App;->i:Ljava/lang/String;

    .line 144
    sget-object v0, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    invoke-static {v0}, Lde/halfbit/tinybus/TinyBus;->a(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->j:Lde/halfbit/tinybus/TinyBus;

    .line 145
    new-instance v0, Lcom/path/android/jobqueue/JobManager;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    const-string v2, "com.franco.kernel:GenericJobManager"

    invoke-direct {v0, v1, v2}, Lcom/path/android/jobqueue/JobManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/franco/kernel/App;->k:Lcom/path/android/jobqueue/JobManager;

    .line 146
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/franco/kernel/App;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/franco/kernel/App;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/franco.kernel_updater/kernel_backups/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->l:Ljava/lang/String;

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/franco/kernel/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/App;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/App;->m:Landroid/content/SharedPreferences;

    .line 149
    new-instance v0, Lcom/loopj/android/http/AsyncHttpClient;

    invoke-direct {v0}, Lcom/loopj/android/http/AsyncHttpClient;-><init>()V

    sput-object v0, Lcom/franco/kernel/App;->n:Lcom/loopj/android/http/AsyncHttpClient;

    .line 150
    new-instance v0, Lcom/franco/kernel/helpers/TunableInfoMap;

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, Lcom/franco/kernel/helpers/TunableInfoMap;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/franco/kernel/App;->o:Ljava/util/HashMap;

    .line 151
    sput-boolean v4, Lcom/franco/kernel/App;->e:Z

    .line 152
    sget-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a0151

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/franco/kernel/App;->g:I

    .line 153
    new-instance v0, Lcom/anjlab/android/iab/v3/BillingProcessor;

    sget-object v1, Lcom/franco/kernel/App;->a:Landroid/content/Context;

    const v2, 0x7f07019c

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/anjlab/android/iab/v3/BillingProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;)V

    sput-object v0, Lcom/franco/kernel/App;->h:Lcom/anjlab/android/iab/v3/BillingProcessor;

    .line 154
    invoke-static {v4}, Leu/chainfire/libsuperuser/Debug;->a(Z)V

    .line 156
    invoke-static {}, Lcom/franco/kernel/App;->l()V

    .line 158
    invoke-virtual {p0, p0}, Lcom/franco/kernel/App;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 159
    return-void
.end method
