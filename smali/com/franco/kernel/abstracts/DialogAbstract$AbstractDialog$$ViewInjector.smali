.class public Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x1020019

    const v5, 0x1020015

    const v2, 0x1020014

    const v4, 0x1020003

    const/4 v3, 0x0

    .line 11
    invoke-virtual {p1, p3, v2, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'tvTitle\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvTitle:Landroid/widget/TextView;

    .line 13
    invoke-virtual {p1, p3, v5, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'tvSummary\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvSummary:Landroid/widget/TextView;

    .line 15
    invoke-virtual {p1, p3, v6, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'bButton1\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton1:Landroid/widget/Button;

    .line 17
    const v0, 0x102001a

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const v1, 0x102001a

    const-string v2, "field \'bButton2\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton2:Landroid/widget/Button;

    .line 19
    invoke-virtual {p1, p3, v4, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    const-string v1, "field \'mEditText\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mEditText:Landroid/widget/EditText;

    .line 21
    const v0, 0x7f0c00c6

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    iput-object v0, p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mUnderLine:Landroid/view/View;

    .line 23
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvTitle:Landroid/widget/TextView;

    .line 27
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvSummary:Landroid/widget/TextView;

    .line 28
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton1:Landroid/widget/Button;

    .line 29
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton2:Landroid/widget/Button;

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mEditText:Landroid/widget/EditText;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mUnderLine:Landroid/view/View;

    .line 32
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$$ViewInjector;->reset(Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;)V

    return-void
.end method
