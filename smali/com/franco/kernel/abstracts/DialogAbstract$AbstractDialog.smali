.class public Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field protected bButton1:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020019
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected bButton2:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x102001a
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020003
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mUnderLine:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00c6
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected tvSummary:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020015
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected tvTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020014
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 109
    new-instance v0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$2;-><init>(Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;)V

    iput-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->a:Landroid/view/View$OnClickListener;

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->requestWindowFeature(I)Z

    .line 48
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->tvSummary:Landroid/widget/TextView;

    return-object v0
.end method

.method public c()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton1:Landroid/widget/Button;

    return-object v0
.end method

.method public d()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton2:Landroid/widget/Button;

    return-object v0
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 119
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public e()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Dialog;)V

    .line 55
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton1:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton1:Landroid/widget/Button;

    iget-object v1, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton2:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->bButton2:Landroid/widget/Button;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog$1;-><init>(Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 87
    :cond_2
    return-void
.end method
