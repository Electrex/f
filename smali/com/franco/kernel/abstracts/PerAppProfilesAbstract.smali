.class public Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5

    .prologue
    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0701a2

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0701b1

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0700c0

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    return-object v0
.end method

.method public b()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->h()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;

    move-result-object v0

    return-object v0
.end method
