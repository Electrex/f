.class Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/viewholders/AppEntryViewHolder;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

.field final synthetic d:Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;Lcom/franco/kernel/viewholders/AppEntryViewHolder;Ljava/util/List;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->d:Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;

    iput-object p2, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->a:Lcom/franco/kernel/viewholders/AppEntryViewHolder;

    iput-object p3, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->b:Ljava/util/List;

    iput-object p4, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->c:Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    .line 130
    invoke-static {}, Lcom/franco/kernel/activities/ApplicationsList;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 131
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->a:Lcom/franco/kernel/viewholders/AppEntryViewHolder;

    iget-object v0, v0, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->b:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 133
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/per-app_profiles/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {v3, v1, v0}, Lorg/apache/commons/io/FileUtils;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->c:Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->dismiss()V

    .line 141
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1$1;->d:Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;->a:Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->notifyDataSetChanged()V

    .line 142
    return-void

    .line 137
    :catch_0
    move-exception v0

    goto :goto_0
.end method
