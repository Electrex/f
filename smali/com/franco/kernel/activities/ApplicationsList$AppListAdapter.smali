.class Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# static fields
.field private static e:Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:I

.field private c:Landroid/view/View$OnClickListener;

.field private d:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$3;

    invoke-direct {v0}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$3;-><init>()V

    sput-object v0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->e:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 118
    new-instance v0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$1;-><init>(Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->c:Landroid/view/View$OnClickListener;

    .line 153
    new-instance v0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter$2;-><init>(Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->d:Landroid/view/View$OnClickListener;

    .line 83
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->a:Landroid/view/LayoutInflater;

    .line 84
    iput p2, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->b:I

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/franco/kernel/activities/ApplicationsList$1;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 89
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->b:I

    invoke-static {v0, p2, v1}, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->a(Landroid/view/LayoutInflater;Landroid/view/View;I)Lcom/franco/kernel/viewholders/AppEntryViewHolder;

    move-result-object v1

    .line 91
    iget-object v2, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->a:Landroid/view/View;

    .line 93
    invoke-static {}, Lcom/franco/kernel/activities/ApplicationsList;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/helpers/AppEntry;

    .line 94
    invoke-virtual {v0}, Lcom/franco/kernel/helpers/AppEntry;->a()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 96
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/franco/kernel/helpers/AppEntry;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->c:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/franco/kernel/helpers/PerAppProfileHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 99
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 102
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->f:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    :goto_0
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/franco/kernel/helpers/AppEntry;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 109
    iget-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 110
    iget-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->e:Landroid/widget/ImageView;

    sget-object v3, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    return-object v2

    .line 104
    :cond_0
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v4, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
