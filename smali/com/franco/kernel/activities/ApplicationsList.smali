.class public Lcom/franco/kernel/activities/ApplicationsList;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field private static n:Ljava/util/List;

.field private static o:Landroid/app/Activity;


# instance fields
.field protected mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field protected mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 77
    return-void
.end method

.method static synthetic k()Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/franco/kernel/activities/ApplicationsList;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic l()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/franco/kernel/activities/ApplicationsList;->o:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Loader;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 193
    sput-object p2, Lcom/franco/kernel/activities/ApplicationsList;->n:Ljava/util/List;

    .line 194
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;

    sget-object v2, Lcom/franco/kernel/activities/ApplicationsList;->o:Landroid/app/Activity;

    const v3, 0x7f040020

    sget-object v4, Lcom/franco/kernel/activities/ApplicationsList;->n:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/franco/kernel/activities/ApplicationsList$AppListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/franco/kernel/activities/ApplicationsList$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 195
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 59
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 61
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f04004a

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/ApplicationsList;->setContentView(I)V

    .line 63
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 65
    sput-object p0, Lcom/franco/kernel/activities/ApplicationsList;->o:Landroid/app/Activity;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/franco/kernel/activities/ApplicationsList;->n:Ljava/util/List;

    .line 68
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/ApplicationsList;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 69
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 70
    iget-object v0, p0, Lcom/franco/kernel/activities/ApplicationsList;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/ApplicationsList;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 71
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/ApplicationsList;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 72
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/ApplicationsList;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 74
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0xfff

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 75
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 204
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 238
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 239
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 240
    const/4 v0, 0x0

    sput-object v0, Lcom/franco/kernel/activities/ApplicationsList;->o:Landroid/app/Activity;

    .line 241
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/activities/ApplicationsList;->a(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 212
    sparse-switch v2, :sswitch_data_0

    .line 233
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 214
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->finish()V

    goto :goto_0

    .line 217
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/ApplicationsList;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 221
    :sswitch_2
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "system_apps"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 224
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "system_apps"

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 227
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ApplicationsList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v2, 0xfff

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Loader;->forceLoad()V

    goto :goto_0

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01a2 -> :sswitch_2
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method
