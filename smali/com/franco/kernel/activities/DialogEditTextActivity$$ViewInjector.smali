.class public Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/DialogEditTextActivity;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x102001a

    const v3, 0x1020014

    const v2, 0x1020003

    .line 11
    const-string v0, "field \'mTitle\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mTitle\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/DialogEditTextActivity;->mTitle:Landroid/widget/TextView;

    .line 13
    const-string v0, "field \'mEditText\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mEditText\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    .line 15
    const-string v0, "field \'mBackup\' and method \'onBackupClick\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mBackup\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/franco/kernel/activities/DialogEditTextActivity;->mBackup:Landroid/widget/Button;

    .line 17
    new-instance v1, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector$1;-><init>(Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector;Lcom/franco/kernel/activities/DialogEditTextActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    const v0, 0x7f0c00c6

    const-string v1, "field \'mUnderLine\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    iput-object v0, p2, Lcom/franco/kernel/activities/DialogEditTextActivity;->mUnderLine:Landroid/view/View;

    .line 27
    const v0, 0x1020019

    const-string v1, "method \'onCancelClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 28
    new-instance v1, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector$2;-><init>(Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector;Lcom/franco/kernel/activities/DialogEditTextActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/DialogEditTextActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/DialogEditTextActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/DialogEditTextActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mTitle:Landroid/widget/TextView;

    .line 40
    iput-object v0, p1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    .line 41
    iput-object v0, p1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mBackup:Landroid/widget/Button;

    .line 42
    iput-object v0, p1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mUnderLine:Landroid/view/View;

    .line 43
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/DialogEditTextActivity;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/DialogEditTextActivity$$ViewInjector;->reset(Lcom/franco/kernel/activities/DialogEditTextActivity;)V

    return-void
.end method
