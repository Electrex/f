.class Lcom/franco/kernel/activities/DialogEditTextActivity$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/DialogEditTextActivity;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/DialogEditTextActivity;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/franco/kernel/activities/DialogEditTextActivity$1;->a:Lcom/franco/kernel/activities/DialogEditTextActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 53
    if-eqz p2, :cond_2

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v0

    .line 54
    :goto_0
    iget-object v1, p0, Lcom/franco/kernel/activities/DialogEditTextActivity$1;->a:Lcom/franco/kernel/activities/DialogEditTextActivity;

    iget-object v1, v1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mUnderLine:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/franco/kernel/activities/DialogEditTextActivity$1;->a:Lcom/franco/kernel/activities/DialogEditTextActivity;

    iget-object v1, v1, Lcom/franco/kernel/activities/DialogEditTextActivity;->mUnderLine:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 58
    :cond_0
    if-eqz p2, :cond_1

    .line 59
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity$1;->a:Lcom/franco/kernel/activities/DialogEditTextActivity;

    iget-object v0, v0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 61
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity$1;->a:Lcom/franco/kernel/activities/DialogEditTextActivity;

    iget-object v0, v0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    const/16 v1, 0x1002

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 67
    :cond_1
    return-void

    .line 53
    :cond_2
    const-string v0, "#12000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
