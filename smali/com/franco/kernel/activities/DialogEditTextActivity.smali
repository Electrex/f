.class public Lcom/franco/kernel/activities/DialogEditTextActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field protected mBackup:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x102001a
    .end annotation
.end field

.field protected mEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020003
    .end annotation
.end field

.field protected mTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020014
    .end annotation
.end field

.field protected mUnderLine:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00c6
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public onBackupClick(Landroid/view/View;)V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x102001a
        }
    .end annotation

    .prologue
    .line 80
    return-void
.end method

.method public onCancelClick(Landroid/view/View;)V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x1020019
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/franco/kernel/activities/DialogEditTextActivity;->finish()V

    .line 75
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f040039

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/DialogEditTextActivity;->setContentView(I)V

    .line 43
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 45
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f070045

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 46
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    invoke-static {}, Lcom/franco/kernel/helpers/KernelVersionHelpers;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mBackup:Landroid/widget/Button;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 49
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/franco/kernel/activities/DialogEditTextActivity;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/franco/kernel/activities/DialogEditTextActivity$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/DialogEditTextActivity$1;-><init>(Lcom/franco/kernel/activities/DialogEditTextActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 70
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 84
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 86
    return-void
.end method
