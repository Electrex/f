.class public Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/InAppPurchasesActivity;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0c00d3

    const v5, 0x7f0c00d1

    const v4, 0x7f0c00cf

    const v3, 0x7f0c00cd

    const v2, 0x7f0c00cb

    .line 11
    const v0, 0x7f0c00c8

    const-string v1, "field \'mDonationPackages\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mDonationPackages:Landroid/view/View;

    .line 13
    const-string v0, "field \'mChocolateBarPrice\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mChocolateBarPrice\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarPrice:Landroid/widget/TextView;

    .line 15
    const-string v0, "field \'mBeerPrice\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mBeerPrice\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerPrice:Landroid/widget/TextView;

    .line 17
    const-string v0, "field \'mFkuPrice\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const-string v1, "field \'mFkuPrice\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuPrice:Landroid/widget/TextView;

    .line 19
    const-string v0, "field \'mMealPrice\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    const-string v1, "field \'mMealPrice\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealPrice:Landroid/widget/TextView;

    .line 21
    const-string v0, "field \'mBigMealPrice\'"

    invoke-virtual {p1, p3, v6, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    const-string v1, "field \'mBigMealPrice\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealPrice:Landroid/widget/TextView;

    .line 23
    const v0, 0x7f0c00ca

    const-string v1, "field \'mChocolateBarDonation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 24
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarDonation:Landroid/view/View;

    .line 25
    const v0, 0x7f0c00cc

    const-string v1, "field \'mBeerDonation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerDonation:Landroid/view/View;

    .line 27
    const v0, 0x7f0c00ce

    const-string v1, "field \'mFkuDonation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 28
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuDonation:Landroid/view/View;

    .line 29
    const v0, 0x7f0c00d0

    const-string v1, "field \'mMealDonation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 30
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealDonation:Landroid/view/View;

    .line 31
    const v0, 0x7f0c00d2

    const-string v1, "field \'mBigMealDonation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 32
    iput-object v0, p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealDonation:Landroid/view/View;

    .line 33
    const v0, 0x7f0c00c9

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 34
    new-instance v1, Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector$1;-><init>(Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector;Lcom/franco/kernel/activities/InAppPurchasesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/InAppPurchasesActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/InAppPurchasesActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mDonationPackages:Landroid/view/View;

    .line 46
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarPrice:Landroid/widget/TextView;

    .line 47
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerPrice:Landroid/widget/TextView;

    .line 48
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuPrice:Landroid/widget/TextView;

    .line 49
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealPrice:Landroid/widget/TextView;

    .line 50
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealPrice:Landroid/widget/TextView;

    .line 51
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarDonation:Landroid/view/View;

    .line 52
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerDonation:Landroid/view/View;

    .line 53
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuDonation:Landroid/view/View;

    .line 54
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealDonation:Landroid/view/View;

    .line 55
    iput-object v0, p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealDonation:Landroid/view/View;

    .line 56
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/InAppPurchasesActivity$$ViewInjector;->reset(Lcom/franco/kernel/activities/InAppPurchasesActivity;)V

    return-void
.end method
