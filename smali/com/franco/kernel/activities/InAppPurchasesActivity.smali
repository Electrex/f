.class public Lcom/franco/kernel/activities/InAppPurchasesActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;


# instance fields
.field private a:Lcom/anjlab/android/iab/v3/BillingProcessor;

.field private b:Landroid/view/View$OnClickListener;

.field protected mBeerDonation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00cc
    .end annotation
.end field

.field protected mBeerPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00cd
    .end annotation
.end field

.field protected mBigMealDonation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d2
    .end annotation
.end field

.field protected mBigMealPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d3
    .end annotation
.end field

.field protected mChocolateBarDonation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ca
    .end annotation
.end field

.field protected mChocolateBarPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00cb
    .end annotation
.end field

.field protected mDonationPackages:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00c8
    .end annotation
.end field

.field protected mFkuDonation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ce
    .end annotation
.end field

.field protected mFkuPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00cf
    .end annotation
.end field

.field protected mMealDonation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d0
    .end annotation
.end field

.field protected mMealPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d1
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 142
    new-instance v0, Lcom/franco/kernel/activities/InAppPurchasesActivity$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/InAppPurchasesActivity$1;-><init>(Lcom/franco/kernel/activities/InAppPurchasesActivity;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    .line 155
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/InAppPurchasesActivity;)Lcom/anjlab/android/iab/v3/BillingProcessor;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public a(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 105
    packed-switch p1, :pswitch_data_0

    .line 111
    :goto_0
    :pswitch_0
    return-void

    .line 108
    :pswitch_1
    const v0, 0x7f070102

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/TransactionDetails;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 74
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    iget-object v2, p2, Lcom/anjlab/android/iab/v3/TransactionDetails;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->c(Ljava/lang/String;)Z

    .line 76
    const v0, 0x7f070185

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/InAppPurchasesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 79
    iget-object v2, p2, Lcom/anjlab/android/iab/v3/TransactionDetails;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 97
    :goto_1
    return-void

    .line 79
    :sswitch_0
    const-string v1, "chocolate_bar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "because_beer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string v1, "fku"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "meal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "big_meal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 81
    :pswitch_0
    const-string v0, "CgkIo_DA4eIIEAIQHQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 84
    :pswitch_1
    const-string v0, "CgkIo_DA4eIIEAIQGQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 87
    :pswitch_2
    const-string v0, "CgkIo_DA4eIIEAIQGg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :pswitch_3
    const-string v0, "CgkIo_DA4eIIEAIQGw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 93
    :pswitch_4
    const-string v0, "CgkIo_DA4eIIEAIQHA"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :sswitch_data_0
    .sparse-switch
        0x18c50 -> :sswitch_2
        0x3313c3 -> :sswitch_3
        0x2bd26fa2 -> :sswitch_4
        0x45a906c9 -> :sswitch_1
        0x57fc8f90 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public b()V
    .locals 3

    .prologue
    .line 115
    const-string v0, "billing init"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarPrice:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v2, "chocolate_bar"

    invoke-virtual {v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/anjlab/android/iab/v3/SkuDetails;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerPrice:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v2, "because_beer"

    invoke-virtual {v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/anjlab/android/iab/v3/SkuDetails;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuPrice:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v2, "fku"

    invoke-virtual {v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/anjlab/android/iab/v3/SkuDetails;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealPrice:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v2, "meal"

    invoke-virtual {v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/anjlab/android/iab/v3/SkuDetails;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealPrice:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v2, "big_meal"

    invoke-virtual {v1, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->d(Ljava/lang/String;)Lcom/anjlab/android/iab/v3/SkuDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/anjlab/android/iab/v3/SkuDetails;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mChocolateBarDonation:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBeerDonation:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mFkuDonation:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mMealDonation:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mBigMealDonation:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 140
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c00c9
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/franco/kernel/activities/InAppPurchasesActivity$HelpDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 153
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 58
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v0, 0x7f04003b

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/InAppPurchasesActivity;->setContentView(I)V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 64
    invoke-virtual {p0}, Lcom/franco/kernel/activities/InAppPurchasesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 67
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->mDonationPackages:Landroid/view/View;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 69
    new-instance v0, Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07019c

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    .line 70
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/franco/kernel/activities/InAppPurchasesActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->c()V

    .line 170
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 171
    return-void
.end method
