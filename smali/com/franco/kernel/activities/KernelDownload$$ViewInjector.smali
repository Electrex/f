.class public Lcom/franco/kernel/activities/KernelDownload$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/KernelDownload;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const v3, 0x7f0c0196

    const v2, 0x7f0c0051

    .line 11
    const-string v0, "field \'mToolbar\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mToolbar\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p2, Lcom/franco/kernel/activities/KernelDownload;->mToolbar:Landroid/support/v7/widget/Toolbar;

    .line 13
    const-string v0, "field \'mChangelogLayout\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mChangelogLayout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p2, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    .line 15
    const/high16 v0, 0x1020000

    const-string v1, "field \'mBackgroundView\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    iput-object v0, p2, Lcom/franco/kernel/activities/KernelDownload;->mBackgroundView:Landroid/view/View;

    .line 17
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/KernelDownload;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/KernelDownload$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/KernelDownload;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/KernelDownload;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    iput-object v0, p1, Lcom/franco/kernel/activities/KernelDownload;->mToolbar:Landroid/support/v7/widget/Toolbar;

    .line 21
    iput-object v0, p1, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    .line 22
    iput-object v0, p1, Lcom/franco/kernel/activities/KernelDownload;->mBackgroundView:Landroid/view/View;

    .line 23
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/KernelDownload;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/KernelDownload$$ViewInjector;->reset(Lcom/franco/kernel/activities/KernelDownload;)V

    return-void
.end method
