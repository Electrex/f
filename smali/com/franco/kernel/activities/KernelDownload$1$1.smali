.class Lcom/franco/kernel/activities/KernelDownload$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/KernelDownload$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/KernelDownload$1;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/high16 v3, 0x41c00000    # 24.0f

    .line 68
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    new-instance v1, Lcom/franco/kernel/changeloglib/ChangeLogListView;

    iget-object v2, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v2, v2, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    invoke-direct {v1, v2}, Lcom/franco/kernel/changeloglib/ChangeLogListView;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/franco/kernel/activities/KernelDownload;->a(Lcom/franco/kernel/activities/KernelDownload;Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 69
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v1, v1, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    invoke-static {v1}, Lcom/franco/kernel/activities/KernelDownload;->a(Lcom/franco/kernel/activities/KernelDownload;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 75
    invoke-static {v3}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {v2}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v2

    invoke-static {v3}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v3

    const/high16 v4, 0x42c00000    # 96.0f

    invoke-static {v4}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 79
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 81
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    invoke-static {v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Landroid/app/Activity;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    const v0, 0x7f02007a

    :goto_1
    invoke-virtual {v1, v0}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(I)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v1, v1, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    invoke-static {v1}, Lcom/franco/kernel/activities/KernelDownload;->a(Lcom/franco/kernel/activities/KernelDownload;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(Landroid/widget/AbsListView;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/KernelDownload$1$1$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/KernelDownload$1$1$1;-><init>(Lcom/franco/kernel/activities/KernelDownload$1$1;)V

    invoke-virtual {v0, v1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(Landroid/view/View$OnClickListener;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a()Lcom/franco/kernel/views/FloatingActionButton;

    goto :goto_0

    :cond_1
    const v0, 0x7f02007b

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1$1;->a:Lcom/franco/kernel/activities/KernelDownload$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 64
    return-void
.end method
