.class Lcom/franco/kernel/activities/KernelDownload$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/KernelDownload;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/KernelDownload;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v0, v0, Lcom/franco/kernel/activities/KernelDownload;->mBackgroundView:Landroid/view/View;

    const-string v1, "scaleY"

    new-array v2, v5, [F

    const/high16 v3, 0x43900000    # 288.0f

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 51
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 53
    iget-object v1, p0, Lcom/franco/kernel/activities/KernelDownload$1;->a:Lcom/franco/kernel/activities/KernelDownload;

    iget-object v1, v1, Lcom/franco/kernel/activities/KernelDownload;->mChangelogLayout:Landroid/widget/FrameLayout;

    const-string v2, "y"

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 56
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 58
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 59
    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 60
    new-instance v0, Lcom/franco/kernel/activities/KernelDownload$1$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/KernelDownload$1$1;-><init>(Lcom/franco/kernel/activities/KernelDownload$1;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 106
    :try_start_0
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    goto :goto_0

    .line 53
    :array_0
    .array-data 4
        -0x3b860000    # -1000.0f
        0x0
    .end array-data
.end method
