.class public Lcom/franco/kernel/activities/KernelDownload;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# static fields
.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;


# instance fields
.field protected mBackgroundView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020000
    .end annotation
.end field

.field protected mChangelogLayout:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0051
    .end annotation
.end field

.field protected mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field

.field private n:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/KernelDownload;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload;->n:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/activities/KernelDownload;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/franco/kernel/activities/KernelDownload;->n:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/franco/kernel/activities/KernelDownload;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/franco/kernel/activities/KernelDownload;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onBackPressed()V

    .line 135
    const v0, 0x7f050011

    const v1, 0x7f050010

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/activities/KernelDownload;->overridePendingTransition(II)V

    .line 136
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 115
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 117
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    const v0, 0x7f040018

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/KernelDownload;->setContentView(I)V

    .line 119
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 121
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/KernelDownload;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 122
    invoke-virtual {p0}, Lcom/franco/kernel/activities/KernelDownload;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 123
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/KernelDownload;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 124
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/KernelDownload;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 125
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload;->mToolbar:Landroid/support/v7/widget/Toolbar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 126
    iget-object v0, p0, Lcom/franco/kernel/activities/KernelDownload;->mBackgroundView:Landroid/view/View;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 128
    invoke-virtual {p0}, Lcom/franco/kernel/activities/KernelDownload;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "new_kernel"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/KernelDownload;->o:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Lcom/franco/kernel/activities/KernelDownload;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "kernel_version"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/KernelDownload;->p:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/franco/kernel/activities/KernelDownload;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 164
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 165
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 166
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 146
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 148
    sparse-switch v1, :sswitch_data_0

    .line 159
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 150
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/KernelDownload;->finish()V

    .line 151
    const v1, 0x7f050011

    const v2, 0x7f050010

    invoke-virtual {p0, v1, v2}, Lcom/franco/kernel/activities/KernelDownload;->overridePendingTransition(II)V

    goto :goto_0

    .line 154
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/KernelDownload;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 148
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/KernelDownload$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/KernelDownload$1;-><init>(Lcom/franco/kernel/activities/KernelDownload;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 111
    return-void
.end method
