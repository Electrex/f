.class public Lcom/franco/kernel/activities/MainActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/MainActivity;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const v2, 0x7f0c0057

    .line 11
    const/4 v0, 0x0

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mDrawerLayout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p2, Lcom/franco/kernel/activities/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 13
    const v0, 0x7f0c0059

    const-string v1, "field \'mNavigationDrawerView\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    iput-object v0, p2, Lcom/franco/kernel/activities/MainActivity;->mNavigationDrawerView:Landroid/view/View;

    .line 15
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/MainActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/MainActivity$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/MainActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/MainActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    iput-object v0, p1, Lcom/franco/kernel/activities/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 19
    iput-object v0, p1, Lcom/franco/kernel/activities/MainActivity;->mNavigationDrawerView:Landroid/view/View;

    .line 20
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/MainActivity;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/MainActivity$$ViewInjector;->reset(Lcom/franco/kernel/activities/MainActivity;)V

    return-void
.end method
