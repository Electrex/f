.class Lcom/franco/kernel/activities/MainActivity$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/franco/kernel/activities/MainActivity;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/MainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    iput-object p2, p0, Lcom/franco/kernel/activities/MainActivity$4;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 176
    invoke-static {}, Lcom/franco/kernel/activities/MainActivity;->k()Ljava/util/LinkedHashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 178
    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    const v3, 0x7f0702a1

    invoke-virtual {v2, v3}, Lcom/franco/kernel/activities/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    const-string v1, "com.cgollner.systemmonitor"

    invoke-static {v1}, Lcom/franco/kernel/helpers/PackageHelpers;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    invoke-virtual {v1}, Lcom/franco/kernel/activities/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.cgollner.systemmonitor"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/franco/kernel/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 186
    :cond_2
    invoke-static {}, Lcom/franco/kernel/activities/MainActivity;->l()Landroid/app/FragmentManager;

    move-result-object v1

    if-nez v1, :cond_3

    .line 187
    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    invoke-virtual {v1}, Lcom/franco/kernel/activities/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/activities/MainActivity;->a(Landroid/app/FragmentManager;)Landroid/app/FragmentManager;

    .line 190
    :cond_3
    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity$4;->b:Lcom/franco/kernel/activities/MainActivity;

    invoke-static {v1}, Lcom/franco/kernel/helpers/LifecycleHelpers;->a(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    :try_start_0
    invoke-static {}, Lcom/franco/kernel/activities/MainActivity;->l()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0c0058

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    goto :goto_0
.end method
