.class public Lcom/franco/kernel/activities/MainActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# static fields
.field public static n:Landroid/support/v7/widget/Toolbar;

.field public static o:Ljava/lang/String;

.field public static p:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

.field private static s:Landroid/app/FragmentManager;

.field private static t:Ljava/util/LinkedHashMap;


# instance fields
.field protected mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0057
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mNavigationDrawerView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0059
    .end annotation
.end field

.field private q:Landroid/support/v7/app/ActionBarDrawerToggle;

.field private r:Lcom/franco/kernel/in_app_billing/Licensing;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    .line 78
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f070122

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f070046

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f0700c1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f070189

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f07010d

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f070096

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f070200

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701f8

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702a4

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/system_monitor/MainActivityMonitor;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f07028c

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/SettingsActivity;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    const v1, 0x7f07029e

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/SupportActivity;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 227
    return-void
.end method

.method static synthetic a(Landroid/app/FragmentManager;)Landroid/app/FragmentManager;
    .locals 0

    .prologue
    .line 52
    sput-object p0, Lcom/franco/kernel/activities/MainActivity;->s:Landroid/app/FragmentManager;

    return-object p0
.end method

.method static synthetic a(Lcom/franco/kernel/activities/MainActivity;Lcom/franco/kernel/in_app_billing/Licensing;)Lcom/franco/kernel/in_app_billing/Licensing;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/franco/kernel/activities/MainActivity;->r:Lcom/franco/kernel/in_app_billing/Licensing;

    return-object p1
.end method

.method static synthetic k()Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic l()Landroid/app/FragmentManager;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->s:Landroid/app/FragmentManager;

    return-object v0
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lcom/franco/kernel/helpers/ColorHelpers;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/ColorHelpers;-><init>()V

    sput-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    .line 96
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "new_kernel_notify"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-static {}, Lcom/franco/kernel/activities/SettingsActivity;->k()V

    .line 101
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "per_app_profiles"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->a()V

    .line 106
    :cond_1
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cpu_temperature"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->c()V

    .line 111
    :cond_2
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "play_games"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    new-instance v0, Lcom/google/example/games/basegameutils/GameHelper;

    invoke-direct {v0, p0, v3}, Lcom/google/example/games/basegameutils/GameHelper;-><init>(Landroid/app/Activity;I)V

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Lcom/google/example/games/basegameutils/GameHelper;)V

    .line 115
    :cond_3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/franco/kernel/activities/MainActivity$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/MainActivity$1;-><init>(Lcom/franco/kernel/activities/MainActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 122
    return-void
.end method

.method private n()V
    .locals 7

    .prologue
    .line 206
    new-instance v0, Lcom/franco/kernel/activities/MainActivity$5;

    iget-object v3, p0, Lcom/franco/kernel/activities/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    sget-object v4, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v5, 0x7f0701f6

    const v6, 0x7f070095

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/franco/kernel/activities/MainActivity$5;-><init>(Lcom/franco/kernel/activities/MainActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    iput-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 222
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->a(Z)V

    .line 223
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 224
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public a(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$FinishMainActivity;)V
    .locals 0
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/franco/kernel/activities/MainActivity;->finish()V

    .line 285
    return-void
.end method

.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 172
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->t:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/MainActivity$4;

    invoke-direct {v1, p0, p1}, Lcom/franco/kernel/activities/MainActivity$4;-><init>(Lcom/franco/kernel/activities/MainActivity;Ljava/lang/String;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 203
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 276
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 277
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 278
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/example/games/basegameutils/GameHelper;->a(IILandroid/content/Intent;)V

    .line 280
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 232
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f07010d

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    const-string v1, "/sdcard/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/MainActivity$BackPress;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/MainActivity$BackPress;-><init>(Lcom/franco/kernel/activities/MainActivity;)V

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 270
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 271
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->a(Landroid/content/res/Configuration;)V

    .line 272
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 126
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 128
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-static {p0}, Lcom/crashlytics/android/Crashlytics;->a(Landroid/content/Context;)V

    .line 131
    const-string v0, "kernel_version"

    invoke-static {}, Lcom/franco/kernel/helpers/KernelVersionHelpers;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/crashlytics/android/Crashlytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const v0, 0x7f04001b

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/MainActivity;->setContentView(I)V

    .line 134
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 136
    const v0, 0x7f0c0196

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    sput-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    .line 137
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/MainActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 139
    invoke-virtual {p0}, Lcom/franco/kernel/activities/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/MainActivity;->s:Landroid/app/FragmentManager;

    .line 140
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->s:Landroid/app/FragmentManager;

    const v1, 0x7f0c0059

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    sput-object v0, Lcom/franco/kernel/activities/MainActivity;->p:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    .line 142
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->p:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity;->mNavigationDrawerView:Landroid/view/View;

    iget-object v2, p0, Lcom/franco/kernel/activities/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Landroid/view/View;Landroid/support/v4/widget/DrawerLayout;)V

    .line 144
    invoke-direct {p0}, Lcom/franco/kernel/activities/MainActivity;->n()V

    .line 145
    invoke-direct {p0}, Lcom/franco/kernel/activities/MainActivity;->m()V

    .line 150
    :try_start_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OPEN_PERFORMANCE_PROFILES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/MainActivity$2;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/MainActivity$2;-><init>(Lcom/franco/kernel/activities/MainActivity;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-virtual {p0}, Lcom/franco/kernel/activities/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MONITOR_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/activities/MainActivity$3;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/MainActivity$3;-><init>(Lcom/franco/kernel/activities/MainActivity;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/franco/kernel/activities/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 322
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 323
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 325
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->r:Lcom/franco/kernel/in_app_billing/Licensing;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->r:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v0, v0, Lcom/franco/kernel/in_app_billing/Licensing;->b:Lcom/google/android/vending/licensing/LicenseChecker;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->r:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v0, v0, Lcom/franco/kernel/in_app_billing/Licensing;->b:Lcom/google/android/vending/licensing/LicenseChecker;

    invoke-virtual {v0}, Lcom/google/android/vending/licensing/LicenseChecker;->a()V

    .line 331
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 334
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 248
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 250
    sparse-switch v1, :sswitch_data_0

    .line 258
    iget-object v1, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v1, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 254
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 250
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 264
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 265
    iget-object v0, p0, Lcom/franco/kernel/activities/MainActivity;->q:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->a()V

    .line 266
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 303
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 304
    sget-boolean v0, Lcom/franco/kernel/App;->e:Z

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Lcom/franco/kernel/activities/MainActivity$6;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/MainActivity$6;-><init>(Lcom/franco/kernel/activities/MainActivity;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "getprop fku.profiles"

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 318
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 289
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 290
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 294
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "play_games"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 296
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b()V

    .line 299
    :cond_1
    return-void
.end method
