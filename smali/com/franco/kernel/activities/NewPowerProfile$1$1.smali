.class Lcom/franco/kernel/activities/NewPowerProfile$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

.field final synthetic b:Lcom/franco/kernel/activities/NewPowerProfile$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/NewPowerProfile$1;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->b:Lcom/franco/kernel/activities/NewPowerProfile$1;

    iput-object p2, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 301
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 304
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701d9

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 331
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->b:Lcom/franco/kernel/activities/NewPowerProfile$1;

    iget-object v1, v1, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    const v2, 0x7f07020a

    invoke-virtual {v1, v2}, Lcom/franco/kernel/activities/NewPowerProfile;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->b:Lcom/franco/kernel/activities/NewPowerProfile$1;

    iget-object v1, v1, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    const v2, 0x7f07004b

    invoke-virtual {v1, v2}, Lcom/franco/kernel/activities/NewPowerProfile;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->b:Lcom/franco/kernel/activities/NewPowerProfile$1;

    iget-object v1, v1, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    const v2, 0x7f070201

    invoke-virtual {v1, v2}, Lcom/franco/kernel/activities/NewPowerProfile;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 313
    :cond_1
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07021d

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 319
    :cond_2
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 324
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->b:Lcom/franco/kernel/activities/NewPowerProfile$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-static {v0}, Lcom/franco/kernel/activities/NewPowerProfile;->c(Lcom/franco/kernel/activities/NewPowerProfile;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 325
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v4, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 328
    :cond_3
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07020f

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 330
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->dismiss()V

    goto/16 :goto_0
.end method
