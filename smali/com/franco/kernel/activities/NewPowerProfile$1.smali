.class Lcom/franco/kernel/activities/NewPowerProfile$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/NewPowerProfile;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/NewPowerProfile;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-static {v0}, Lcom/franco/kernel/activities/NewPowerProfile;->a(Lcom/franco/kernel/activities/NewPowerProfile;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-static {v1}, Lcom/franco/kernel/activities/NewPowerProfile;->b(Lcom/franco/kernel/activities/NewPowerProfile;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 286
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$1;->a:Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-static {v0}, Lcom/franco/kernel/activities/NewPowerProfile;->c(Lcom/franco/kernel/activities/NewPowerProfile;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 287
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v4, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 290
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070211

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 291
    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 334
    :goto_1
    return-void

    .line 293
    :cond_1
    new-instance v0, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;-><init>(Landroid/content/Context;)V

    .line 295
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->show()V

    .line 296
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->a()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0701d8

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v1

    const v2, 0x7f0701da

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 298
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->d()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/activities/NewPowerProfile$1$1;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/activities/NewPowerProfile$1$1;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$1;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
