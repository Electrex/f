.class Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;->a:Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 176
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;

    .line 177
    iget-object v0, v0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 179
    iget-object v2, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 257
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;-><init>(Landroid/content/Context;)V

    .line 259
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->show()V

    .line 260
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->a()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 262
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->d()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$4;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$4;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    :cond_1
    :goto_1
    return-void

    .line 179
    :sswitch_0
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 185
    :pswitch_0
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 190
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 191
    new-instance v3, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$1;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    .line 201
    if-eqz v2, :cond_1

    .line 202
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 203
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 205
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 209
    :pswitch_1
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 214
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 215
    new-instance v3, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$2;

    invoke-direct {v3, p0, v0, v2, v1}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$2;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;Ljava/util/ArrayList;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    .line 224
    if-eqz v2, :cond_1

    .line 225
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 226
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 227
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 228
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 233
    :pswitch_2
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 238
    invoke-static {}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 239
    new-instance v3, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1$3;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    .line 249
    if-eqz v2, :cond_1

    .line 250
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 251
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 252
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 253
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x1762c048 -> :sswitch_4
        -0xfada1c7 -> :sswitch_2
        -0x2e11bc0 -> :sswitch_5
        0x20ffd54f -> :sswitch_1
        0x5e3068d7 -> :sswitch_6
        0x69c25429 -> :sswitch_3
        0x7d4614e7 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
