.class public Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const v3, 0x1020016

    const v2, 0x1020010

    .line 11
    const-string v0, "field \'title\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'title\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 13
    const-string v0, "field \'summary\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'summary\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    .line 15
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    iput-object v0, p1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 19
    iput-object v0, p1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    .line 20
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder$$ViewInjector;->reset(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;)V

    return-void
.end method
