.class public Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:I

.field private c:Ljava/util/List;

.field private d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 173
    new-instance v0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$1;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->d:Landroid/view/View$OnClickListener;

    .line 135
    iput p2, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->b:I

    .line 136
    iput-object p3, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->c:Ljava/util/List;

    .line 137
    invoke-static {}, Lcom/franco/kernel/activities/NewPowerProfile;->k()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->a:Landroid/view/LayoutInflater;

    .line 138
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 154
    if-nez p2, :cond_0

    .line 155
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 156
    new-instance v0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;-><init>(Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;Landroid/view/View;)V

    .line 157
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 162
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 164
    iget-object v2, v1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v2, v1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 166
    iget-object v1, v1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/franco/kernel/activities/NewPowerProfile;->a(Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    return-object p2

    .line 159
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter$ViewHolder;

    move-object v1, v0

    goto :goto_0
.end method
