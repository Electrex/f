.class public Lcom/franco/kernel/activities/NewPowerProfile;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# static fields
.field private static n:Landroid/app/Activity;


# instance fields
.field protected mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field protected mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field

.field private o:Ljava/util/List;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->p:Z

    .line 277
    new-instance v0, Lcom/franco/kernel/activities/NewPowerProfile$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/NewPowerProfile$1;-><init>(Lcom/franco/kernel/activities/NewPowerProfile;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->r:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, Lcom/franco/kernel/activities/NewPowerProfile;->b(Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/activities/NewPowerProfile;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->p:Z

    return v0
.end method

.method private static b(Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->c:Ljava/lang/String;

    .line 340
    iget-object v2, p0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 365
    :goto_1
    return-object v0

    .line 340
    :sswitch_0
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "/sys/module/msm_thermal/parameters/temp_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    .line 347
    :pswitch_0
    :try_start_0
    invoke-static {v0}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 353
    :pswitch_1
    invoke-static {v0}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 356
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 359
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00baC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 348
    :catch_0
    move-exception v1

    goto/16 :goto_1

    .line 340
    nop

    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x5dc7c1dd -> :sswitch_8
        -0x4728b32c -> :sswitch_7
        -0x1762c048 -> :sswitch_4
        -0xfada1c7 -> :sswitch_2
        0x20ffd54f -> :sswitch_1
        0x5e3068d7 -> :sswitch_5
        0x69c25429 -> :sswitch_3
        0x7d4614e7 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/franco/kernel/activities/NewPowerProfile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/franco/kernel/activities/NewPowerProfile;)Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    return-object v0
.end method

.method static synthetic k()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/franco/kernel/activities/NewPowerProfile;->n:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 58
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 60
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v0, 0x7f04004a

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/NewPowerProfile;->setContentView(I)V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 64
    sput-object p0, Lcom/franco/kernel/activities/NewPowerProfile;->n:Landroid/app/Activity;

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/NewPowerProfile;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 67
    invoke-virtual {p0}, Lcom/franco/kernel/activities/NewPowerProfile;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 68
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 69
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 70
    sget-object v0, Lcom/franco/kernel/activities/NewPowerProfile;->n:Landroid/app/Activity;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 73
    invoke-virtual {p0}, Lcom/franco/kernel/activities/NewPowerProfile;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 76
    if-eqz v1, :cond_0

    .line 77
    const-string v2, "edit_profile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->q:Ljava/lang/String;

    .line 79
    iput-boolean v3, p0, Lcom/franco/kernel/activities/NewPowerProfile;->p:Z

    .line 85
    :cond_0
    new-instance v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;

    invoke-direct {v0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;-><init>()V

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;->b()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;

    move-result-object v0

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 90
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 91
    new-instance v2, Ljava/io/File;

    iget-object v3, v0, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 92
    iget-object v2, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :cond_2
    iget-boolean v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->p:Z

    if-eqz v0, :cond_5

    .line 98
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->q:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/activities/NewPowerProfile;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 100
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    .line 102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 110
    :cond_5
    invoke-static {p0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Landroid/app/Activity;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_6

    const v0, 0x7f020080

    :goto_2
    invoke-virtual {v1, v0}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(I)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(Landroid/widget/AbsListView;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(Landroid/view/View$OnClickListener;)Lcom/franco/kernel/views/FloatingActionButton$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a()Lcom/franco/kernel/views/FloatingActionButton;

    .line 116
    return-void

    .line 110
    :cond_6
    const v0, 0x7f020079

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/franco/kernel/activities/NewPowerProfile;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 371
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 393
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 394
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 395
    const/4 v0, 0x0

    sput-object v0, Lcom/franco/kernel/activities/NewPowerProfile;->n:Landroid/app/Activity;

    .line 396
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 376
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 378
    sparse-switch v1, :sswitch_data_0

    .line 388
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 380
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/NewPowerProfile;->finish()V

    goto :goto_0

    .line 383
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/NewPowerProfile;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 378
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 121
    iget-object v1, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mToolbar:Landroid/support/v7/widget/Toolbar;

    iget-boolean v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->p:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070210

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 122
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;

    const v2, 0x7f040069

    iget-object v3, p0, Lcom/franco/kernel/activities/NewPowerProfile;->o:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Lcom/franco/kernel/activities/NewPowerProfile$ParametersArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 123
    iget-object v0, p0, Lcom/franco/kernel/activities/NewPowerProfile;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 124
    return-void

    .line 121
    :cond_0
    const v0, 0x7f0701cf

    goto :goto_0
.end method
