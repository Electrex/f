.class public Lcom/franco/kernel/activities/ProfileManager$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/ProfileManager;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x7f0c0196

    const v3, 0x102000a

    const v2, 0x1020004

    .line 11
    const-string v0, "field \'mToolbar\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mToolbar\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p2, Lcom/franco/kernel/activities/ProfileManager;->mToolbar:Landroid/support/v7/widget/Toolbar;

    .line 13
    const-string v0, "field \'mListView\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p2, Lcom/franco/kernel/activities/ProfileManager;->mListView:Landroid/widget/ListView;

    .line 15
    const-string v0, "field \'mEmpty\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mEmpty\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p2, Lcom/franco/kernel/activities/ProfileManager;->mEmpty:Landroid/view/ViewStub;

    .line 17
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/ProfileManager;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/ProfileManager$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/ProfileManager;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/ProfileManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    iput-object v0, p1, Lcom/franco/kernel/activities/ProfileManager;->mToolbar:Landroid/support/v7/widget/Toolbar;

    .line 21
    iput-object v0, p1, Lcom/franco/kernel/activities/ProfileManager;->mListView:Landroid/widget/ListView;

    .line 22
    iput-object v0, p1, Lcom/franco/kernel/activities/ProfileManager;->mEmpty:Landroid/view/ViewStub;

    .line 23
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/ProfileManager;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/ProfileManager$$ViewInjector;->reset(Lcom/franco/kernel/activities/ProfileManager;)V

    return-void
.end method
