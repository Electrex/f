.class Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->d:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;

    iput-object p2, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    iput-object p3, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->c:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 145
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701d9

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 187
    :goto_0
    return-void

    .line 150
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/franco/kernel/helpers/PerAppProfileHelpers;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%s%s%s"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "/"

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->b:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ".xml"

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/franco/kernel/helpers/PerAppProfileHelpers;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%s%s%s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "/"

    aput-object v4, v3, v5

    invoke-static {v1}, Lcom/franco/kernel/helpers/StringHelpers;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, ".xml"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 155
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    :try_start_0
    invoke-static {v3, v0}, Lorg/apache/commons/io/FileUtils;->b(Ljava/io/File;Ljava/io/File;)V

    .line 160
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->d:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;->a:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;

    invoke-static {v0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->a(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)Ljava/util/List;

    move-result-object v2

    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->d:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;

    iget-object v0, v0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;->a:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->notifyDataSetChanged()V

    .line 162
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->dismiss()V

    .line 164
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1$1;

    invoke-direct {v2, p0, v1}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1$1;-><init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1$1;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method
