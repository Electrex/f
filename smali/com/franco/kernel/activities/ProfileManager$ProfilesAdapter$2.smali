.class Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;->a:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    sget-object v1, Lcom/franco/kernel/helpers/PerAppProfileHelpers;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 197
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%s%s%s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "/"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    const-string v4, ".xml"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 199
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    invoke-static {}, Lcom/franco/kernel/activities/ProfileManager;->k()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 201
    iget-object v1, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;->a:Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;

    invoke-virtual {v1}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->notifyDataSetChanged()V

    .line 203
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2$1;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2$1;-><init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 223
    :cond_0
    return-void
.end method
