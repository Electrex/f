.class Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/ProfileManager;

.field private final b:I

.field private final c:Ljava/util/List;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/activities/ProfileManager;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 92
    iput-object p1, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->a:Lcom/franco/kernel/activities/ProfileManager;

    .line 93
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 132
    new-instance v0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$1;-><init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->e:Landroid/view/View$OnClickListener;

    .line 192
    new-instance v0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$2;-><init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->f:Landroid/view/View$OnClickListener;

    .line 226
    new-instance v0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter$3;-><init>(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->g:Landroid/view/View$OnClickListener;

    .line 94
    iput p3, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->b:I

    .line 95
    iput-object p4, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->c:Ljava/util/List;

    .line 96
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->d:Landroid/view/LayoutInflater;

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 101
    if-nez p2, :cond_0

    .line 102
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 105
    :cond_0
    const v0, 0x1020016

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 106
    const v1, 0x1020007

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 107
    const v2, 0x1020008

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 109
    iget-object v3, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->c:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 111
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 119
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 125
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->a:Lcom/franco/kernel/activities/ProfileManager;

    iget-object v0, v0, Lcom/franco/kernel/activities/ProfileManager;->mEmpty:Landroid/view/ViewStub;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/activities/ProfileManager$ProfilesAdapter;->a:Lcom/franco/kernel/activities/ProfileManager;

    iget-object v0, v0, Lcom/franco/kernel/activities/ProfileManager;->mEmpty:Landroid/view/ViewStub;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    goto :goto_0
.end method
