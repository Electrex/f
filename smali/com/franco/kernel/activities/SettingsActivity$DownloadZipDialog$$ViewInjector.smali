.class public Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const v3, 0x7f0c004f

    const v2, 0x102000a

    .line 11
    const-string v0, "field \'mListView\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    .line 13
    const-string v0, "field \'tvPath\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'tvPath\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    .line 15
    const v0, 0x1020019

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    new-instance v1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$1;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    const v0, 0x102001a

    const-string v1, "method \'onClickSet\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 25
    new-instance v1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$2;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    const v0, 0x7f0c004e

    const-string v1, "method \'onClickNewFolder\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 34
    new-instance v1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$3;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector$3;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    .line 46
    iput-object v0, p1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    .line 47
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$$ViewInjector;->reset(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;)V

    return-void
.end method
