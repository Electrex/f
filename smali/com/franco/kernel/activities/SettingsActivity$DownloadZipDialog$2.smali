.class Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

.field final synthetic b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iput-object p2, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 418
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 420
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070100

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 435
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iget-object v1, v1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 426
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 428
    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iget-object v0, v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->clear()V

    .line 430
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iget-object v0, v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;

    iget-object v1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iget-object v2, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->b:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    iget-object v2, v2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->a(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->addAll(Ljava/util/Collection;)V

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;->a:Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->dismiss()V

    goto :goto_0

    .line 426
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
