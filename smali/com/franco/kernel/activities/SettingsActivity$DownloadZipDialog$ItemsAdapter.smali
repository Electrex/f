.class Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

.field private b:I

.field private c:Ljava/util/ArrayList;

.field private d:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 460
    iput-object p1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->a:Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    .line 461
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 462
    iput p3, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->b:I

    .line 463
    iput-object p4, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->c:Ljava/util/ArrayList;

    .line 464
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->d:Landroid/view/LayoutInflater;

    .line 465
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 469
    if-nez p2, :cond_0

    .line 470
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 473
    :cond_0
    const v0, 0x1020016

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 476
    new-instance v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter$1;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    return-object p2
.end method
