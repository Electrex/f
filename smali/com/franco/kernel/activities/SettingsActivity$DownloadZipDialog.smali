.class public Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field protected mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field protected tvPath:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c004f
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 371
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 440
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 442
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 443
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 444
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 450
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    return-object v1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 497
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->clear()V

    .line 498
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;->addAll(Ljava/util/Collection;)V

    .line 502
    :goto_0
    return-void

    .line 500
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onClick(Landroid/view/View;)V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x1020019
        }
    .end annotation

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->finish()V

    .line 392
    return-void
.end method

.method protected onClickNewFolder(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c004e
        }
    .end annotation

    .prologue
    .line 404
    new-instance v0, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    invoke-direct {v0, p0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;-><init>(Landroid/content/Context;)V

    .line 405
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->show()V

    .line 406
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->a()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0701d1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 407
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v1

    const v2, 0x7f0701d2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 408
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->c()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$1;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$1;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->d()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$2;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    return-void
.end method

.method protected onClickSet(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x102001a
        }
    .end annotation

    .prologue
    .line 396
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_zip"

    iget-object v2, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->tvPath:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 399
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->finish()V

    .line 400
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 375
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 377
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 378
    const v0, 0x7f040016

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->setContentView(I)V

    .line 379
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 381
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 384
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;

    const v2, 0x7f040042

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog$ItemsAdapter;-><init>(Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;Landroid/content/Context;ILjava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 387
    return-void
.end method
