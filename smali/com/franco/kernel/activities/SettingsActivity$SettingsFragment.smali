.class public Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field protected static a:Landroid/preference/PreferenceScreen;

.field protected static b:Landroid/preference/PreferenceCategory;

.field protected static c:Landroid/preference/PreferenceCategory;

.field protected static d:Landroid/preference/Preference;

.field protected static e:Landroid/preference/Preference;

.field protected static f:Landroid/preference/Preference;

.field protected static g:Landroid/preference/Preference;

.field protected static h:Landroid/preference/PreferenceScreen;

.field protected static i:Landroid/preference/ListPreference;

.field protected static j:Landroid/preference/SwitchPreference;

.field protected static k:Landroid/preference/SwitchPreference;

.field protected static l:Landroid/preference/SwitchPreference;

.field protected static m:Landroid/preference/SwitchPreference;

.field protected static n:Landroid/preference/SwitchPreference;

.field protected static o:Landroid/preference/SwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f0700c9

    .line 177
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    const/high16 v0, 0x7f060000

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->addPreferencesFromResource(I)V

    .line 180
    const-string v0, "theme_category"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->c:Landroid/preference/PreferenceCategory;

    .line 181
    const-string v0, "preferences_parent"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->a:Landroid/preference/PreferenceScreen;

    .line 182
    const-string v0, "cpu_temperature_category"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->b:Landroid/preference/PreferenceCategory;

    .line 185
    const-string v0, "app_theme"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->h:Landroid/preference/PreferenceScreen;

    .line 186
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->c:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->i:Landroid/preference/ListPreference;

    .line 187
    const-string v0, "clear_data"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->d:Landroid/preference/Preference;

    .line 188
    const-string v0, "download_zip"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->e:Landroid/preference/Preference;

    .line 189
    const-string v0, "temperature_type"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    .line 190
    const-string v0, "achievements"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->g:Landroid/preference/Preference;

    .line 193
    const-string v0, "new_kernel_notify"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->j:Landroid/preference/SwitchPreference;

    .line 194
    const-string v0, "per_app_profiles"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->k:Landroid/preference/SwitchPreference;

    .line 195
    const-string v0, "cpu_temperature"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->l:Landroid/preference/SwitchPreference;

    .line 196
    const-string v0, "play_games"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->m:Landroid/preference/SwitchPreference;

    .line 197
    const-string v0, "cpu_temp_notif_low_priority"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->n:Landroid/preference/SwitchPreference;

    .line 198
    const-string v0, "navigation_bar_color"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->o:Landroid/preference/SwitchPreference;

    .line 201
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->h:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 202
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 203
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->e:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 204
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 205
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->g:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 208
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 209
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->j:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 210
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->k:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 211
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->l:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 212
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->m:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 213
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->n:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 214
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->o:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 216
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->h:Landroid/preference/PreferenceScreen;

    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "app_theme"

    const-string v3, "light"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 217
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->i:Landroid/preference/ListPreference;

    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "app_theme"

    const-string v3, "light"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 220
    :try_start_0
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "temperature_type"

    const-string v2, "c"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    const v1, 0x7f0700c9

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->i()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0702aa

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->a:Landroid/preference/PreferenceScreen;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->l:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 232
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->a:Landroid/preference/PreferenceScreen;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 233
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->a:Landroid/preference/PreferenceScreen;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->b:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 236
    :cond_0
    sget-object v0, Lcom/franco/kernel/App;->h:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v1, "dark_theme"

    invoke-virtual {v0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->c:Landroid/preference/PreferenceCategory;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->h:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 242
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 243
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->c:Landroid/preference/PreferenceCategory;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->o:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 245
    :cond_1
    return-void

    .line 224
    :cond_2
    :try_start_1
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    invoke-virtual {p0, v4}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 239
    :cond_3
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->c:Landroid/preference/PreferenceCategory;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 249
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->i:Landroid/preference/ListPreference;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->o:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_2

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 252
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 254
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 283
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 256
    :cond_2
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->j:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_4

    .line 257
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    invoke-static {}, Lcom/franco/kernel/activities/SettingsActivity;->k()V

    goto :goto_0

    .line 260
    :cond_3
    invoke-static {}, Lcom/franco/kernel/activities/SettingsActivity;->l()V

    goto :goto_0

    .line 262
    :cond_4
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->k:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_6

    .line 263
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->a()V

    goto :goto_0

    .line 266
    :cond_5
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->b()V

    goto :goto_0

    .line 268
    :cond_6
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->l:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_8

    .line 269
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 270
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->c()V

    goto :goto_0

    .line 272
    :cond_7
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->d()V

    goto :goto_0

    .line 274
    :cond_8
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->m:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_1

    .line 275
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 276
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->e()V

    .line 277
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b()V

    goto :goto_0

    .line 279
    :cond_9
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->d()V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 288
    instance-of v0, p1, Landroid/preference/ListPreference;

    if-nez v0, :cond_1

    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->h:Landroid/preference/PreferenceScreen;

    if-ne p1, v0, :cond_1

    .line 289
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/ThemeChooserActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 327
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 290
    :cond_1
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->d:Landroid/preference/Preference;

    if-ne p1, v0, :cond_2

    .line 292
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 295
    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 296
    :catch_0
    move-exception v0

    goto :goto_0

    .line 299
    :cond_2
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->e:Landroid/preference/Preference;

    if-ne p1, v0, :cond_3

    .line 300
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/SettingsActivity$DownloadZipDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 301
    :cond_3
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    if-ne p1, v0, :cond_6

    .line 302
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "temperature_type"

    const-string v2, "c"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    const-string v1, "c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 306
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "temperature_type"

    const-string v2, "f"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 317
    :cond_4
    :goto_1
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->l:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    invoke-static {}, Lcom/franco/kernel/helpers/ServiceHelpers;->c()V

    goto/16 :goto_0

    .line 310
    :cond_5
    const-string v1, "f"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 311
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "temperature_type"

    const-string v2, "c"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 314
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->f:Landroid/preference/Preference;

    const v1, 0x7f0700c9

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 320
    :cond_6
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->g:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 321
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    sget-object v0, Lcom/google/android/gms/games/Games;->g:Lcom/google/android/gms/games/achievement/Achievements;

    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/achievement/Achievements;->a(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xfab

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 332
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 333
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity$SettingsFragment;->e:Landroid/preference/Preference;

    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "download_zip"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 335
    return-void
.end method
