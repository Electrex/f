.class public Lcom/franco/kernel/activities/SettingsActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# static fields
.field private static n:Landroid/app/AlarmManager;

.field private static o:Ljava/lang/String;

.field private static p:Landroid/content/Intent;

.field private static q:Landroid/app/PendingIntent;

.field private static r:I

.field private static s:J

.field private static t:J


# instance fields
.field protected mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 364
    return-void
.end method

.method public static k()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 339
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->n:Landroid/app/AlarmManager;

    .line 340
    const-string v0, "com.franco.kernel.NEW_KERNEL_ALARM"

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->o:Ljava/lang/String;

    .line 341
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity;->o:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->p:Landroid/content/Intent;

    .line 342
    const/4 v0, 0x1

    sput v0, Lcom/franco/kernel/activities/SettingsActivity;->r:I

    .line 343
    const-wide/32 v0, 0x5265c00

    sput-wide v0, Lcom/franco/kernel/activities/SettingsActivity;->s:J

    .line 344
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/franco/kernel/activities/SettingsActivity;->s:J

    add-long/2addr v0, v2

    sput-wide v0, Lcom/franco/kernel/activities/SettingsActivity;->t:J

    .line 345
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity;->p:Landroid/content/Intent;

    invoke-static {v0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    .line 347
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity;->n:Landroid/app/AlarmManager;

    sget v1, Lcom/franco/kernel/activities/SettingsActivity;->r:I

    sget-wide v2, Lcom/franco/kernel/activities/SettingsActivity;->t:J

    sget-wide v4, Lcom/franco/kernel/activities/SettingsActivity;->s:J

    sget-object v6, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 351
    return-void
.end method

.method public static l()V
    .locals 4

    .prologue
    .line 354
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->n:Landroid/app/AlarmManager;

    .line 355
    const-string v0, "com.franco.kernel.NEW_KERNEL_ALARM"

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->o:Ljava/lang/String;

    .line 356
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity;->o:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->p:Landroid/content/Intent;

    .line 357
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/franco/kernel/activities/SettingsActivity;->p:Landroid/content/Intent;

    const/high16 v3, 0x20000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    .line 359
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 360
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 361
    :cond_0
    sget-object v0, Lcom/franco/kernel/activities/SettingsActivity;->n:Landroid/app/AlarmManager;

    sget-object v1, Lcom/franco/kernel/activities/SettingsActivity;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 362
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 96
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/example/games/basegameutils/GameHelper;->a(IILandroid/content/Intent;)V

    .line 97
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 70
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const v0, 0x7f04001d

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity;->setContentView(I)V

    .line 72
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 74
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 75
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 76
    iget-object v0, p0, Lcom/franco/kernel/activities/SettingsActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/SettingsActivity;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 77
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/SettingsActivity;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 78
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/SettingsActivity;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 80
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 81
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 83
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "play_games"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/google/example/games/basegameutils/GameHelper;

    invoke-direct {v0, p0, v4}, Lcom/google/example/games/basegameutils/GameHelper;-><init>(Landroid/app/Activity;I)V

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Lcom/google/example/games/basegameutils/GameHelper;)V

    .line 86
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 124
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 125
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 126
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 107
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 109
    sparse-switch v1, :sswitch_data_0

    .line 119
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 111
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SettingsActivity;->finish()V

    goto :goto_0

    .line 114
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 91
    return-void
.end method
