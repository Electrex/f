.class public Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;
.super Licepick/Injector$Object;
.source "SourceFile"


# static fields
.field private static final H:Licepick/Injector$Helper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    new-instance v0, Licepick/Injector$Helper;

    const-string v1, "com.franco.kernel.activities.SocTunablesAbstractActivity$$Icicle."

    invoke-direct {v0, v1}, Licepick/Injector$Helper;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;->H:Licepick/Injector$Helper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Licepick/Injector$Object;-><init>()V

    return-void
.end method


# virtual methods
.method public restore(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 12
    if-nez p2, :cond_0

    .line 15
    :goto_0
    return-void

    .line 13
    :cond_0
    sget-object v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;->H:Licepick/Injector$Helper;

    const-string v1, "socTunablesAbstract"

    invoke-virtual {v0, p2, v1}, Licepick/Injector$Helper;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/SocTunablesAbstract;

    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->n:Lcom/franco/kernel/SocTunablesAbstract;

    .line 14
    invoke-super {p0, p1, p2}, Licepick/Injector$Object;->restore(Ljava/lang/Object;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public bridge synthetic restore(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;->restore(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/os/Bundle;)V

    return-void
.end method

.method public save(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Licepick/Injector$Object;->save(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 19
    sget-object v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;->H:Licepick/Injector$Helper;

    const-string v1, "socTunablesAbstract"

    iget-object v2, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->n:Lcom/franco/kernel/SocTunablesAbstract;

    invoke-virtual {v0, p2, v1, v2}, Licepick/Injector$Helper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 20
    return-void
.end method

.method public bridge synthetic save(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$$Icicle;->save(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/os/Bundle;)V

    return-void
.end method
