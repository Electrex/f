.class Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;


# direct methods
.method constructor <init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;->a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;

    .line 182
    iget-object v0, v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 184
    iget-object v2, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 246
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    iget-object v2, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;->a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;-><init>(Landroid/content/Context;)V

    .line 248
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->show()V

    .line 249
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->a()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 251
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->d()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$4;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$4;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    :goto_1
    return-void

    .line 184
    :sswitch_0
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/hispeed_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/sync_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/screen_off_maxfreq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/governor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_9
    const-string v3, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_a
    const-string v3, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v3, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    .line 193
    :pswitch_0
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;->a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 194
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 195
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 196
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$1;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 206
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 207
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 210
    :pswitch_1
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;->a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 211
    invoke-static {}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 212
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 213
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$2;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 223
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 224
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 229
    :pswitch_2
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;->a:Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 230
    invoke-static {}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 231
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 232
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1$3;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 242
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 243
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 184
    :sswitch_data_0
    .sparse-switch
        -0x2f8f2466 -> :sswitch_8
        -0x1762c048 -> :sswitch_6
        -0xfada1c7 -> :sswitch_4
        -0xbae3357 -> :sswitch_a
        0x33180d7 -> :sswitch_5
        0x983a231 -> :sswitch_0
        0x3dc7bab9 -> :sswitch_3
        0x4e333e9c -> :sswitch_1
        0x5e3068d7 -> :sswitch_9
        0x66937de4 -> :sswitch_7
        0x69c25429 -> :sswitch_2
        0x7d4614e7 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
