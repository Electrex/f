.class public Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const v5, 0x1020016

    const v4, 0x1020010

    const v3, 0x1020007

    const v2, 0x1020006

    .line 11
    const v0, 0x7f0c0099

    const-string v1, "field \'clickableView\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    .line 13
    const-string v0, "field \'info\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'info\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/views/ImageViewInfo;

    iput-object v0, p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->info:Lcom/franco/kernel/views/ImageViewInfo;

    .line 15
    const-string v0, "field \'title\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'title\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 17
    const-string v0, "field \'summary\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const-string v1, "field \'summary\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    .line 19
    const-string v0, "field \'setOnBoot\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    const-string v1, "field \'setOnBoot\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/views/ImageViewSetOnBoot;

    iput-object v0, p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    .line 21
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    .line 25
    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->info:Lcom/franco/kernel/views/ImageViewInfo;

    .line 26
    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 27
    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    .line 28
    iput-object v0, p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    .line 29
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder$$ViewInjector;->reset(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;)V

    return-void
.end method
