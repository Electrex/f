.class public Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:I

.field public c:Ljava/util/List;

.field final synthetic d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 129
    iput-object p1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    .line 130
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 178
    new-instance v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$1;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    .line 131
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    .line 132
    iput p3, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->b:I

    .line 133
    iput-object p4, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->c:Ljava/util/List;

    .line 134
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 157
    if-nez p2, :cond_0

    .line 158
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 159
    new-instance v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;Landroid/view/View;)V

    .line 160
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 165
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 167
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 169
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    iget-object v4, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->a(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->info:Lcom/franco/kernel/views/ImageViewInfo;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewInfo;->setTag(Ljava/lang/Object;)V

    .line 171
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v2, v0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setTag(Ljava/lang/Object;)V

    .line 172
    iget-object v2, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v0}, Lcom/franco/kernel/FileTunable;->a()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setChecked(Z)V

    .line 173
    iget-object v0, v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    return-object p2

    .line 162
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter$ViewHolder;

    move-object v1, v0

    goto :goto_0
.end method
