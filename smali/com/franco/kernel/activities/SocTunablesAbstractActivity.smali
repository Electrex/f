.class public Lcom/franco/kernel/activities/SocTunablesAbstractActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# instance fields
.field mList:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field

.field n:Lcom/franco/kernel/SocTunablesAbstract;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/LinkedHashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 124
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 266
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 281
    :goto_1
    return-object p2

    .line 266
    :sswitch_0
    const-string v1, "/sys/devices/system/cpu/cpufreq/interactive/hispeed_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "/sys/devices/system/cpu/cpufreq/interactive/sync_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "/sys/devices/system/cpu/cpufreq/interactive/screen_off_maxfreq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "/sys/module/cpu_boost/parameters/input_boost_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "/sys/module/cpu_boost/parameters/sync_threshold"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/min_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/max_freq"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    .line 275
    :pswitch_0
    invoke-static {p2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 279
    :pswitch_1
    invoke-static {p2}, Lcom/franco/kernel/helpers/GPUfreqHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 266
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1762c048 -> :sswitch_6
        -0xfada1c7 -> :sswitch_4
        -0xbae3357 -> :sswitch_9
        0x33180d7 -> :sswitch_5
        0x983a231 -> :sswitch_0
        0x3dc7bab9 -> :sswitch_3
        0x4e333e9c -> :sswitch_1
        0x5e3068d7 -> :sswitch_a
        0x66937de4 -> :sswitch_7
        0x69c25429 -> :sswitch_2
        0x7d4614e7 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->p:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->p:Ljava/util/LinkedHashMap;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/content/Loader;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mList:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    const v2, 0x7f04007b

    invoke-direct {v1, p0, p0, v2, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->clear()V

    .line 69
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;

    invoke-virtual {v0, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$ItemsAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 79
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 81
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    const v0, 0x7f04001f

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->setContentView(I)V

    .line 83
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 84
    invoke-static {p0, p1}, Licepick/Icepick;->b(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Lcom/franco/kernel/SocTunablesAbstract;

    invoke-virtual {p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "path"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/franco/kernel/SocTunablesAbstract;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->n:Lcom/franco/kernel/SocTunablesAbstract;

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->n:Lcom/franco/kernel/SocTunablesAbstract;

    iget-object v1, v1, Lcom/franco/kernel/SocTunablesAbstract;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->o:Ljava/lang/String;

    .line 90
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    const-string v1, "ls -d %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->o:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$1;

    invoke-direct {v2, p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity$1;-><init>(Lcom/franco/kernel/activities/SocTunablesAbstractActivity;)V

    invoke-virtual {v0, v1, v4, v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Ljava/lang/String;ILeu/chainfire/libsuperuser/Shell$OnCommandResultListener;)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0xabc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/franco/kernel/loaders/FileTunablesLoader;

    iget-object v1, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->p:Ljava/util/LinkedHashMap;

    invoke-direct {v0, p0, v1}, Lcom/franco/kernel/loaders/FileTunablesLoader;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 288
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 321
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 322
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 323
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->a(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 293
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 295
    sparse-switch v1, :sswitch_data_0

    .line 305
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 297
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->finish()V

    goto :goto_0

    .line 300
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 295
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 310
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 311
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->n:Lcom/franco/kernel/SocTunablesAbstract;

    invoke-virtual {v0}, Lcom/franco/kernel/SocTunablesAbstract;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 314
    invoke-virtual {p0}, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 315
    iget-object v0, p0, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 316
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 317
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    invoke-static {p0, p1}, Licepick/Icepick;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 122
    return-void
.end method
