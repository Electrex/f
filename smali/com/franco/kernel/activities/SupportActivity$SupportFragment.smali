.class public Lcom/franco/kernel/activities/SupportActivity$SupportFragment;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field protected static a:Landroid/preference/Preference;

.field protected static b:Landroid/preference/Preference;

.field protected static c:Landroid/preference/Preference;

.field protected static d:Landroid/preference/Preference;

.field protected static e:Landroid/preference/Preference;

.field protected static f:Landroid/preference/Preference;

.field protected static g:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->addPreferencesFromResource(I)V

    .line 104
    const-string v0, "app_info"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->a:Landroid/preference/Preference;

    .line 105
    const-string v0, "donate"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->b:Landroid/preference/Preference;

    .line 106
    const-string v0, "device_status"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->c:Landroid/preference/Preference;

    .line 107
    const-string v0, "plus"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->d:Landroid/preference/Preference;

    .line 108
    const-string v0, "twitter"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->e:Landroid/preference/Preference;

    .line 109
    const-string v0, "community"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->f:Landroid/preference/Preference;

    .line 110
    const-string v0, "learner_lounge"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->g:Landroid/preference/Preference;

    .line 112
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->a:Landroid/preference/Preference;

    const v1, 0x7f07001f

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/franco/kernel/helpers/SupportHelpers;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/franco/kernel/helpers/SupportHelpers;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 114
    sget-object v1, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->c:Landroid/preference/Preference;

    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f07029f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 116
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->b:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 117
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 118
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->e:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 119
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->f:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 120
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->g:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 121
    return-void

    .line 114
    :cond_0
    const v0, 0x7f0701e3

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 125
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->b:Landroid/preference/Preference;

    if-ne p1, v0, :cond_1

    .line 126
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->startActivity(Landroid/content/Intent;)V

    .line 137
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 127
    :cond_1
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->d:Landroid/preference/Preference;

    if-ne p1, v0, :cond_2

    .line 128
    const-string v0, "https://plus.google.com/u/0/+FranciscoFranco1990/posts"

    invoke-static {v0}, Lcom/franco/kernel/helpers/SupportHelpers;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_2
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->e:Landroid/preference/Preference;

    if-ne p1, v0, :cond_3

    .line 130
    const-string v0, "https://twitter.com/franciscof_1990"

    invoke-static {v0}, Lcom/franco/kernel/helpers/SupportHelpers;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->f:Landroid/preference/Preference;

    if-ne p1, v0, :cond_4

    .line 132
    const-string v0, "https://plus.google.com/u/0/communities/117966512071636110546"

    invoke-static {v0}, Lcom/franco/kernel/helpers/SupportHelpers;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_4
    sget-object v0, Lcom/franco/kernel/activities/SupportActivity$SupportFragment;->g:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 134
    const-string v0, "http://forum.xda-developers.com/showthread.php?t=2532422"

    invoke-static {v0}, Lcom/franco/kernel/helpers/SupportHelpers;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
