.class public Lcom/franco/kernel/activities/ThemeChooserActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;


# instance fields
.field private a:Lcom/anjlab/android/iab/v3/BillingProcessor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/ThemeChooserActivity;Lcom/anjlab/android/iab/v3/BillingProcessor;)Lcom/anjlab/android/iab/v3/BillingProcessor;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/franco/kernel/activities/ThemeChooserActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    return-object p1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public a(ILjava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/anjlab/android/iab/v3/TransactionDetails;)V
    .locals 1

    .prologue
    .line 50
    const-string v0, "CgkIo_DA4eIIEAIQHg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/franco/kernel/activities/ThemeChooserActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 70
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f040041

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/ThemeChooserActivity;->setContentView(I)V

    .line 29
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 31
    invoke-virtual {p0}, Lcom/franco/kernel/activities/ThemeChooserActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 34
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/franco/kernel/activities/ThemeChooserActivity$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/activities/ThemeChooserActivity$1;-><init>(Lcom/franco/kernel/activities/ThemeChooserActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 74
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 76
    return-void
.end method

.method protected onDonateClick(Landroid/view/View;)V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c00fb
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/franco/kernel/activities/ThemeChooserActivity;->a:Lcom/anjlab/android/iab/v3/BillingProcessor;

    const-string v1, "dark_theme"

    invoke-virtual {v0, p0, v1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 46
    return-void
.end method
