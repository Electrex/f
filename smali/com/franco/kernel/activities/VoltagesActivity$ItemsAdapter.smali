.class public Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:I

.field public c:Ljava/util/List;

.field final synthetic d:Lcom/franco/kernel/activities/VoltagesActivity;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/activities/VoltagesActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 82
    iput-object p1, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->d:Lcom/franco/kernel/activities/VoltagesActivity;

    .line 83
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 127
    new-instance v0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$1;-><init>(Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    .line 134
    new-instance v0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$2;-><init>(Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->f:Landroid/view/View$OnClickListener;

    .line 84
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    .line 85
    iput p3, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->b:I

    .line 86
    iput-object p4, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->c:Ljava/util/List;

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;Ljava/util/List;Landroid/view/View;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->a(Ljava/util/List;Landroid/view/View;ILjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/util/List;Landroid/view/View;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 142
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    .line 143
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    invoke-static {v1}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 144
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    const-string v2, "p"

    invoke-virtual {p4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/2addr v1, p3

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;Ljava/lang/String;)Ljava/lang/String;

    .line 145
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->notifyDataSetChanged()V

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    .line 150
    invoke-static {v0}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 144
    :cond_0
    sub-int/2addr v1, p3

    goto :goto_0

    .line 152
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 154
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "echo %s > %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 156
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 107
    if-nez p2, :cond_0

    .line 108
    iget-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 109
    new-instance v0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;-><init>(Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;Landroid/view/View;)V

    .line 110
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 115
    :goto_0
    invoke-virtual {p0, p1}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    .line 117
    iget-object v2, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mV"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->b(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " MHz"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v2, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->minus:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-object v2, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->minus:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v2, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->plus:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 122
    iget-object v0, v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;->plus:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    return-object p2

    .line 112
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter$ViewHolder;

    move-object v1, v0

    goto :goto_0
.end method
