.class public Lcom/franco/kernel/activities/VoltagesActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "SourceFile"


# instance fields
.field mList:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field mToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0196
    .end annotation
.end field

.field private n:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 77
    return-void
.end method

.method private a(Ljava/util/List;ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 160
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 162
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 163
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    invoke-static {v0}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 164
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    const-string v5, "p"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/2addr v3, p2

    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;->a(Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 164
    :cond_0
    sub-int/2addr v3, p2

    goto :goto_1

    .line 168
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 170
    new-array v1, v6, [Ljava/lang/String;

    const-string v3, "echo %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v2

    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 173
    iget-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;->notifyDataSetChanged()V

    .line 174
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 53
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->a(Landroid/app/Activity;)V

    .line 55
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f04001f

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/VoltagesActivity;->setContentView(I)V

    .line 57
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 59
    iget-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/activities/VoltagesActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 60
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity;->g()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 61
    iget-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity;->mToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/activities/VoltagesActivity;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 62
    sget-object v0, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v1, Lcom/franco/kernel/activities/VoltagesActivity;

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 63
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 179
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100010

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 180
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000f

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 181
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 209
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 210
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 211
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/16 v3, 0x19

    const/4 v0, 0x1

    .line 186
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 188
    sparse-switch v1, :sswitch_data_0

    .line 204
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 190
    :sswitch_0
    invoke-virtual {p0}, Lcom/franco/kernel/activities/VoltagesActivity;->finish()V

    goto :goto_0

    .line 193
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/franco/kernel/activities/InAppPurchasesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/activities/VoltagesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 197
    :sswitch_2
    iget-object v1, p0, Lcom/franco/kernel/activities/VoltagesActivity;->n:Ljava/util/List;

    const-string v2, "p"

    invoke-direct {p0, v1, v3, v2}, Lcom/franco/kernel/activities/VoltagesActivity;->a(Ljava/util/List;ILjava/lang/String;)V

    goto :goto_0

    .line 200
    :sswitch_3
    iget-object v1, p0, Lcom/franco/kernel/activities/VoltagesActivity;->n:Ljava/util/List;

    const-string v2, "m"

    invoke-direct {p0, v1, v3, v2}, Lcom/franco/kernel/activities/VoltagesActivity;->a(Ljava/util/List;ILjava/lang/String;)V

    goto :goto_0

    .line 188
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c01b0 -> :sswitch_1
        0x7f0c01b2 -> :sswitch_3
        0x7f0c01b3 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity;->n:Ljava/util/List;

    .line 70
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->c()Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 71
    iget-object v3, p0, Lcom/franco/kernel/activities/VoltagesActivity;->n:Ljava/util/List;

    new-instance v4, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v1, v0, v5}, Lcom/franco/kernel/activities/VoltagesActivity$VoltageTunable;-><init>(Lcom/franco/kernel/activities/VoltagesActivity;Ljava/lang/String;Ljava/lang/String;Lcom/franco/kernel/activities/VoltagesActivity$1;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/activities/VoltagesActivity;->mList:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;

    const v2, 0x7f04007c

    iget-object v3, p0, Lcom/franco/kernel/activities/VoltagesActivity;->n:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/franco/kernel/activities/VoltagesActivity$ItemsAdapter;-><init>(Lcom/franco/kernel/activities/VoltagesActivity;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-void
.end method
