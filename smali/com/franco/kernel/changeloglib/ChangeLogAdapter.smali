.class public Lcom/franco/kernel/changeloglib/ChangeLogAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private final d:Landroid/content/Context;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 42
    const v0, 0x7f040030

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->a:I

    .line 43
    const v0, 0x7f040031

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->b:I

    .line 44
    const v0, 0x7f070084

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->c:I

    .line 48
    iput-boolean v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->e:Z

    .line 53
    iput-object p1, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->d:Landroid/content/Context;

    .line 54
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 219
    iput p1, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->a:I

    .line 220
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 227
    iput p1, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->b:I

    .line 228
    return-void
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;

    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 67
    invoke-virtual {p0, p1}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;

    .line 69
    invoke-virtual {p0, p1}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->getItemViewType(I)I

    move-result v2

    .line 71
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->d:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 73
    packed-switch v2, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    return-object p2

    .line 77
    :pswitch_0
    if-eqz p2, :cond_d

    .line 78
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 79
    instance-of v4, v2, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;

    if-eqz v4, :cond_4

    .line 80
    check-cast v2, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;

    .line 86
    :goto_1
    if-eqz p2, :cond_1

    if-nez v2, :cond_c

    .line 87
    :cond_1
    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->b:I

    .line 88
    invoke-virtual {v1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 91
    const v1, 0x7f0c00a4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 93
    const v2, 0x7f0c00a5

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 94
    new-instance v3, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;

    invoke-direct {v3, v1, v2}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 96
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v3

    .line 99
    :goto_2
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 100
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->c:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    if-eqz v3, :cond_2

    .line 105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_2
    iget-object v3, v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-object v3, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :cond_3
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->b:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 116
    iget-object v2, v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 117
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->b:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move-object v2, v3

    .line 82
    goto :goto_1

    .line 121
    :cond_5
    iget-object v0, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->b:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderHeader;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 131
    :pswitch_1
    if-eqz p2, :cond_b

    .line 132
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 133
    instance-of v4, v2, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;

    if-eqz v4, :cond_8

    .line 134
    check-cast v2, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;

    .line 140
    :goto_3
    if-eqz p2, :cond_6

    if-nez v2, :cond_a

    .line 141
    :cond_6
    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->a:I

    .line 142
    invoke-virtual {v1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 144
    const v1, 0x7f0c00a2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 145
    const v2, 0x7f0c00a1

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 146
    new-instance v3, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;

    invoke-direct {v3, v1, v2}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 147
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v3

    .line 152
    :goto_4
    if-eqz v0, :cond_0

    .line 153
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    .line 154
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;->a:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<br/>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    :cond_7
    iget-object v2, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;->b:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 157
    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 158
    iget-object v0, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    move-object v2, v3

    .line 136
    goto :goto_3

    .line 160
    :cond_9
    iget-object v0, v1, Lcom/franco/kernel/changeloglib/ChangeLogAdapter$ViewHolderRow;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    move-object v1, v2

    goto :goto_4

    :cond_b
    move-object v2, v3

    goto :goto_3

    :cond_c
    move-object v1, v2

    goto/16 :goto_2

    :cond_d
    move-object v2, v3

    goto/16 :goto_1

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
