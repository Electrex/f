.class public Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/franco/kernel/changeloglib/ChangeLogListView;

.field private b:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

.field private c:Lcom/franco/kernel/changeloglib/XmlParser;

.field private d:Lcom/franco/kernel/changeloglib/ChangeLog;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/changeloglib/ChangeLogListView;Lcom/franco/kernel/changeloglib/ChangeLogAdapter;Lcom/franco/kernel/changeloglib/XmlParser;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->a:Lcom/franco/kernel/changeloglib/ChangeLogListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->b:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    .line 178
    iput-object p3, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->c:Lcom/franco/kernel/changeloglib/XmlParser;

    .line 179
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;)Lcom/franco/kernel/changeloglib/ChangeLog;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->d:Lcom/franco/kernel/changeloglib/ChangeLog;

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;)Lcom/franco/kernel/changeloglib/ChangeLogAdapter;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->b:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 184
    :try_start_0
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->c:Lcom/franco/kernel/changeloglib/XmlParser;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->c:Lcom/franco/kernel/changeloglib/XmlParser;

    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/XmlParser;->a()Lcom/franco/kernel/changeloglib/ChangeLog;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->d:Lcom/franco/kernel/changeloglib/ChangeLog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;-><init>(Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    :try_start_1
    sget-object v1, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;->a:Lcom/franco/kernel/changeloglib/ChangeLogListView;

    invoke-virtual {v2}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070085

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;-><init>(Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;

    invoke-direct {v2, p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread$1;-><init>(Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method
