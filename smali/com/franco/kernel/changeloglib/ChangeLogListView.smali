.class public Lcom/franco/kernel/changeloglib/ChangeLogListView;
.super Landroid/widget/ListView;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field protected static a:Ljava/lang/String;


# instance fields
.field protected b:I

.field protected c:I

.field protected d:I

.field protected e:Ljava/lang/String;

.field protected f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "ChangeLogListView"

    sput-object v0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 45
    const v0, 0x7f040030

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    .line 46
    const v0, 0x7f040031

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    .line 48
    iput-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 45
    const v0, 0x7f040030

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    .line 46
    const v0, 0x7f040031

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    .line 47
    iput v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    .line 48
    iput-object v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, v2, v1}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a(Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const v0, 0x7f040030

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    .line 46
    const v0, 0x7f040031

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    .line 47
    iput v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    .line 67
    invoke-virtual {p0, p2, v1}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a(Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    const v0, 0x7f040030

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    .line 46
    const v0, 0x7f040031

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    .line 72
    invoke-virtual {p0, p2, p3}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a(Landroid/util/AttributeSet;I)V

    .line 73
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 135
    new-instance v0, Lcom/franco/kernel/changeloglib/XmlParser;

    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/franco/kernel/changeloglib/XmlParser;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 139
    :goto_0
    new-instance v1, Lcom/franco/kernel/changeloglib/ChangeLog;

    invoke-direct {v1}, Lcom/franco/kernel/changeloglib/ChangeLog;-><init>()V

    .line 142
    new-instance v2, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Lcom/franco/kernel/changeloglib/ChangeLog;->a()Ljava/util/LinkedList;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    .line 143
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    invoke-virtual {v1, v2}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->a(I)V

    .line 144
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    invoke-virtual {v1, v2}, Lcom/franco/kernel/changeloglib/ChangeLogAdapter;->b(I)V

    .line 147
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/changeloglib/Util;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 148
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;

    iget-object v3, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    invoke-direct {v2, p0, v3, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView$ChangelogThread;-><init>(Lcom/franco/kernel/changeloglib/ChangeLogListView;Lcom/franco/kernel/changeloglib/ChangeLogAdapter;Lcom/franco/kernel/changeloglib/XmlParser;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 152
    :goto_1
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->f:Lcom/franco/kernel/changeloglib/ChangeLogAdapter;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setAdapter(Lcom/franco/kernel/changeloglib/ChangeLogAdapter;)V

    .line 157
    :goto_2
    return-void

    .line 137
    :cond_1
    new-instance v0, Lcom/franco/kernel/changeloglib/XmlParser;

    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    invoke-direct {v0, v1, v2}, Lcom/franco/kernel/changeloglib/XmlParser;-><init>(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    sget-object v1, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070085

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 150
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701de

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected a(Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b(Landroid/util/AttributeSet;I)V

    .line 89
    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->a()V

    .line 92
    const/high16 v0, -0x3e800000    # -16.0f

    invoke-static {v0}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setDividerHeight(I)V

    .line 93
    invoke-virtual {p0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setClipToPadding(Z)V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setVerticalScrollBarEnabled(Z)V

    .line 95
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setOverScrollMode(I)V

    .line 96
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setFadingEdgeLength(I)V

    .line 97
    invoke-virtual {p0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 98
    return-void
.end method

.method protected b(Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/R$styleable;->ChangeLogListView:[I

    invoke-virtual {v0, p1, v1, p2, p2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 112
    const/4 v0, 0x0

    :try_start_0
    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->b:I

    .line 113
    const/4 v0, 0x1

    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->c:I

    .line 116
    const/4 v0, 0x2

    iget v2, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->d:I

    .line 117
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/ChangeLogListView;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 124
    return-void

    .line 122
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public setAdapter(Lcom/franco/kernel/changeloglib/ChangeLogAdapter;)V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 164
    return-void
.end method
