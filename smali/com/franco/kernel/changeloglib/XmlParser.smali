.class public Lcom/franco/kernel/changeloglib/XmlParser;
.super Lcom/franco/kernel/changeloglib/BaseParser;
.source "SourceFile"


# static fields
.field private static c:Ljava/lang/String;


# instance fields
.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-string v0, "XmlParser"

    sput-object v0, Lcom/franco/kernel/changeloglib/XmlParser;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/franco/kernel/changeloglib/BaseParser;-><init>(Landroid/content/Context;)V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/franco/kernel/changeloglib/XmlParser;->d:I

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/XmlParser;->e:Ljava/lang/String;

    .line 106
    iput p2, p0, Lcom/franco/kernel/changeloglib/XmlParser;->d:I

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/franco/kernel/changeloglib/BaseParser;-><init>(Landroid/content/Context;)V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/franco/kernel/changeloglib/XmlParser;->d:I

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/changeloglib/XmlParser;->e:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/franco/kernel/changeloglib/XmlParser;->e:Ljava/lang/String;

    .line 118
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 261
    if-nez p1, :cond_0

    .line 298
    :goto_0
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x2

    const-string v1, "changelogtext"

    invoke-interface {p1, v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 266
    const-string v1, "changelogtext"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267
    new-instance v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;

    invoke-direct {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;-><init>()V

    .line 268
    invoke-virtual {v0, p3}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->c(Ljava/lang/String;)V

    .line 271
    const-string v1, "changeTextTitle"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    if-eqz v1, :cond_1

    .line 273
    invoke-virtual {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->d(Ljava/lang/String;)V

    .line 276
    :cond_1
    const-string v1, "bulletedList"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    if-eqz v1, :cond_3

    .line 278
    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 279
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->b(Z)V

    .line 288
    :goto_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    .line 289
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 290
    if-nez v1, :cond_4

    .line 291
    new-instance v0, Lcom/franco/kernel/changeloglib/ChangeLogException;

    const-string v1, "ChangeLogText required in changeLogText node"

    invoke-direct {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->b(Z)V

    goto :goto_1

    .line 284
    :cond_3
    iget-boolean v1, p0, Lcom/franco/kernel/changeloglib/BaseParser;->b:Z

    invoke-virtual {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->b(Z)V

    goto :goto_1

    .line 292
    :cond_4
    invoke-virtual {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->a(Ljava/lang/String;)V

    .line 293
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 295
    :cond_5
    invoke-virtual {p2, v0}, Lcom/franco/kernel/changeloglib/ChangeLog;->a(Lcom/franco/kernel/changeloglib/ChangeLogRow;)V

    .line 297
    :cond_6
    const/4 v0, 0x3

    const-string v1, "changelogtext"

    invoke-interface {p1, v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/franco/kernel/changeloglib/ChangeLog;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 134
    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/XmlParser;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/franco/kernel/changeloglib/BaseParser;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/franco/kernel/changeloglib/Util;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/franco/kernel/changeloglib/XmlParser;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    .line 144
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 147
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 148
    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 149
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 150
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 153
    new-instance v2, Lcom/franco/kernel/changeloglib/ChangeLog;

    invoke-direct {v2}, Lcom/franco/kernel/changeloglib/ChangeLog;-><init>()V

    .line 155
    invoke-virtual {p0, v1, v2}, Lcom/franco/kernel/changeloglib/XmlParser;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;)V

    .line 158
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 171
    return-object v2

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/changeloglib/XmlParser;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/franco/kernel/changeloglib/XmlParser;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_2
    sget-object v0, Lcom/franco/kernel/changeloglib/XmlParser;->c:Ljava/lang/String;

    const-string v1, "Changelog.xml not found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    new-instance v0, Lcom/franco/kernel/changeloglib/ChangeLogException;

    const-string v1, "Changelog.xml not found"

    invoke-direct {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :catch_0
    move-exception v0

    .line 164
    sget-object v1, Lcom/franco/kernel/changeloglib/XmlParser;->c:Ljava/lang/String;

    const-string v2, "XmlPullParseException while parsing changelog file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 165
    throw v0

    .line 166
    :catch_1
    move-exception v0

    .line 167
    sget-object v1, Lcom/franco/kernel/changeloglib/XmlParser;->c:Ljava/lang/String;

    const-string v2, "Error i/o with changelog.xml"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 168
    throw v0
.end method

.method protected a(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const-string v0, "changelog"

    invoke-interface {p1, v4, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "bulletedList"

    invoke-interface {p1, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_2

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 191
    :cond_2
    invoke-virtual {p2, v3}, Lcom/franco/kernel/changeloglib/ChangeLog;->a(Z)V

    .line 192
    iput-boolean v3, p0, Lcom/franco/kernel/changeloglib/BaseParser;->b:Z

    .line 199
    :cond_3
    :goto_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 200
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 204
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 206
    const-string v1, "changelogversion"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/changeloglib/XmlParser;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;)V

    goto :goto_0

    .line 194
    :cond_4
    invoke-virtual {p2, v2}, Lcom/franco/kernel/changeloglib/ChangeLog;->a(Z)V

    .line 195
    iput-boolean v2, p0, Lcom/franco/kernel/changeloglib/BaseParser;->b:Z

    goto :goto_1
.end method

.method protected b(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 224
    if-nez p1, :cond_1

    .line 250
    :cond_0
    return-void

    .line 226
    :cond_1
    const-string v0, "changelogversion"

    invoke-interface {p1, v3, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v0, "versionName"

    invoke-interface {p1, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    const-string v1, "changeDate"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 231
    if-nez v0, :cond_2

    .line 232
    new-instance v0, Lcom/franco/kernel/changeloglib/ChangeLogException;

    const-string v1, "VersionName required in changeLogVersion node"

    invoke-direct {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_2
    new-instance v2, Lcom/franco/kernel/changeloglib/ChangeLogRowHeader;

    invoke-direct {v2}, Lcom/franco/kernel/changeloglib/ChangeLogRowHeader;-><init>()V

    .line 235
    invoke-virtual {v2, v0}, Lcom/franco/kernel/changeloglib/ChangeLogRowHeader;->c(Ljava/lang/String;)V

    .line 236
    invoke-virtual {v2, v1}, Lcom/franco/kernel/changeloglib/ChangeLogRowHeader;->e(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p2, v2}, Lcom/franco/kernel/changeloglib/ChangeLog;->a(Lcom/franco/kernel/changeloglib/ChangeLogRow;)V

    .line 240
    :cond_3
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 241
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 244
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 246
    const-string v2, "changelogtext"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 247
    invoke-direct {p0, p1, p2, v0}, Lcom/franco/kernel/changeloglib/XmlParser;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/franco/kernel/changeloglib/ChangeLog;Ljava/lang/String;)V

    goto :goto_0
.end method
