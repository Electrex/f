.class public Lcom/franco/kernel/devices/Flo;
.super Lcom/franco/kernel/abstracts/Device;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/Device;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f070114

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 27
    const-string v0, "http://kernels.franco-lnx.net/%s/%s/"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Nexus7_2"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/franco/kernel/devices/Flo;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f070115

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/franco/kernel/abstracts/AbstractColors;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/franco/kernel/display_calibration/FloColors;

    invoke-direct {v0}, Lcom/franco/kernel/display_calibration/FloColors;-><init>()V

    return-object v0
.end method

.method public h()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/franco/kernel/per_app_profiles/FloPowerProfile;

    invoke-direct {v0}, Lcom/franco/kernel/per_app_profiles/FloPowerProfile;-><init>()V

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "/sys/devices/virtual/thermal/thermal_zone0/temp"

    invoke-static {v0}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/helpers/TemperatureHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
