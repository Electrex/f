.class public Lcom/franco/kernel/devices/Grouper;
.super Lcom/franco/kernel/abstracts/Device;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/Device;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f07013c

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 27
    const-string v0, "http://kernels.franco-lnx.net/%s/%s/"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Nexus7"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/franco/kernel/devices/Grouper;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f07013d

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/franco/kernel/per_app_profiles/GrouperPowerProfile;

    invoke-direct {v0}, Lcom/franco/kernel/per_app_profiles/GrouperPowerProfile;-><init>()V

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    const-string v0, "/sys/devices/platform/tegra-i2c.4/i2c-4/4-004c/temperature"

    invoke-static {v0}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/franco/kernel/helpers/TemperatureHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
