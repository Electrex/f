.class public Lcom/franco/kernel/devices/Manta;
.super Lcom/franco/kernel/abstracts/Device;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/Device;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    const v0, 0x7f07019f

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 22
    const-string v0, "http://kernels.franco-lnx.net/%s/%s/"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Nexus10"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/franco/kernel/devices/Manta;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f0701a0

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/franco/kernel/per_app_profiles/MantaPowerProfile;

    invoke-direct {v0}, Lcom/franco/kernel/per_app_profiles/MantaPowerProfile;-><init>()V

    return-object v0
.end method
