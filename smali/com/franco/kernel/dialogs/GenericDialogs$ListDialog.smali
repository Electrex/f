.class public Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field public a:Landroid/widget/ArrayAdapter;

.field protected mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field protected tvAddNewProfile:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020015
    .end annotation
.end field

.field protected tvTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020014
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->requestWindowFeature(I)Z

    .line 62
    const v0, 0x7f040060

    invoke-virtual {p0, v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->setContentView(I)V

    .line 63
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Dialog;)V

    .line 65
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04005f

    invoke-direct {v0, p1, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a:Landroid/widget/ArrayAdapter;

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 71
    return-void
.end method

.method public b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->tvTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public c()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->tvAddNewProfile:Landroid/widget/TextView;

    return-object v0
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 93
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 94
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 95
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x1020015
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->dismiss()V

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/franco/kernel/activities/NewPowerProfile;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 89
    return-void
.end method
