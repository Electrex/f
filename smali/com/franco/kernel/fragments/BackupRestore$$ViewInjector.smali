.class public Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/BackupRestore;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const v5, 0x7f0c0108

    const v4, 0x7f0c0107

    const v3, 0x7f0c0106

    const v2, 0x1020004

    .line 11
    const-string v0, "field \'mCustomToolbar\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mCustomToolbar\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 13
    const-string v0, "field \'mViewStub\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mViewStub\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    .line 15
    const-string v0, "field \'ivBackup\' and method \'onBackupClick\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'ivBackup\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p2, Lcom/franco/kernel/fragments/BackupRestore;->ivBackup:Landroid/widget/ImageButton;

    .line 17
    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector;Lcom/franco/kernel/fragments/BackupRestore;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    const-string v0, "field \'mRecyclerView\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    const-string v1, "field \'mRecyclerView\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 27
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/BackupRestore;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/BackupRestore;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    .line 32
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore;->ivBackup:Landroid/widget/ImageButton;

    .line 33
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 34
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/BackupRestore$$ViewInjector;->reset(Lcom/franco/kernel/fragments/BackupRestore;)V

    return-void
.end method
