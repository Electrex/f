.class public Lcom/franco/kernel/fragments/BackupRestore$BackupDialog;
.super Lcom/franco/kernel/activities/DialogEditTextActivity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/franco/kernel/activities/DialogEditTextActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackupClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 278
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/helpers/StringHelpers;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 280
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    invoke-static {}, Lcom/franco/kernel/helpers/KernelVersionHelpers;->a()Ljava/lang/String;

    move-result-object v0

    .line 285
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/franco/kernel/App;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    new-instance v0, Lcom/franco/kernel/fragments/BackupRestore$BackupDialog$1;

    invoke-direct {v0, p0, v1}, Lcom/franco/kernel/fragments/BackupRestore$BackupDialog$1;-><init>(Lcom/franco/kernel/fragments/BackupRestore$BackupDialog;Ljava/io/File;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v3}, Lcom/franco/kernel/abstracts/Device;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/franco/kernel/helpers/BackupHelpers;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07010e

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
