.class public Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x1020016

    const v3, 0x1020010

    const v2, 0x1020007

    .line 11
    const-string v0, "field \'title\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'title\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->title:Landroid/widget/TextView;

    .line 13
    const-string v0, "field \'summary\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'summary\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->summary:Landroid/widget/TextView;

    .line 15
    const-string v0, "field \'icon\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'icon\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 17
    const v0, 0x7f0c0062

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector;Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->title:Landroid/widget/TextView;

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->summary:Landroid/widget/TextView;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 32
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$$ViewInjector;->reset(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;)V

    return-void
.end method
