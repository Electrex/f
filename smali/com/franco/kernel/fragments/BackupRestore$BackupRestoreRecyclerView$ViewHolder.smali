.class public Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# instance fields
.field final synthetic i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

.field public icon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020007
    .end annotation
.end field

.field public summary:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020010
    .end annotation
.end field

.field public title:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020016
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    .line 184
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 185
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 186
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0062
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 192
    new-instance v1, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v2, v2, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/BackupRestore;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;-><init>(Landroid/content/Context;)V

    .line 194
    const v2, 0x7f04003c

    invoke-virtual {v1, v2}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->setContentView(I)V

    .line 195
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->show()V

    .line 197
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->a()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v3, v3, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    const v4, 0x7f070032

    invoke-virtual {v3, v4}, Lcom/franco/kernel/fragments/BackupRestore;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->b()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v3, v3, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    const v4, 0x7f070033

    invoke-virtual {v3, v4}, Lcom/franco/kernel/fragments/BackupRestore;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->d()Landroid/widget/Button;

    move-result-object v2

    .line 201
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/DialogAbstract$AbstractDialog;->c()Landroid/widget/Button;

    move-result-object v3

    .line 203
    iget-object v4, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v4, v4, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Lcom/franco/kernel/fragments/BackupRestore;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 204
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 206
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v1, v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    const v3, 0x7f0702ee

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/BackupRestore;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->i:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    iget-object v1, v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v1}, Lcom/franco/kernel/fragments/BackupRestore;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0105

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 208
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 209
    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$1;

    invoke-direct {v1, p0, v0}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder$1;-><init>(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    return-void
.end method
