.class public Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/BackupRestore;

.field private b:Ljava/util/ArrayList;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/franco/kernel/fragments/BackupRestore;Ljava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 144
    iput-object p2, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    .line 145
    iput p3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->c:I

    .line 146
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->c(Landroid/view/ViewGroup;I)Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 139
    check-cast p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;I)V

    return-void
.end method

.method public a(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 171
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->d(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 172
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public a(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;I)V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 157
    iget-object v1, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v1, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/franco/kernel/helpers/TimeHelpers;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v1, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 160
    iget-object v1, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-static {v2}, Lcom/franco/kernel/fragments/BackupRestore;->a(Lcom/franco/kernel/fragments/BackupRestore;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v1, p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;->a:Landroid/view/View;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    iget-object v0, v0, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    iget-object v0, v0, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 237
    :cond_0
    iget-object v3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->c(I)V

    .line 240
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    iget-object v0, v0, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    iget-object v3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v3}, Lcom/franco/kernel/fragments/BackupRestore;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0e0000

    iget-object v5, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 242
    return-void

    :cond_1
    move v0, v2

    .line 237
    goto :goto_0

    :cond_2
    move v0, v2

    .line 238
    goto :goto_1
.end method

.method public b(Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 245
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 247
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    iget-object v0, v0, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    invoke-virtual {v0, v6}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 251
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 252
    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->d(I)V

    .line 254
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    iget-object v0, v0, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v1}, Lcom/franco/kernel/fragments/BackupRestore;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    iget-object v3, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 257
    :cond_1
    return-void
.end method

.method public c(Landroid/view/ViewGroup;I)Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a:Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/BackupRestore;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 151
    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;-><init>(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;Landroid/view/View;)V

    return-object v1
.end method

.method public synthetic d(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 139
    check-cast p1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;->a(Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView$ViewHolder;)V

    return-void
.end method
