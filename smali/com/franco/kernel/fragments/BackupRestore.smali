.class public Lcom/franco/kernel/fragments/BackupRestore;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;


# static fields
.field public static a:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

.field static final c:I


# instance fields
.field protected b:Landroid/support/v7/widget/RecyclerView$LayoutManager;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Landroid/view/ViewStub$OnInflateListener;

.field protected ivBackup:Landroid/widget/ImageButton;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0108
    .end annotation
.end field

.field protected mCustomToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0106
    .end annotation
.end field

.field protected mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0107
    .end annotation
.end field

.field protected mViewStub:Landroid/view/ViewStub;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020004
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/franco/kernel/fragments/BackupRestore;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 260
    new-instance v0, Lcom/franco/kernel/fragments/BackupRestore$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/BackupRestore$2;-><init>(Lcom/franco/kernel/fragments/BackupRestore;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->d:Landroid/view/View$OnClickListener;

    .line 267
    new-instance v0, Lcom/franco/kernel/fragments/BackupRestore$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/BackupRestore$3;-><init>(Lcom/franco/kernel/fragments/BackupRestore;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->e:Landroid/view/ViewStub$OnInflateListener;

    .line 275
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/BackupRestore;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->d:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 132
    invoke-static {}, Lcom/franco/kernel/helpers/RootHelpers;->a()V

    .line 133
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 78
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/BackupRestore;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 81
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/BackupRestore;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 83
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->e:Landroid/view/ViewStub$OnInflateListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/franco/kernel/App;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 90
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 97
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 98
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mViewStub:Landroid/view/ViewStub;

    invoke-virtual {v1, v7}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 102
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/BackupRestore;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0e0000

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 105
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/BackupRestore;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->b:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    .line 106
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/franco/kernel/fragments/BackupRestore;->b:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 107
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v2}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 109
    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    const v2, 0x7f040022

    invoke-direct {v1, p0, v0, v2}, Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;-><init>(Lcom/franco/kernel/fragments/BackupRestore;Ljava/util/ArrayList;I)V

    sput-object v1, Lcom/franco/kernel/fragments/BackupRestore;->a:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    .line 110
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    sget-object v1, Lcom/franco/kernel/fragments/BackupRestore;->a:Lcom/franco/kernel/fragments/BackupRestore$BackupRestoreRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 112
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->ivBackup:Landroid/widget/ImageButton;

    new-instance v1, Lcom/franco/kernel/fragments/BackupRestore$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/BackupRestore$1;-><init>(Lcom/franco/kernel/fragments/BackupRestore;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 120
    iget-object v0, p0, Lcom/franco/kernel/fragments/BackupRestore;->ivBackup:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setClipToOutline(Z)V

    goto/16 :goto_0
.end method

.method public onBackupClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0108
        }
    .end annotation

    .prologue
    .line 311
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/fragments/BackupRestore$BackupDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/BackupRestore;->startActivity(Landroid/content/Intent;)V

    .line 312
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 64
    const v0, 0x7f04004b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 65
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 67
    sget-object v1, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/BackupRestore;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 68
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    const v2, 0x7f0701bd

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 70
    sget-object v1, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 71
    iget-object v1, p0, Lcom/franco/kernel/fragments/BackupRestore;->ivBackup:Landroid/widget/ImageButton;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-static {v2}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 73
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 127
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 128
    return-void
.end method
