.class public Lcom/franco/kernel/fragments/ColorControl$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 11
    const v0, 0x7f0c0109

    const-string v1, "field \'mRgbCalibration\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    .line 13
    const v0, 0x7f0c010a

    const-string v1, "field \'mGammaCalibration\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    .line 15
    const v0, 0x7f0c010e

    const-string v1, "field \'mProfiles\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    .line 17
    const v0, 0x7f0c00ef

    const-string v1, "field \'mContrast\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    .line 19
    const v0, 0x7f0c010c

    const-string v1, "field \'mHue\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    .line 21
    const v0, 0x7f0c010d

    const-string v1, "field \'mInvert\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    .line 23
    const v0, 0x7f0c00ed

    const-string v1, "field \'mSaturation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 24
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    .line 25
    const v0, 0x7f0c010b

    const-string v1, "field \'mDisplayValue\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    .line 27
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/ColorControl$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/ColorControl;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    .line 32
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    .line 33
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    .line 34
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    .line 35
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    .line 36
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    .line 37
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    .line 38
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/ColorControl$$ViewInjector;->reset(Lcom/franco/kernel/fragments/ColorControl;)V

    return-void
.end method
