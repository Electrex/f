.class Lcom/franco/kernel/fragments/ColorControl$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->n()Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 159
    const-string v0, "0"

    .line 162
    :goto_1
    new-array v3, v2, [Ljava/lang/String;

    iget-object v4, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    const v5, 0x7f0700fc

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    iget-object v7, v7, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v7}, Lcom/franco/kernel/abstracts/AbstractColors;->m()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/franco/kernel/fragments/ColorControl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v3}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 163
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    iget-object v2, v1, Lcom/franco/kernel/fragments/ColorControl;->w:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    const v3, 0x7f0700e5

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/ColorControl;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    iget-object v2, v2, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v2}, Lcom/franco/kernel/abstracts/AbstractColors;->m()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 166
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    iget-object v2, v2, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v2}, Lcom/franco/kernel/abstracts/AbstractColors;->m()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 168
    :cond_1
    return-void

    .line 151
    :pswitch_0
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto/16 :goto_0

    .line 153
    :pswitch_2
    const-string v0, "1"

    goto/16 :goto_1

    .line 156
    :pswitch_3
    const-string v0, "0"

    goto/16 :goto_1

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$1;->a:Lcom/franco/kernel/fragments/ColorControl;

    const v3, 0x7f070101

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/ColorControl;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 151
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
