.class Lcom/franco/kernel/fragments/ColorControl$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$4;->a:Lcom/franco/kernel/fragments/ColorControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 267
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/franco/kernel/devices/Tuna;

    if-ne v0, v1, :cond_0

    .line 268
    const-class v0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    .line 273
    :goto_0
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$4;->a:Lcom/franco/kernel/fragments/ColorControl;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/franco/kernel/fragments/ColorControl$4;->a:Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {v3}, Lcom/franco/kernel/fragments/ColorControl;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/franco/kernel/fragments/ColorControl;->startActivity(Landroid/content/Intent;)V

    .line 274
    return-void

    .line 270
    :cond_0
    const-class v0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;

    goto :goto_0
.end method
