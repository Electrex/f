.class Lcom/franco/kernel/fragments/ColorControl$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$7;->a:Lcom/franco/kernel/fragments/ColorControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 423
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 424
    const v1, 0x102000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 425
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 427
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 432
    :goto_0
    return-void

    .line 431
    :cond_0
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method
