.class public Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ArrayAdapter;

.field private b:Lcom/franco/kernel/abstracts/AbstractColors;

.field protected mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field

.field protected tvTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020014
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 929
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 930
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;)Lcom/franco/kernel/abstracts/AbstractColors;
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->b:Lcom/franco/kernel/abstracts/AbstractColors;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 934
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 936
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 937
    const v0, 0x7f040038

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->setContentView(I)V

    .line 938
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 940
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 943
    const v0, 0x3ec8c8c9

    .line 944
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 945
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 946
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 948
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->g()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->b:Lcom/franco/kernel/abstracts/AbstractColors;

    .line 950
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->tvTitle:Landroid/widget/TextView;

    const v1, 0x7f070098

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 952
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->mListView:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 980
    :goto_0
    return-void

    .line 956
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04005f

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->a:Landroid/widget/ArrayAdapter;

    .line 957
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->a:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->b:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->w()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 958
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 960
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/ColorControl$ColorProfiles$1;-><init>(Lcom/franco/kernel/fragments/ColorControl$ColorProfiles;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 984
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 985
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 986
    return-void
.end method
