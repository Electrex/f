.class public Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0c00d9

    const v5, 0x7f0c00d7

    const v4, 0x7f0c00d5

    const v2, 0x1020016

    const/4 v3, 0x0

    .line 11
    const-string v0, "field \'tvDialogTitle\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'tvDialogTitle\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    .line 13
    const v0, 0x7f0c00f5

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const v1, 0x7f0c00f5

    const-string v2, "field \'mRed\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mRed:Landroid/widget/EditText;

    .line 15
    const v0, 0x7f0c00f7

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const v1, 0x7f0c00f7

    const-string v2, "field \'mGreen\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mGreen:Landroid/widget/EditText;

    .line 17
    const v0, 0x7f0c00f9

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const v1, 0x7f0c00f9

    const-string v2, "field \'mBlue\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mBlue:Landroid/widget/EditText;

    .line 19
    const-string v0, "field \'sRedGrays\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    const-string v1, "field \'sRedGrays\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    .line 21
    const-string v0, "field \'sRedMids\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    const-string v1, "field \'sRedMids\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    .line 23
    const-string v0, "field \'sRedBlacks\'"

    invoke-virtual {p1, p3, v6, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 24
    const-string v1, "field \'sRedBlacks\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    .line 25
    const v0, 0x7f0c00db

    const-string v1, "field \'sRedWhites\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    const v1, 0x7f0c00db

    const-string v2, "field \'sRedWhites\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    .line 27
    const v0, 0x7f0c00dd

    const-string v1, "field \'sGreenGrays\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 28
    const v1, 0x7f0c00dd

    const-string v2, "field \'sGreenGrays\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    .line 29
    const v0, 0x7f0c00df

    const-string v1, "field \'sGreenMids\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 30
    const v1, 0x7f0c00df

    const-string v2, "field \'sGreenMids\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    .line 31
    const v0, 0x7f0c00e1

    const-string v1, "field \'sGreenBlacks\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 32
    const v1, 0x7f0c00e1

    const-string v2, "field \'sGreenBlacks\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    .line 33
    const v0, 0x7f0c00e3

    const-string v1, "field \'sGreenWhites\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 34
    const v1, 0x7f0c00e3

    const-string v2, "field \'sGreenWhites\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    .line 35
    const v0, 0x7f0c00e5

    const-string v1, "field \'sBlueGrays\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 36
    const v1, 0x7f0c00e5

    const-string v2, "field \'sBlueGrays\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    .line 37
    const v0, 0x7f0c00e7

    const-string v1, "field \'sBlueMids\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 38
    const v1, 0x7f0c00e7

    const-string v2, "field \'sBlueMids\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    .line 39
    const v0, 0x7f0c00e9

    const-string v1, "field \'sBlueBlacks\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 40
    const v1, 0x7f0c00e9

    const-string v2, "field \'sBlueBlacks\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    .line 41
    const v0, 0x7f0c00eb

    const-string v1, "field \'sBlueWhites\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 42
    const v1, 0x7f0c00eb

    const-string v2, "field \'sBlueWhites\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    .line 43
    const v0, 0x7f0c00ed

    const-string v1, "field \'sSaturation\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 44
    const v1, 0x7f0c00ed

    const-string v2, "field \'sSaturation\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    .line 45
    const v0, 0x7f0c00ef

    const-string v1, "field \'sContrast\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 46
    const v1, 0x7f0c00ef

    const-string v2, "field \'sContrast\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    .line 47
    const v0, 0x7f0c00f1

    const-string v1, "field \'sBrightness\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 48
    const v1, 0x7f0c00f1

    const-string v2, "field \'sBrightness\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    .line 49
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    .line 53
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mRed:Landroid/widget/EditText;

    .line 54
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mGreen:Landroid/widget/EditText;

    .line 55
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->mBlue:Landroid/widget/EditText;

    .line 56
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    .line 57
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    .line 58
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    .line 59
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    .line 60
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    .line 61
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    .line 62
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    .line 63
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    .line 64
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    .line 65
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    .line 66
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    .line 67
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    .line 68
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    .line 69
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    .line 70
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    .line 71
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration$$ViewInjector;->reset(Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;)V

    return-void
.end method
