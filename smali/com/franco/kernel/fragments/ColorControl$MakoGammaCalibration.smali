.class public Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/franco/kernel/abstracts/AbstractColors;

.field private b:Ljava/util/List;

.field private c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBlue:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f9
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mGreen:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f7
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mRed:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f5
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected sBlueBlacks:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00e9
    .end annotation
.end field

.field protected sBlueGrays:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00e5
    .end annotation
.end field

.field protected sBlueMids:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00e7
    .end annotation
.end field

.field protected sBlueWhites:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00eb
    .end annotation
.end field

.field protected sBrightness:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f1
    .end annotation
.end field

.field protected sContrast:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ef
    .end annotation
.end field

.field protected sGreenBlacks:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00e1
    .end annotation
.end field

.field protected sGreenGrays:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00dd
    .end annotation
.end field

.field protected sGreenMids:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00df
    .end annotation
.end field

.field protected sGreenWhites:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00e3
    .end annotation
.end field

.field protected sRedBlacks:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d9
    .end annotation
.end field

.field protected sRedGrays:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d5
    .end annotation
.end field

.field protected sRedMids:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d7
    .end annotation
.end field

.field protected sRedWhites:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00db
    .end annotation
.end field

.field protected sSaturation:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ed
    .end annotation
.end field

.field protected tvDialogTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020016
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 801
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 898
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration$1;-><init>(Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 802
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 806
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 808
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 809
    const v0, 0x7f04003d

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->setContentView(I)V

    .line 810
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 812
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 815
    const v0, 0x3ec8c8c9

    .line 816
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 817
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 818
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 820
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    const v1, 0x7f070130

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 822
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->g()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    .line 823
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->h()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    .line 831
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 832
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 833
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 834
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 835
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 836
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 837
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 838
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 839
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 840
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 841
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 842
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 843
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 844
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 845
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 848
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 849
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 850
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 851
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 852
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 853
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 854
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 855
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 856
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0x8

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 857
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0x9

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 858
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0xa

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 859
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0xb

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 860
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0xc

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 861
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0xd

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 862
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->b:Ljava/util/List;

    const/16 v2, 0xe

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 865
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 866
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 867
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 868
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 869
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 870
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 871
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 872
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 873
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 874
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 875
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 876
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 877
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 878
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 879
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 881
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 882
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 883
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 884
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sRedWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 885
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 886
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 887
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 888
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sGreenWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 889
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueGrays:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0x8

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 890
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueMids:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0x9

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 891
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueBlacks:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0xa

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 892
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBlueWhites:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0xb

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 893
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sSaturation:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0xc

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 894
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sContrast:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0xd

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 895
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->sBrightness:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$MakoGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/16 v2, 0xe

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 896
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 915
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 916
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 917
    return-void
.end method
