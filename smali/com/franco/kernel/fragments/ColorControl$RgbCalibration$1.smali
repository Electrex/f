.class Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4

    .prologue
    .line 542
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 545
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Ljava/util/List;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v2}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/franco/kernel/abstracts/AbstractColors;->a(Ljava/util/List;)V

    .line 548
    if-nez v0, :cond_2

    .line 549
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    .line 557
    :goto_0
    if-eqz p3, :cond_0

    .line 558
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 561
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 562
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-static {v2}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v2

    invoke-virtual {v2}, Lcom/franco/kernel/abstracts/AbstractColors;->g()Ljava/util/List;

    move-result-object v2

    const-string v3, "\t"

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 564
    :cond_1
    return-void

    .line 550
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 551
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    goto :goto_0

    .line 553
    :cond_3
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 568
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 572
    return-void
.end method
