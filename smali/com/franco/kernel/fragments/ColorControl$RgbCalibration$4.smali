.class Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V
    .locals 0

    .prologue
    .line 617
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 628
    const/4 v0, 0x0

    .line 629
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v1, v1, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 630
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sRed:Landroid/widget/SeekBar;

    .line 637
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_1

    .line 638
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 639
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 641
    :cond_1
    return-void

    .line 631
    :cond_2
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v1, v1, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-ne p1, v1, :cond_3

    .line 632
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sGreen:Landroid/widget/SeekBar;

    goto :goto_0

    .line 633
    :cond_3
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v1, v1, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 634
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;->a:Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sBlue:Landroid/widget/SeekBar;

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 620
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 624
    return-void
.end method
