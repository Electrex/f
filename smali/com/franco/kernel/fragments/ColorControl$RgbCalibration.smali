.class public Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/franco/kernel/abstracts/AbstractColors;

.field private b:Ljava/util/List;

.field private c:Z

.field private d:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private e:Landroid/view/View$OnFocusChangeListener;

.field private f:Landroid/view/View$OnKeyListener;

.field private g:Landroid/text/TextWatcher;

.field protected mBlue:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f9
    .end annotation
.end field

.field protected mColorPalette:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00d4
    .end annotation
.end field

.field protected mGreen:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f7
    .end annotation
.end field

.field protected mRed:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f5
    .end annotation
.end field

.field protected sBlue:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f8
    .end annotation
.end field

.field protected sGreen:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f6
    .end annotation
.end field

.field protected sRed:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f4
    .end annotation
.end field

.field protected tvDialogTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020016
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 476
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 539
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$1;-><init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->d:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 579
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$2;-><init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->e:Landroid/view/View$OnFocusChangeListener;

    .line 596
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$3;-><init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->f:Landroid/view/View$OnKeyListener;

    .line 617
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration$4;-><init>(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->g:Landroid/text/TextWatcher;

    .line 477
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Ljava/util/List;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;Z)Z
    .locals 0

    .prologue
    .line 448
    iput-boolean p1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 481
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 483
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 484
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->setContentView(I)V

    .line 485
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 487
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 490
    const v0, 0x3ec8c8c9

    .line 491
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 492
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 493
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 495
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->tvDialogTitle:Landroid/widget/TextView;

    const v1, 0x7f070228

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 497
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->g()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    .line 498
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    .line 501
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 503
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 506
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 507
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 508
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 511
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->e:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 512
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->e:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 513
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->e:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 516
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mRed:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->f:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 517
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mGreen:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->f:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 518
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->mBlue:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->f:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 521
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->s()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 522
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->s()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 523
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->s()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 526
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 527
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 528
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 531
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->d:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 532
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->d:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 533
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$RgbCalibration;->d:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 534
    return-void
.end method
