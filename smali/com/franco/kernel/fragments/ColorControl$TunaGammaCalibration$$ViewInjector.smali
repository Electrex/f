.class public Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0c00f7

    const v5, 0x7f0c00f6

    const v4, 0x7f0c00f5

    const v3, 0x7f0c00f4

    const v2, 0x1020016

    .line 11
    const-string v0, "field \'tvDialogTitle\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'tvDialogTitle\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    .line 13
    const-string v0, "field \'mRed\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mRed\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mRed:Landroid/widget/EditText;

    .line 15
    const-string v0, "field \'mGreen\'"

    invoke-virtual {p1, p3, v6, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mGreen\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mGreen:Landroid/widget/EditText;

    .line 17
    const v0, 0x7f0c00f9

    const-string v1, "field \'mBlue\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const v1, 0x7f0c00f9

    const-string v2, "field \'mBlue\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mBlue:Landroid/widget/EditText;

    .line 19
    const-string v0, "field \'sRed\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    const-string v1, "field \'sRed\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sRed:Landroid/widget/SeekBar;

    .line 21
    const-string v0, "field \'sGreen\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    const-string v1, "field \'sGreen\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sGreen:Landroid/widget/SeekBar;

    .line 23
    const v0, 0x7f0c00f8

    const-string v1, "field \'sBlue\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 24
    const v1, 0x7f0c00f8

    const-string v2, "field \'sBlue\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sBlue:Landroid/widget/SeekBar;

    .line 25
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    .line 29
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mRed:Landroid/widget/EditText;

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mGreen:Landroid/widget/EditText;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mBlue:Landroid/widget/EditText;

    .line 32
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sRed:Landroid/widget/SeekBar;

    .line 33
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sGreen:Landroid/widget/SeekBar;

    .line 34
    iput-object v0, p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sBlue:Landroid/widget/SeekBar;

    .line 35
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$$ViewInjector;->reset(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)V

    return-void
.end method
