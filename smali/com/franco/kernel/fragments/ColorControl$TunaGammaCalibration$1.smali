.class Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 713
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 716
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)Ljava/util/List;

    move-result-object v1

    add-int/lit8 v2, p2, -0x1e

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 717
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-static {v1}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    invoke-static {v2}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/franco/kernel/abstracts/AbstractColors;->b(Ljava/util/List;)V

    .line 719
    if-nez v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mRed:Landroid/widget/EditText;

    .line 727
    :goto_0
    add-int/lit8 v1, p2, -0x1e

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 728
    return-void

    .line 721
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 722
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mGreen:Landroid/widget/EditText;

    goto :goto_0

    .line 724
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;->a:Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;

    iget-object v0, v0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mBlue:Landroid/widget/EditText;

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 732
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 736
    return-void
.end method
