.class public Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/franco/kernel/abstracts/AbstractColors;

.field private b:Ljava/util/List;

.field private c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBlue:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f9
    .end annotation
.end field

.field protected mGreen:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f7
    .end annotation
.end field

.field protected mRed:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f5
    .end annotation
.end field

.field protected sBlue:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f8
    .end annotation
.end field

.field protected sGreen:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f6
    .end annotation
.end field

.field protected sRed:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00f4
    .end annotation
.end field

.field protected tvDialogTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020016
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 665
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 710
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration$1;-><init>(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 666
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)Ljava/util/List;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;)Lcom/franco/kernel/abstracts/AbstractColors;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 670
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 672
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 673
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->setContentView(I)V

    .line 674
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 676
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 679
    const v0, 0x3ec8c8c9

    .line 680
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 681
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 682
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 684
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->tvDialogTitle:Landroid/widget/TextView;

    const v1, 0x7f070130

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 686
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->g()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    .line 687
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->h()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    .line 690
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mRed:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mGreen:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 692
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->mBlue:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 695
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 696
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 697
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->a:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 700
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1e

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 701
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1e

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 702
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1e

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 705
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sRed:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 706
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sGreen:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 707
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->sBlue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl$TunaGammaCalibration;->c:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 708
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 741
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 742
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 743
    return-void
.end method
