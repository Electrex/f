.class public Lcom/franco/kernel/fragments/ColorControl;
.super Landroid/app/Fragment;
.source "SourceFile"


# instance fields
.field protected A:Landroid/view/View;

.field protected B:Landroid/widget/SeekBar;

.field protected C:Landroid/widget/ImageView;

.field protected D:Landroid/widget/ImageView;

.field protected E:Landroid/widget/TextView;

.field protected F:Landroid/widget/TextView;

.field protected G:Landroid/view/View;

.field protected H:Landroid/widget/SeekBar;

.field protected I:Landroid/widget/ImageView;

.field protected J:Landroid/widget/ImageView;

.field protected K:Landroid/support/v7/widget/SwitchCompat;

.field protected L:Landroid/support/v7/widget/SwitchCompat;

.field protected M:Landroid/support/v7/widget/SwitchCompat;

.field protected N:Landroid/support/v7/widget/SwitchCompat;

.field protected O:Landroid/support/v7/widget/SwitchCompat;

.field protected P:Landroid/support/v7/widget/SwitchCompat;

.field protected Q:Landroid/support/v7/widget/SwitchCompat;

.field protected R:Landroid/support/v7/widget/SwitchCompat;

.field S:Lcom/franco/kernel/abstracts/AbstractColors;

.field private T:Landroid/view/View$OnClickListener;

.field private U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private V:Landroid/view/View$OnClickListener;

.field private W:Landroid/view/View$OnClickListener;

.field protected a:Landroid/widget/TextView;

.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/view/View;

.field protected g:Landroid/widget/TextView;

.field protected h:Landroid/widget/TextView;

.field protected i:Landroid/view/View;

.field protected j:Landroid/widget/TextView;

.field protected k:Landroid/widget/TextView;

.field protected l:Landroid/view/View;

.field protected m:Landroid/widget/SeekBar;

.field protected mContrast:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ef
    .end annotation
.end field

.field protected mDisplayValue:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010b
    .end annotation
.end field

.field protected mGammaCalibration:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010a
    .end annotation
.end field

.field protected mHue:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010c
    .end annotation
.end field

.field protected mInvert:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010d
    .end annotation
.end field

.field protected mProfiles:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010e
    .end annotation
.end field

.field protected mRgbCalibration:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0109
    .end annotation
.end field

.field protected mSaturation:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c00ed
    .end annotation
.end field

.field protected n:Landroid/widget/ImageView;

.field protected o:Landroid/widget/ImageView;

.field protected p:Landroid/widget/TextView;

.field protected q:Landroid/widget/TextView;

.field protected r:Landroid/view/View;

.field protected s:Landroid/widget/SeekBar;

.field protected t:Landroid/widget/ImageView;

.field protected u:Landroid/widget/ImageView;

.field protected v:Landroid/widget/TextView;

.field protected w:Landroid/widget/TextView;

.field protected x:Landroid/view/View;

.field protected y:Landroid/widget/TextView;

.field protected z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 146
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$1;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->T:Landroid/view/View$OnClickListener;

    .line 171
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$2;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 405
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$6;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$6;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->V:Landroid/view/View$OnClickListener;

    .line 420
    new-instance v0, Lcom/franco/kernel/fragments/ColorControl$7;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/ColorControl$7;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->W:Landroid/view/View$OnClickListener;

    .line 920
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 437
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 438
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 439
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ColorControl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/ColorControl;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 440
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x1020016

    const v6, 0x1020010

    const v5, 0x1020007

    const v4, 0x1020006

    const/4 v3, 0x0

    .line 194
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 196
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->a:Landroid/widget/TextView;

    .line 199
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->b:Landroid/widget/TextView;

    .line 200
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->K:Landroid/support/v7/widget/SwitchCompat;

    .line 201
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->c:Landroid/widget/ImageView;

    .line 203
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->a:Landroid/widget/TextView;

    const v1, 0x7f070228

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 204
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->b:Landroid/widget/TextView;

    const v1, 0x7f070227

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 205
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->K:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->K:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 207
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->d:Landroid/widget/TextView;

    .line 213
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->e:Landroid/widget/TextView;

    .line 214
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->L:Landroid/support/v7/widget/SwitchCompat;

    .line 215
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->f:Landroid/view/View;

    .line 217
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->d:Landroid/widget/TextView;

    const v1, 0x7f070130

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 218
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->e:Landroid/widget/TextView;

    const v1, 0x7f07012f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 227
    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    .line 229
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 230
    check-cast v0, Ljava/lang/String;

    .line 235
    :goto_0
    if-eqz v0, :cond_1

    .line 236
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->L:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 237
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->L:Landroid/support/v7/widget/SwitchCompat;

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->g:Landroid/widget/TextView;

    .line 244
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->h:Landroid/widget/TextView;

    .line 245
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->M:Landroid/support/v7/widget/SwitchCompat;

    .line 246
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->i:Landroid/view/View;

    .line 248
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->g:Landroid/widget/TextView;

    const v1, 0x7f070098

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 249
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->h:Landroid/widget/TextView;

    const v1, 0x7f070097

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 251
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->M:Landroid/support/v7/widget/SwitchCompat;

    const-string v1, "color_profile"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 252
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->M:Landroid/support/v7/widget/SwitchCompat;

    const-string v1, "color_profile"

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 253
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mRgbCalibration:Landroid/view/View;

    new-instance v1, Lcom/franco/kernel/fragments/ColorControl$3;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/ColorControl$3;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mGammaCalibration:Landroid/view/View;

    new-instance v1, Lcom/franco/kernel/fragments/ColorControl$4;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/ColorControl$4;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mProfiles:Landroid/view/View;

    new-instance v1, Lcom/franco/kernel/fragments/ColorControl$5;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/ColorControl$5;-><init>(Lcom/franco/kernel/fragments/ColorControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    return-void

    .line 231
    :cond_3
    instance-of v2, v0, Ljava/util/List;

    if-eqz v2, :cond_4

    .line 232
    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-static {p0, p1}, Licepick/Icepick;->b(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 137
    invoke-direct {p0}, Lcom/franco/kernel/fragments/ColorControl;->a()V

    .line 138
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 122
    const v0, 0x7f04004c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 123
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 125
    if-nez p3, :cond_0

    .line 126
    sget-object v1, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/Device;->g()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    iput-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    .line 129
    :cond_0
    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 444
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 445
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 446
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const v7, 0x1020016

    const v6, 0x1020010

    const v5, 0x1020007

    const v4, 0x1020006

    const/4 v3, 0x0

    .line 287
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 288
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 290
    invoke-direct {p0}, Lcom/franco/kernel/fragments/ColorControl;->a()V

    .line 292
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->j:Landroid/widget/TextView;

    .line 296
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->k:Landroid/widget/TextView;

    .line 297
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->N:Landroid/support/v7/widget/SwitchCompat;

    .line 298
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->l:Landroid/view/View;

    .line 299
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    const v1, 0x102000d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    .line 300
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    const v1, 0x7f0c007d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->n:Landroid/widget/ImageView;

    .line 301
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mContrast:Landroid/view/View;

    const v1, 0x7f0c007c

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->o:Landroid/widget/ImageView;

    .line 303
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->V:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->W:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 306
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->i()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 307
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    const/16 v1, 0x17f

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 308
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 309
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 311
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->j:Landroid/widget/TextView;

    const v1, 0x7f0700ba

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 312
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->m:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->N:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->i()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 315
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->N:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->i()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 318
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->p:Landroid/widget/TextView;

    .line 320
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->q:Landroid/widget/TextView;

    .line 321
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->O:Landroid/support/v7/widget/SwitchCompat;

    .line 322
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->r:Landroid/view/View;

    .line 323
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    const v1, 0x102000d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    .line 324
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    const v1, 0x7f0c007d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->t:Landroid/widget/ImageView;

    .line 325
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mHue:Landroid/view/View;

    const v1, 0x7f0c007c

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->u:Landroid/widget/ImageView;

    .line 327
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->V:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->u:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->W:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->k()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 331
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    const/16 v1, 0x600

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 332
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 333
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 335
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->p:Landroid/widget/TextView;

    const v1, 0x7f07017c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 336
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->s:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->O:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->k()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 339
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->O:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->k()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 342
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->v:Landroid/widget/TextView;

    .line 344
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->w:Landroid/widget/TextView;

    .line 345
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->P:Landroid/support/v7/widget/SwitchCompat;

    .line 346
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->x:Landroid/view/View;

    .line 348
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mInvert:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->v:Landroid/widget/TextView;

    const v1, 0x7f07017f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 350
    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->w:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->n()Ljava/lang/String;

    move-result-object v0

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0700e5

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->P:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->m()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 353
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->P:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->m()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 356
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->y:Landroid/widget/TextView;

    .line 358
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->z:Landroid/widget/TextView;

    .line 359
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->Q:Landroid/support/v7/widget/SwitchCompat;

    .line 360
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->A:Landroid/view/View;

    .line 361
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    const v1, 0x102000d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    .line 362
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    const v1, 0x7f0c007d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->C:Landroid/widget/ImageView;

    .line 363
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mSaturation:Landroid/view/View;

    const v1, 0x7f0c007c

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->D:Landroid/widget/ImageView;

    .line 365
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->C:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->V:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->D:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->W:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->o()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 369
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    const/16 v1, 0x17f

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 370
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 371
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 373
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->y:Landroid/widget/TextView;

    const v1, 0x7f07022a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 374
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->B:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->Q:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->o()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 377
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->Q:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->o()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 380
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    invoke-static {v0, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->E:Landroid/widget/TextView;

    .line 382
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    invoke-static {v0, v6}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->F:Landroid/widget/TextView;

    .line 383
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    invoke-static {v0, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->R:Landroid/support/v7/widget/SwitchCompat;

    .line 384
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    invoke-static {v0, v5}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->G:Landroid/view/View;

    .line 385
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    const v1, 0x102000d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    .line 386
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    const v1, 0x7f0c007d

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->I:Landroid/widget/ImageView;

    .line 387
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->mDisplayValue:Landroid/view/View;

    const v1, 0x7f0c007c

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->J:Landroid/widget/ImageView;

    .line 389
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->I:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->V:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->J:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->W:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->q()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 393
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    const/16 v1, 0x17f

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 394
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 395
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->U:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 397
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->E:Landroid/widget/TextView;

    const v1, 0x7f0700ea

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 398
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->F:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->H:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->R:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->q()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setTag(Ljava/lang/Object;)V

    .line 401
    iget-object v0, p0, Lcom/franco/kernel/fragments/ColorControl;->R:Landroid/support/v7/widget/SwitchCompat;

    iget-object v1, p0, Lcom/franco/kernel/fragments/ColorControl;->S:Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->q()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 403
    :cond_0
    return-void

    .line 350
    :cond_1
    const v0, 0x7f070101

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ColorControl;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 143
    invoke-static {p0, p1}, Licepick/Icepick;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 144
    return-void
.end method
