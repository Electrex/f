.class public Lcom/franco/kernel/fragments/CpuManager$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const v2, 0x102000a

    .line 11
    const-string v0, "field \'listView\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'listView\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    .line 13
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/CpuManager$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/CpuManager;)V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    .line 17
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/CpuManager$$ViewInjector;->reset(Lcom/franco/kernel/fragments/CpuManager;)V

    return-void
.end method
