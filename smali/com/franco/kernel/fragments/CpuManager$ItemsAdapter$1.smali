.class Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 157
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 159
    iget-object v2, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 211
    sget-object v1, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v2, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$4;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$4;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 221
    :goto_1
    return-void

    .line 159
    :sswitch_0
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 163
    :pswitch_0
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 164
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 165
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 166
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$1;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 178
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 179
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 182
    :pswitch_1
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 183
    invoke-static {}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 184
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 185
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$2;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 198
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 199
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 202
    :pswitch_2
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$3;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1$3;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 159
    nop

    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x6c90c8a0 -> :sswitch_2
        -0xcb9691c -> :sswitch_4
        -0x2e11bc0 -> :sswitch_3
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
