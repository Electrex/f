.class public Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:I

.field public c:Ljava/util/List;

.field final synthetic d:Lcom/franco/kernel/fragments/CpuManager;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/fragments/CpuManager;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    .line 104
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 154
    new-instance v0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$1;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    .line 105
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    .line 106
    iput p3, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->b:I

    .line 107
    iput-object p4, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->c:Ljava/util/List;

    .line 108
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 130
    iget-object v0, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 132
    if-nez p2, :cond_0

    .line 133
    iget-object v1, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 134
    new-instance v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;-><init>(Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;Landroid/view/View;)V

    .line 135
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 141
    :goto_0
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    invoke-static {v3, v0}, Lcom/franco/kernel/fragments/CpuManager;->a(Lcom/franco/kernel/fragments/CpuManager;Lcom/franco/kernel/FileTunable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    iget-object v4, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/franco/kernel/fragments/CpuManager;->a(Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->info:Lcom/franco/kernel/views/ImageViewInfo;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewInfo;->setTag(Ljava/lang/Object;)V

    .line 145
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v2, v0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setTag(Ljava/lang/Object;)V

    .line 146
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    iget-object v3, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->d:Lcom/franco/kernel/fragments/CpuManager;

    iget-object v4, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/franco/kernel/fragments/CpuManager;->b(Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setVisibility(I)V

    .line 147
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v0}, Lcom/franco/kernel/FileTunable;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setChecked(Z)V

    .line 148
    iget-object v2, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 149
    iget-object v0, v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    return-object p2

    .line 137
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter$ViewHolder;

    goto :goto_0
.end method
