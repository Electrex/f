.class public Lcom/franco/kernel/fragments/CpuManager;
.super Landroid/app/ListFragment;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field private static a:Ljava/util/LinkedHashMap;


# instance fields
.field protected listView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const v3, 0x7f070135

    .line 43
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    .line 46
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701a2

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701b1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0700c0

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702d5

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070132

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0700bd

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/cpu_boost/parameters/"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    invoke-static {v3}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/devfreq/fdb00000.qcom,kgsl-3d0/"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :goto_0
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070178

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/mako_hotplug_control/"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void

    .line 55
    :cond_0
    sget-object v0, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    invoke-static {v3}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/kgsl/kgsl-3d0/"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/franco/kernel/fragments/CpuManager;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 226
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 233
    const/16 v0, 0x8

    :pswitch_0
    return v0

    .line 226
    :sswitch_0
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x6c90c8a0 -> :sswitch_2
        -0x2e11bc0 -> :sswitch_3
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/franco/kernel/FileTunable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    iget-object v0, p1, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    :cond_0
    iget-object v0, p1, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/CpuManager;Lcom/franco/kernel/FileTunable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/franco/kernel/fragments/CpuManager;->a(Lcom/franco/kernel/FileTunable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/fragments/CpuManager;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/franco/kernel/fragments/CpuManager;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 238
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 246
    const/16 v0, 0x8

    :pswitch_0
    return v0

    .line 238
    :sswitch_0
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x6c90c8a0 -> :sswitch_2
        -0xcb9691c -> :sswitch_4
        -0x2e11bc0 -> :sswitch_3
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/content/Loader;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f04002b

    invoke-direct {v1, p0, v2, v3, p2}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;-><init>(Lcom/franco/kernel/fragments/CpuManager;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->clear()V

    .line 75
    iget-object v0, p0, Lcom/franco/kernel/fragments/CpuManager;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;

    invoke-virtual {v0, p2}, Lcom/franco/kernel/fragments/CpuManager$ItemsAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lcom/franco/kernel/loaders/FileTunablesLoader;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/franco/kernel/fragments/CpuManager;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1, v2}, Lcom/franco/kernel/loaders/FileTunablesLoader;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xfad

    .line 86
    const v0, 0x7f04004d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 87
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 89
    if-nez p3, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 95
    :goto_0
    return-object v0

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 273
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 274
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    .line 275
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/CpuManager;->a(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 262
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 263
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0700c1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 265
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 266
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 267
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 268
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/CpuManager;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/CpuManager;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 269
    return-void
.end method
