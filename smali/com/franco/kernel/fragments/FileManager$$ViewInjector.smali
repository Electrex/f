.class public Lcom/franco/kernel/fragments/FileManager$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/FileManager;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const v3, 0x7f0c0106

    const v2, 0x102000a

    .line 11
    const-string v0, "field \'mCustomToolbar\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mCustomToolbar\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 13
    const v0, 0x7f0c010f

    const-string v1, "field \'mFirstTimeHelp\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager;->mFirstTimeHelp:Landroid/view/View;

    .line 15
    const-string v0, "field \'mList\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mList\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    .line 17
    const v0, 0x7f0c0110

    const-string v1, "method \'onTutorialSeenClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    new-instance v1, Lcom/franco/kernel/fragments/FileManager$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/FileManager$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/FileManager$$ViewInjector;Lcom/franco/kernel/fragments/FileManager;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/FileManager$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/FileManager;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/FileManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 30
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager;->mFirstTimeHelp:Landroid/view/View;

    .line 31
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    .line 32
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/FileManager$$ViewInjector;->reset(Lcom/franco/kernel/fragments/FileManager;)V

    return-void
.end method
