.class Lcom/franco/kernel/fragments/FileManager$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/FileManager;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/FileManager;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;

    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FileObject;

    .line 183
    iget-boolean v1, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->a:Z

    if-eqz v1, :cond_1

    .line 184
    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->e:Ljava/lang/String;

    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 185
    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    :goto_0
    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/franco/kernel/helpers/FileManagerHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 188
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/fragments/FileManager$1$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/FileManager$1$1;-><init>(Lcom/franco/kernel/fragments/FileManager$1;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 211
    :goto_1
    return-void

    .line 185
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f0701f6

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f07010c

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f0700d6

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v1, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/franco/kernel/helpers/BackupHelpers;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 200
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f070031

    invoke-virtual {v1, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_2
    new-instance v3, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v1}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 204
    const v1, 0x102000a

    invoke-virtual {v3, v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 205
    iget-object v4, p0, Lcom/franco/kernel/fragments/FileManager$1;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-static {v4}, Lcom/franco/kernel/fragments/FileManager;->b(Lcom/franco/kernel/fragments/FileManager;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 206
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->e:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setTag(Ljava/lang/Object;)V

    .line 207
    invoke-virtual {v3, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 208
    invoke-virtual {v3}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 209
    invoke-virtual {v3}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0701f7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1
.end method
