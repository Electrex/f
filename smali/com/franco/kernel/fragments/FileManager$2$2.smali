.class Lcom/franco/kernel/fragments/FileManager$2$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;

.field final synthetic c:Landroid/widget/AdapterView;

.field final synthetic d:Lcom/franco/kernel/fragments/FileManager$2;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/FileManager$2;Ljava/io/File;Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->d:Lcom/franco/kernel/fragments/FileManager$2;

    iput-object p2, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->b:Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;

    iput-object p4, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->c:Landroid/widget/AdapterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->d:Lcom/franco/kernel/fragments/FileManager$2;

    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    iget-object v0, v0, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->notifyDataSetChanged()V

    .line 286
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->b:Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->dismiss()V

    .line 287
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2$2;->c:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->dismiss()V

    .line 290
    :cond_0
    return-void
.end method
