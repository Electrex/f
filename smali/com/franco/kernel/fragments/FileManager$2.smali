.class Lcom/franco/kernel/fragments/FileManager$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/FileManager;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/FileManager;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 217
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v0, v0, v9

    check-cast v0, Ljava/lang/String;

    .line 218
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    packed-switch p3, :pswitch_data_0

    .line 317
    :goto_0
    return-void

    .line 222
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "application/*"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 226
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    const-string v1, "com.android.packageinstaller"

    const-string v2, "com.android.packageinstaller.PackageInstallerActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v2, 0x7f0700b9

    invoke-virtual {v1, v2}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 233
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v1, v0, v8}, Lcom/franco/kernel/fragments/FileManager;->startActivityForResult(Landroid/content/Intent;I)V

    .line 235
    const-string v0, "CgkIo_DA4eIIEAIQGA"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :pswitch_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 239
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f070126

    invoke-virtual {v0, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f070219

    invoke-virtual {v0, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f0702e6

    invoke-virtual {v0, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v3, 0x7f070110

    invoke-virtual {v0, v3}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v8

    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v4, 0x7f07018c

    invoke-virtual {v1, v4}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v9

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 250
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 251
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 252
    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 253
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 254
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07010c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 257
    :pswitch_2
    new-instance v0, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;-><init>(Landroid/content/Context;)V

    .line 259
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->show()V

    .line 260
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->a()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v4, 0x7f0700d6

    invoke-virtual {v3, v4}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->b()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    const v4, 0x7f0700d7

    invoke-virtual {v3, v4}, Lcom/franco/kernel/fragments/FileManager;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->c()Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f0701dc

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    .line 265
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->d()Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f0702ee

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    .line 266
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->d()Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/FileManager$2;->a:Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v3}, Lcom/franco/kernel/fragments/FileManager;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00cf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 270
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->c()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/FileManager$2$1;

    invoke-direct {v3, p0, v0}, Lcom/franco/kernel/fragments/FileManager$2$1;-><init>(Lcom/franco/kernel/fragments/FileManager$2;Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->d()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/FileManager$2$2;

    invoke-direct {v3, p0, v1, v0, p1}, Lcom/franco/kernel/fragments/FileManager$2$2;-><init>(Lcom/franco/kernel/fragments/FileManager$2;Ljava/io/File;Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;Landroid/widget/AdapterView;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 296
    :pswitch_3
    new-instance v0, Lcom/franco/kernel/fragments/FileManager$2$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/FileManager$2$3;-><init>(Lcom/franco/kernel/fragments/FileManager$2;)V

    new-array v2, v9, [Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v3}, Lcom/franco/kernel/abstracts/Device;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v8

    invoke-static {v0, v2}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    goto/16 :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
