.class public Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x1020016

    const v3, 0x1020010

    const v2, 0x1020006

    .line 11
    const-string v0, "field \'mIcon\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mIcon\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 13
    const-string v0, "field \'mTitle\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mTitle\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 15
    const-string v0, "field \'mSummary\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const-string v1, "field \'mSummary\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mSummary:Landroid/widget/TextView;

    .line 17
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 21
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 22
    iput-object v0, p1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mSummary:Landroid/widget/TextView;

    .line 23
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder$$ViewInjector;->reset(Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;)V

    return-void
.end method
