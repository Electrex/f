.class public Lcom/franco/kernel/fragments/FileManager$FilesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:I

.field public c:Ljava/util/List;

.field final synthetic d:Lcom/franco/kernel/fragments/FileManager;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/fragments/FileManager;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 134
    iput-object p1, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->d:Lcom/franco/kernel/fragments/FileManager;

    .line 135
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 136
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->a:Landroid/view/LayoutInflater;

    .line 137
    iput p3, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->b:I

    .line 138
    iput-object p4, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->c:Ljava/util/List;

    .line 139
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 158
    if-nez p2, :cond_0

    .line 159
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->b:I

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 160
    new-instance v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;-><init>(Lcom/franco/kernel/fragments/FileManager$FilesAdapter;Landroid/view/View;)V

    .line 161
    iget-object v1, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->d:Lcom/franco/kernel/fragments/FileManager;

    invoke-static {v1}, Lcom/franco/kernel/fragments/FileManager;->a(Lcom/franco/kernel/fragments/FileManager;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FileObject;

    .line 169
    iget-object v2, v1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v2, v1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 171
    iget-object v2, v1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mSummary:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->d:Lcom/franco/kernel/fragments/FileManager;

    const v4, 0x7f07018d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->d:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/franco/kernel/fragments/FileManager;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, v1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget v0, v0, Lcom/franco/kernel/fragments/FileManager$FileObject;->b:I

    invoke-static {v0}, Lcom/franco/kernel/App;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    return-object p2

    .line 164
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter$ViewHolder;

    move-object v1, v0

    goto :goto_0
.end method
