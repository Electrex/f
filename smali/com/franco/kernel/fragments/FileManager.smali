.class public Lcom/franco/kernel/fragments/FileManager;
.super Lcom/franco/kernel/abstracts/ListFragmentAbstract;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View$OnClickListener;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field protected mCustomToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0106
    .end annotation
.end field

.field protected mFirstTimeHelp:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c010f
    .end annotation
.end field

.field protected mList:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/franco/kernel/App;->c()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/ListFragmentAbstract;-><init>()V

    .line 178
    new-instance v0, Lcom/franco/kernel/fragments/FileManager$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/FileManager$1;-><init>(Lcom/franco/kernel/fragments/FileManager;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->b:Landroid/view/View$OnClickListener;

    .line 214
    new-instance v0, Lcom/franco/kernel/fragments/FileManager$2;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/FileManager$2;-><init>(Lcom/franco/kernel/fragments/FileManager;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->c:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/FileManager;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/fragments/FileManager;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->c:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Lcom/franco/kernel/loaders/FileManagerLoader;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lcom/franco/kernel/loaders/FileManagerLoader;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/franco/kernel/loaders/FileManagerLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/content/Loader;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f040048

    invoke-direct {v1, p0, v2, v3, p2}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;-><init>(Lcom/franco/kernel/fragments/FileManager;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->clear()V

    .line 77
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;

    invoke-virtual {v0, p2}, Lcom/franco/kernel/fragments/FileManager$FilesAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public a(Lcom/franco/kernel/activities/MainActivity$BackPress;)V
    .locals 3
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    .line 123
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 124
    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    :goto_0
    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/franco/kernel/helpers/FileManagerHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0xfab

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 127
    return-void

    .line 124
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/franco/kernel/abstracts/ListFragmentAbstract;->onActivityCreated(Landroid/os/Bundle;)V

    .line 118
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public synthetic onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/FileManager;->a(ILandroid/os/Bundle;)Lcom/franco/kernel/loaders/FileManagerLoader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xfab

    const/4 v4, 0x0

    .line 88
    const v0, 0x7f04004e

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 89
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 91
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 92
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 93
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 94
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/FileManager;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 96
    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    :goto_0
    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 97
    sget-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    const-string v2, "/storage/emulated/0/"

    const-string v3, "/sdcard/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    const v2, 0x7f0700d1

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 100
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    sget-object v2, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/franco/kernel/helpers/FileManagerHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 102
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "file_manager_tutorial"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mFirstTimeHelp:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 106
    :cond_0
    if-nez p3, :cond_2

    .line 107
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v6, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 112
    :goto_1
    return-object v1

    .line 96
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/franco/kernel/fragments/FileManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/FileManager;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v6, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 335
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 336
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 337
    invoke-super {p0}, Lcom/franco/kernel/abstracts/ListFragmentAbstract;->onDestroyView()V

    .line 338
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/FileManager;->a(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 329
    invoke-super {p0}, Lcom/franco/kernel/abstracts/ListFragmentAbstract;->onResume()V

    .line 330
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f07010d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 331
    return-void
.end method

.method protected onTutorialSeenClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0110
        }
    .end annotation

    .prologue
    .line 322
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "file_manager_tutorial"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 324
    iget-object v0, p0, Lcom/franco/kernel/fragments/FileManager;->mFirstTimeHelp:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 325
    return-void
.end method
