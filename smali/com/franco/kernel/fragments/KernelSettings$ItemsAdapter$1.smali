.class Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 168
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 171
    iget-object v1, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    const v3, 0x7f070180

    invoke-virtual {v2, v3}, Lcom/franco/kernel/fragments/KernelSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;-><init>(Landroid/app/Activity;)V

    .line 173
    iget-object v2, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-static {v2}, Lcom/franco/kernel/helpers/KernelSettingsHelpers;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a(Ljava/util/List;)V

    .line 175
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->a()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1$1;-><init>(Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 184
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->show()V

    .line 185
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$ListDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    iget-object v1, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    iget-object v2, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/franco/kernel/fragments/KernelSettings;->a(Lcom/franco/kernel/fragments/KernelSettings;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 189
    iget-object v2, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 206
    :goto_2
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 189
    :sswitch_0
    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "OFF"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string v3, "AUTO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    .line 192
    :pswitch_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/franco/kernel/FileTunable;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 196
    :pswitch_1
    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/franco/kernel/FileTunable;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 199
    :pswitch_2
    const-string v1, "AUTO"

    invoke-virtual {v0, v1}, Lcom/franco/kernel/FileTunable;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 202
    :pswitch_3
    const-string v1, "OFF"

    invoke-virtual {v0, v1}, Lcom/franco/kernel/FileTunable;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 210
    :cond_2
    new-instance v1, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;

    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;->a:Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    iget-object v2, v2, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v2}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;-><init>(Landroid/content/Context;)V

    .line 212
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->show()V

    .line 213
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->a()Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->e()Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 215
    invoke-virtual {v1}, Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;->d()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1$2;-><init>(Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;Lcom/franco/kernel/FileTunable;Lcom/franco/kernel/dialogs/GenericDialogs$EditDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 189
    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_2
        0x4e -> :sswitch_1
        0x59 -> :sswitch_3
        0x1314f -> :sswitch_4
        0x1ed5af -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
