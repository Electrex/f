.class public Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:I

.field public c:Ljava/util/List;

.field final synthetic d:Lcom/franco/kernel/fragments/KernelSettings;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/fragments/KernelSettings;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 116
    iput-object p1, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    .line 117
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 165
    new-instance v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$1;-><init>(Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    .line 118
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    .line 119
    iput p3, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->b:I

    .line 120
    iput-object p4, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->c:Ljava/util/List;

    .line 121
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 144
    if-nez p2, :cond_0

    .line 145
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 146
    new-instance v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;-><init>(Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;Landroid/view/View;)V

    .line 147
    iget-object v1, v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 153
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 155
    iget-object v2, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v2, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->summary:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->d:Lcom/franco/kernel/fragments/KernelSettings;

    iget-object v4, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/franco/kernel/fragments/KernelSettings;->a(Lcom/franco/kernel/fragments/KernelSettings;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v2, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->info:Lcom/franco/kernel/views/ImageViewInfo;

    iget-object v3, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewInfo;->setTag(Ljava/lang/Object;)V

    .line 158
    iget-object v2, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v2, v0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setTag(Ljava/lang/Object;)V

    .line 159
    iget-object v2, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->setOnBoot:Lcom/franco/kernel/views/ImageViewSetOnBoot;

    invoke-virtual {v0}, Lcom/franco/kernel/FileTunable;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setChecked(Z)V

    .line 160
    iget-object v1, v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;->clickableView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 162
    return-object p2

    .line 150
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter$ViewHolder;

    move-object v1, v0

    goto :goto_0
.end method
