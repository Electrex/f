.class public Lcom/franco/kernel/fragments/KernelSettings;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field private static a:Ljava/util/LinkedHashMap;


# instance fields
.field protected listView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x102000a
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    .line 44
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070180

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702ab

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/msm_thermal/parameters/temp_threshold"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070124

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/sync/parameters/fsync_enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702d1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/timed_output/vibrator/amp"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702d2

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/timed_output/vtg_level"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702d3

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/timed_output/vibrator/vtg_level"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702d4

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/drv2605/rtp_strength"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702cc

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702a8

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/tegradc.0/smartdimmer/enable"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701f2

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/omap_temp_sensor/parameters/throttle_enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070138

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/gpu_oc"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f07019a

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/logger/parameters/enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070077

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/batterylifeextender/charging_limit"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070040

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/lm3630_bl/parameters/backlight_dimmer"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070042

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0700f0

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/bus/i2c/drivers/atmel_mxt_ts/1-004a/tsp"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070232

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/wakeup/parameters/enable_si_ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701bb

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/wakeup/parameters/enable_msm_hsic_ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702e4

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/wakeup/parameters/enable_wlan_rx_wake_ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702e3

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/wakeup/parameters/enable_wlan_ctrl_wake_ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0702e5

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/module/wakeup/parameters/enable_wlan_wake_ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070191

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/leds/charging/max_brightness"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070140

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/soundcontrol/volume_boost"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070142

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/soundcontrol/headset_boost"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701ad

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/soundcontrol/mic_gain"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f0701ae

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/soundcontrol/mic_boost"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    const v1, 0x7f070298

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/sys/class/misc/soundcontrol/speaker_boost"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/KernelSettings;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/franco/kernel/fragments/KernelSettings;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 244
    invoke-direct {p0, p1}, Lcom/franco/kernel/fragments/KernelSettings;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-object p2

    .line 249
    :cond_1
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Y"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "N"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "AUTO"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "OFF"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v3

    .line 257
    :goto_1
    if-nez v0, :cond_4

    .line 258
    const-string v0, "/sys/block/mmcblk0/queue/scheduler"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {p2}, Lcom/franco/kernel/helpers/KernelSettingsHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 249
    goto :goto_1

    .line 265
    :cond_4
    const v0, 0x7f0702c6

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/KernelSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 267
    const/4 v2, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_5
    move v1, v2

    :goto_2
    packed-switch v1, :pswitch_data_0

    :goto_3
    move-object p2, v0

    .line 280
    goto :goto_0

    .line 267
    :sswitch_0
    const-string v3, "0"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_2

    :sswitch_1
    const-string v1, "N"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    goto :goto_2

    :sswitch_2
    const-string v1, "OFF"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v1, "1"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v1, "Y"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v1, "AUTO"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x5

    goto :goto_2

    .line 271
    :pswitch_0
    const v0, 0x7f0700e5

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/KernelSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 276
    :pswitch_1
    const v0, 0x7f070101

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/KernelSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 267
    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_3
        0x4e -> :sswitch_1
        0x59 -> :sswitch_4
        0x1314f -> :sswitch_2
        0x1ed5af -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/KernelSettings;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/franco/kernel/fragments/KernelSettings;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 229
    const-string v0, "/sys/class/misc/soundcontrol/volume_boost"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/misc/soundcontrol/headset_boost"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/misc/soundcontrol/speaker_boost"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/misc/soundcontrol/mic_gain"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/misc/soundcontrol/mic_boost"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/timed_output/vibrator/amp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/timed_output/vtg_level"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/class/timed_output/vibrator/vtg_level"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/drv2605/rtp_strength"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/module/msm_thermal/parameters/temp_threshold"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/gpu_oc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/module/lm3630_bl/parameters/min_brightness"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Loader;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f04002b

    invoke-direct {v1, p0, v2, v3, p2}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;-><init>(Lcom/franco/kernel/fragments/KernelSettings;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    invoke-virtual {v0}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->clear()V

    .line 88
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelSettings;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;

    invoke-virtual {v0, p2}, Lcom/franco/kernel/fragments/KernelSettings$ItemsAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lcom/franco/kernel/loaders/FileTunablesLoader;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/franco/kernel/fragments/KernelSettings;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1, v2}, Lcom/franco/kernel/loaders/FileTunablesLoader;-><init>(Landroid/content/Context;Ljava/util/LinkedHashMap;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xfac

    .line 99
    const v0, 0x7f04004f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 100
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 102
    if-nez p3, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 108
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 296
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 297
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 298
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/franco/kernel/fragments/KernelSettings;->a(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 285
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 286
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f070189

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 288
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 289
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 290
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 291
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/KernelSettings;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 292
    return-void
.end method
