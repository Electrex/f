.class public Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/KernelUpdater;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const v7, 0x102001a

    const v6, 0x1020019

    const v5, 0x1020016

    const v4, 0x1020010

    const/4 v3, 0x0

    .line 11
    const v0, 0x7f0c0094

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mCardView:Landroid/view/View;

    .line 13
    const v0, 0x7f0c0111

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const v1, 0x7f0c0111

    const-string v2, "field \'mSwipeToRefreshLayout\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 15
    const/high16 v0, 0x1020000

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mBackground:Landroid/view/View;

    .line 17
    const v0, 0x7f0c0096

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mInnerTopParent:Landroid/view/View;

    .line 19
    const v0, 0x7f0c009a

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mShareLayout:Landroid/view/View;

    .line 21
    const v0, 0x7f0c0095

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    .line 23
    const v0, 0x7f0c0098

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 24
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mDownloadButtons:Landroid/view/View;

    .line 25
    invoke-virtual {p1, p3, v6, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    const-string v1, "field \'bDownloadZip\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/franco/kernel/fragments/KernelUpdater;->bDownloadZip:Landroid/widget/Button;

    .line 27
    if-eqz v0, :cond_0

    .line 28
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    :cond_0
    invoke-virtual {p1, p3, v7, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 38
    const-string v1, "field \'bAutoFlash\'"

    invoke-virtual {p1, v0, v7, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/franco/kernel/fragments/KernelUpdater;->bAutoFlash:Landroid/widget/Button;

    .line 39
    if-eqz v0, :cond_1

    .line 40
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$2;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    :cond_1
    const v0, 0x7f0c0103

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 50
    const v1, 0x7f0c0103

    const-string v2, "field \'tvEmptyView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->tvEmptyView:Landroid/widget/TextView;

    .line 51
    invoke-virtual {p1, p3, v5, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 52
    const-string v1, "field \'tvTitle\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->tvTitle:Landroid/widget/TextView;

    .line 53
    invoke-virtual {p1, p3, v4, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 54
    const-string v1, "field \'tvSummary\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->tvSummary:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0c0106

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 56
    const v1, 0x7f0c0106

    const-string v2, "field \'mCustomToolbar\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 57
    const v0, 0x7f0c019e

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 58
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->mCardXdaLayout:Landroid/view/View;

    .line 59
    const v0, 0x7f0c009b

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 60
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->googlePlus:Landroid/view/View;

    .line 61
    if-eqz v0, :cond_2

    .line 62
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$3;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$3;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    :cond_2
    const v0, 0x7f0c009c

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 72
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->facebook:Landroid/view/View;

    .line 73
    if-eqz v0, :cond_3

    .line 74
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$4;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$4;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_3
    const v0, 0x7f0c009d

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 84
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->twitter:Landroid/view/View;

    .line 85
    if-eqz v0, :cond_4

    .line 86
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$5;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$5;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    :cond_4
    const v0, 0x7f0c009e

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 96
    iput-object v0, p2, Lcom/franco/kernel/fragments/KernelUpdater;->share:Landroid/view/View;

    .line 97
    if-eqz v0, :cond_5

    .line 98
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$6;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$6;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_5
    const v0, 0x7f0c019f

    invoke-virtual {p1, p3, v0, v3}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 108
    if-eqz v0, :cond_6

    .line 109
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$7;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector$7;-><init>(Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;Lcom/franco/kernel/fragments/KernelUpdater;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    :cond_6
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/KernelUpdater;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/KernelUpdater;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mCardView:Landroid/view/View;

    .line 122
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 123
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mBackground:Landroid/view/View;

    .line 124
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mInnerTopParent:Landroid/view/View;

    .line 125
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mShareLayout:Landroid/view/View;

    .line 126
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    .line 127
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mDownloadButtons:Landroid/view/View;

    .line 128
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->bDownloadZip:Landroid/widget/Button;

    .line 129
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->bAutoFlash:Landroid/widget/Button;

    .line 130
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->tvEmptyView:Landroid/widget/TextView;

    .line 131
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->tvTitle:Landroid/widget/TextView;

    .line 132
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->tvSummary:Landroid/widget/TextView;

    .line 133
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    .line 134
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->mCardXdaLayout:Landroid/view/View;

    .line 135
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->googlePlus:Landroid/view/View;

    .line 136
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->facebook:Landroid/view/View;

    .line 137
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->twitter:Landroid/view/View;

    .line 138
    iput-object v0, p1, Lcom/franco/kernel/fragments/KernelUpdater;->share:Landroid/view/View;

    .line 139
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/KernelUpdater$$ViewInjector;->reset(Lcom/franco/kernel/fragments/KernelUpdater;)V

    return-void
.end method
