.class public Lcom/franco/kernel/fragments/KernelUpdater;
.super Lcom/franco/kernel/abstracts/FragmentAbstract;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field protected bAutoFlash:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x102001a
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected bDownloadZip:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020019
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field private c:I

.field private d:Landroid/graphics/Rect;

.field private e:Landroid/view/View$OnTouchListener;

.field protected facebook:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c009c
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected googlePlus:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c009b
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mBackground:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020000
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mCardView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0094
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mCardXdaLayout:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c019e
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mCustomToolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0106
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mDownloadButtons:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0098
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mInnerTopParent:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0096
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mKernelState:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0095
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mShareLayout:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c009a
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0111
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected share:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c009e
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected tvEmptyView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0103
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected tvSummary:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020010
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected tvTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020016
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected twitter:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c009d
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;-><init>()V

    .line 316
    new-instance v0, Lcom/franco/kernel/fragments/KernelUpdater$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/KernelUpdater$3;-><init>(Lcom/franco/kernel/fragments/KernelUpdater;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->e:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/KernelUpdater;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->d:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/KernelUpdater;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->d:Landroid/graphics/Rect;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 299
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 313
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 305
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$2;

    invoke-direct {v1, p0, v0}, Lcom/franco/kernel/fragments/KernelUpdater$2;-><init>(Lcom/franco/kernel/fragments/KernelUpdater;I)V

    .line 312
    invoke-virtual {p1, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 295
    :goto_0
    return-void

    .line 276
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 278
    new-instance v1, Lcom/franco/kernel/fragments/KernelUpdater$1;

    invoke-direct {v1, p0, v0}, Lcom/franco/kernel/fragments/KernelUpdater$1;-><init>(Lcom/franco/kernel/fragments/KernelUpdater;I)V

    .line 285
    invoke-virtual {p1, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 286
    invoke-virtual {p2, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 287
    invoke-virtual {p3, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 288
    invoke-virtual {p4, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 291
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 292
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 293
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 294
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;)V
    .locals 5
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    .line 341
    iget-object v0, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->c:Lcom/franco/kernel/changeloglib/ChangeLogListView;

    invoke-static {v0}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a(Lcom/franco/kernel/changeloglib/ChangeLogListView;)Ljava/util/List;

    move-result-object v0

    .line 344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f070290

    invoke-static {v3}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "*\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 355
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " * "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "<br/>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 359
    :cond_0
    const-string v0, "\n\nboot.img: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v2}, Lcom/franco/kernel/abstracts/Device;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "boot-r"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".img"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    const-string v0, "\n\nzip: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v2}, Lcom/franco/kernel/abstracts/Device;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "zips/franco.Kernel-nightly-r"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".zip"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f070291

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (http://goo.gl/MtECP5)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#becauseFKU #francoKernel"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    iget v0, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->d:I

    packed-switch v0, :pswitch_data_0

    .line 389
    :goto_1
    :pswitch_0
    return-void

    .line 383
    :pswitch_1
    iget-object v0, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_1

    .line 386
    :pswitch_2
    iget-object v0, p1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$ChangelogObjects;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->b(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_1

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c009b
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;)V
    .locals 8
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 158
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v7}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 160
    invoke-virtual {p1}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;->a()Ljava/lang/String;

    move-result-object v4

    .line 167
    :try_start_0
    invoke-static {}, Lcom/franco/kernel/helpers/KernelVersionHelpers;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v2, v0

    .line 174
    :goto_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 193
    iget v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I

    if-ge v2, v0, :cond_2

    .line 194
    const v0, 0x7f02014d

    .line 195
    const v1, 0x7f07018b

    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/KernelUpdater;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0165

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    .line 203
    :goto_1
    iget-object v5, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    const v6, 0x7f0702f0

    invoke-virtual {p0, v6}, Lcom/franco/kernel/fragments/KernelUpdater;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v7

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelUpdater;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    invoke-static {v0}, Lcom/franco/kernel/App;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 208
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->bAutoFlash:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCardView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mShareLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mBackground:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mInnerTopParent:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mDownloadButtons:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->bAutoFlash:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->bDownloadZip:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCardXdaLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->googlePlus:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->facebook:Landroid/view/View;

    iget-object v2, p0, Lcom/franco/kernel/fragments/KernelUpdater;->twitter:Landroid/view/View;

    iget-object v3, p0, Lcom/franco/kernel/fragments/KernelUpdater;->share:Landroid/view/View;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/franco/kernel/fragments/KernelUpdater;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 223
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCardXdaLayout:Landroid/view/View;

    const v1, 0x1020014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0702ec

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 224
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCardXdaLayout:Landroid/view/View;

    const v1, 0x1020006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/franco/kernel/fragments/KernelUpdater;->a(Landroid/view/View;)V

    .line 226
    :cond_0
    :goto_2
    return-void

    .line 169
    :catch_0
    move-exception v0

    move v2, v3

    .line 170
    goto/16 :goto_0

    .line 175
    :catch_1
    move-exception v0

    .line 176
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCardView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mBackground:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mInnerTopParent:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mKernelState:Landroid/view/View;

    const v1, 0x7f020101

    invoke-static {v1}, Lcom/franco/kernel/App;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 183
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0701f5

    .line 184
    :goto_3
    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 186
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->tvSummary:Landroid/widget/TextView;

    const v1, 0x7f0702a0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 188
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->tvSummary:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 183
    :cond_1
    const v0, 0x7f0702cb

    goto :goto_3

    .line 198
    :cond_2
    const v0, 0x7f02014e

    .line 199
    const v1, 0x7f07018a

    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/KernelUpdater;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00bc

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    goto/16 :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 132
    invoke-super {p0, p1}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onActivityCreated(Landroid/os/Bundle;)V

    .line 134
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 135
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 137
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/high16 v1, 0x42600000    # 56.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(F)I

    move-result v1

    invoke-virtual {v0, v5, v5, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ZII)V

    .line 143
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x4

    new-array v1, v1, [I

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a014e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v1, v5

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0151

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v1, v6

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0153

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0155

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    .line 149
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 150
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v6}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 153
    :cond_1
    invoke-static {}, Lcom/franco/kernel/App;->e()Lcom/path/android/jobqueue/JobManager;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$GetKernelVersionJob;

    invoke-direct {v1}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$GetKernelVersionJob;-><init>()V

    invoke-virtual {v0, v1}, Lcom/path/android/jobqueue/JobManager;->b(Lcom/path/android/jobqueue/Job;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onCreate(Landroid/os/Bundle;)V

    .line 126
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->a:Z

    .line 127
    iget-boolean v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f040050

    :goto_0
    iput v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->c:I

    .line 128
    return-void

    .line 127
    :cond_0
    const v0, 0x7f040067

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 107
    iget v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->c:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 108
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 110
    sget-object v1, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v2, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v3, Lcom/franco/kernel/fragments/KernelUpdater;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 111
    sget-object v1, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mCustomToolbar:Landroid/support/v7/widget/Toolbar;

    const v2, 0x7f0701c0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 114
    sget-object v1, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 116
    iget-boolean v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->a:Z

    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->tvEmptyView:Landroid/widget/TextView;

    const v2, 0x7f0701de

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/KernelUpdater;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_0
    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 424
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->removeAllViewsInLayout()V

    .line 427
    :cond_0
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 429
    invoke-super {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onDestroyView()V

    .line 430
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 411
    invoke-super {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onDetach()V

    .line 412
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 415
    iget-object v0, p0, Lcom/franco/kernel/fragments/KernelUpdater;->mSwipeToRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->removeAllViewsInLayout()V

    .line 417
    :cond_0
    return-void
.end method

.method public onFlashClick(Landroid/view/View;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x1020019,
            0x102001a
        }
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020019

    if-ne v0, v1, :cond_0

    .line 235
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    iget v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/abstracts/Device;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 241
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/franco/kernel/activities/KernelDownload;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 242
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 243
    const-string v2, "new_kernel"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v0, "kernel_version"

    iget v2, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/KernelUpdater;->startActivity(Landroid/content/Intent;)V

    .line 246
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f050011

    const v2, 0x7f050010

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 247
    return-void

    .line 237
    :cond_0
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    iget v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/abstracts/Device;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 405
    invoke-super {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onResume()V

    .line 406
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f070122

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 407
    return-void
.end method

.method public onShareClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c009b,
            0x7f0c009c,
            0x7f0c009d,
            0x7f0c009e
        }
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/KernelUpdater;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/franco/kernel/fragments/KernelUpdater;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 268
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onStart()V

    .line 394
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 395
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 399
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 400
    invoke-super {p0}, Lcom/franco/kernel/abstracts/FragmentAbstract;->onStop()V

    .line 401
    return-void
.end method

.method public onXdaClick(Landroid/view/View;)V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c019f
        }
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation

    .prologue
    .line 252
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 254
    sget-object v1, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/Device;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/KernelUpdater;->startActivity(Landroid/content/Intent;)V

    .line 257
    :cond_0
    return-void
.end method
