.class public Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 11
    const v0, 0x7f0c0113

    const-string v1, "field \'tvFkUpdater\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFkUpdater:Landroid/view/View;

    .line 13
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const v0, 0x7f0c0115

    const-string v1, "field \'tvBackupRestore\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvBackupRestore:Landroid/view/View;

    .line 23
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$2;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    const v0, 0x7f0c0117

    const-string v1, "field \'tvCpuManager\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 32
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvCpuManager:Landroid/view/View;

    .line 33
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$3;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v0, 0x7f0c0119

    const-string v1, "field \'tvKernelSettings\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 42
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvKernelSettings:Landroid/view/View;

    .line 43
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$4;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const v0, 0x7f0c011b

    const-string v1, "field \'tvFileManager\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 52
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFileManager:Landroid/view/View;

    .line 53
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$5;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const v0, 0x7f0c011d

    const-string v1, "field \'tvColorControl\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 62
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvColorControl:Landroid/view/View;

    .line 63
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$6;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$6;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v0, 0x7f0c011f

    const-string v1, "field \'tvPerformanceProfiles\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 72
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerformanceProfiles:Landroid/view/View;

    .line 73
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$7;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$7;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v0, 0x7f0c0121

    const-string v1, "field \'tvPerAppModes\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 82
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerAppModes:Landroid/view/View;

    .line 83
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$8;

    invoke-direct {v1, p0, p2, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$8;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Lbutterknife/ButterKnife$Finder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v0, 0x7f0c0123

    const-string v1, "field \'tvSystemMonitor\' and method \'onSystemMonitorClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 92
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSystemMonitor:Landroid/view/View;

    .line 93
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$9;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$9;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v0, 0x7f0c0125

    const-string v1, "field \'tvSettings\' and method \'onSecondaryClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 102
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSettings:Landroid/view/View;

    .line 103
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$10;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$10;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const v0, 0x7f0c0126

    const-string v1, "field \'tvSupport\' and method \'onSecondaryClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 112
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSupport:Landroid/view/View;

    .line 113
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$11;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$11;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    const v0, 0x7f0c0127

    const-string v1, "field \'tvFeedback\' and method \'onSecondaryClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 122
    iput-object v0, p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFeedback:Landroid/view/View;

    .line 123
    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$12;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector$12;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/NavigationDrawerFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 134
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFkUpdater:Landroid/view/View;

    .line 135
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvBackupRestore:Landroid/view/View;

    .line 136
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvCpuManager:Landroid/view/View;

    .line 137
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvKernelSettings:Landroid/view/View;

    .line 138
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFileManager:Landroid/view/View;

    .line 139
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvColorControl:Landroid/view/View;

    .line 140
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerformanceProfiles:Landroid/view/View;

    .line 141
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerAppModes:Landroid/view/View;

    .line 142
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSystemMonitor:Landroid/view/View;

    .line 143
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSettings:Landroid/view/View;

    .line 144
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSupport:Landroid/view/View;

    .line 145
    iput-object v0, p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFeedback:Landroid/view/View;

    .line 146
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$$ViewInjector;->reset(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    return-void
.end method
