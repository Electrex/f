.class Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/widget/ImageView;

.field final synthetic c:Landroid/widget/LinearLayout;

.field final synthetic d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    iput-object p2, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->b:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 213
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 215
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x1020006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 220
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->a:Landroid/widget/TextView;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->b:Landroid/widget/ImageView;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 222
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->d:Lcom/franco/kernel/fragments/NavigationDrawerFragment;

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;Landroid/view/View;)Landroid/view/View;

    .line 223
    return-void
.end method
