.class public Lcom/franco/kernel/fragments/NavigationDrawerFragment;
.super Landroid/app/Fragment;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static h:Ljava/util/LinkedHashMap;


# instance fields
.field private b:Landroid/support/v4/widget/DrawerLayout;

.field private c:Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Z

.field private g:Z

.field protected tvBackupRestore:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0115
    .end annotation
.end field

.field protected tvColorControl:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c011d
    .end annotation
.end field

.field protected tvCpuManager:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0117
    .end annotation
.end field

.field protected tvFeedback:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0127
    .end annotation
.end field

.field protected tvFileManager:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c011b
    .end annotation
.end field

.field protected tvFkUpdater:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0113
    .end annotation
.end field

.field protected tvKernelSettings:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0119
    .end annotation
.end field

.field protected tvPerAppModes:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0121
    .end annotation
.end field

.field protected tvPerformanceProfiles:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c011f
    .end annotation
.end field

.field protected tvSettings:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0125
    .end annotation
.end field

.field protected tvSupport:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0126
    .end annotation
.end field

.field protected tvSystemMonitor:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0123
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/NavigationDrawerFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->e:Landroid/view/View;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148
    sget-object v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->f:Z

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    const v0, 0x7f0702a4

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 153
    sput-object p1, Lcom/franco/kernel/activities/MainActivity;->o:Ljava/lang/String;

    sput-object p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->i(Landroid/view/View;)V

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->c:Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->c:Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;

    invoke-interface {v0, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/support/v4/widget/DrawerLayout;)V
    .locals 3

    .prologue
    .line 134
    iput-object p1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->d:Landroid/view/View;

    .line 135
    iput-object p2, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    .line 137
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f020078

    const v2, 0x800003

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(II)V

    .line 140
    iget-boolean v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->f:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->h(Landroid/view/View;)V

    .line 144
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 96
    sget-object v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 97
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 175
    :try_start_0
    check-cast p1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;

    iput-object p1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->c:Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 177
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Activity must implement NavigationDrawerCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/widget/LinearLayout;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0113,
            0x7f0c0115,
            0x7f0c0117,
            0x7f0c0119,
            0x7f0c011b,
            0x7f0c011d,
            0x7f0c011f,
            0x7f0c0121
        }
    .end annotation

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->e:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    .line 225
    :goto_0
    return-void

    .line 203
    :cond_0
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 204
    const v1, 0x1020006

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 206
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Ljava/lang/String;)V

    .line 208
    sget-object v2, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v3, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;

    invoke-direct {v3, p0, v0, v1, p1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$1;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/LinearLayout;)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "navigation_drawer_learned"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->g:Z

    .line 82
    if-eqz p1, :cond_1

    .line 83
    const-string v0, "selected_navigation_drawer_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->f:Z

    .line 85
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    sput-object v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    const v0, 0x7f070122

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 102
    const v0, 0x7f040052

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 105
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f070122

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFkUpdater:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f070046

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvBackupRestore:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f0700c1

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvCpuManager:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f070189

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvKernelSettings:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f07010d

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFileManager:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f070096

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvColorControl:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f070200

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerformanceProfiles:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f0701f8

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvPerAppModes:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f0702a4

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSystemMonitor:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f07028c

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSettings:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f07029e

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSupport:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->h:Ljava/util/LinkedHashMap;

    const v2, 0x7f07010a

    invoke-virtual {p0, v2}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFeedback:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    new-instance v1, Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-direct {v1}, Lcom/franco/kernel/abstracts/AbstractColors;-><init>()V

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->x()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvColorControl:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 124
    :cond_0
    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 167
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 168
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 169
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->c:Lcom/franco/kernel/fragments/NavigationDrawerFragment$NavigationDrawerCallbacks;

    .line 185
    const-string v0, "Navigation Drawer onDetach"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 186
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 191
    const-string v0, "selected_navigation_drawer_position"

    sget-object v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method public onSecondaryClick(Landroid/view/View;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0125,
            0x7f0c0126,
            0x7f0c0127
        }
    .end annotation

    .prologue
    .line 235
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 236
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/activities/MainActivity;->o:Ljava/lang/String;

    .line 237
    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSettings:Landroid/view/View;

    if-ne p1, v1, :cond_2

    .line 240
    const-class v0, Lcom/franco/kernel/activities/SettingsActivity;

    .line 249
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 250
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->startActivity(Landroid/content/Intent;)V

    .line 251
    iget-object v0, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->b:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_1

    .line 252
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/fragments/NavigationDrawerFragment$2;

    invoke-direct {v1, p0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment$2;-><init>(Lcom/franco/kernel/fragments/NavigationDrawerFragment;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    :cond_1
    return-void

    .line 241
    :cond_2
    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvSupport:Landroid/view/View;

    if-ne p1, v1, :cond_3

    .line 242
    const-class v0, Lcom/franco/kernel/activities/SupportActivity;

    goto :goto_0

    .line 243
    :cond_3
    iget-object v1, p0, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->tvFeedback:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 244
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 245
    const-string v2, "https://play.google.com/store/apps/details?id=com.franco.kernel"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 246
    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onSystemMonitorClick(Landroid/view/View;)V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0123
        }
    .end annotation

    .prologue
    .line 229
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 230
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/franco/kernel/fragments/NavigationDrawerFragment;->a(Ljava/lang/String;)V

    .line 231
    return-void
.end method
