.class public Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/PerAppProfiles;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0c012f

    const v5, 0x7f0c012d

    const v4, 0x7f0c012c

    const v3, 0x7f0c012a

    const v2, 0x7f0c0129

    .line 11
    const v0, 0x7f0c0128

    const-string v1, "field \'mApplicationsList\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsList:Landroid/view/View;

    .line 13
    new-instance v1, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerAppProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const v0, 0x7f0c012b

    const-string v1, "field \'mNewAppProfile\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 22
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfile:Landroid/view/View;

    .line 23
    new-instance v1, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$2;-><init>(Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerAppProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    const v0, 0x7f0c012e

    const-string v1, "field \'mProfileManager\' and method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 32
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileManager:Landroid/view/View;

    .line 33
    new-instance v1, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$3;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector$3;-><init>(Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerAppProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const-string v0, "field \'mAppListShape\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 42
    const-string v1, "field \'mAppListShape\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mAppListShape:Landroid/widget/ImageView;

    .line 43
    const-string v0, "field \'mNewAppShape\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 44
    const-string v1, "field \'mNewAppShape\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppShape:Landroid/widget/ImageView;

    .line 45
    const-string v0, "field \'mProfileShape\'"

    invoke-virtual {p1, p3, v6, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 46
    const-string v1, "field \'mProfileShape\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShape:Landroid/widget/ImageView;

    .line 47
    const-string v0, "field \'mNewAppProfileTextview\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 48
    const-string v1, "field \'mNewAppProfileTextview\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfileTextview:Landroid/widget/TextView;

    .line 49
    const-string v0, "field \'mApplicationsListTextview\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 50
    const-string v1, "field \'mApplicationsListTextview\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsListTextview:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0c0130

    const-string v1, "field \'mProfileShapeTextview\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 52
    const v1, 0x7f0c0130

    const-string v2, "field \'mProfileShapeTextview\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShapeTextview:Landroid/widget/TextView;

    .line 53
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/PerAppProfiles;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/PerAppProfiles;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsList:Landroid/view/View;

    .line 57
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfile:Landroid/view/View;

    .line 58
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileManager:Landroid/view/View;

    .line 59
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mAppListShape:Landroid/widget/ImageView;

    .line 60
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppShape:Landroid/widget/ImageView;

    .line 61
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShape:Landroid/widget/ImageView;

    .line 62
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfileTextview:Landroid/widget/TextView;

    .line 63
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsListTextview:Landroid/widget/TextView;

    .line 64
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShapeTextview:Landroid/widget/TextView;

    .line 65
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/PerAppProfiles$$ViewInjector;->reset(Lcom/franco/kernel/fragments/PerAppProfiles;)V

    return-void
.end method
