.class public Lcom/franco/kernel/fragments/PerAppProfiles;
.super Landroid/app/Fragment;
.source "SourceFile"


# instance fields
.field protected mAppListShape:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0129
    .end annotation
.end field

.field protected mApplicationsList:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0128
    .end annotation
.end field

.field mApplicationsListTextview:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012a
    .end annotation
.end field

.field protected mNewAppProfile:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012b
    .end annotation
.end field

.field mNewAppProfileTextview:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012d
    .end annotation
.end field

.field protected mNewAppShape:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012c
    .end annotation
.end field

.field protected mProfileManager:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012e
    .end annotation
.end field

.field protected mProfileShape:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c012f
    .end annotation
.end field

.field mProfileShapeTextview:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0130
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/high16 v4, 0x40a00000    # 5.0f

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    .line 59
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mAppListShape:Landroid/widget/ImageView;

    invoke-static {v4}, Lcom/franco/kernel/helpers/ViewHelpers;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 62
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppShape:Landroid/widget/ImageView;

    invoke-static {v4}, Lcom/franco/kernel/helpers/ViewHelpers;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 63
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShape:Landroid/widget/ImageView;

    invoke-static {v4}, Lcom/franco/kernel/helpers/ViewHelpers;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 65
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsListTextview:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v2, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfileTextview:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v2, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 67
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileShapeTextview:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v2, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerAppProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 72
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0128,
            0x7f0c012b,
            0x7f0c012e
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mApplicationsList:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 79
    const-class v0, Lcom/franco/kernel/activities/ApplicationsList;

    .line 86
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerAppProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/PerAppProfiles;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mNewAppProfile:Landroid/view/View;

    if-ne p1, v1, :cond_2

    .line 81
    const-class v0, Lcom/franco/kernel/activities/NewPowerProfile;

    goto :goto_0

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/franco/kernel/fragments/PerAppProfiles;->mProfileManager:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 83
    const-class v0, Lcom/franco/kernel/activities/ProfileManager;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 51
    const v0, 0x7f040053

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 52
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 54
    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 102
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 92
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0701f8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 93
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerAppProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 94
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 95
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 96
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerAppProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerAppProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 97
    return-void
.end method
