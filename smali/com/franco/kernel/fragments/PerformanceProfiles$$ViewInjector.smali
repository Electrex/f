.class public Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/PerformanceProfiles;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const v3, 0x7f0c0133

    const v2, 0x7f0c0131

    .line 11
    const v0, 0x7f0c0132

    const-string v1, "field \'mBatterySaverToggle\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBatterySaverToggle:Landroid/view/View;

    .line 13
    const v0, 0x7f0c0134

    const-string v1, "field \'mProfilesParentLayout\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->mProfilesParentLayout:Landroid/view/View;

    .line 15
    const v0, 0x7f0c0135

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPowerSaving:Landroid/view/View;

    .line 17
    new-instance v1, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$1;-><init>(Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    const v0, 0x7f0c0136

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 26
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    .line 27
    new-instance v1, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$2;-><init>(Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    const v0, 0x7f0c0137

    const-string v1, "method \'onClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 36
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPerformance:Landroid/view/View;

    .line 37
    new-instance v1, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$3;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$3;-><init>(Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    const-string v0, "field \'batterySaverView\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 46
    const-string v1, "field \'batterySaverView\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->batterySaverView:Landroid/widget/LinearLayout;

    .line 47
    const-string v0, "field \'emptyView\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 48
    const-string v1, "field \'emptyView\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->emptyView:Landroid/widget/LinearLayout;

    .line 49
    const v0, 0x7f0c0138

    const-string v1, "field \'broadcastsCard\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 50
    iput-object v0, p2, Lcom/franco/kernel/fragments/PerformanceProfiles;->broadcastsCard:Landroid/view/View;

    .line 51
    const v0, 0x7f0c0139

    const-string v1, "method \'onCloseClick\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 52
    new-instance v1, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$4;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector$4;-><init>(Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/fragments/PerformanceProfiles;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/fragments/PerformanceProfiles;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBatterySaverToggle:Landroid/view/View;

    .line 64
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->mProfilesParentLayout:Landroid/view/View;

    .line 65
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPowerSaving:Landroid/view/View;

    .line 66
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    .line 67
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPerformance:Landroid/view/View;

    .line 68
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->batterySaverView:Landroid/widget/LinearLayout;

    .line 69
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->emptyView:Landroid/widget/LinearLayout;

    .line 70
    iput-object v0, p1, Lcom/franco/kernel/fragments/PerformanceProfiles;->broadcastsCard:Landroid/view/View;

    .line 71
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/fragments/PerformanceProfiles$$ViewInjector;->reset(Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    return-void
.end method
