.class Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/fragments/PerformanceProfiles$1;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 156
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 183
    :goto_0
    invoke-static {}, Lcom/franco/kernel/App;->l()V

    .line 185
    const/4 v0, 0x1

    return v0

    .line 158
    :sswitch_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0701ce

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 159
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 162
    :sswitch_1
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 163
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 166
    :sswitch_2
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07004c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 167
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 170
    :sswitch_3
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07004d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 171
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 174
    :sswitch_4
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07004e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 175
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/16 v2, 0x14

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 178
    :sswitch_5
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles$1$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    iget-object v0, v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;->a:Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-static {v0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07004f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 179
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "battery_saver_level"

    const/16 v2, 0x19

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0028 -> :sswitch_0
        0x7f0c01a4 -> :sswitch_1
        0x7f0c01a5 -> :sswitch_2
        0x7f0c01a6 -> :sswitch_3
        0x7f0c01a7 -> :sswitch_4
        0x7f0c01a8 -> :sswitch_5
    .end sparse-switch
.end method
