.class public Lcom/franco/kernel/fragments/PerformanceProfiles;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field protected batterySaverView:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0131
    .end annotation
.end field

.field protected broadcastsCard:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0138
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View$OnClickListener;

.field protected emptyView:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0133
    .end annotation
.end field

.field protected mBalance:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0136
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mBatterySaverToggle:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0132
    .end annotation
.end field

.field protected mPerformance:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0137
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mPowerSaving:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0135
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field protected mProfilesParentLayout:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0134
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 145
    new-instance v0, Lcom/franco/kernel/fragments/PerformanceProfiles$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/fragments/PerformanceProfiles$1;-><init>(Lcom/franco/kernel/fragments/PerformanceProfiles;)V

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->d:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/fragments/PerformanceProfiles;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 140
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->batterySaverView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mProfilesParentLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->emptyView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 143
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const v4, 0x1020016

    .line 229
    invoke-static {p1}, Lcom/franco/kernel/helpers/StdoutHelpers;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    .line 231
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 232
    invoke-static {}, Lcom/franco/kernel/helpers/ThemeHelpers;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x106000b

    .line 233
    :goto_0
    iget-object v2, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    sget-object v3, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 236
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_1

    .line 253
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    .line 258
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    :cond_3
    return-void

    .line 232
    :cond_4
    const v0, 0x106000c

    goto :goto_0

    .line 236
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_2
    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    .line 238
    :pswitch_3
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPowerSaving:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPowerSaving:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    goto :goto_2

    .line 243
    :pswitch_4
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBalance:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    goto :goto_2

    .line 248
    :pswitch_5
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPerformance:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 249
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mPerformance:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    goto :goto_2

    .line 236
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 125
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->b:Landroid/widget/TextView;

    const v1, 0x7f0702c3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 128
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "battery_saver_level"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 130
    if-lez v0, :cond_0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->c:Landroid/widget/TextView;

    const v2, 0x7f070063

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/franco/kernel/fragments/PerformanceProfiles;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBatterySaverToggle:Landroid/view/View;

    iget-object v1, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->c:Landroid/widget/TextView;

    const v1, 0x7f0701ce

    invoke-virtual {p0, v1}, Lcom/franco/kernel/fragments/PerformanceProfiles;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0135,
            0x7f0c0136,
            0x7f0c0137
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 196
    packed-switch v0, :pswitch_data_0

    .line 207
    const-string v0, "com.franco.kernel.BALANCE"

    .line 210
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 214
    invoke-static {}, Lcom/franco/kernel/helpers/ThemeHelpers;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x106000b

    .line 215
    :goto_1
    iget-object v1, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    sget-object v2, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 218
    :cond_0
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    .line 219
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->a:Landroid/widget/TextView;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    return-void

    .line 198
    :pswitch_0
    const-string v0, "com.franco.kernel.POWER_SAVING"

    goto :goto_0

    .line 201
    :pswitch_1
    const-string v0, "com.franco.kernel.BALANCE"

    goto :goto_0

    .line 204
    :pswitch_2
    const-string v0, "com.franco.kernel.PERFORMANCE"

    goto :goto_0

    .line 214
    :cond_1
    const v0, 0x106000c

    goto :goto_1

    .line 196
    :pswitch_data_0
    .packed-switch 0x7f0c0135
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCloseClick(Landroid/view/View;)V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0c0139
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->broadcastsCard:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 267
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "broadcasts_layout"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 268
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 121
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 105
    const v0, 0x7f040054

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 106
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBatterySaverToggle:Landroid/view/View;

    const v2, 0x1020016

    invoke-static {v0, v2}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->b:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->mBatterySaverToggle:Landroid/view/View;

    const v2, 0x1020010

    invoke-static {v0, v2}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->c:Landroid/widget/TextView;

    .line 111
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "broadcasts_layout"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->broadcastsCard:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    :cond_0
    return-object v1
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 294
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 295
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 296
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 272
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 273
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f070200

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 275
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/NavigationDrawerHelpers;->a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V

    .line 276
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 277
    sget-object v0, Lcom/franco/kernel/activities/MainActivity;->n:Landroid/support/v7/widget/Toolbar;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->b(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 278
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->f:Lcom/franco/kernel/helpers/ColorHelpers;

    const-class v2, Lcom/franco/kernel/fragments/PerformanceProfiles;

    invoke-virtual {v1, v2}, Lcom/franco/kernel/helpers/ColorHelpers;->b(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(Landroid/app/Activity;I)V

    .line 280
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-virtual {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    invoke-direct {p0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->b()V

    .line 282
    iget-object v0, p0, Lcom/franco/kernel/fragments/PerformanceProfiles;->emptyView:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0103

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0701fe

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 290
    :goto_0
    return-void

    .line 284
    :cond_0
    sget-boolean v0, Lcom/franco/kernel/App;->e:Z

    if-eqz v0, :cond_1

    .line 285
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "getprop fku.perf.profile"

    aput-object v2, v0, v1

    invoke-static {p0, v3, v0}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    goto :goto_0

    .line 287
    :cond_1
    invoke-direct {p0}, Lcom/franco/kernel/fragments/PerformanceProfiles;->b()V

    goto :goto_0
.end method
