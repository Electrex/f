.class public Lcom/franco/kernel/fragments/ThemeDonateListPreference;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const v0, 0x7f060002

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ThemeDonateListPreference;->addPreferencesFromResource(I)V

    .line 18
    const-string v0, "app_theme"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ThemeDonateListPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 19
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ThemeDonateListPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 25
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/franco/kernel/fragments/ThemeDonateListPreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/activities/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 27
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 28
    invoke-virtual {p0, v0}, Lcom/franco/kernel/fragments/ThemeDonateListPreference;->startActivity(Landroid/content/Intent;)V

    .line 30
    const/4 v0, 0x1

    return v0
.end method
