.class public Lcom/franco/kernel/helpers/AchievementHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/google/android/gms/games/Games;->g:Lcom/google/android/gms/games/achievement/Achievements;

    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/achievement/Achievements;->a(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    .line 31
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 34
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lcom/google/android/gms/games/Games;->g:Lcom/google/android/gms/games/achievement/Achievements;

    invoke-static {}, Lcom/franco/kernel/App;->i()Lcom/google/example/games/basegameutils/GameHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, p0, v2}, Lcom/google/android/gms/games/achievement/Achievements;->a(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;I)V

    .line 39
    :cond_0
    return-void
.end method
