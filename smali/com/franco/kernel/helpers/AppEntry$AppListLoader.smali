.class public Lcom/franco/kernel/helpers/AppEntry$AppListLoader;
.super Landroid/content/AsyncTaskLoader;
.source "SourceFile"


# instance fields
.field final a:Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;

.field b:Ljava/util/List;

.field c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 156
    new-instance v0, Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->a:Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;

    .line 163
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 173
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 174
    if-nez v0, :cond_0

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 180
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "system_apps"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 184
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 185
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 186
    if-nez v2, :cond_2

    .line 187
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 192
    :cond_2
    new-instance v5, Lcom/franco/kernel/helpers/AppEntry;

    invoke-direct {v5, p0, v0}, Lcom/franco/kernel/helpers/AppEntry;-><init>(Lcom/franco/kernel/helpers/AppEntry$AppListLoader;Landroid/content/pm/ApplicationInfo;)V

    .line 193
    invoke-virtual {v5, v1}, Lcom/franco/kernel/helpers/AppEntry;->a(Landroid/content/Context;)V

    .line 194
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    :cond_3
    sget-object v0, Lcom/franco/kernel/helpers/AppEntry;->a:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 201
    return-object v3
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->isReset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    if-eqz p1, :cond_0

    .line 215
    invoke-virtual {p0, p1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c(Ljava/util/List;)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    .line 219
    iput-object p1, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    .line 221
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 230
    :cond_1
    if-eqz v0, :cond_2

    .line 231
    invoke-virtual {p0, v0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c(Ljava/util/List;)V

    .line 233
    :cond_2
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 276
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->onCanceled(Ljava/lang/Object;)V

    .line 280
    invoke-virtual {p0, p1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c(Ljava/util/List;)V

    .line 281
    return-void
.end method

.method protected c(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 314
    return-void
.end method

.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 155
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 155
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b(Ljava/util/List;)V

    return-void
.end method

.method protected onReset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 288
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 291
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->onStopLoading()V

    .line 295
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c(Ljava/util/List;)V

    .line 297
    iput-object v2, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 303
    iput-object v2, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    .line 305
    :cond_1
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->a(Ljava/util/List;)V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    if-nez v0, :cond_1

    .line 248
    new-instance v0, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    invoke-direct {v0, p0}, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;-><init>(Lcom/franco/kernel/helpers/AppEntry$AppListLoader;)V

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->c:Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->a:Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;

    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/franco/kernel/helpers/AppEntry$InterestingConfigChanges;->a(Landroid/content/res/Resources;)Z

    move-result v0

    .line 255
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->takeContentChanged()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->b:Ljava/util/List;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_3

    .line 258
    :cond_2
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->forceLoad()V

    .line 260
    :cond_3
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->cancelLoad()Z

    .line 269
    return-void
.end method
