.class public Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final a:Lcom/franco/kernel/helpers/AppEntry$AppListLoader;


# direct methods
.method public constructor <init>(Lcom/franco/kernel/helpers/AppEntry$AppListLoader;)V
    .locals 2

    .prologue
    .line 131
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;->a:Lcom/franco/kernel/helpers/AppEntry$AppListLoader;

    .line 133
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 134
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;->a:Lcom/franco/kernel/helpers/AppEntry$AppListLoader;

    invoke-virtual {v1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 139
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 140
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;->a:Lcom/franco/kernel/helpers/AppEntry$AppListLoader;

    invoke-virtual {v1}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 143
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry$PackageIntentReceiver;->a:Lcom/franco/kernel/helpers/AppEntry$AppListLoader;

    invoke-virtual {v0}, Lcom/franco/kernel/helpers/AppEntry$AppListLoader;->onContentChanged()V

    .line 149
    return-void
.end method
