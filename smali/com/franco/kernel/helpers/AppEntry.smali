.class public Lcom/franco/kernel/helpers/AppEntry;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Comparator;


# instance fields
.field private final b:Landroid/content/pm/ApplicationInfo;

.field private final c:Ljava/io/File;

.field private d:Ljava/lang/String;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/franco/kernel/helpers/AppEntry$1;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/AppEntry$1;-><init>()V

    sput-object v0, Lcom/franco/kernel/helpers/AppEntry;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/franco/kernel/helpers/AppEntry$AppListLoader;Landroid/content/pm/ApplicationInfo;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p2, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    .line 30
    new-instance v0, Ljava/io/File;

    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->c:Ljava/io/File;

    .line 31
    return-void
.end method


# virtual methods
.method public a()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    if-nez v0, :cond_1

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    .line 77
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->d:Ljava/lang/String;

    .line 84
    :cond_1
    :goto_0
    return-void

    .line 79
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    .line 80
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->d:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    .line 49
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    .line 65
    :goto_0
    return-object v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    .line 65
    :cond_1
    const v0, 0x1080093

    invoke-static {v0}, Lcom/franco/kernel/App;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_2
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    if-nez v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/AppEntry;->f:Z

    .line 58
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->b:Landroid/content/pm/ApplicationInfo;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    .line 59
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/franco/kernel/helpers/AppEntry;->d:Ljava/lang/String;

    return-object v0
.end method
