.class public Lcom/franco/kernel/helpers/BackupHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([B)I
    .locals 2

    .prologue
    .line 167
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 168
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 169
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180
    invoke-static {p0}, Lcom/franco/kernel/helpers/BackupHelpers;->b(Ljava/lang/String;)I

    move-result v0

    .line 181
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dd if="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " count=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 36
    const/16 v2, 0x8

    new-array v2, v2, [B

    .line 37
    invoke-virtual {v1, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 38
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    .line 39
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MAGIC: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 41
    const-string v2, "ANDROID!"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    const/4 v0, 0x1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 46
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 11

    .prologue
    const/high16 v1, 0xc00000

    const/4 v0, 0x0

    .line 54
    sget-object v2, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/franco/kernel/devices/Bacon;

    if-ne v2, v3, :cond_1

    .line 56
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/high16 v0, 0xa00000

    .line 155
    :goto_0
    return v0

    .line 61
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 64
    :cond_1
    invoke-static {}, Leu/chainfire/libsuperuser/Shell$SU;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const-string v0, "SELinux is enforcing"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    move v0, v1

    .line 68
    goto :goto_0

    .line 74
    :cond_2
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 76
    const/16 v3, 0x8

    new-array v3, v3, [B

    .line 77
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 78
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    .line 79
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MAGIC: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 80
    const-string v3, "ANDROID!"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 81
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_3
    const/4 v3, 0x4

    new-array v3, v3, [B

    .line 87
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 88
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v4

    .line 89
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Kernel size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 93
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v5

    .line 94
    const-string v6, "Kernel address: (0x%08x)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 98
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v5

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ramdisk size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 103
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v6

    .line 104
    const-string v7, "Ramdisk address: (0x%08x)"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 108
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v6

    .line 109
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Second size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 113
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v7

    .line 114
    const-string v8, "Second address: (0x%08x)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 118
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v7

    .line 119
    const-string v8, "Tags address: (0x%08x)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 123
    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->a([B)I

    move-result v7

    .line 124
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PageSize: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 128
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 131
    const/16 v3, 0x10

    new-array v3, v3, [B

    .line 132
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 133
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 136
    const/16 v3, 0x200

    new-array v3, v3, [B

    .line 137
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 138
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Args: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/franco/kernel/helpers/BackupHelpers;->c(Ljava/lang/String;)V

    .line 141
    const/16 v3, 0x20

    new-array v3, v3, [B

    .line 142
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 145
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    add-int v1, v4, v7

    add-int/lit8 v1, v1, -0x1

    .line 148
    add-int v2, v5, v7

    add-int/lit8 v2, v2, -0x1

    .line 149
    if-lez v6, :cond_4

    .line 150
    add-int v0, v6, v7

    add-int/lit8 v0, v0, -0x1

    .line 152
    :cond_4
    add-int/2addr v1, v7

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto/16 :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    .line 155
    goto/16 :goto_0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method
