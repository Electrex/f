.class public Lcom/franco/kernel/helpers/ColorHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const v9, 0x7f0a00a5

    const v8, 0x7f0a003a

    const v7, 0x7f0a0039

    const v6, 0x7f0a0066

    const v5, 0x7f0a0065

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    .line 44
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/KernelUpdater;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a0151

    const v4, 0x7f0a0152

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-static {}, Lcom/franco/kernel/helpers/ThemeHelpers;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/BackupRestore;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a010b

    const v4, 0x7f0a010c

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :goto_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/CpuManager;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v7, v8}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/SocTunablesAbstractActivity;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v7, v8}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/VoltagesActivity;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v7, v8}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/KernelSettings;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a013d

    const v4, 0x7f0a013f

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/FileManager;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a00cf

    const v4, 0x7f0a00d0

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/ColorControl;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a0082

    const v4, 0x7f0a0084

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-static {}, Lcom/franco/kernel/helpers/ThemeHelpers;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/PerformanceProfiles;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a0078

    const v4, 0x7f0a0079

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :goto_1
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/SettingsActivity;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v9, v3}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/SupportActivity;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v9, v3}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/KernelDownload;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v5, v6}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/PerAppProfiles;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v5, v6}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/ApplicationsList;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v5, v6}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/NewPowerProfile;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v5, v6}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/activities/ProfileManager;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    invoke-virtual {v2, v5, v6}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/BackupRestore;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a0105

    const v4, 0x7f0a0107

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    const-class v1, Lcom/franco/kernel/fragments/PerformanceProfiles;

    new-instance v2, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    invoke-direct {v2, p0}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;-><init>(Lcom/franco/kernel/helpers/ColorHelpers;)V

    const v3, 0x7f0a0072

    const v4, 0x7f0a0074

    invoke-virtual {v2, v3, v4}, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a(II)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    return-object v0
.end method

.method public b(Ljava/lang/Class;)I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/franco/kernel/helpers/ColorHelpers;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;

    iget v0, v0, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a:I

    .line 77
    invoke-static {v0}, Lcom/franco/kernel/App;->c(I)V

    .line 79
    return v0
.end method
