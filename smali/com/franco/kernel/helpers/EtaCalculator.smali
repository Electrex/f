.class public Lcom/franco/kernel/helpers/EtaCalculator;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:[I

.field private static final f:[Ljava/lang/String;


# instance fields
.field private a:I

.field private b:Ljava/util/List;

.field private c:J

.field private d:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 96
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/franco/kernel/helpers/EtaCalculator;->e:[I

    .line 97
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "second"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "minute"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "hour"

    aput-object v2, v0, v1

    const-string v1, "day"

    aput-object v1, v0, v3

    sput-object v0, Lcom/franco/kernel/helpers/EtaCalculator;->f:[Ljava/lang/String;

    return-void

    .line 96
    nop

    :array_0
    .array-data 4
        0x3c
        0x3c
        0x18
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0xc8

    invoke-direct {p0, v0}, Lcom/franco/kernel/helpers/EtaCalculator;->a(I)V

    .line 31
    return-void
.end method

.method private a()D
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 110
    .line 112
    iget-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 113
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 114
    goto :goto_0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v4, v2, v0

    .line 121
    :cond_1
    return-wide v4
.end method

.method private a(JI)Ljava/lang/String;
    .locals 5

    .prologue
    .line 100
    sget-object v0, Lcom/franco/kernel/helpers/EtaCalculator;->e:[I

    array-length v0, v0

    if-eq p3, v0, :cond_0

    sget-object v0, Lcom/franco/kernel/helpers/EtaCalculator;->e:[I

    aget v0, v0, p3

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    .line 101
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/helpers/EtaCalculator;->f:[Ljava/lang/String;

    aget-object v1, v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-wide/16 v2, 0x1

    cmp-long v0, p1, v2

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_1
    return-object v0

    .line 101
    :cond_1
    const-string v0, "s"

    goto :goto_0

    .line 102
    :cond_2
    sget-object v0, Lcom/franco/kernel/helpers/EtaCalculator;->e:[I

    aget v0, v0, p3

    int-to-long v0, v0

    div-long v0, p1, v0

    add-int/lit8 v2, p3, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/franco/kernel/helpers/EtaCalculator;->a(JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 41
    iput p1, p0, Lcom/franco/kernel/helpers/EtaCalculator;->a:I

    .line 42
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    .line 43
    return-void
.end method


# virtual methods
.method public a(D)J
    .locals 7

    .prologue
    .line 52
    const-wide/16 v0, -0x1

    .line 55
    iget-wide v2, p0, Lcom/franco/kernel/helpers/EtaCalculator;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/franco/kernel/helpers/EtaCalculator;->c:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 57
    iget-wide v2, p0, Lcom/franco/kernel/helpers/EtaCalculator;->d:D

    sub-double v2, p1, v2

    .line 60
    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v0, v4, v0

    mul-double/2addr v0, v2

    .line 62
    iget-object v2, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/franco/kernel/helpers/EtaCalculator;->a:I

    if-le v0, v1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/franco/kernel/helpers/EtaCalculator;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 69
    :cond_0
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    sub-double/2addr v0, p1

    invoke-direct {p0}, Lcom/franco/kernel/helpers/EtaCalculator;->a()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-long v0, v0

    .line 73
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/franco/kernel/helpers/EtaCalculator;->c:J

    .line 74
    iput-wide p1, p0, Lcom/franco/kernel/helpers/EtaCalculator;->d:D

    .line 78
    return-wide v0
.end method

.method public a(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/franco/kernel/helpers/EtaCalculator;->a(JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " left"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
