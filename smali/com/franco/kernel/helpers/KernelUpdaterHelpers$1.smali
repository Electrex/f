.class final Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/franco/kernel/helpers/EtaCalculator;

.field protected b:I

.field protected c:F

.field protected d:I

.field final synthetic e:Landroid/app/NotificationManager;

.field final synthetic f:Z

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Landroid/support/v4/app/NotificationCompat$Builder;


# direct methods
.method constructor <init>(Landroid/app/NotificationManager;ZLjava/lang/String;Landroid/support/v4/app/NotificationCompat$Builder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iput-object p1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    iput-boolean p2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    iput-object p3, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->h:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    .line 91
    new-instance v0, Lcom/franco/kernel/helpers/EtaCalculator;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/EtaCalculator;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->a:Lcom/franco/kernel/helpers/EtaCalculator;

    .line 92
    iput v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->b:I

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    .line 94
    iput v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->d:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->a()V

    .line 99
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0700f6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 100
    return-void
.end method

.method public a(II)V
    .locals 6

    .prologue
    .line 201
    invoke-super {p0, p1, p2}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->a(II)V

    .line 202
    iget v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->b:I

    if-nez v0, :cond_0

    .line 203
    iput p2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->b:I

    .line 205
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    .line 206
    iget v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    .line 208
    iget v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    float-to-int v0, v0

    iget v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->d:I

    if-le v0, v1, :cond_1

    .line 209
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->a:Lcom/franco/kernel/helpers/EtaCalculator;

    iget v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/franco/kernel/helpers/EtaCalculator;->a(D)J

    move-result-wide v0

    .line 210
    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->h:Landroid/support/v4/app/NotificationCompat$Builder;

    const/16 v3, 0x64

    iget v4, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    float-to-int v4, v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->a(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 211
    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->h:Landroid/support/v4/app/NotificationCompat$Builder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "% - Downloaded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int/lit16 v4, p1, 0x400

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kB/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int/lit16 v4, p2, 0x400

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kB"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 213
    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->h:Landroid/support/v4/app/NotificationCompat$Builder;

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_2

    const v0, 0x7f070081

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 216
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    const/16 v1, 0xff

    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->h:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 219
    :cond_1
    iget v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->c:F

    float-to-int v0, v0

    iput v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->d:I

    .line 220
    return-void

    .line 213
    :cond_2
    iget-object v3, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->a:Lcom/franco/kernel/helpers/EtaCalculator;

    invoke-virtual {v3, v0, v1}, Lcom/franco/kernel/helpers/EtaCalculator;->a(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I[Lorg/apache/http/Header;[B)V
    .locals 9

    .prologue
    const v8, 0x7f070035

    const v7, 0x7f070034

    const/4 v4, 0x1

    const v6, 0x1080082

    const/16 v5, 0xfe

    .line 110
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 113
    iget v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->b:I

    array-length v1, p3

    if-eq v0, v1, :cond_0

    .line 114
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 116
    const v1, 0x7f0701a7

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 117
    const v1, 0x7f0701a7

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 118
    const v1, 0x7f0701a8

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 119
    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 120
    iget-object v1, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 192
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-nez v0, :cond_2

    .line 127
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "download_zip"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 130
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/franco.Kernel-r"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".zip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    .line 137
    :goto_1
    :try_start_0
    invoke-static {v2, p3}, Lorg/apache/commons/io/FileUtils;->a(Ljava/io/File;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "auto_reboot"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 144
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 146
    const v0, 0x7f0700f2

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 148
    invoke-virtual {v3, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 149
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_1

    .line 150
    if-eqz v1, :cond_3

    invoke-static {v8}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_2
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 155
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 159
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_4

    .line 161
    const-string v0, "CgkIo_DA4eIIEAIQDQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    .line 162
    const-string v0, "CgkIo_DA4eIIEAIQEw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 163
    const-string v0, "CgkIo_DA4eIIEAIQFA"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 164
    const-string v0, "CgkIo_DA4eIIEAIQFQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 165
    const-string v0, "CgkIo_DA4eIIEAIQFw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v3, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;-><init>(Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;ZLjava/io/File;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 132
    :cond_2
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/franco/kernel/App;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kernel-r"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    goto/16 :goto_1

    .line 150
    :cond_3
    invoke-static {v7}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 189
    :cond_4
    const-string v0, "CgkIo_DA4eIIEAIQDg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "auto_reboot"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 144
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 146
    const v0, 0x7f0700f2

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 148
    invoke-virtual {v3, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 149
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_5

    .line 150
    if-eqz v1, :cond_6

    invoke-static {v8}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_3
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 155
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 157
    :cond_5
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 159
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_7

    .line 161
    const-string v0, "CgkIo_DA4eIIEAIQDQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    .line 162
    const-string v0, "CgkIo_DA4eIIEAIQEw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 163
    const-string v0, "CgkIo_DA4eIIEAIQFA"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 164
    const-string v0, "CgkIo_DA4eIIEAIQFQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 165
    const-string v0, "CgkIo_DA4eIIEAIQFw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v3, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;-><init>(Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;ZLjava/io/File;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 150
    :cond_6
    invoke-static {v7}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 189
    :cond_7
    const-string v0, "CgkIo_DA4eIIEAIQDg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :catchall_0
    move-exception v0

    move-object v1, v0

    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "auto_reboot"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 144
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 146
    const v0, 0x7f0700f2

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 148
    invoke-virtual {v4, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 149
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_8

    .line 150
    if-eqz v3, :cond_9

    invoke-static {v8}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_4
    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 155
    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 157
    :cond_8
    iget-object v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->e:Landroid/app/NotificationManager;

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 159
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;->f:Z

    if-eqz v0, :cond_a

    .line 161
    const-string v0, "CgkIo_DA4eIIEAIQDQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    .line 162
    const-string v0, "CgkIo_DA4eIIEAIQEw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 163
    const-string v0, "CgkIo_DA4eIIEAIQFA"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 164
    const-string v0, "CgkIo_DA4eIIEAIQFQ"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 165
    const-string v0, "CgkIo_DA4eIIEAIQFw"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->b(Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v4, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;

    invoke-direct {v4, p0, v3, v2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1$1;-><init>(Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;ZLjava/io/File;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 191
    :goto_5
    throw v1

    .line 150
    :cond_9
    invoke-static {v7}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 189
    :cond_a
    const-string v0, "CgkIo_DA4eIIEAIQDg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V

    goto :goto_5
.end method

.method public a(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->b()V

    .line 105
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0700f3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 106
    return-void
.end method
