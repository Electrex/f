.class public Lcom/franco/kernel/helpers/KernelUpdaterHelpers$GetKernelVersionJob;
.super Lcom/path/android/jobqueue/Job;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 401
    new-instance v0, Lcom/path/android/jobqueue/Params;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/path/android/jobqueue/Params;-><init>(I)V

    invoke-virtual {v0}, Lcom/path/android/jobqueue/Params;->a()Lcom/path/android/jobqueue/Params;

    move-result-object v0

    invoke-virtual {v0}, Lcom/path/android/jobqueue/Params;->b()Lcom/path/android/jobqueue/Params;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/path/android/jobqueue/Job;-><init>(Lcom/path/android/jobqueue/Params;)V

    .line 402
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 406
    return-void
.end method

.method protected a(Ljava/lang/Throwable;)Z
    .locals 4

    .prologue
    .line 416
    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    .line 418
    const-string v1, "Malformed URL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;-><init>(Ljava/lang/String;Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;)V

    invoke-interface {v1, v2}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 420
    const/4 v0, 0x0

    .line 423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 410
    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/Device;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 411
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$KernelVersion;-><init>(Ljava/lang/String;Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;)V

    invoke-interface {v1, v2}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 412
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method
