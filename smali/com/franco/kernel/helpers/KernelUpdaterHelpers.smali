.class public Lcom/franco/kernel/helpers/KernelUpdaterHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/squareup/okhttp/OkHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/squareup/okhttp/OkHttpClient;

    invoke-direct {v0}, Lcom/squareup/okhttp/OkHttpClient;-><init>()V

    sput-object v0, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a:Lcom/squareup/okhttp/OkHttpClient;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/squareup/okhttp/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/okhttp/Request$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/okhttp/Request$Builder;->a(Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/Request$Builder;->a()Lcom/squareup/okhttp/Request;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a:Lcom/squareup/okhttp/OkHttpClient;

    if-nez v1, :cond_0

    .line 56
    new-instance v1, Lcom/squareup/okhttp/OkHttpClient;

    invoke-direct {v1}, Lcom/squareup/okhttp/OkHttpClient;-><init>()V

    sput-object v1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a:Lcom/squareup/okhttp/OkHttpClient;

    .line 59
    :cond_0
    sget-object v1, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a:Lcom/squareup/okhttp/OkHttpClient;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/OkHttpClient;->a(Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Call;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/Call;->a()Lcom/squareup/okhttp/Response;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/squareup/okhttp/Response;->f()Lcom/squareup/okhttp/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/ResponseBody;->e()Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v1, "404"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    const-string v0, "Not found"

    .line 66
    :cond_1
    return-object v0
.end method

.method public static a(Lcom/franco/kernel/changeloglib/ChangeLogListView;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 225
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 226
    invoke-virtual {p0}, Lcom/franco/kernel/changeloglib/ChangeLogListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 227
    invoke-interface {v3, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;

    .line 228
    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->d()Ljava/lang/String;

    move-result-object v4

    .line 230
    :goto_0
    invoke-interface {v3}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 231
    invoke-interface {v3, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/changeloglib/ChangeLogRow;

    .line 232
    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->d()Ljava/lang/String;

    move-result-object v5

    .line 233
    invoke-virtual {v0}, Lcom/franco/kernel/changeloglib/ChangeLogRow;->c()Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 247
    :cond_0
    return-object v2

    .line 239
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 240
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 244
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static a()V
    .locals 5

    .prologue
    .line 316
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 317
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    const-string v0, "android.intent.extra.TEXT"

    const-string v2, "http://goo.gl/MtECP5"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 319
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 320
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 321
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 322
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v4, "facebook"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 324
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const/high16 v0, 0x10200000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 327
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 328
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 332
    :cond_1
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 306
    new-instance v0, Lcom/google/android/gms/plus/PlusShare$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/PlusShare$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusShare$Builder;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/PlusShare$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/PlusShare$Builder;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/PlusShare$Builder;

    move-result-object v0

    const-string v1, "http://goo.gl/MtECP5"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusShare$Builder;->a(Landroid/net/Uri;)Lcom/google/android/gms/plus/PlusShare$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusShare$Builder;->a()Landroid/content/Intent;

    move-result-object v0

    .line 312
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 313
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 266
    packed-switch p2, :pswitch_data_0

    .line 278
    new-instance v0, Lcom/franco/kernel/changeloglib/ChangeLogListView;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/franco/kernel/changeloglib/ChangeLogListView;-><init>(Landroid/content/Context;)V

    .line 281
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$2;

    invoke-direct {v2, v0, p0, p1, p2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$2;-><init>(Lcom/franco/kernel/changeloglib/ChangeLogListView;Landroid/app/Activity;Ljava/lang/String;I)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 302
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 303
    :goto_0
    return-void

    .line 268
    :pswitch_0
    invoke-static {}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a()V

    goto :goto_0

    .line 271
    :pswitch_1
    const v0, 0x7f0701db

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->c(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x7f0c009c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 351
    sget-object v0, Landroid/os/Build;->HOST:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cyanogen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/franco/kernel/devices/Bacon;

    if-eq v0, v1, :cond_0

    .line 352
    new-instance v0, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;

    invoke-direct {v0, p0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;-><init>(Landroid/content/Context;)V

    .line 355
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->show()V

    .line 357
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->a()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0702d7

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->b()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0702d8

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->c()Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0701dc

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 360
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->d()Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0702ee

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 362
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->d()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$3;

    invoke-direct {v2, v0, p1, p2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$3;-><init>(Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 370
    invoke-virtual {v0}, Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;->c()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$4;

    invoke-direct {v2, v0}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$4;-><init>(Lcom/franco/kernel/dialogs/GenericDialogs$TwoButtonDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    :goto_0
    return-void

    .line 381
    :cond_0
    invoke-static {p1, p2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const v9, 0x7f0700f8

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Kernel url: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Kernel version: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 73
    const-string v0, ".img"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 75
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 79
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-static {v9}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "r"

    aput-object v5, v4, v6

    aput-object p1, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 83
    invoke-static {v9}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "r"

    aput-object v5, v4, v6

    aput-object p1, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 84
    const v3, 0x1080081

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 85
    invoke-virtual {v2, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 86
    invoke-virtual {v2, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 88
    const/16 v3, 0xff

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 90
    invoke-static {}, Lcom/franco/kernel/App;->a()Lcom/loopj/android/http/AsyncHttpClient;

    move-result-object v3

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;

    invoke-direct {v5, v0, v1, p1, v2}, Lcom/franco/kernel/helpers/KernelUpdaterHelpers$1;-><init>(Landroid/app/NotificationManager;ZLjava/lang/String;Landroid/support/v4/app/NotificationCompat$Builder;)V

    invoke-virtual {v3, v4, p0, v5}, Lcom/loopj/android/http/AsyncHttpClient;->a(Landroid/content/Context;Ljava/lang/String;Lcom/loopj/android/http/ResponseHandlerInterface;)Lcom/loopj/android/http/RequestHandle;

    .line 222
    return-void
.end method

.method public static b(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 343
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 348
    return-void
.end method

.method private static c(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 335
    const-string v0, "http://www.twitter.com/intent/tweet?url=http://goo.gl/MtECP5&text=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 336
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 337
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 338
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 339
    return-void
.end method
