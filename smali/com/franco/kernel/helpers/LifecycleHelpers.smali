.class public Lcom/franco/kernel/helpers/LifecycleHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/app/Activity;)Z
    .locals 2

    .prologue
    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 28
    invoke-static {p0}, Lcom/franco/kernel/helpers/LifecycleHelpers;->d(Landroid/app/Activity;)Z

    move-result v0

    .line 32
    :goto_0
    return v0

    .line 29
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 30
    invoke-static {p0}, Lcom/franco/kernel/helpers/LifecycleHelpers;->c(Landroid/app/Activity;)Z

    move-result v0

    goto :goto_0

    .line 32
    :cond_1
    invoke-static {p0}, Lcom/franco/kernel/helpers/LifecycleHelpers;->b(Landroid/app/Activity;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 10
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/app/Activity;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 16
    invoke-static {p0}, Lcom/franco/kernel/helpers/LifecycleHelpers;->b(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/app/Activity;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 22
    invoke-static {p0}, Lcom/franco/kernel/helpers/LifecycleHelpers;->c(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
