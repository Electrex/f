.class public Lcom/franco/kernel/helpers/NavigationDrawerHelpers;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/app/Activity;Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;)V
    .locals 3

    .prologue
    .line 13
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 14
    if-eqz p0, :cond_1

    .line 15
    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 16
    if-eqz v0, :cond_2

    .line 17
    iget v1, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setStatusBarBackgroundColor(I)V

    .line 23
    :goto_0
    invoke-static {}, Lcom/franco/kernel/helpers/ThemeHelpers;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    iget v0, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->b:I

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->b:I

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->b:I

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 28
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget-object v1, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 35
    :cond_1
    :goto_1
    return-void

    .line 19
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 20
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->b:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 30
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p1, Lcom/franco/kernel/helpers/ColorHelpers$TwoColors;->b:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_1
.end method
