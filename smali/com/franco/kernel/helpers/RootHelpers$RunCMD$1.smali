.class Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu/chainfire/libsuperuser/Shell$OnCommandLineListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/helpers/RootHelpers$RunCMD;


# direct methods
.method constructor <init>(Lcom/franco/kernel/helpers/RootHelpers$RunCMD;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;->a:Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;->a:Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    invoke-static {v0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a(Lcom/franco/kernel/helpers/RootHelpers$RunCMD;)Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1$1;-><init>(Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 111
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 115
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/helpers/RootHelpers$Stdout;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/franco/kernel/helpers/RootHelpers$Stdout;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 117
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;->a:Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    iget-object v0, v0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
