.class public Lcom/franco/kernel/helpers/RootHelpers$RunCMD;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;

.field private b:[Ljava/lang/String;

.field private c:Z

.field private d:Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;


# direct methods
.method public varargs constructor <init>(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->c:Z

    .line 76
    iput-object p3, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->b:[Ljava/lang/String;

    .line 77
    iput-object p1, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->d:Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a:Ljava/util/ArrayList;

    .line 79
    iput-boolean p2, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->c:Z

    .line 80
    invoke-direct {p0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a()V

    .line 82
    return-void
.end method

.method public varargs constructor <init>(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->c:Z

    .line 69
    iput-object p2, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->b:[Ljava/lang/String;

    .line 70
    iput-object p1, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->d:Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a:Ljava/util/ArrayList;

    .line 72
    invoke-direct {p0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a()V

    .line 73
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->c:Z

    .line 63
    iput-object p1, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->b:[Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a:Ljava/util/ArrayList;

    .line 65
    invoke-direct {p0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->a()V

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/helpers/RootHelpers$RunCMD;)Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->d:Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 86
    :try_start_0
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v1, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;

    invoke-direct {v3, p0}, Lcom/franco/kernel/helpers/RootHelpers$RunCMD$1;-><init>(Lcom/franco/kernel/helpers/RootHelpers$RunCMD;)V

    invoke-virtual {v0, v1, v2, v3}, Leu/chainfire/libsuperuser/Shell$Interactive;->a([Ljava/lang/String;ILeu/chainfire/libsuperuser/Shell$OnCommandLineListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/franco/kernel/helpers/RootHelpers$RunCMD;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/franco/kernel/helpers/RootHelpers$RunCMD;->c:Z

    return v0
.end method
