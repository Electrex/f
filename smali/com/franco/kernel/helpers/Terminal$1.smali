.class Lcom/franco/kernel/helpers/Terminal$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/franco/kernel/helpers/Terminal;


# direct methods
.method constructor <init>(Lcom/franco/kernel/helpers/Terminal;I)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/franco/kernel/helpers/Terminal$1;->b:Lcom/franco/kernel/helpers/Terminal;

    iput p2, p0, Lcom/franco/kernel/helpers/Terminal$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/util/List;)V
    .locals 2

    .prologue
    .line 29
    if-eqz p2, :cond_1

    .line 30
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/franco/kernel/helpers/Terminal;->a(Lcom/franco/kernel/helpers/Terminal;)Lcom/franco/kernel/helpers/Terminal;

    .line 32
    iget-object v0, p0, Lcom/franco/kernel/helpers/Terminal$1;->b:Lcom/franco/kernel/helpers/Terminal;

    iget v1, p0, Lcom/franco/kernel/helpers/Terminal$1;->a:I

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/Terminal;->a(Lcom/franco/kernel/helpers/Terminal;I)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 33
    const-string v0, "Interactive Shell is not running, let\'s retry"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 34
    new-instance v0, Lcom/franco/kernel/helpers/Terminal;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/Terminal;-><init>()V

    invoke-static {v0}, Lcom/franco/kernel/helpers/Terminal;->a(Lcom/franco/kernel/helpers/Terminal;)Lcom/franco/kernel/helpers/Terminal;

    .line 50
    :goto_0
    return-void

    .line 36
    :cond_0
    const-string v0, "Interactive Shell is not running, we are NOT trying again"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 37
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/helpers/Terminal$1$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/helpers/Terminal$1$1;-><init>(Lcom/franco/kernel/helpers/Terminal$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 44
    iget-object v0, p0, Lcom/franco/kernel/helpers/Terminal$1;->b:Lcom/franco/kernel/helpers/Terminal;

    invoke-static {v0}, Lcom/franco/kernel/helpers/Terminal;->b(Lcom/franco/kernel/helpers/Terminal;)V

    goto :goto_0

    .line 47
    :cond_1
    const-string v0, "Interactive Shell init SUCCESS"

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/franco/kernel/helpers/Terminal$1;->b:Lcom/franco/kernel/helpers/Terminal;

    invoke-static {v0}, Lcom/franco/kernel/helpers/Terminal;->b(Lcom/franco/kernel/helpers/Terminal;)V

    goto :goto_0
.end method
