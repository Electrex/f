.class public Lcom/franco/kernel/helpers/Terminal;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lcom/franco/kernel/helpers/Terminal;


# instance fields
.field public a:Leu/chainfire/libsuperuser/Shell$Interactive;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "root_acquire_count"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 21
    new-instance v1, Leu/chainfire/libsuperuser/Shell$Builder;

    invoke-direct {v1}, Leu/chainfire/libsuperuser/Shell$Builder;-><init>()V

    invoke-virtual {v1}, Leu/chainfire/libsuperuser/Shell$Builder;->a()Leu/chainfire/libsuperuser/Shell$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Leu/chainfire/libsuperuser/Shell$Builder;->a(Z)Leu/chainfire/libsuperuser/Shell$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Leu/chainfire/libsuperuser/Shell$Builder;->b(Z)Leu/chainfire/libsuperuser/Shell$Builder;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Leu/chainfire/libsuperuser/Shell$Builder;->a(I)Leu/chainfire/libsuperuser/Shell$Builder;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/helpers/Terminal$1;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/helpers/Terminal$1;-><init>(Lcom/franco/kernel/helpers/Terminal;I)V

    invoke-virtual {v1, v2}, Leu/chainfire/libsuperuser/Shell$Builder;->a(Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;)Leu/chainfire/libsuperuser/Shell$Interactive;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    .line 52
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 63
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "root_acquire_count"

    add-int/lit8 v2, p1, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    return v2
.end method

.method static synthetic a(Lcom/franco/kernel/helpers/Terminal;I)I
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/franco/kernel/helpers/Terminal;->a(I)I

    move-result v0

    return v0
.end method

.method public static a()Lcom/franco/kernel/helpers/Terminal;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/franco/kernel/helpers/Terminal;->b:Lcom/franco/kernel/helpers/Terminal;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/franco/kernel/helpers/Terminal;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/Terminal;-><init>()V

    sput-object v0, Lcom/franco/kernel/helpers/Terminal;->b:Lcom/franco/kernel/helpers/Terminal;

    .line 59
    :cond_0
    sget-object v0, Lcom/franco/kernel/helpers/Terminal;->b:Lcom/franco/kernel/helpers/Terminal;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/helpers/Terminal;)Lcom/franco/kernel/helpers/Terminal;
    .locals 0

    .prologue
    .line 12
    sput-object p0, Lcom/franco/kernel/helpers/Terminal;->b:Lcom/franco/kernel/helpers/Terminal;

    return-object p0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "root_acquire_count"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    return-void
.end method

.method static synthetic b(Lcom/franco/kernel/helpers/Terminal;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/franco/kernel/helpers/Terminal;->b()V

    return-void
.end method
