.class public Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbutterknife/ButterKnife$Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0c0052

    const v5, 0x102001a

    const v4, 0x1020019

    const v3, 0x1020015

    const v2, 0x1020014

    .line 11
    const-string v0, "field \'mText1\'"

    invoke-virtual {p1, p3, v2, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    const-string v1, "field \'mText1\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText1:Landroid/widget/TextView;

    .line 13
    const-string v0, "field \'mText2\'"

    invoke-virtual {p1, p3, v3, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 14
    const-string v1, "field \'mText2\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText2:Landroid/widget/TextView;

    .line 15
    const v0, 0x7f0c0054

    const-string v1, "field \'mInnerLayout\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    const v1, 0x7f0c0054

    const-string v2, "field \'mInnerLayout\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mInnerLayout:Landroid/widget/LinearLayout;

    .line 17
    const-string v0, "field \'mButton1\' and method \'onCancelClick\'"

    invoke-virtual {p1, p3, v4, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 18
    const-string v1, "field \'mButton1\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton1:Landroid/widget/Button;

    .line 19
    new-instance v1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector$1;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector$1;-><init>(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector;Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    const-string v0, "field \'mButton2\' and method \'onRetryButton\'"

    invoke-virtual {p1, p3, v5, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 28
    const-string v1, "field \'mButton2\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton2:Landroid/widget/Button;

    .line 29
    new-instance v1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector$2;

    invoke-direct {v1, p0, p2}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector$2;-><init>(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector;Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const v0, 0x7f0c0053

    const-string v1, "field \'mRetryingImg\'"

    invoke-virtual {p1, p3, v0, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 38
    const v1, 0x7f0c0053

    const-string v2, "field \'mRetryingImg\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetryingImg:Landroid/widget/ImageView;

    .line 39
    const-string v0, "field \'mRetrying\'"

    invoke-virtual {p1, p3, v6, v0}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 40
    const-string v1, "field \'mRetrying\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetrying:Landroid/widget/LinearLayout;

    .line 41
    return-void
.end method

.method public bridge synthetic inject(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;

    invoke-virtual {p0, p1, p2, p3}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;Ljava/lang/Object;)V

    return-void
.end method

.method public reset(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText1:Landroid/widget/TextView;

    .line 45
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText2:Landroid/widget/TextView;

    .line 46
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mInnerLayout:Landroid/widget/LinearLayout;

    .line 47
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton1:Landroid/widget/Button;

    .line 48
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton2:Landroid/widget/Button;

    .line 49
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetryingImg:Landroid/widget/ImageView;

    .line 50
    iput-object v0, p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetrying:Landroid/widget/LinearLayout;

    .line 51
    return-void
.end method

.method public bridge synthetic reset(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$$ViewInjector;->reset(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V

    return-void
.end method
