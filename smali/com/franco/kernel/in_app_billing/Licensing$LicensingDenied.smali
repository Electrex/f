.class public Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field mButton1:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020019
    .end annotation
.end field

.field mButton2:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x102001a
    .end annotation
.end field

.field mInnerLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0054
    .end annotation
.end field

.field mRetrying:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0052
    .end annotation
.end field

.field mRetryingImg:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0c0053
    .end annotation
.end field

.field mText1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020014
    .end annotation
.end field

.field mText2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x1020015
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 98
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method protected onCancelClick(Landroid/view/View;)V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x1020019
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->finish()V

    .line 123
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$FinishMainActivity;

    invoke-direct {v1, p0}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$FinishMainActivity;-><init>(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 102
    invoke-static {p0}, Lcom/franco/kernel/helpers/ThemeHelpers;->b(Landroid/app/Activity;)V

    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    const v0, 0x7f040019

    invoke-virtual {p0, v0}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->setContentView(I)V

    .line 106
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 108
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->setFinishOnTouchOutside(Z)V

    .line 110
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText1:Landroid/widget/TextView;

    const v1, 0x7f070192

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 111
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mText2:Landroid/widget/TextView;

    const v1, 0x7f070194

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 112
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton1:Landroid/widget/Button;

    const v1, 0x7f070082

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 113
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton2:Landroid/widget/Button;

    const v1, 0x7f070193

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 114
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mButton2:Landroid/widget/Button;

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 115
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 152
    invoke-static {p0}, Lbutterknife/ButterKnife;->reset(Ljava/lang/Object;)V

    .line 153
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 154
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 146
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mInnerLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetrying:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 148
    return-void
.end method

.method protected onRetryButton(Landroid/view/View;)V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x102001a
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mInnerLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetrying:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 131
    const v0, 0x7f05000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 132
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 133
    iget-object v1, p0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;->mRetryingImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 135
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$1;

    invoke-direct {v1, p0}, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied$1;-><init>(Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 141
    return-void
.end method
