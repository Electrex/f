.class public Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/vending/licensing/LicenseCheckerCallback;


# instance fields
.field final synthetic a:Lcom/franco/kernel/in_app_billing/Licensing;


# direct methods
.method protected constructor <init>(Lcom/franco/kernel/in_app_billing/Licensing;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v0, v0, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    instance-of v0, v0, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v0, v0, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v0, v0, Lcom/franco/kernel/in_app_billing/Licensing;->b:Lcom/google/android/vending/licensing/LicenseChecker;

    invoke-virtual {v0}, Lcom/google/android/vending/licensing/LicenseChecker;->a()V

    .line 64
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v1, v1, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 71
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/franco/kernel/in_app_billing/Licensing$LicensingDenied;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    iget-object v1, p0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;->a:Lcom/franco/kernel/in_app_billing/Licensing;

    iget-object v1, v1, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 78
    return-void
.end method
