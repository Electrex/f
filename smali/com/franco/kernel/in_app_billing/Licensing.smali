.class public Lcom/franco/kernel/in_app_billing/Licensing;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[B

.field private static d:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/vending/licensing/LicenseChecker;

.field public c:Landroid/app/Activity;

.field private e:Lcom/google/android/vending/licensing/LicenseCheckerCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/franco/kernel/in_app_billing/Licensing;->a:[B

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    return-void

    .line 33
    nop

    :array_0
    .array-data 1
        -0x45t
        -0x69t
        -0x4ft
        -0x61t
        -0x5t
        -0x5bt
        0x11t
        -0x64t
        -0x2bt
        -0x3t
        -0x34t
        -0x15t
        -0x39t
        0x28t
        0x18t
        0x5t
        0x3t
        0x14t
        -0x1ct
        0x4et
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 7

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    .line 47
    new-instance v0, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;

    invoke-direct {v0, p0}, Lcom/franco/kernel/in_app_billing/Licensing$QueryCallBack;-><init>(Lcom/franco/kernel/in_app_billing/Licensing;)V

    iput-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing;->e:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    .line 49
    new-instance v0, Lcom/google/android/vending/licensing/LicenseChecker;

    new-instance v1, Lcom/google/android/vending/licensing/ServerManagedPolicy;

    iget-object v2, p0, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/vending/licensing/AESObfuscator;

    sget-object v4, Lcom/franco/kernel/in_app_billing/Licensing;->a:[B

    iget-object v5, p0, Lcom/franco/kernel/in_app_billing/Licensing;->c:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/franco/kernel/in_app_billing/Licensing;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/vending/licensing/AESObfuscator;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/vending/licensing/ServerManagedPolicy;-><init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Obfuscator;)V

    const-string v2, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu52a5/MrMz9izETkQDvucQOvnh2tzXbXuKlz97gAB4yMq/UueeQsfWhvqcekWl69tUH3tzW7Dn7KXZeMSCoX6rkEZzD+janaLI3K0ZtRxGEet71BGROD0Q3EYCu7y3DYUZcZNispTvC041Gm7ryUKy+4wX2/UzniFlFBdiwRVMPinrK76URpee64i2EGeqr2zmO7jI9Hjv3kOW0gu40od1pN3KalS9wHtH/bN1KnJRnRbhGIRAQJiZdIuWsrIx+0ckAgL91GAdslXu/4ZmIyrBCXh5B2X2nxbRnb3vJh01CCBFV9ErkuFKLduPq1rQe19GyjgT5cM4oQlJV+Piq3BwIDAQAB"

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/vending/licensing/LicenseChecker;-><init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Policy;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing;->b:Lcom/google/android/vending/licensing/LicenseChecker;

    .line 53
    iget-object v0, p0, Lcom/franco/kernel/in_app_billing/Licensing;->b:Lcom/google/android/vending/licensing/LicenseChecker;

    iget-object v1, p0, Lcom/franco/kernel/in_app_billing/Licensing;->e:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/vending/licensing/LicenseChecker;->a(Lcom/google/android/vending/licensing/LicenseCheckerCallback;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected declared-synchronized a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 163
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PREF_UNIQUE_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 164
    const-string v1, "PREF_UNIQUE_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    .line 166
    sget-object v1, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 167
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    .line 168
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 169
    const-string v1, "PREF_UNIQUE_ID"

    sget-object v2, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 170
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    :cond_0
    sget-object v0, Lcom/franco/kernel/in_app_billing/Licensing;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
