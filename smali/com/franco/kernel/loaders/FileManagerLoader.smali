.class public Lcom/franco/kernel/loaders/FileManagerLoader;
.super Landroid/content/AsyncTaskLoader;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p2, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->a:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    .line 67
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 53
    new-instance v0, Lcom/franco/kernel/helpers/FileManagerHelpers$DirectoriesComparator;

    invoke-direct {v0}, Lcom/franco/kernel/helpers/FileManagerHelpers$DirectoriesComparator;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 55
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 56
    new-instance v3, Lcom/franco/kernel/fragments/FileManager$FileObject;

    invoke-direct {v3}, Lcom/franco/kernel/fragments/FileManager$FileObject;-><init>()V

    .line 57
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    iput-boolean v1, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->a:Z

    .line 58
    iget-boolean v1, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->a:Z

    if-eqz v1, :cond_1

    const v1, 0x7f020083

    :goto_2
    iput v1, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->b:I

    .line 60
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->c:Ljava/lang/String;

    .line 61
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->e:Ljava/lang/String;

    .line 62
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/TimeHelpers;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/franco/kernel/fragments/FileManager$FileObject;->d:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 58
    :cond_1
    const v1, 0x7f020082

    goto :goto_2

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    .line 37
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/loaders/FileManagerLoader;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileManagerLoader;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileManagerLoader;->forceLoad()V

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileManagerLoader;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/loaders/FileManagerLoader;->a(Ljava/util/List;)V

    goto :goto_0
.end method
