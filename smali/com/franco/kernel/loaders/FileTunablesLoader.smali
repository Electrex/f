.class public Lcom/franco/kernel/loaders/FileTunablesLoader;
.super Landroid/content/AsyncTaskLoader;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/LinkedHashMap;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/LinkedHashMap;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->a:Ljava/util/LinkedHashMap;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    .line 65
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->a:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    .line 98
    :goto_0
    return-object v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 70
    new-instance v2, Ljava/io/File;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 74
    new-instance v2, Lcom/franco/kernel/FileTunable;

    invoke-direct {v2}, Lcom/franco/kernel/FileTunable;-><init>()V

    .line 75
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    .line 76
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/franco/kernel/FileTunable;->b:Ljava/lang/String;

    .line 77
    invoke-static {}, Lcom/franco/kernel/App;->j()Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, v2, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/franco/kernel/FileTunable;->d:Ljava/lang/String;

    .line 78
    const-string v0, "0"

    iput-object v0, v2, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    .line 80
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070083

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v2, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/franco/kernel/loaders/FileTunablesLoader$1;

    invoke-direct {v4, p0, v2}, Lcom/franco/kernel/loaders/FileTunablesLoader$1;-><init>(Lcom/franco/kernel/loaders/FileTunablesLoader;Lcom/franco/kernel/FileTunable;)V

    invoke-virtual {v0, v3, v7, v4}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Ljava/lang/String;ILeu/chainfire/libsuperuser/Shell$OnCommandResultListener;)V

    .line 92
    :cond_2
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-virtual {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    .line 56
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 59
    :cond_0
    return-void
.end method

.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/franco/kernel/loaders/FileTunablesLoader;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onReset()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 49
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->onStopLoading()V

    .line 50
    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->a(Ljava/util/List;)V

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/franco/kernel/loaders/FileTunablesLoader;->b:Ljava/util/List;

    if-nez v0, :cond_2

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->forceLoad()V

    .line 39
    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/franco/kernel/loaders/FileTunablesLoader;->cancelLoad()Z

    .line 44
    return-void
.end method
