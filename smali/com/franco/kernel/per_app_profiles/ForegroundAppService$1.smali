.class Lcom/franco/kernel/per_app_profiles/ForegroundAppService$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;


# instance fields
.field final synthetic a:Lcom/franco/kernel/per_app_profiles/ForegroundAppService;


# direct methods
.method constructor <init>(Lcom/franco/kernel/per_app_profiles/ForegroundAppService;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$1;->a:Lcom/franco/kernel/per_app_profiles/ForegroundAppService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    const-string v1, "\t"

    invoke-static {p1, v1}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 115
    :goto_1
    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$1;->a:Lcom/franco/kernel/per_app_profiles/ForegroundAppService;

    const-string v2, "misc_prefs"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "saved_profile"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 117
    return-void

    .line 103
    :pswitch_0
    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 105
    :pswitch_3
    const v0, 0x7f07020a

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 108
    :pswitch_4
    const v0, 0x7f07004b

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 111
    :pswitch_5
    const v0, 0x7f070201

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
