.class public Lcom/franco/kernel/per_app_profiles/ForegroundAppService;
.super Landroid/app/IntentService;
.source "SourceFile"

# interfaces
.implements Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;


# instance fields
.field protected volatile a:Z

.field protected volatile b:Z

.field protected volatile c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const-string v0, "ForegroundAppService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 33
    iput-object v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->d:Ljava/lang/String;

    .line 42
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 218
    invoke-static {p1}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 230
    new-array v0, v2, [Ljava/lang/String;

    const-string v4, "echo %s > %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v1

    aput-object p1, v3, v2

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 234
    :cond_1
    :goto_1
    return-void

    .line 219
    :sswitch_0
    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    .line 221
    :pswitch_0
    const-string v0, "max"

    invoke-static {v0, p2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 224
    :pswitch_1
    const-string v0, "min"

    invoke-static {v0, p2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 227
    :pswitch_2
    invoke-static {p2}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 219
    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x2e11bc0 -> :sswitch_2
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 212
    if-eqz p1, :cond_1

    const v0, 0x7f07020a

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f07004b

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f070201

    invoke-static {v0}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 242
    const-string v0, "\t"

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    const-string v1, "system_default_values"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 246
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x4

    const/4 v8, 0x1

    .line 46
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "foreground_app"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 53
    const-string v0, "/data/data/%s/per-app_profiles/"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->d:Ljava/lang/String;

    .line 56
    :cond_2
    const-string v0, "misc_prefs"

    invoke-virtual {p0, v0, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "boosted"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    .line 57
    iput-boolean v2, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    .line 59
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->d:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 61
    if-eqz v3, :cond_0

    .line 65
    array-length v4, v3

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 66
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".xml"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 67
    iput-boolean v8, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    .line 74
    :cond_3
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-nez v0, :cond_0

    .line 76
    :cond_4
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-eqz v0, :cond_0

    .line 80
    :cond_5
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-eqz v0, :cond_6

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->f:Landroid/content/SharedPreferences;

    .line 85
    :cond_6
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-eqz v0, :cond_7

    .line 86
    const-string v0, "misc_prefs"

    invoke-virtual {p0, v0, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "saved_profile"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    .line 89
    :cond_7
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 92
    :goto_2
    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 93
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-nez v0, :cond_b

    .line 95
    new-instance v0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$1;

    invoke-direct {v0, p0}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$1;-><init>(Lcom/franco/kernel/per_app_profiles/ForegroundAppService;)V

    new-array v1, v8, [Ljava/lang/String;

    const-string v3, "getprop fku.perf.profile"

    aput-object v3, v1, v2

    invoke-static {v0, v8, v1}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 124
    :goto_3
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    const v1, 0x7f07020a

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 125
    const-string v0, "com.franco.kernel.POWER_SAVING"

    .line 132
    :goto_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->sendBroadcast(Landroid/content/Intent;)V

    .line 174
    :cond_8
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-nez v0, :cond_13

    .line 175
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "per_app_profiles_toast"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 178
    const-string v1, "misc_prefs"

    invoke-virtual {p0, v1, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "boosted"

    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 181
    if-eqz v0, :cond_0

    .line 182
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$2;

    invoke-direct {v1, p0}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$2;-><init>(Lcom/franco/kernel/per_app_profiles/ForegroundAppService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 65
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 89
    :cond_a
    const-string v0, "system_default_values"

    invoke-virtual {p0, v0, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    goto :goto_2

    .line 120
    :cond_b
    const-string v0, "misc_prefs"

    invoke-virtual {p0, v0, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "saved_profile"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3

    .line 126
    :cond_c
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->c:Ljava/lang/String;

    const v1, 0x7f07004b

    invoke-static {v1}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 127
    const-string v0, "com.franco.kernel.BALANCE"

    goto :goto_4

    .line 129
    :cond_d
    const-string v0, "com.franco.kernel.PERFORMANCE"

    goto :goto_4

    .line 134
    :cond_e
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 140
    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 141
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 142
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 148
    iget-boolean v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-nez v1, :cond_10

    .line 154
    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"

    .line 158
    :goto_6
    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_12

    .line 159
    iput-object v1, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->e:Ljava/lang/String;

    .line 160
    new-array v5, v8, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cat "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {p0, v8, v5}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 169
    :cond_10
    :goto_7
    invoke-direct {p0, v0, v4}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_11
    move-object v1, v0

    .line 154
    goto :goto_6

    .line 163
    :cond_12
    const-string v5, "system_default_values"

    invoke-virtual {p0, v5, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-static {v1}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_7

    .line 191
    :cond_13
    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->a:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "per_app_profiles_toast"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 195
    const-string v1, "misc_prefs"

    invoke-virtual {p0, v1, v9}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "boosted"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 198
    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/franco/kernel/App;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$3;

    invoke-direct {v1, p0}, Lcom/franco/kernel/per_app_profiles/ForegroundAppService$3;-><init>(Lcom/franco/kernel/per_app_profiles/ForegroundAppService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 143
    :catch_0
    move-exception v0

    goto/16 :goto_5
.end method
