.class public Lcom/franco/kernel/per_app_profiles/MakoPowerProfile;
.super Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6

    .prologue
    const v5, 0x7f07017e

    .line 14
    invoke-super {p0}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract;->a()Ljava/util/List;

    move-result-object v0

    .line 15
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    invoke-static {v5}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    const-string v4, "/sys/devices/system/cpu/cpufreq/interactive/input_boost_freq"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    invoke-static {v5}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    const-string v4, "/sys/devices/system/cpu/cpufreq/conservative/input_boost_freq"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0701a4

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    const-string v4, "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f07013a

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/module/msm_kgsl_core/parameters/up_threshold"

    const-string v4, "/sys/module/msm_kgsl_core/parameters/up_threshold"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f070136

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/module/msm_kgsl_core/parameters/down_threshold"

    const-string v4, "/sys/module/msm_kgsl_core/parameters/down_threshold"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0701f4

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/class/misc/mako_hotplug_control/load_threshold"

    const-string v4, "/sys/class/misc/mako_hotplug_control/load_threshold"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    new-instance v1, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;

    const v2, 0x7f0702ab

    invoke-static {v2}, Lcom/franco/kernel/App;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/sys/module/msm_thermal/parameters/temp_threshold"

    const-string v4, "/sys/module/msm_thermal/parameters/temp_threshold"

    invoke-static {v4}, Lcom/franco/kernel/helpers/StringHelpers;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/franco/kernel/abstracts/PerAppProfilesAbstract$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    return-object v0
.end method
