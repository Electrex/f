.class Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;


# instance fields
.field final synthetic a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/util/List;)V
    .locals 4

    .prologue
    const v3, 0x7f07004b

    .line 153
    const-string v0, "\t"

    invoke-static {p3, v0}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 166
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v0, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "saved_profile"

    iget-object v2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v2, v2, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_1
    return-void

    .line 155
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 157
    :pswitch_3
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v0, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "saved_profile"

    iget-object v2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v2, v2, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const v3, 0x7f07020a

    invoke-virtual {v2, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 160
    :pswitch_4
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v0, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "saved_profile"

    iget-object v2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v2, v2, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-virtual {v2, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 163
    :pswitch_5
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v0, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "saved_profile"

    iget-object v2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v2, v2, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const v3, 0x7f070201

    invoke-virtual {v2, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 155
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
