.class Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;


# direct methods
.method constructor <init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->c:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iput-object p2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/util/List;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 187
    const-string v0, "\t"

    invoke-static {p3, v0}, Lorg/apache/commons/lang3/StringUtils;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    iget-object v4, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->c:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v4, v4, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    iget-object v5, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->a:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v4, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 199
    new-array v0, v2, [Ljava/lang/String;

    iget-object v4, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->c:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    iget-object v4, v4, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const v5, 0x7f0700fc

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->a:Ljava/lang/String;

    aput-object v6, v3, v1

    iget-object v6, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->b:Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v4, v5, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 202
    :goto_1
    return-void

    .line 190
    :sswitch_0
    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v5, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    .line 193
    :pswitch_0
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 196
    :pswitch_1
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 190
    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x2e11bc0 -> :sswitch_2
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
