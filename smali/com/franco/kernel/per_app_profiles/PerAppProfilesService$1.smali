.class Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

.field final synthetic b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;


# direct methods
.method constructor <init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    iput-object p2, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v11, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/per-app_profiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 120
    const/4 v0, 0x0

    .line 122
    if-nez v2, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    array-length v6, v2

    move v1, v3

    :goto_1
    if-ge v1, v6, :cond_c

    aget-object v7, v2, v1

    .line 127
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, ".xml"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    iget-object v9, v9, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 130
    :try_start_0
    invoke-static {v7}, Lorg/apache/commons/io/FileUtils;->f(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    move v0, v4

    .line 138
    :goto_2
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    iget-boolean v1, v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->b:Z

    if-nez v1, :cond_0

    .line 140
    :cond_2
    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    iget-boolean v1, v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->b:Z

    if-eqz v1, :cond_0

    .line 145
    :cond_3
    if-eqz v0, :cond_8

    .line 147
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-static {v0, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 149
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    const-string v1, "getprop fku.perf.profile"

    new-instance v5, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;

    invoke-direct {v5, p0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$1;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;)V

    invoke-virtual {v0, v1, v3, v5}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Ljava/lang/String;ILeu/chainfire/libsuperuser/Shell$OnCommandResultListener;)V

    .line 172
    :cond_4
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-virtual {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 176
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-static {v0, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V

    .line 209
    :cond_5
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "profile_applied"

    invoke-static {v0, v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "boosted_app"

    iget-object v3, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    iget-object v3, v3, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->a:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "per_app_profiles_toast"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$3;

    invoke-direct {v1, p0, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$3;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 131
    :catch_0
    move-exception v1

    .line 132
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v0

    move v0, v4

    .line 134
    goto :goto_2

    .line 126
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 178
    :cond_7
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-virtual {v0, v2, v11}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 179
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 180
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 183
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v6

    iget-object v6, v6, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v7, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const v8, 0x7f070083

    new-array v9, v4, [Ljava/lang/Object;

    aput-object v0, v9, v3

    invoke-virtual {v7, v8, v9}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;

    invoke-direct {v8, p0, v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$2;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v3, v8}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Ljava/lang/String;ILeu/chainfire/libsuperuser/Shell$OnCommandResultListener;)V

    goto :goto_3

    .line 226
    :cond_8
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "profile_applied"

    invoke-static {v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-static {v1, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 229
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    iget-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v2, "saved_profile"

    invoke-static {v1, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V

    .line 253
    :goto_4
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "saved_profile"

    invoke-static {v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "profile_applied"

    invoke-static {v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "boosted_app"

    invoke-static {v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "per_app_profiles_toast"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lcom/franco/kernel/App;->d:Landroid/os/Handler;

    new-instance v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$4;

    invoke-direct {v1, p0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1$4;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 231
    :cond_9
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "system_default_values"

    invoke-virtual {v0, v1, v11}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 232
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 233
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 234
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 236
    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_a
    :goto_6
    packed-switch v2, :pswitch_data_0

    .line 245
    new-array v2, v4, [Ljava/lang/String;

    iget-object v7, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const v8, 0x7f0700fc

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v1, v9, v3

    aput-object v0, v9, v4

    invoke-virtual {v7, v8, v9}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    goto :goto_5

    .line 236
    :sswitch_0
    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    move v2, v3

    goto :goto_6

    :sswitch_1
    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    move v2, v4

    goto :goto_6

    :sswitch_2
    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    move v2, v5

    goto :goto_6

    .line 239
    :pswitch_0
    invoke-static {v1, v0}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 242
    :pswitch_1
    invoke-static {v0}, Lcom/franco/kernel/helpers/CPUfreqHelpers;->c(Ljava/lang/String;)V

    goto :goto_5

    .line 250
    :cond_b
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;->b:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v1, "system_default_values"

    invoke-virtual {v0, v1, v11}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_4

    :cond_c
    move-object v2, v0

    move v0, v3

    goto/16 :goto_2

    .line 236
    :sswitch_data_0
    .sparse-switch
        -0x75218e83 -> :sswitch_0
        -0x2e11bc0 -> :sswitch_2
        0x20ffd54f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
