.class Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;


# direct methods
.method private constructor <init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-lt v0, v3, :cond_1

    .line 93
    invoke-static {}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 98
    :goto_0
    iget-object v3, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    const-string v4, "boosted_app"

    invoke-static {v3, v4}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 101
    if-nez v0, :cond_2

    if-nez v3, :cond_2

    .line 109
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_1
    invoke-static {}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b()Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    goto :goto_0

    .line 105
    :cond_2
    invoke-static {}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c()Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    move-result-object v0

    iget-object v4, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;->a:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;

    invoke-static {v4}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->a:Ljava/lang/String;

    .line 106
    invoke-static {}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c()Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    move-result-object v0

    if-eqz v3, :cond_3

    :goto_2
    iput-boolean v1, v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;->b:Z

    .line 108
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-static {}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c()Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    move-result-object v1

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 106
    goto :goto_2
.end method
