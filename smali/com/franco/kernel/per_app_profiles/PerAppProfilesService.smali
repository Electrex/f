.class public Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Landroid/app/ActivityManager;

.field private static b:Landroid/os/PowerManager;

.field private static c:Landroid/view/Display;

.field private static d:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;


# instance fields
.field private e:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 86
    return-void
.end method

.method static synthetic a()Landroid/view/Display;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 272
    const v0, 0x7f07020a

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.franco.kernel.POWER_SAVING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->sendBroadcast(Landroid/content/Intent;)V

    .line 279
    :goto_0
    return-void

    .line 274
    :cond_0
    const v0, 0x7f07004b

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.franco.kernel.BALANCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 277
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.franco.kernel.PERFORMANCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 282
    const-string v0, "misc_prefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 283
    return-void
.end method

.method static synthetic b()Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b:Landroid/os/PowerManager;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    const-string v0, "misc_prefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 294
    const-string v0, "system_default_values"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 295
    return-void
.end method

.method static synthetic b(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c()Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    return-object v0
.end method

.method static synthetic c(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 290
    const-string v0, "misc_prefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 291
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 304
    sget-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 305
    iget v2, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v3, 0x64

    if-ne v2, v3, :cond_0

    .line 306
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 310
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 298
    if-eqz p1, :cond_1

    const v0, 0x7f07020a

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f07004b

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f070201

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 314
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 315
    invoke-direct {p0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d()Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a:Landroid/app/ActivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;)V
    .locals 1
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;

    invoke-direct {v0, p0, p1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 269
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 61
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a:Landroid/app/ActivityManager;

    .line 62
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b:Landroid/os/PowerManager;

    .line 63
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c:Landroid/view/Display;

    .line 64
    new-instance v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    invoke-direct {v0, p0}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;)V

    sput-object v0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 324
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 326
    iput-object v1, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 329
    :cond_0
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 333
    :cond_1
    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->a:Landroid/app/ActivityManager;

    .line 334
    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->c:Landroid/view/Display;

    .line 335
    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->b:Landroid/os/PowerManager;

    .line 336
    sput-object v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->d:Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ForegroundApp;

    .line 337
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    .line 69
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->d(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_1

    .line 74
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 75
    iget-object v0, p0, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$ServiceRunnable;-><init>(Lcom/franco/kernel/per_app_profiles/PerAppProfilesService;Lcom/franco/kernel/per_app_profiles/PerAppProfilesService$1;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 78
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
