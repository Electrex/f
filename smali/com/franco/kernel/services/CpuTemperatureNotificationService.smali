.class public Lcom/franco/kernel/services/CpuTemperatureNotificationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;

.field private static b:Landroid/app/NotificationManager;

.field private static c:Landroid/support/v4/app/NotificationCompat$Builder;

.field private static d:Ljava/util/Timer;

.field private static e:Ljava/util/TimerTask;

.field private static f:Landroid/os/PowerManager;

.field private static g:Landroid/view/WindowManager;

.field private static h:Landroid/view/Display;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    return-void
.end method

.method static synthetic a()Landroid/view/Display;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->h:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic b()Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->f:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic c()Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    return-object v0
.end method

.method static synthetic d()Landroid/app/NotificationManager;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->b:Landroid/app/NotificationManager;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 36
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->b:Landroid/app/NotificationManager;

    .line 37
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 38
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->d:Ljava/util/Timer;

    .line 39
    new-instance v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService$TempTimerTask;

    invoke-direct {v0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService$TempTimerTask;-><init>()V

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->e:Ljava/util/TimerTask;

    .line 41
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->f:Landroid/os/PowerManager;

    .line 42
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->g:Landroid/view/WindowManager;

    .line 43
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->g:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->h:Landroid/view/Display;

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 97
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->d:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->d:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 99
    sput-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->e:Ljava/util/TimerTask;

    .line 100
    sput-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->d:Ljava/util/Timer;

    .line 101
    sput-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->f:Landroid/os/PowerManager;

    .line 102
    sput-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->g:Landroid/view/WindowManager;

    .line 103
    sput-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->h:Landroid/view/Display;

    .line 106
    :cond_0
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->b:Landroid/app/NotificationManager;

    const v1, 0xabcd

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 107
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "temperature_type"

    const-string v2, "c"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->a:Ljava/lang/String;

    .line 53
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    sget-object v1, Lcom/franco/kernel/App;->b:Lcom/franco/kernel/abstracts/Device;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/Device;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const v1, 0x7f0700c8

    invoke-virtual {p0, v1}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    const v0, 0x7f0200f3

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 59
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cpu_temp_notif_low_priority"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->b(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 63
    :cond_0
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->b:Landroid/app/NotificationManager;

    const v1, 0xabcd

    sget-object v2, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 66
    :try_start_0
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->d:Ljava/util/Timer;

    sget-object v1, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->e:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_1
    sget-object v0, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->c:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/franco/kernel/services/CpuTemperatureNotificationService;->startForeground(ILandroid/app/Notification;)V

    .line 72
    return v7

    .line 53
    :cond_1
    const v0, 0x7f0200f4

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    goto :goto_1
.end method
