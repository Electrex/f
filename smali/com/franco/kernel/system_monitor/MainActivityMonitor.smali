.class public Lcom/franco/kernel/system_monitor/MainActivityMonitor;
.super Lcom/cgollner/systemmonitor/MainActivityAbstract;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/MainActivityAbstract;-><init>()V

    return-void
.end method


# virtual methods
.method protected k()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/GpuFragment;

    return-object v0
.end method

.method protected l()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/CpuTemperatureFragment;

    return-object v0
.end method

.method protected m()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/franco/kernel/system_monitor/settings/Settings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/franco/kernel/system_monitor/MainActivityMonitor;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method

.method protected n()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/NetworkFragmentLite;

    return-object v0
.end method

.method protected o()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/IoFragmentLite;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/cgollner/systemmonitor/MainActivityAbstract;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-static {p0}, Lcom/crashlytics/android/Crashlytics;->a(Landroid/content/Context;)V

    .line 29
    invoke-static {p0}, Lcom/crashlytics/android/Crashlytics;->a(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method protected p()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/RamFragmentLite;

    return-object v0
.end method

.method protected q()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/CpuFragmentLite;

    return-object v0
.end method

.method protected r()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/cgollner/systemmonitor/systemfragments/ProcessesFragment;

    return-object v0
.end method
