.class Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Landroid/preference/PreferenceCategory;

.field final synthetic c:Landroid/preference/Preference;

.field final synthetic d:Ljava/lang/Long;

.field final synthetic e:Ljava/lang/Long;

.field final synthetic f:Landroid/content/Context;

.field final synthetic g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;


# direct methods
.method constructor <init>(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;Ljava/io/File;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;Ljava/lang/Long;Ljava/lang/Long;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;

    iput-object p2, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->b:Landroid/preference/PreferenceCategory;

    iput-object p4, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->c:Landroid/preference/Preference;

    iput-object p5, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->d:Ljava/lang/Long;

    iput-object p6, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->e:Ljava/lang/Long;

    iput-object p7, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->f:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;

    invoke-virtual {v1}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0700d6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;

    const v2, 0x7f07002d

    invoke-virtual {v1, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;

    const v2, 0x7f0702ee

    invoke-virtual {v1, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2$2;

    invoke-direct {v2, p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2$2;-><init>(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;->g:Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2$1;

    invoke-direct {v2, p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2$1;-><init>(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 157
    const/4 v0, 0x1

    return v0
.end method
