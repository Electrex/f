.class public Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;
.super Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;-><init>()V

    .line 167
    new-instance v0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$3;

    invoke-direct {v0, p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$3;-><init>(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;)V

    iput-object v0, p0, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->a:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 37
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 18

    .prologue
    .line 100
    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v11

    .line 101
    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v12

    .line 103
    const v2, 0x7f070165

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 104
    if-nez v2, :cond_6

    .line 105
    new-instance v5, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 106
    const v2, 0x7f070163

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 107
    const v2, 0x7f070165

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    .line 108
    const/4 v2, 0x2

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 112
    :goto_0
    invoke-virtual {v5}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 113
    const-string v2, "schedules"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v13

    .line 114
    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v14

    .line 115
    if-eqz v14, :cond_0

    array-length v2, v14

    if-nez v2, :cond_1

    .line 116
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 118
    :cond_1
    array-length v15, v14

    const/4 v2, 0x0

    move v10, v2

    :goto_1
    if-ge v10, v15, :cond_3

    aget-object v4, v14, v10

    .line 119
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x22

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 121
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x22

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    const/16 v7, 0x2d

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 122
    new-instance v9, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v9, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 123
    const/16 v6, 0x2d

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 124
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 125
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 126
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 118
    :goto_2
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_1

    .line 129
    :cond_2
    new-instance v6, Landroid/preference/Preference;

    invoke-virtual/range {p0 .. p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 130
    invoke-virtual {v6, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v16, ", "

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, " - "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nClick to delete"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 134
    new-instance v2, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;

    move-object/from16 v3, p0

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment$2;-><init>(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;Ljava/io/File;Landroid/preference/PreferenceCategory;Landroid/preference/Preference;Ljava/lang/Long;Ljava/lang/Long;Landroid/content/Context;)V

    invoke-virtual {v6, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2

    .line 161
    :cond_3
    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 162
    if-eqz v2, :cond_4

    array-length v2, v2

    if-nez v2, :cond_5

    .line 163
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 165
    :cond_5
    return-void

    :cond_6
    move-object v5, v2

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 177
    const v0, 0x7f060006

    return v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 42
    invoke-super {p0}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;->onResume()V

    .line 43
    const-string v0, "schedule_key"

    invoke-virtual {p0, v0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 45
    const v2, 0x7f070286

    invoke-virtual {p0, v2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 46
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 47
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 48
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 185
    invoke-super {p0, p1, p2}, Lcom/cgollner/systemmonitor/settings/SettingsAbstract$SettingsFragment;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    const-string v0, "history_bg_status_key"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_2

    .line 194
    invoke-virtual {p0, p2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 195
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 201
    :goto_1
    if-eqz v1, :cond_3

    .line 202
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0, p2}, Lcom/franco/kernel/system_monitor/settings/SettingsBgHistory$SettingsBgHistoryFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/TwoStatePreference;

    .line 198
    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_1

    .line 204
    :cond_3
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/cgollner/systemmonitor/historybg/HistoryBgService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method
