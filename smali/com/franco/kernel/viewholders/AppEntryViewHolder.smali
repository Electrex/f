.class public Lcom/franco/kernel/viewholders/AppEntryViewHolder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/View;I)Lcom/franco/kernel/viewholders/AppEntryViewHolder;
    .locals 3

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 19
    new-instance v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;

    invoke-direct {v1}, Lcom/franco/kernel/viewholders/AppEntryViewHolder;-><init>()V

    .line 20
    iput-object v2, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->a:Landroid/view/View;

    .line 21
    const v0, 0x1020016

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->b:Landroid/widget/TextView;

    .line 22
    const v0, 0x1020010

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->c:Landroid/widget/TextView;

    .line 23
    const v0, 0x1020006

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->d:Landroid/widget/ImageView;

    .line 24
    const v0, 0x1020007

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->e:Landroid/widget/ImageView;

    .line 25
    const v0, 0x1020008

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/franco/kernel/viewholders/AppEntryViewHolder;->f:Landroid/widget/ImageView;

    .line 26
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 29
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/franco/kernel/viewholders/AppEntryViewHolder;

    goto :goto_0
.end method
