.class public Lcom/franco/kernel/views/FloatingActionButton$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:I

.field private c:Landroid/view/ViewGroup;

.field private d:Landroid/widget/AbsListView;

.field private e:I

.field private f:Landroid/animation/TimeInterpolator;

.field private g:J

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    const v0, 0x1020002

    iput v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->b:I

    .line 178
    const v0, -0xec6101

    iput v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->e:I

    .line 180
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->g:J

    .line 186
    const-string v0, "Invalid Activity provided."

    invoke-static {p1, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iput-object p1, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a:Landroid/app/Activity;

    .line 188
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lcom/franco/kernel/views/FloatingActionButton$1;)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;-><init>(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic a(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/animation/TimeInterpolator;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->f:Landroid/animation/TimeInterpolator;

    return-object v0
.end method

.method static synthetic c(Lcom/franco/kernel/views/FloatingActionButton$Builder;)J
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->g:J

    return-wide v0
.end method

.method static synthetic d(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic e(Lcom/franco/kernel/views/FloatingActionButton$Builder;)I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->b:I

    return v0
.end method

.method static synthetic f(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->i:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic g(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic h(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->d:Landroid/widget/AbsListView;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/franco/kernel/views/FloatingActionButton$Builder;
    .locals 1

    .prologue
    .line 224
    const-string v0, "Invalid bg drawable"

    invoke-static {p1, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(ILjava/lang/String;)V

    .line 225
    invoke-static {p1}, Lcom/franco/kernel/App;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->i:Landroid/graphics/drawable/Drawable;

    .line 226
    return-object p0
.end method

.method public a(Landroid/view/View$OnClickListener;)Lcom/franco/kernel/views/FloatingActionButton$Builder;
    .locals 1

    .prologue
    .line 261
    const-string v0, "Invalid click listener provided."

    invoke-static {p1, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iput-object p1, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->h:Landroid/view/View$OnClickListener;

    .line 263
    return-object p0
.end method

.method public a(Landroid/widget/AbsListView;)Lcom/franco/kernel/views/FloatingActionButton$Builder;
    .locals 1

    .prologue
    .line 213
    const-string v0, "Invalid AbsListView provided."

    invoke-static {p1, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    iput-object p1, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->d:Landroid/widget/AbsListView;

    .line 215
    return-object p0
.end method

.method public a()Lcom/franco/kernel/views/FloatingActionButton;
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->f:Landroid/animation/TimeInterpolator;

    if-nez v0, :cond_0

    .line 269
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Builder;->f:Landroid/animation/TimeInterpolator;

    .line 271
    :cond_0
    new-instance v0, Lcom/franco/kernel/views/FloatingActionButton;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/franco/kernel/views/FloatingActionButton;-><init>(Lcom/franco/kernel/views/FloatingActionButton$Builder;Lcom/franco/kernel/views/FloatingActionButton$1;)V

    return-object v0
.end method
