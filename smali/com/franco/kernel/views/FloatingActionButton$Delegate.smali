.class Lcom/franco/kernel/views/FloatingActionButton$Delegate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 312
    iput v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->a:I

    .line 313
    iput v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->b:I

    .line 314
    iput-boolean v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->c:Z

    .line 315
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 283
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_5

    .line 286
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 290
    :goto_0
    iget v2, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->a:I

    if-ne v2, p2, :cond_3

    .line 291
    iget v2, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->b:I

    sub-int v4, v2, v0

    .line 292
    iget v2, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->b:I

    if-ge v0, v2, :cond_2

    move v2, v3

    .line 293
    :goto_1
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v3, :cond_0

    move v1, v3

    .line 298
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->c:Z

    if-eqz v1, :cond_1

    .line 299
    invoke-static {v2}, Lcom/franco/kernel/views/FloatingActionButton;->a(Z)V

    .line 301
    :cond_1
    iput p2, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->a:I

    .line 302
    iput v0, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->b:I

    .line 303
    iput-boolean v3, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->c:Z

    .line 304
    return-void

    :cond_2
    move v2, v1

    .line 292
    goto :goto_1

    .line 296
    :cond_3
    iget v2, p0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->a:I

    if-le p2, v2, :cond_4

    move v1, v3

    :cond_4
    move v2, v1

    move v1, v3

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 309
    return-void
.end method
