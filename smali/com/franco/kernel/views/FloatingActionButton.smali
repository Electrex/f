.class public Lcom/franco/kernel/views/FloatingActionButton;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/app/Activity;

.field private static b:Landroid/view/ViewGroup;

.field private static c:Landroid/widget/ImageButton;

.field private static d:Landroid/animation/TimeInterpolator;

.field private static e:Z

.field private static f:Lcom/franco/kernel/views/FloatingActionButton$Delegate;

.field private static g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/franco/kernel/views/FloatingActionButton$Delegate;

    invoke-direct {v0}, Lcom/franco/kernel/views/FloatingActionButton$Delegate;-><init>()V

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->f:Lcom/franco/kernel/views/FloatingActionButton$Delegate;

    return-void
.end method

.method private constructor <init>(Lcom/franco/kernel/views/FloatingActionButton$Builder;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->a(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/app/Activity;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->a:Landroid/app/Activity;

    .line 41
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->b(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/animation/TimeInterpolator;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->d:Landroid/animation/TimeInterpolator;

    .line 42
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->c(Lcom/franco/kernel/views/FloatingActionButton$Builder;)J

    move-result-wide v0

    sput-wide v0, Lcom/franco/kernel/views/FloatingActionButton;->g:J

    .line 44
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->d(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 45
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->d(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/view/ViewGroup;

    move-result-object v0

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->b:Landroid/view/ViewGroup;

    .line 51
    :goto_0
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040049

    sget-object v2, Lcom/franco/kernel/views/FloatingActionButton;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 52
    const v1, 0x7f0c0104

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    .line 53
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->f(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->f(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 56
    :cond_0
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->g(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    sget-object v0, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 60
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 61
    sget-object v1, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    new-instance v2, Lcom/franco/kernel/views/FloatingActionButton$1;

    invoke-direct {v2, p0, v0}, Lcom/franco/kernel/views/FloatingActionButton$1;-><init>(Lcom/franco/kernel/views/FloatingActionButton;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 68
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setClipToOutline(Z)V

    .line 71
    :cond_1
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/franco/kernel/helpers/ViewHelpers;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->f(Landroid/view/View;F)V

    .line 74
    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->h(Lcom/franco/kernel/views/FloatingActionButton$Builder;)Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(Landroid/widget/AbsListView;)V

    .line 75
    return-void

    .line 47
    :cond_2
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->a:Landroid/app/Activity;

    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->e(Lcom/franco/kernel/views/FloatingActionButton$Builder;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sput-object v0, Lcom/franco/kernel/views/FloatingActionButton;->b:Landroid/view/ViewGroup;

    .line 48
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->b:Landroid/view/ViewGroup;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No parent found with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;->e(Lcom/franco/kernel/views/FloatingActionButton$Builder;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/franco/kernel/views/FloatingActionButton;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method synthetic constructor <init>(Lcom/franco/kernel/views/FloatingActionButton$Builder;Lcom/franco/kernel/views/FloatingActionButton$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/franco/kernel/views/FloatingActionButton;-><init>(Lcom/franco/kernel/views/FloatingActionButton$Builder;)V

    return-void
.end method

.method static synthetic a()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Lcom/franco/kernel/views/FloatingActionButton$Builder;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/franco/kernel/views/FloatingActionButton$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/franco/kernel/views/FloatingActionButton$Builder;-><init>(Landroid/app/Activity;Lcom/franco/kernel/views/FloatingActionButton$1;)V

    return-object v0
.end method

.method static a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    if-gez p0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    return-void
.end method

.method static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    if-nez p0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    return-void
.end method

.method static synthetic a(Z)V
    .locals 0

    .prologue
    .line 29
    invoke-static {p0}, Lcom/franco/kernel/views/FloatingActionButton;->b(Z)V

    return-void
.end method

.method private static a(ZZ)V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/franco/kernel/views/FloatingActionButton;->b(ZZZ)V

    .line 131
    return-void
.end method

.method static synthetic a(ZZZ)V
    .locals 0

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/franco/kernel/views/FloatingActionButton;->b(ZZZ)V

    return-void
.end method

.method private static b(Z)V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/franco/kernel/views/FloatingActionButton;->a(ZZ)V

    .line 111
    return-void
.end method

.method private static b(ZZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 134
    sget-boolean v0, Lcom/franco/kernel/views/FloatingActionButton;->e:Z

    if-ne v0, p0, :cond_0

    if-eqz p2, :cond_1

    .line 135
    :cond_0
    sput-boolean p0, Lcom/franco/kernel/views/FloatingActionButton;->e:Z

    .line 136
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHeight()I

    move-result v2

    .line 137
    if-nez v2, :cond_2

    if-nez p2, :cond_2

    .line 139
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    new-instance v1, Lcom/franco/kernel/views/FloatingActionButton$2;

    invoke-direct {v1, p0, p1}, Lcom/franco/kernel/views/FloatingActionButton$2;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 171
    :cond_1
    :goto_0
    return-void

    .line 157
    :cond_2
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 158
    instance-of v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v3, :cond_5

    .line 159
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 161
    :goto_1
    sget-boolean v3, Lcom/franco/kernel/views/FloatingActionButton;->e:Z

    if-eqz v3, :cond_3

    add-int v1, v2, v0

    .line 162
    :cond_3
    if-eqz p1, :cond_4

    .line 163
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v2, Lcom/franco/kernel/views/FloatingActionButton;->d:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-wide v2, Lcom/franco/kernel/views/FloatingActionButton;->g:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 168
    :cond_4
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->c:Landroid/widget/ImageButton;

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTranslationY(F)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/widget/AbsListView;)V
    .locals 1

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 95
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->f:Lcom/franco/kernel/views/FloatingActionButton$Delegate;

    invoke-virtual {v0}, Lcom/franco/kernel/views/FloatingActionButton$Delegate;->a()V

    .line 96
    sget-object v0, Lcom/franco/kernel/views/FloatingActionButton;->f:Lcom/franco/kernel/views/FloatingActionButton$Delegate;

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 98
    :cond_0
    return-void
.end method
