.class public Lcom/franco/kernel/views/ImageViewInfo;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewInfo;->a()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewInfo;->a()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewInfo;->a()V

    .line 29
    return-void
.end method

.method private a()V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p0}, Lcom/franco/kernel/views/ImageViewInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 40
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 41
    :try_start_1
    invoke-static {}, Lcom/franco/kernel/App;->j()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 43
    new-instance v2, Lcom/franco/kernel/dialogs/GenericDialogs$SimpleDialog;

    invoke-virtual {p0}, Lcom/franco/kernel/views/ImageViewInfo;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/franco/kernel/dialogs/GenericDialogs$SimpleDialog;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {v2}, Lcom/franco/kernel/dialogs/GenericDialogs$SimpleDialog;->show()V

    .line 46
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 47
    invoke-virtual {v2}, Lcom/franco/kernel/dialogs/GenericDialogs$SimpleDialog;->b()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    :cond_0
    const-string v0, "CgkIo_DA4eIIEAIQEg"

    invoke-static {v0}, Lcom/franco/kernel/helpers/AchievementHelpers;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 57
    :goto_0
    if-eqz v1, :cond_1

    .line 58
    invoke-static {v1}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 60
    :cond_1
    return-void

    .line 51
    :catch_0
    move-exception v1

    .line 52
    :goto_1
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v3, 0x7f070107

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move-object v1, v0

    goto :goto_0

    .line 51
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method
