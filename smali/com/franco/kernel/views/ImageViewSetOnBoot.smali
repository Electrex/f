.class public Lcom/franco/kernel/views/ImageViewSetOnBoot;
.super Landroid/support/v7/widget/SwitchCompat;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v7/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->a()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->a()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    invoke-direct {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->a()V

    .line 37
    return-void
.end method

.method private a()V
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0, p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 41
    return-void
.end method

.method private a(ZLjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, -0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 44
    instance-of v0, p2, Lcom/franco/kernel/FileTunable;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 45
    check-cast v0, Lcom/franco/kernel/FileTunable;

    .line 46
    if-eqz p1, :cond_1

    .line 47
    check-cast p2, Lcom/franco/kernel/FileTunable;

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/franco/kernel/FileTunable;->c(Ljava/lang/String;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    check-cast p2, Lcom/franco/kernel/FileTunable;

    iget-object v0, v0, Lcom/franco/kernel/FileTunable;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/franco/kernel/FileTunable;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_2
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 52
    if-nez p1, :cond_6

    move-object v0, p2

    .line 53
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_3
    :goto_1
    packed-switch v2, :pswitch_data_1

    .line 67
    new-instance v0, Lcom/franco/kernel/views/ImageViewSetOnBoot$1;

    invoke-direct {v0, p0, p2}, Lcom/franco/kernel/views/ImageViewSetOnBoot$1;-><init>(Lcom/franco/kernel/views/ImageViewSetOnBoot;Ljava/lang/Object;)V

    new-array v2, v5, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cat "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v5, v2}, Lcom/franco/kernel/helpers/RootHelpers;->a(Lcom/franco/kernel/helpers/RootHelpers$JobFinishedCallback;Z[Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    goto :goto_0

    .line 53
    :pswitch_0
    const-string v3, "color_profile"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    goto :goto_1

    .line 55
    :pswitch_1
    new-instance v0, Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-direct {v0}, Lcom/franco/kernel/abstracts/AbstractColors;-><init>()V

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->x()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->b()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 58
    :cond_4
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->h()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/franco/kernel/abstracts/AbstractColors;->b(Ljava/util/List;Ljava/util/List;)V

    .line 64
    :goto_2
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    const-string v1, "1"

    invoke-interface {v0, p2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 61
    :cond_5
    invoke-virtual {v1, v4, v4}, Lcom/franco/kernel/abstracts/AbstractColors;->b(Ljava/util/List;Ljava/util/List;)V

    goto :goto_2

    :cond_6
    move-object v0, p2

    .line 89
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_2

    :cond_7
    move v0, v2

    :goto_3
    packed-switch v0, :pswitch_data_3

    .line 101
    :goto_4
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-interface {v0, p2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 89
    :pswitch_2
    const-string v3, "color_profile"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    goto :goto_3

    .line 91
    :pswitch_3
    new-instance v0, Lcom/franco/kernel/abstracts/AbstractColors;

    invoke-direct {v0}, Lcom/franco/kernel/abstracts/AbstractColors;-><init>()V

    invoke-virtual {v0}, Lcom/franco/kernel/abstracts/AbstractColors;->x()Lcom/franco/kernel/abstracts/AbstractColors;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->b()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 94
    :cond_8
    invoke-virtual {v1}, Lcom/franco/kernel/abstracts/AbstractColors;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/franco/kernel/abstracts/AbstractColors;->c(Ljava/util/List;)V

    goto :goto_4

    .line 96
    :cond_9
    invoke-virtual {v1, v4}, Lcom/franco/kernel/abstracts/AbstractColors;->c(Ljava/util/List;)V

    goto :goto_4

    .line 53
    :pswitch_data_0
    .packed-switch -0x3062b333
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    .line 89
    :pswitch_data_2
    .packed-switch -0x3062b333
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method private setSwitchColors(Z)V
    .locals 3

    .prologue
    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->getThumbDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 140
    invoke-virtual {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->getTrackDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Lcom/franco/kernel/App;->k()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 141
    invoke-virtual {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->getTrackDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->getTrackDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 110
    if-nez v1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-direct {p0, p2}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->setSwitchColors(Z)V

    .line 116
    instance-of v0, v1, Lcom/franco/kernel/FileTunable;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 117
    check-cast v0, Lcom/franco/kernel/FileTunable;

    invoke-virtual {v0}, Lcom/franco/kernel/FileTunable;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_0

    :cond_2
    move-object v0, v1

    .line 119
    check-cast v0, Lcom/franco/kernel/FileTunable;

    invoke-virtual {v0}, Lcom/franco/kernel/FileTunable;->a()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p2, :cond_0

    .line 130
    :cond_3
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v4, "set_on_boot_toast"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 131
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f070234

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Lcom/franco/kernel/App;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 134
    :cond_4
    if-nez p2, :cond_7

    move v0, v2

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/franco/kernel/views/ImageViewSetOnBoot;->a(ZLjava/lang/Object;)V

    goto :goto_0

    .line 122
    :cond_5
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 123
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    if-nez p2, :cond_0

    .line 125
    :cond_6
    invoke-static {}, Lcom/franco/kernel/App;->h()Landroid/content/SharedPreferences;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    if-nez p2, :cond_3

    goto :goto_0

    :cond_7
    move v0, v3

    .line 134
    goto :goto_1
.end method
