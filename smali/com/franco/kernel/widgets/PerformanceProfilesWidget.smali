.class public Lcom/franco/kernel/widgets/PerformanceProfilesWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "SourceFile"


# static fields
.field private static a:Landroid/widget/RemoteViews;

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/franco/kernel/helpers/RootHelpers$Stdout;)V
    .locals 6
    .annotation runtime Lde/halfbit/tinybus/Subscribe;
    .end annotation

    .prologue
    const v5, 0x7f0c0136

    .line 71
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 72
    new-instance v0, Landroid/content/ComponentName;

    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    invoke-virtual {v1, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 75
    invoke-virtual {p1}, Lcom/franco/kernel/helpers/RootHelpers$Stdout;->a()Ljava/lang/String;

    move-result-object v3

    .line 77
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 88
    sput v5, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    .line 91
    :goto_1
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_1

    .line 92
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    sget-object v4, Lcom/franco/kernel/App;->c:Landroid/content/res/Resources;

    const v5, 0x7f0a0105

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 96
    :cond_1
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 97
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 98
    return-void

    .line 77
    :pswitch_0
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const-string v4, "2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 79
    :pswitch_3
    const v0, 0x7f0c0135

    sput v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    goto :goto_1

    .line 82
    :pswitch_4
    sput v5, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    goto :goto_1

    .line 85
    :pswitch_5
    const v0, 0x7f0c0137

    sput v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    goto :goto_1

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 164
    const/4 v0, 0x0

    sput v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    .line 165
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const v3, 0x7f0c0137

    const v2, 0x7f0c0136

    const v1, 0x7f0c0135

    const v8, 0x7f0a00a6

    .line 103
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 105
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 106
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 109
    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;

    invoke-direct {v0, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "action null: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v4, :cond_2

    const-string v0, "true"

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 114
    if-eqz v4, :cond_1

    .line 115
    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 126
    invoke-static {v4}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 159
    :cond_1
    :goto_2
    return-void

    .line 112
    :cond_2
    const-string v0, "false"

    goto :goto_0

    .line 115
    :sswitch_0
    const-string v7, "com.franco.kernel.POWER_SAVING"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v7, "com.franco.kernel.BALANCE"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v7, "com.franco.kernel.PERFORMANCE"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :pswitch_0
    move v0, v1

    .line 130
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "viewId: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 132
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    if-nez v4, :cond_3

    .line 133
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0, v4, v5, v6}, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 136
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "view null: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    if-nez v4, :cond_4

    const-string v4, "true"

    :goto_4
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 137
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "widgetIds null: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v6, :cond_5

    const-string v4, "true"

    :goto_5
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/franco/kernel/App;->a(Ljava/lang/Object;)V

    .line 139
    sget v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    if-nez v4, :cond_6

    .line 140
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v4, v1, v7}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 142
    sget-object v1, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 144
    sget-object v1, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 151
    :goto_6
    sget-object v1, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0105

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 153
    sput v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    .line 155
    if-eqz v6, :cond_1

    .line 156
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v5, v6, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_2

    :pswitch_1
    move v0, v2

    .line 121
    goto/16 :goto_3

    :pswitch_2
    move v0, v3

    .line 124
    goto/16 :goto_3

    .line 136
    :cond_4
    const-string v4, "false"

    goto :goto_4

    .line 137
    :cond_5
    const-string v4, "false"

    goto :goto_5

    .line 147
    :cond_6
    sget-object v1, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_6

    .line 115
    :sswitch_data_0
    .sparse-switch
        -0x7cc125e5 -> :sswitch_2
        -0x27b47d17 -> :sswitch_0
        0x72c6ee47 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 12

    .prologue
    const v11, 0x7f0c0137

    const v10, 0x7f0c0136

    const v9, 0x7f0c0135

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 27
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 29
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04007e

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/franco/kernel/activities/MainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    const-string v1, "OPEN_PERFORMANCE_PROFILES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.franco.kernel.POWER_SAVING"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 36
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.franco.kernel.BALANCE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.franco.kernel.PERFORMANCE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 41
    invoke-static {p1, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 42
    invoke-static {p1, v7, v2, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 43
    invoke-static {p1, v7, v3, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 45
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    const v5, 0x7f0c019d

    const v6, 0x7f070200

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 46
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    const v5, 0x7f07020a

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v9, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 47
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    const v5, 0x7f07004b

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v10, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 48
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    const v5, 0x7f070201

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v11, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 50
    sget-object v4, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    const v5, 0x7f0c019d

    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 51
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v9, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 52
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v10, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 53
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v11, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 55
    sget v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->b:I

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lcom/franco/kernel/App;->d()Lde/halfbit/tinybus/Bus;

    move-result-object v0

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 58
    invoke-static {}, Lcom/franco/kernel/helpers/Terminal;->a()Lcom/franco/kernel/helpers/Terminal;

    move-result-object v0

    iget-object v0, v0, Lcom/franco/kernel/helpers/Terminal;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-virtual {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "getprop fku.perf.profile"

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/franco/kernel/helpers/RootHelpers;->a([Ljava/lang/String;)Lcom/franco/kernel/helpers/RootHelpers$RunCMD;

    .line 66
    :cond_0
    :goto_0
    sget-object v0, Lcom/franco/kernel/widgets/PerformanceProfilesWidget;->a:Landroid/widget/RemoteViews;

    invoke-virtual {p2, p3, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 67
    return-void

    .line 61
    :cond_1
    invoke-static {}, Lcom/franco/kernel/App;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701ff

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
