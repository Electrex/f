.class final Lcom/google/android/gms/common/api/zzd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient;


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field private D:Lcom/google/android/gms/common/internal/zzq;

.field private E:Z

.field private F:Z

.field private final G:Ljava/util/Set;

.field private final H:Lcom/google/android/gms/common/api/zzd$zze;

.field private final I:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

.field private final J:Lcom/google/android/gms/common/api/GoogleApiClient$zza;

.field private final K:Lcom/google/android/gms/common/internal/zzl$zza;

.field final a:Ljava/util/Queue;

.field b:Landroid/content/BroadcastReceiver;

.field final c:Ljava/util/Set;

.field private final d:Ljava/util/concurrent/locks/Lock;

.field private final e:Ljava/util/concurrent/locks/Condition;

.field private final f:Lcom/google/android/gms/common/internal/zzl;

.field private final g:I

.field private final h:Landroid/content/Context;

.field private final i:Landroid/os/Looper;

.field private j:Lcom/google/android/gms/common/ConnectionResult;

.field private k:I

.field private volatile l:I

.field private volatile m:Z

.field private n:I

.field private o:Z

.field private p:I

.field private q:J

.field private r:J

.field private final s:Lcom/google/android/gms/common/api/zzd$zzc;

.field private final t:Landroid/os/Bundle;

.field private final u:Ljava/util/Map;

.field private final v:Ljava/util/Set;

.field private final w:Ljava/util/Map;

.field private final x:Ljava/util/List;

.field private y:Z

.field private z:Lcom/google/android/gms/internal/zzur;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/Api$zzb;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V
    .locals 13

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->e:Ljava/util/concurrent/locks/Condition;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Lcom/google/android/gms/common/api/zzd;->q:J

    const-wide/16 v2, 0x1388

    iput-wide v2, p0, Lcom/google/android/gms/common/api/zzd;->r:J

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->t:Landroid/os/Bundle;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->v:Ljava/util/Set;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->G:Ljava/util/Set;

    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v3, 0x10

    const/high16 v4, 0x3f400000    # 0.75f

    const/4 v5, 0x2

    invoke-direct {v2, v3, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->c:Ljava/util/Set;

    new-instance v2, Lcom/google/android/gms/common/api/zzd$1;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/zzd$1;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->H:Lcom/google/android/gms/common/api/zzd$zze;

    new-instance v2, Lcom/google/android/gms/common/api/zzd$6;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/zzd$6;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->I:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    new-instance v2, Lcom/google/android/gms/common/api/zzd$7;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/zzd$7;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->J:Lcom/google/android/gms/common/api/GoogleApiClient$zza;

    new-instance v2, Lcom/google/android/gms/common/api/zzd$8;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/zzd$8;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->K:Lcom/google/android/gms/common/internal/zzl$zza;

    iput-object p1, p0, Lcom/google/android/gms/common/api/zzd;->h:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/common/internal/zzl;

    iget-object v3, p0, Lcom/google/android/gms/common/api/zzd;->K:Lcom/google/android/gms/common/internal/zzl$zza;

    invoke-direct {v2, p2, v3}, Lcom/google/android/gms/common/internal/zzl;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzl$zza;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    iput-object p2, p0, Lcom/google/android/gms/common/api/zzd;->i:Landroid/os/Looper;

    new-instance v2, Lcom/google/android/gms/common/api/zzd$zzc;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/common/api/zzd$zzc;-><init>(Lcom/google/android/gms/common/api/zzd;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->g:I

    invoke-interface/range {p7 .. p7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    iget-object v4, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/zzl;->a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    goto :goto_0

    :cond_0
    invoke-interface/range {p8 .. p8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    iget-object v4, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/zzl;->a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    goto :goto_1

    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/common/internal/zzf;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/zzd;->x:Ljava/util/List;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/gms/common/api/zzd;->A:I

    const/4 v2, 0x0

    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v10, v2

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/gms/common/api/Api;

    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_9

    move-object/from16 v0, p6

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    :goto_3
    move v11, v2

    :goto_4
    invoke-virtual {v9}, Lcom/google/android/gms/common/api/Api;->a()Lcom/google/android/gms/common/api/Api$zzb;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gms/common/api/zzd;->I:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    invoke-direct {p0, v9, v11}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/Api;I)Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    move-result-object v8

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/Api$zzb;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->J:Lcom/google/android/gms/common/api/GoogleApiClient$zza;

    invoke-interface {v3, v2}, Lcom/google/android/gms/common/api/Api$zza;->a(Lcom/google/android/gms/common/api/GoogleApiClient$zza;)V

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-virtual {v9}, Lcom/google/android/gms/common/api/Api;->c()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v4

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/google/android/gms/common/api/Api;->a()Lcom/google/android/gms/common/api/Api$zzb;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/api/Api$zzb;->a()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    const/4 v2, 0x1

    :goto_5
    or-int/2addr v2, v10

    invoke-interface {v3}, Lcom/google/android/gms/common/api/Api$zza;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    iget v3, p0, Lcom/google/android/gms/common/api/zzd;->A:I

    if-ge v11, v3, :cond_2

    iput v11, p0, Lcom/google/android/gms/common/api/zzd;->A:I

    :cond_2
    if-eqz v11, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/common/api/zzd;->v:Ljava/util/Set;

    invoke-virtual {v9}, Lcom/google/android/gms/common/api/Api;->c()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    move v10, v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    if-eqz v10, :cond_7

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/internal/zzf;->a(Ljava/lang/Integer;)V

    invoke-direct/range {p0 .. p4}, Lcom/google/android/gms/common/api/zzd;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/Api$zzb;)V

    :cond_8
    return-void

    :cond_9
    move v11, v2

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    return-object p1
.end method

.method private static a(Lcom/google/android/gms/common/api/Api$zzb;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/Api$zza;
    .locals 7

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/common/api/Api$zzb;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Ljava/lang/Object;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lcom/google/android/gms/common/api/Api;I)Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;
    .locals 1

    new-instance v0, Lcom/google/android/gms/common/api/zzd$9;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/gms/common/api/zzd$9;-><init>(Lcom/google/android/gms/common/api/zzd;ILcom/google/android/gms/common/api/Api;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/internal/zzq;)Lcom/google/android/gms/common/internal/zzq;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;)Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    if-eq v0, v3, :cond_c

    if-ne p1, v5, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/zzd$zzg;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/zzd$zzg;->f()I

    move-result v4

    if-eq v4, v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/zzd$zzg;->b()V

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/zzd$zzg;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/zzd$zzg;->b()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/zzd$zzg;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Lcom/google/android/gms/common/api/zzd$zzg;->a(Lcom/google/android/gms/common/api/zzd$zze;)V

    invoke-interface {v0}, Lcom/google/android/gms/common/api/zzd$zzg;->b()V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->G:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/zze;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/zze;->a()V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->G:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_4
    return-void

    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->e()Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->d()Z

    move-result v3

    const/4 v4, 0x3

    iput v4, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    if-eqz v0, :cond_9

    if-ne p1, v5, :cond_8

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Api$zza;->b()V

    goto :goto_5

    :cond_a
    if-ne p1, v5, :cond_d

    move v0, v1

    :goto_6
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    if-eqz v3, :cond_c

    if-eq p1, v5, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->a(I)V

    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->y:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_4

    :cond_d
    move v0, v2

    goto :goto_6
.end method

.method private a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/Api$zzb;)V
    .locals 7

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/zzf;->j()Lcom/google/android/gms/internal/zzus;

    move-result-object v1

    new-instance v5, Lcom/google/android/gms/common/api/zzd$10;

    invoke-direct {v5, p0}, Lcom/google/android/gms/common/api/zzd$10;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    new-instance v6, Lcom/google/android/gms/common/api/zzd$11;

    invoke-direct {v6, p0}, Lcom/google/android/gms/common/api/zzd$11;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    move-object v0, p4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/Api$zzb;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzur;

    iput-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    return-void
.end method

.method private a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(Z)V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->h:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->c()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->d(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->p()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/api/zzd$zzg;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/zzd$zzg;->e()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->H:Lcom/google/android/gms/common/api/zzd$zze;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/zzd$zzg;->a(Lcom/google/android/gms/common/api/zzd$zze;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/zzd$zzg;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/common/api/zzd$zzg;->e()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/Api$zzc;)Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Api$zza;->c()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/common/api/zzd$zzg;->e()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x11

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/zzd$zzg;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/zzd$zzg;->a(Lcom/google/android/gms/common/api/Api$zza;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/internal/zzaa;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/internal/zzaa;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/internal/zzaa;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/internal/zzaa;->b()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    new-instance v1, Lcom/google/android/gms/common/api/zzd$12;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/common/api/zzd$12;-><init>(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/internal/zzaa;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/zzd$zzc;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzur;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzur;->h_()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzur;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    :cond_2
    return-void
.end method

.method private a(IILcom/google/android/gms/common/ConnectionResult;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-ne p2, v1, :cond_1

    invoke-virtual {p3}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/common/api/zzd;->k:I

    if-ge p1, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;IILcom/google/android/gms/common/ConnectionResult;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/common/api/zzd;->a(IILcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/zzd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/zzd;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/common/api/zzd;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/common/api/zzd;->k:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/common/api/zzd;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->t:Landroid/os/Bundle;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    new-instance v1, Lcom/google/android/gms/common/api/zzd$13;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/common/api/zzd$13;-><init>(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/zzd$zzc;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/common/api/zzd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/zzd;->C:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->h()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->d(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method private c(Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/common/api/zzd;->A:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/common/api/zzd;->A:I

    if-ne v1, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/common/api/zzd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/zzd;->E:Z

    return p1
.end method

.method static synthetic d(Lcom/google/android/gms/common/api/zzd;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->h:Landroid/content/Context;

    return-object v0
.end method

.method private d(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    new-instance v1, Lcom/google/android/gms/common/api/zzd$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/zzd$2;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/zzd$zzc;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->c(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/common/api/zzd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/zzd;->F:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/gms/common/api/zzd;)Lcom/google/android/gms/common/api/zzd$zzc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/common/api/zzd;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/common/api/zzd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    return p1
.end method

.method static synthetic f(Lcom/google/android/gms/common/api/zzd;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/common/api/zzd;->q:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/android/gms/common/api/zzd;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/common/api/zzd;->r:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/gms/common/api/zzd;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    return v0
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->l()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->m()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->k()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->p()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->E:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->F:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzur;->a(Lcom/google/android/gms/common/internal/zzq;Z)V

    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/gms/common/api/zzd;->a(Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->n()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    if-eqz v0, :cond_5

    iput-boolean v3, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->t:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/zzl;->a(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->t:Landroid/os/Bundle;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic i(Lcom/google/android/gms/common/api/zzd;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    return v0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    new-instance v1, Lcom/google/android/gms/common/api/zzd$zzb;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/zzd$zzb;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzur;->a(Lcom/google/android/gms/common/internal/zzt;)V

    return-void
.end method

.method private j()Ljava/util/Set;
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->x:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzmh;->a(Ljava/util/List;)[Lcom/google/android/gms/common/api/Scope;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/common/api/zzd;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    return v0
.end method

.method private k()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->j()Ljava/util/Set;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/api/zzd$zza;

    invoke-direct {v3, p0}, Lcom/google/android/gms/common/api/zzd$zza;-><init>(Lcom/google/android/gms/common/api/zzd;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzur;->a(Lcom/google/android/gms/common/internal/zzq;Ljava/util/Set;Lcom/google/android/gms/internal/zzuz;)V

    return-void
.end method

.method static synthetic k(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->o()V

    return-void
.end method

.method private l()V
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->a()Landroid/os/Looper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/zzd$zzc;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "This method must be run on the mHandlerForCallbacks thread"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/zzx;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->C:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zzc;

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/Api$zza;->a(Lcom/google/android/gms/common/internal/zzq;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->p()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/gms/common/api/zzd;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    return-object v0
.end method

.method private m()V
    .locals 3

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zzc;

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->h()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->D:Lcom/google/android/gms/common/internal/zzq;

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/Api$zza;->b(Lcom/google/android/gms/common/internal/zzq;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private n()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->a(ZLjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/zzd$zzg;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/zzd$zzg;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->i()V

    return-void
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic o(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->l()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/gms/common/api/zzd;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->v:Ljava/util/Set;

    return-object v0
.end method

.method private p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->m:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/zzd$zzc;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->s:Lcom/google/android/gms/common/api/zzd$zzc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/zzd$zzc;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->b:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/zzd;->b:Landroid/content/BroadcastReceiver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic q(Lcom/google/android/gms/common/api/zzd;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/common/api/zzd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->m()V

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->i:Landroid/os/Looper;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/Api$zzc;)Lcom/google/android/gms/common/api/Api$zza;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/zza$zza;)Lcom/google/android/gms/common/api/zza$zza;
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/zzx;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->n()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/zzd;->a(Lcom/google/android/gms/common/api/zzd$zzg;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/api/zzd;->a(I)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "GoogleApiClient:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "mConnectionState="

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    const-string v0, " mResuming="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->m:Z

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mWaitingToDisconnect="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "mWorkQueue.size()="

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mUnconsumedRunners.size()="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/zzd;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/common/api/Api$zza;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto :goto_1

    :pswitch_0
    const-string v0, "CONNECTING"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "CONNECTED"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "DISCONNECTING"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "DISCONNECTED"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/android/gms/common/api/Api;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Api;->c()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->o:Z

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/zzd;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->y:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/zzd;->j:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->n:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->t:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/zzd;->p:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->C:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->E:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->F:Z

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->B:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->z:Lcom/google/android/gms/internal/zzur;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzur;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Api$zza;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->b(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    return-void
.end method

.method public b(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->f:Lcom/google/android/gms/common/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzl;->b(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public b(Lcom/google/android/gms/common/api/Api;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/zzd;->u:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Api;->c()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Api$zza;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/common/api/Api$zza;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/zzd;->p()V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/zzd;->a(I)V

    return-void
.end method

.method public d()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/common/api/zzd;->l:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/zzd;->m:Z

    return v0
.end method

.method public g()I
    .locals 1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
