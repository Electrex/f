.class public final Lcom/google/android/gms/common/internal/zzk$zzd;
.super Lcom/google/android/gms/common/internal/zzr$zza;


# instance fields
.field private a:Lcom/google/android/gms/common/internal/zzk;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/zzk;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/zzr$zza;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    return-void
.end method

.method private a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    const-string v1, "onAccountValidationComplete can be called only once per call to validateAccount"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/common/internal/zzk;->a(ILandroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/zzk$zzd;->a()V

    return-void
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    const-string v1, "onPostInitComplete can be called only once per call to getRemoteService"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzk$zzd;->a:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/internal/zzk;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/zzk$zzd;->a()V

    return-void
.end method
