.class public final Lcom/google/android/gms/drive/internal/zzam;
.super Lcom/google/android/gms/internal/zzws;


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzws;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/zzam;->a()Lcom/google/android/gms/drive/internal/zzam;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/drive/internal/zzam;
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/zzam;->a:I

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/zzam;->f:Lcom/google/android/gms/internal/zzwu;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/zzam;->g:I

    return-object p0
.end method

.method public a(Lcom/google/android/gms/internal/zzwr;)V
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/drive/internal/zzam;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/zzwr;->a(II)V

    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/zzwr;->a(IJ)V

    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/zzwr;->a(IJ)V

    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/zzwr;->a(IJ)V

    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzws;->a(Lcom/google/android/gms/internal/zzwr;)V

    return-void
.end method

.method protected b()I
    .locals 4

    invoke-super {p0}, Lcom/google/android/gms/internal/zzws;->b()I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/drive/internal/zzam;->a:I

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/zzwr;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/zzwr;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/zzwr;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/zzwr;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/drive/internal/zzam;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/drive/internal/zzam;

    iget v1, p0, Lcom/google/android/gms/drive/internal/zzam;->a:I

    iget v2, p1, Lcom/google/android/gms/drive/internal/zzam;->a:I

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/zzam;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/zzam;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/zzam;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/internal/zzam;->a(Lcom/google/android/gms/internal/zzws;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    iget v0, p0, Lcom/google/android/gms/drive/internal/zzam;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/zzam;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/zzam;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/zzam;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/zzam;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
