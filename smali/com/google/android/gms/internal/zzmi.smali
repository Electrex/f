.class public Lcom/google/android/gms/internal/zzmi;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/google/android/gms/internal/zzmi$zze;

.field public static final B:Lcom/google/android/gms/internal/zzmi$zzg;

.field public static final C:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final D:Lcom/google/android/gms/internal/zzmi$zzh;

.field public static final E:Lcom/google/android/gms/internal/zzmi$zzi;

.field public static final F:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final G:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final H:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final I:Lcom/google/android/gms/drive/metadata/internal/zzb;

.field public static final J:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final K:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final L:Lcom/google/android/gms/internal/zzmi$zzf;

.field public static final a:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final b:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final c:Lcom/google/android/gms/internal/zzmi$zza;

.field public static final d:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final e:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final f:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final g:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final h:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final i:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final j:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final k:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final l:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final m:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final n:Lcom/google/android/gms/internal/zzmi$zzb;

.field public static final o:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final p:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final q:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final r:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final s:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final t:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final u:Lcom/google/android/gms/internal/zzmi$zzc;

.field public static final v:Lcom/google/android/gms/drive/metadata/MetadataField;

.field public static final w:Lcom/google/android/gms/drive/metadata/zzb;

.field public static final x:Lcom/google/android/gms/drive/metadata/internal/zzo;

.field public static final y:Lcom/google/android/gms/drive/metadata/internal/zzo;

.field public static final z:Lcom/google/android/gms/internal/zzmi$zzd;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const v8, 0x432380

    const v7, 0x5b8d80

    const v6, 0x6acfc0

    const v5, 0x3e8fa0

    const v4, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/zzml;->a:Lcom/google/android/gms/internal/zzml;

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->a:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->b:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zza;

    const v1, 0x4c4b40

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzmi$zza;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->c:Lcom/google/android/gms/internal/zzmi$zza;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->d:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->e:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->f:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzg;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->g:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->h:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->i:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->j:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->k:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->l:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$1;

    const-string v1, "isExplicitlyTrashed"

    const-string v2, "trashed"

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/gms/internal/zzmi$1;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->m:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzb;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/zzmi$zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->n:Lcom/google/android/gms/internal/zzmi$zzb;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->o:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->p:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isGooglePhotosFolder"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->q:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isGooglePhotosRootFolder"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->r:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isTrashable"

    invoke-direct {v0, v1, v8}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->s:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->t:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzc;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/zzmi$zzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->u:Lcom/google/android/gms/internal/zzmi$zzc;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->v:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzm;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->w:Lcom/google/android/gms/drive/metadata/zzb;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzo;

    const-string v1, "lastModifyingUser"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/zzo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->x:Lcom/google/android/gms/drive/metadata/internal/zzo;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzo;

    const-string v1, "sharingUser"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/zzo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->y:Lcom/google/android/gms/drive/metadata/internal/zzo;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzd;

    invoke-direct {v0, v5}, Lcom/google/android/gms/internal/zzmi$zzd;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->z:Lcom/google/android/gms/internal/zzmi$zzd;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zze;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/internal/zzmi$zze;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->A:Lcom/google/android/gms/internal/zzmi$zze;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzg;

    const-string v1, "starred"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/zzmi$zzg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->B:Lcom/google/android/gms/internal/zzmi$zzg;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$2;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/android/gms/internal/zzmi$2;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->C:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzh;

    const-string v1, "title"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/zzmi$zzh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->D:Lcom/google/android/gms/internal/zzmi$zzh;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzi;

    const-string v1, "trashed"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/zzmi$zzi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->E:Lcom/google/android/gms/internal/zzmi$zzi;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->F:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->G:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "uniqueIdentifier"

    const v2, 0x4c4b40

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->H:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzb;

    const-string v1, "writersCanShare"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/zzb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->I:Lcom/google/android/gms/drive/metadata/internal/zzb;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "role"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->J:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/zzn;

    const-string v1, "md5Checksum"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/zzn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->K:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/zzmi$zzf;

    invoke-direct {v0, v6}, Lcom/google/android/gms/internal/zzmi$zzf;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/zzmi;->L:Lcom/google/android/gms/internal/zzmi$zzf;

    return-void
.end method
