.class Lcom/google/android/gms/internal/zzvc$zza;
.super Lcom/google/android/gms/internal/zzuy$zza;


# instance fields
.field private final a:Lcom/google/android/gms/internal/zzus;

.field private final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzus;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzuy$zza;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzvc$zza;->a:Lcom/google/android/gms/internal/zzus;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzvc$zza;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private a()Lcom/google/android/gms/common/api/GoogleApiClient$ServerAuthCodeCallbacks;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/zzvc$zza;->a:Lcom/google/android/gms/internal/zzus;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzus;->d()Lcom/google/android/gms/common/api/GoogleApiClient$ServerAuthCodeCallbacks;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/zzvc$zza;)Lcom/google/android/gms/common/api/GoogleApiClient$ServerAuthCodeCallbacks;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzvc$zza;->a()Lcom/google/android/gms/common/api/GoogleApiClient$ServerAuthCodeCallbacks;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzva;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzvc$zza;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/internal/zzvc$zza$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/internal/zzvc$zza$2;-><init>(Lcom/google/android/gms/internal/zzvc$zza;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzva;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/internal/zzva;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzvc$zza;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/internal/zzvc$zza$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/internal/zzvc$zza$1;-><init>(Lcom/google/android/gms/internal/zzvc$zza;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/internal/zzva;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
