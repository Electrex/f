.class public abstract Lcom/google/android/gms/internal/zzws;
.super Lcom/google/android/gms/internal/zzwy;


# instance fields
.field protected f:Lcom/google/android/gms/internal/zzwu;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzwy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/zzwr;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzwu;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzwu;->a(I)Lcom/google/android/gms/internal/zzwv;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/zzwv;->a(Lcom/google/android/gms/internal/zzwr;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/internal/zzws;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzwu;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzwu;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    iget-object v1, p1, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzwu;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzwu;->a()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/zzwu;->a(I)Lcom/google/android/gms/internal/zzwv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzwv;->a()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    return v1
.end method

.method protected final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzwu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/zzws;->f:Lcom/google/android/gms/internal/zzwu;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzwu;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzws;->d()Lcom/google/android/gms/internal/zzws;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/android/gms/internal/zzws;
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/internal/zzwy;->e()Lcom/google/android/gms/internal/zzwy;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzws;

    invoke-static {p0, v0}, Lcom/google/android/gms/internal/zzww;->a(Lcom/google/android/gms/internal/zzws;Lcom/google/android/gms/internal/zzws;)V

    return-object v0
.end method

.method public synthetic e()Lcom/google/android/gms/internal/zzwy;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzws;->d()Lcom/google/android/gms/internal/zzws;

    move-result-object v0

    return-object v0
.end method
