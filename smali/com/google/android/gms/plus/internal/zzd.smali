.class public interface abstract Lcom/google/android/gms/plus/internal/zzd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;IIILjava/lang/String;)Lcom/google/android/gms/common/internal/ICancelToken;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/internal/zzlk;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;Landroid/net/Uri;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;Lcom/google/android/gms/internal/zzlk;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/zzb;Ljava/util/List;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/zzkw;Lcom/google/android/gms/internal/zzkw;)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/zzb;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;)V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract c(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;)V
.end method

.method public abstract d(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;)V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e(Lcom/google/android/gms/plus/internal/zzb;Ljava/lang/String;)V
.end method
