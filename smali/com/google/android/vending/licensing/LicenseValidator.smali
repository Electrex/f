.class Lcom/google/android/vending/licensing/LicenseValidator;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/vending/licensing/Policy;

.field private final b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/vending/licensing/DeviceLimiter;


# direct methods
.method constructor <init>(Lcom/google/android/vending/licensing/Policy;Lcom/google/android/vending/licensing/DeviceLimiter;Lcom/google/android/vending/licensing/LicenseCheckerCallback;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/vending/licensing/LicenseValidator;->a:Lcom/google/android/vending/licensing/Policy;

    .line 60
    iput-object p2, p0, Lcom/google/android/vending/licensing/LicenseValidator;->f:Lcom/google/android/vending/licensing/DeviceLimiter;

    .line 61
    iput-object p3, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    .line 62
    iput p4, p0, Lcom/google/android/vending/licensing/LicenseValidator;->c:I

    .line 63
    iput-object p5, p0, Lcom/google/android/vending/licensing/LicenseValidator;->d:Ljava/lang/String;

    .line 64
    iput-object p6, p0, Lcom/google/android/vending/licensing/LicenseValidator;->e:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    invoke-interface {v0, p1}, Lcom/google/android/vending/licensing/LicenseCheckerCallback;->c(I)V

    .line 218
    return-void
.end method

.method private a(ILcom/google/android/vending/licensing/ResponseData;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->a:Lcom/google/android/vending/licensing/Policy;

    invoke-interface {v0, p1, p2}, Lcom/google/android/vending/licensing/Policy;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->a:Lcom/google/android/vending/licensing/Policy;

    invoke-interface {v0}, Lcom/google/android/vending/licensing/Policy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    invoke-interface {v0, p1}, Lcom/google/android/vending/licensing/LicenseCheckerCallback;->a(I)V

    .line 214
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    invoke-interface {v0, p1}, Lcom/google/android/vending/licensing/LicenseCheckerCallback;->b(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    const/16 v1, 0x231

    invoke-interface {v0, v1}, Lcom/google/android/vending/licensing/LicenseCheckerCallback;->b(I)V

    .line 222
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/vending/licensing/LicenseCheckerCallback;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->b:Lcom/google/android/vending/licensing/LicenseCheckerCallback;

    return-object v0
.end method

.method public a(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x123

    .line 90
    .line 93
    if-eqz p2, :cond_0

    if-eq p2, v4, :cond_0

    if-ne p2, v5, :cond_6

    .line 97
    :cond_0
    :try_start_0
    const-string v0, "SHA1withRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 99
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 101
    invoke-static {p4}, Lcom/google/android/vending/licensing/util/Base64;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 102
    const-string v0, "LicenseValidator"

    const-string v1, "Signature verification failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/vending/licensing/util/Base64DecoderException; {:try_start_0 .. :try_end_0} :catch_2

    .line 195
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 108
    :goto_1
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 110
    :catch_1
    move-exception v0

    .line 111
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(I)V

    goto :goto_0

    .line 113
    :catch_2
    move-exception v0

    .line 114
    const-string v0, "LicenseValidator"

    const-string v1, "Could not Base64-decode signature."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 121
    :cond_1
    :try_start_1
    invoke-static {p3}, Lcom/google/android/vending/licensing/ResponseData;->a(Ljava/lang/String;)Lcom/google/android/vending/licensing/ResponseData;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 128
    iget v1, v0, Lcom/google/android/vending/licensing/ResponseData;->a:I

    if-eq v1, p2, :cond_2

    .line 129
    const-string v0, "LicenseValidator"

    const-string v1, "Response codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 122
    :catch_3
    move-exception v0

    .line 123
    const-string v0, "LicenseValidator"

    const-string v1, "Could not parse response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 134
    :cond_2
    iget v1, v0, Lcom/google/android/vending/licensing/ResponseData;->b:I

    iget v2, p0, Lcom/google/android/vending/licensing/LicenseValidator;->c:I

    if-eq v1, v2, :cond_3

    .line 135
    const-string v0, "LicenseValidator"

    const-string v1, "Nonce doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 140
    :cond_3
    iget-object v1, v0, Lcom/google/android/vending/licensing/ResponseData;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/vending/licensing/LicenseValidator;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 141
    const-string v0, "LicenseValidator"

    const-string v1, "Package name doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 146
    :cond_4
    iget-object v1, v0, Lcom/google/android/vending/licensing/ResponseData;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/vending/licensing/LicenseValidator;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 147
    const-string v0, "LicenseValidator"

    const-string v1, "Version codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto :goto_0

    .line 153
    :cond_5
    iget-object v1, v0, Lcom/google/android/vending/licensing/ResponseData;->e:Ljava/lang/String;

    .line 154
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 155
    const-string v0, "LicenseValidator"

    const-string v1, "User identifier is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    .line 161
    :cond_7
    sparse-switch p2, :sswitch_data_0

    .line 192
    const-string v0, "LicenseValidator"

    const-string v1, "Unknown response code for license check."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-direct {p0}, Lcom/google/android/vending/licensing/LicenseValidator;->d()V

    goto/16 :goto_0

    .line 164
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/vending/licensing/LicenseValidator;->f:Lcom/google/android/vending/licensing/DeviceLimiter;

    invoke-interface {v2, v1}, Lcom/google/android/vending/licensing/DeviceLimiter;->a(Ljava/lang/String;)I

    move-result v1

    .line 165
    invoke-direct {p0, v1, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    goto/16 :goto_0

    .line 168
    :sswitch_1
    const/16 v1, 0x231

    invoke-direct {p0, v1, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    goto/16 :goto_0

    .line 171
    :sswitch_2
    const-string v1, "LicenseValidator"

    const-string v2, "Error contacting licensing server."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-direct {p0, v3, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    goto/16 :goto_0

    .line 175
    :sswitch_3
    const-string v1, "LicenseValidator"

    const-string v2, "An error has occurred on the licensing server."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-direct {p0, v3, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    goto/16 :goto_0

    .line 179
    :sswitch_4
    const-string v1, "LicenseValidator"

    const-string v2, "Licensing server is refusing to talk to this device, over quota."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-direct {p0, v3, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(ILcom/google/android/vending/licensing/ResponseData;)V

    goto/16 :goto_0

    .line 183
    :sswitch_5
    invoke-direct {p0, v4}, Lcom/google/android/vending/licensing/LicenseValidator;->a(I)V

    goto/16 :goto_0

    .line 186
    :sswitch_6
    invoke-direct {p0, v5}, Lcom/google/android/vending/licensing/LicenseValidator;->a(I)V

    goto/16 :goto_0

    .line 189
    :sswitch_7
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/vending/licensing/LicenseValidator;->a(I)V

    goto/16 :goto_0

    .line 106
    :catch_4
    move-exception v0

    goto/16 :goto_1

    :catch_5
    move-exception v0

    goto/16 :goto_1

    .line 161
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_7
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x101 -> :sswitch_2
        0x102 -> :sswitch_5
        0x103 -> :sswitch_6
    .end sparse-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->c:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/vending/licensing/LicenseValidator;->d:Ljava/lang/String;

    return-object v0
.end method
