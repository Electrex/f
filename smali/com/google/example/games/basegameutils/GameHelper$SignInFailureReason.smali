.class public Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1053
    const/16 v0, -0x64

    invoke-direct {p0, p1, v0}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;-><init>(II)V

    .line 1054
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->a:I

    .line 1037
    const/16 v0, -0x64

    iput v0, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    .line 1048
    iput p1, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->a:I

    .line 1049
    iput p2, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    .line 1050
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1040
    iget v0, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1044
    iget v0, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1058
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignInFailureReason(serviceErrorCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->a:I

    invoke-static {v1}, Lcom/google/example/games/basegameutils/GameHelperUtils;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    const/16 v2, -0x64

    if-ne v0, v2, :cond_0

    const-string v0, ")"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",activityResultCode:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    invoke-static {v2}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
