.class public Lcom/google/example/games/basegameutils/GameHelper;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# instance fields
.field a:Z

.field b:Z

.field c:Landroid/app/Activity;

.field d:Landroid/content/Context;

.field e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

.field f:Lcom/google/android/gms/games/Games$GamesOptions;

.field g:Lcom/google/android/gms/plus/Plus$PlusOptions;

.field h:Lcom/google/android/gms/common/api/Api$ApiOptions$NoOptions;

.field i:Lcom/google/android/gms/common/api/GoogleApiClient;

.field j:I

.field k:Z

.field l:Z

.field m:Lcom/google/android/gms/common/ConnectionResult;

.field n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

.field o:Z

.field p:Z

.field q:Landroid/os/Handler;

.field r:Lcom/google/android/gms/games/multiplayer/Invitation;

.field s:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

.field t:Ljava/util/ArrayList;

.field u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

.field v:I

.field private w:Z

.field private x:Z

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->w:Z

    .line 87
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 90
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->a:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->b:Z

    .line 101
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    .line 104
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    .line 114
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 117
    invoke-static {}, Lcom/google/android/gms/games/Games$GamesOptions;->a()Lcom/google/android/gms/games/Games$GamesOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/Games$GamesOptions$Builder;->a()Lcom/google/android/gms/games/Games$GamesOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->f:Lcom/google/android/gms/games/Games$GamesOptions;

    .line 118
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->g:Lcom/google/android/gms/plus/Plus$PlusOptions;

    .line 119
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->h:Lcom/google/android/gms/common/api/Api$ApiOptions$NoOptions;

    .line 122
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 134
    iput v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    .line 139
    iput-boolean v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 147
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->l:Z

    .line 150
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    .line 153
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .line 156
    iput-boolean v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->o:Z

    .line 159
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->p:Z

    .line 182
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    .line 188
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->v:I

    .line 756
    const-string v0, "GAMEHELPER_SHARED_PREFS"

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->y:Ljava/lang/String;

    .line 757
    const-string v0, "KEY_SIGN_IN_CANCELLATIONS"

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->z:Ljava/lang/String;

    .line 199
    iput-object p1, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    .line 200
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    .line 201
    iput p2, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    .line 202
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->q:Landroid/os/Handler;

    .line 203
    return-void
.end method

.method static a(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 992
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;II)V
    .locals 2

    .prologue
    .line 951
    if-nez p0, :cond_0

    .line 952
    const-string v0, "GameHelper"

    const-string v1, "*** No Activity. Can\'t show failure dialog!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    :goto_0
    return-void

    .line 957
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 973
    const/16 v0, 0x232a

    const/4 v1, 0x0

    invoke-static {p2, p0, v0, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    .line 975
    if-nez v0, :cond_1

    .line 977
    const-string v0, "GameHelper"

    const-string v1, "No standard error dialog available. Making fallback dialog."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/example/games/basegameutils/GameHelperUtils;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    .line 988
    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 959
    :pswitch_0
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_1

    .line 963
    :pswitch_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_1

    .line 967
    :pswitch_2
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_1

    .line 957
    nop

    :pswitch_data_0
    .packed-switch 0x2712
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/GoogleApiClient$Builder;
    .locals 3

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->w:Z

    if-eqz v0, :cond_0

    .line 274
    const-string v0, "GameHelper: you called GameHelper.createApiClientBuilder() after calling setup. You can only get a client builder BEFORE performing setup."

    .line 276
    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->c(Ljava/lang/String;)V

    .line 277
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 280
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    .line 283
    iget v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 284
    sget-object v1, Lcom/google/android/gms/games/Games;->c:Lcom/google/android/gms/common/api/Api;

    iget-object v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->f:Lcom/google/android/gms/games/Games$GamesOptions;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 285
    sget-object v1, Lcom/google/android/gms/games/Games;->b:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 288
    :cond_1
    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 289
    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 924
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnectionSuspended, cause="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 925
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->l()V

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .line 927
    const-string v0, "Making extraordinary call to onSignInFailed callback"

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 928
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 929
    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    .line 930
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v2, 0x2329

    const/4 v4, 0x0

    .line 566
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult: req="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ne p1, v2, :cond_0

    const-string v0, "RC_RESOLVE"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 570
    if-eq p1, v2, :cond_1

    .line 571
    const-string v0, "onActivityResult: request code not meant for us. Ignoring."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 619
    :goto_1
    return-void

    .line 566
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 576
    :cond_1
    iput-boolean v4, p0, Lcom/google/example/games/basegameutils/GameHelper;->a:Z

    .line 578
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    if-nez v0, :cond_2

    .line 579
    const-string v0, "onActivityResult: ignoring because we are not connecting."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 585
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 587
    const-string v0, "onAR: Resolution was RESULT_OK, so connecting current client again."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 588
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->f()V

    goto :goto_1

    .line 589
    :cond_3
    const/16 v0, 0x2711

    if-ne p2, v0, :cond_4

    .line 590
    const-string v0, "onAR: Resolution was RECONNECT_REQUIRED, so reconnecting."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->f()V

    goto :goto_1

    .line 592
    :cond_4
    if-nez p2, :cond_5

    .line 594
    const-string v0, "onAR: Got a cancellation result, so disconnecting."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->b:Z

    .line 596
    iput-boolean v4, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 597
    iput-boolean v4, p0, Lcom/google/example/games/basegameutils/GameHelper;->l:Z

    .line 598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .line 599
    iput-boolean v4, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 600
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()V

    .line 603
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->h()I

    move-result v0

    .line 604
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->i()I

    move-result v1

    .line 605
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAR: # of cancellations "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " --> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 608
    invoke-virtual {p0, v4}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    goto :goto_1

    .line 612
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAR: responseCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", so giving up."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 616
    new-instance v0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->c()I

    move-result v1

    invoke-direct {v0, v1, p2}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;)V

    goto/16 :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 716
    const-string v0, "onConnected: connected!"

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 718
    if-eqz p1, :cond_2

    .line 719
    const-string v0, "onConnected: connection hint provided. Checking for invite."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 720
    const-string v0, "invitation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 722
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 724
    const-string v1, "onConnected: connection hint has a room invite!"

    invoke-virtual {p0, v1}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 725
    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->r:Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invitation ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->r:Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 730
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/Games;->r:Lcom/google/android/gms/games/request/Requests;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/request/Requests;->a(Landroid/os/Bundle;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->t:Ljava/util/ArrayList;

    .line 732
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 734
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnected: connection hint has "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " request(s)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 738
    :cond_1
    const-string v0, "onConnected: connection hint provided. Checking for TBMP game."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 739
    const-string v0, "turn_based_match"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->s:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 744
    :cond_2
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->g()V

    .line 745
    return-void
.end method

.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 794
    const-string v2, "onConnectionFailed"

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 796
    iput-object p1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    .line 797
    const-string v2, "Connection failure:"

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 798
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->c()I

    move-result v3

    invoke-static {v3}, Lcom/google/example/games/basegameutils/GameHelperUtils;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 801
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - resolvable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 802
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - details: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 804
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->h()I

    move-result v2

    .line 807
    iget-boolean v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->l:Z

    if-eqz v3, :cond_0

    .line 808
    const-string v2, "onConnectionFailed: WILL resolve because user initiated sign-in."

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 829
    :goto_0
    if-nez v0, :cond_3

    .line 831
    const-string v0, "onConnectionFailed: since we won\'t resolve, failing now."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 832
    iput-object p1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    .line 833
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 834
    invoke-virtual {p0, v1}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    .line 844
    :goto_1
    return-void

    .line 810
    :cond_0
    iget-boolean v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->b:Z

    if-eqz v3, :cond_1

    .line 811
    const-string v0, "onConnectionFailed WILL NOT resolve (user already cancelled once)."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    move v0, v1

    .line 812
    goto :goto_0

    .line 813
    :cond_1
    iget v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->v:I

    if-ge v2, v3, :cond_2

    .line 814
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConnectionFailed: WILL resolve because we have below the max# of attempts, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/example/games/basegameutils/GameHelper;->v:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 822
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onConnectionFailed: Will NOT resolve; not user-initiated and max attempts reached: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " >= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->v:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 838
    :cond_3
    const-string v0, "onConnectionFailed: resolving problem..."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 843
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->k()V

    goto :goto_1
.end method

.method public a(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V
    .locals 2

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->w:Z

    if-eqz v0, :cond_0

    .line 302
    const-string v0, "GameHelper: you cannot call GameHelper.setup() more than once!"

    .line 303
    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->c(Ljava/lang/String;)V

    .line 304
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 306
    :cond_0
    iput-object p1, p0, Lcom/google/example/games/basegameutils/GameHelper;->u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Setup: requested clients: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    if-nez v0, :cond_1

    .line 311
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->a()Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->e:Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->w:Z

    .line 317
    return-void
.end method

.method a(Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 905
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 906
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->l()V

    .line 907
    iput-object p1, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .line 909
    iget v0, p1, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b:I

    const/16 v1, 0x2714

    if-ne v0, v1, :cond_0

    .line 911
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/example/games/basegameutils/GameHelperUtils;->a(Landroid/content/Context;)V

    .line 914
    :cond_0
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->m()V

    .line 915
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 916
    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    .line 917
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1020
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->p:Z

    if-eqz v0, :cond_0

    .line 1021
    const-string v0, "GameHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GameHelper: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    :cond_0
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Notifying LISTENER of sign-in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string v0, "SUCCESS"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    if-eqz v0, :cond_0

    .line 627
    if-eqz p1, :cond_3

    .line 628
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    invoke-interface {v0}, Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;->b()V

    .line 633
    :cond_0
    :goto_1
    return-void

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    if-eqz v0, :cond_2

    const-string v0, "FAILURE (error)"

    goto :goto_0

    :cond_2
    const-string v0, "FAILURE (no error)"

    goto :goto_0

    .line 630
    :cond_3
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->u:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    invoke-interface {v0}, Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;->a()V

    goto :goto_1
.end method

.method public b()Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-nez v0, :cond_0

    .line 325
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No GoogleApiClient. Did you call setup()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1026
    const-string v0, "GameHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "!!! GameHelper WARNING: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1030
    const-string v0, "GameHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*** GameHelper ERROR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 532
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 534
    const-string v0, "signOut: was already disconnected, ignoring."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 557
    :goto_0
    return-void

    .line 540
    :cond_0
    iget v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 541
    const-string v0, "Clearing default account on PlusClient."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 542
    sget-object v0, Lcom/google/android/gms/plus/Plus;->h:Lcom/google/android/gms/plus/Account;

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/Account;->a(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 547
    :cond_1
    iget v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->j:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 548
    const-string v0, "Signing out from the Google API Client."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-static {v0}, Lcom/google/android/gms/games/Games;->b(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    .line 553
    :cond_2
    const-string v0, "Disconnecting client."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 554
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 555
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 556
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 642
    const-string v0, "beginUserInitiatedSignIn: resetting attempt count."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 643
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->j()V

    .line 644
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->b:Z

    .line 645
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 647
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649
    const-string v0, "beginUserInitiatedSignIn() called when already connected. Calling listener directly to notify of success."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->b(Ljava/lang/String;)V

    .line 651
    invoke-virtual {p0, v1}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    .line 683
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    if-eqz v0, :cond_1

    .line 654
    const-string v0, "beginUserInitiatedSignIn() called when already connecting. Be patient! You can only call this method after you get an onSignInSucceeded() or onSignInFailed() callback. Suggestion: disable the sign-in button on startup and also when it\'s clicked, and re-enable when you get the callback."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 664
    :cond_1
    const-string v0, "Starting USER-INITIATED sign-in flow."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 669
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->l:Z

    .line 671
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_2

    .line 674
    const-string v0, "beginUserInitiatedSignIn: continuing pending sign-in flow."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 675
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 676
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->k()V

    goto :goto_0

    .line 679
    :cond_2
    const-string v0, "beginUserInitiatedSignIn: starting new sign-in flow."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 680
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 681
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->f()V

    goto :goto_0
.end method

.method f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 686
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    const-string v0, "Already connected."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 695
    :goto_0
    return-void

    .line 690
    :cond_0
    const-string v0, "Starting connection."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 691
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 692
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->r:Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 693
    iput-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->s:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 694
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b()V

    goto :goto_0
.end method

.method g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 748
    const-string v0, "succeedSignIn"

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 749
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .line 750
    iput-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->k:Z

    .line 751
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->l:Z

    .line 752
    iput-boolean v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->x:Z

    .line 753
    invoke-virtual {p0, v2}, Lcom/google/example/games/basegameutils/GameHelper;->a(Z)V

    .line 754
    return-void
.end method

.method h()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 762
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    const-string v1, "GAMEHELPER_SHARED_PREFS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 764
    const-string v1, "KEY_SIGN_IN_CANCELLATIONS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method i()I
    .locals 4

    .prologue
    .line 771
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->h()I

    move-result v0

    .line 772
    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    const-string v2, "GAMEHELPER_SHARED_PREFS"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 774
    const-string v2, "KEY_SIGN_IN_CANCELLATIONS"

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 775
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 776
    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 782
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->d:Landroid/content/Context;

    const-string v1, "GAMEHELPER_SHARED_PREFS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 784
    const-string v1, "KEY_SIGN_IN_CANCELLATIONS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 785
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 786
    return-void
.end method

.method k()V
    .locals 3

    .prologue
    .line 853
    iget-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->a:Z

    if-eqz v0, :cond_0

    .line 854
    const-string v0, "We\'re already expecting the result of a previous resolution."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 885
    :goto_0
    return-void

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 859
    const-string v0, "No need to resolve issue, activity does not exist anymore"

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 863
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolveConnectionResult: trying to resolve result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 865
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 867
    const-string v0, "Result has resolution. Starting it."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 871
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->a:Z

    .line 872
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    const/16 v2, 0x2329

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 874
    :catch_0
    move-exception v0

    .line 876
    const-string v0, "SendIntentException, so connecting again."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 877
    invoke-virtual {p0}, Lcom/google/example/games/basegameutils/GameHelper;->f()V

    goto :goto_0

    .line 882
    :cond_2
    const-string v0, "resolveConnectionResult: result has no resolution. Giving up."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 883
    new-instance v0, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->m:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;)V

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 889
    const-string v0, "Disconnecting client."

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    .line 890
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->i:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()V

    .line 895
    :goto_0
    return-void

    .line 892
    :cond_0
    const-string v0, "GameHelper"

    const-string v1, "disconnect() called when client was already disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public m()V
    .locals 3

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->a()I

    move-result v0

    .line 935
    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->b()I

    move-result v1

    .line 937
    iget-boolean v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->o:Z

    if-eqz v2, :cond_1

    .line 938
    iget-object v2, p0, Lcom/google/example/games/basegameutils/GameHelper;->c:Landroid/app/Activity;

    invoke-static {v2, v1, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Landroid/app/Activity;II)V

    .line 944
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not showing error dialog because mShowErrorDialogs==false. Error was: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/example/games/basegameutils/GameHelper;->n:Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/basegameutils/GameHelper;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
