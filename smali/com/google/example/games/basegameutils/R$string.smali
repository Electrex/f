.class public final Lcom/google/example/games/basegameutils/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f070002

.field public static final abc_action_bar_home_description_format:I = 0x7f070003

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070004

.field public static final abc_action_bar_up_description:I = 0x7f070005

.field public static final abc_action_menu_overflow_description:I = 0x7f070006

.field public static final abc_action_mode_done:I = 0x7f070007

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070008

.field public static final abc_activitychooserview_choose_application:I = 0x7f070009

.field public static final abc_searchview_description_clear:I = 0x7f07000a

.field public static final abc_searchview_description_query:I = 0x7f07000b

.field public static final abc_searchview_description_search:I = 0x7f07000c

.field public static final abc_searchview_description_submit:I = 0x7f07000d

.field public static final abc_searchview_description_voice:I = 0x7f07000e

.field public static final abc_shareactionprovider_share_with:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070010

.field public static final abc_toolbar_collapse_description:I = 0x7f070011

.field public static final app_misconfigured:I = 0x7f070020

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f070099

.field public static final common_android_wear_update_text:I = 0x7f07009a

.field public static final common_android_wear_update_title:I = 0x7f07009b

.field public static final common_google_play_services_enable_button:I = 0x7f07009c

.field public static final common_google_play_services_enable_text:I = 0x7f07009d

.field public static final common_google_play_services_enable_title:I = 0x7f07009e

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f07009f

.field public static final common_google_play_services_install_button:I = 0x7f0700a0

.field public static final common_google_play_services_install_text_phone:I = 0x7f0700a1

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0700a2

.field public static final common_google_play_services_install_title:I = 0x7f0700a3

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0700a4

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0700a5

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0700a6

.field public static final common_google_play_services_network_error_text:I = 0x7f0700a7

.field public static final common_google_play_services_network_error_title:I = 0x7f0700a8

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0700a9

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0700aa

.field public static final common_google_play_services_notification_ticker:I = 0x7f0700ab

.field public static final common_google_play_services_sign_in_failed_text:I = 0x7f0700ac

.field public static final common_google_play_services_sign_in_failed_title:I = 0x7f0700ad

.field public static final common_google_play_services_unknown_issue:I = 0x7f0700ae

.field public static final common_google_play_services_unsupported_text:I = 0x7f0700af

.field public static final common_google_play_services_unsupported_title:I = 0x7f0700b0

.field public static final common_google_play_services_update_button:I = 0x7f0700b1

.field public static final common_google_play_services_update_text:I = 0x7f0700b2

.field public static final common_google_play_services_update_title:I = 0x7f0700b3

.field public static final common_open_on_phone:I = 0x7f0700b4

.field public static final common_signin_button_text:I = 0x7f0700b5

.field public static final common_signin_button_text_long:I = 0x7f0700b6

.field public static final commono_google_play_services_api_unavailable_text:I = 0x7f0700b7

.field public static final gamehelper_app_misconfigured:I = 0x7f07012b

.field public static final gamehelper_license_failed:I = 0x7f07012c

.field public static final gamehelper_sign_in_failed:I = 0x7f07012d

.field public static final gamehelper_unknown_error:I = 0x7f07012e

.field public static final license_failed:I = 0x7f070195

.field public static final sign_in_failed:I = 0x7f070295

.field public static final unknown_error:I = 0x7f0702c7
