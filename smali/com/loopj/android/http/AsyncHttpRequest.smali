.class public Lcom/loopj/android/http/AsyncHttpRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lorg/apache/http/impl/client/AbstractHttpClient;

.field private final b:Lorg/apache/http/protocol/HttpContext;

.field private final c:Lorg/apache/http/client/methods/HttpUriRequest;

.field private final d:Lcom/loopj/android/http/ResponseHandlerInterface;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/impl/client/AbstractHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/loopj/android/http/ResponseHandlerInterface;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->a:Lorg/apache/http/impl/client/AbstractHttpClient;

    .line 49
    iput-object p2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->b:Lorg/apache/http/protocol/HttpContext;

    .line 50
    iput-object p3, p0, Lcom/loopj/android/http/AsyncHttpRequest;->c:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 51
    iput-object p4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    .line 52
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->c:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 144
    new-instance v0, Ljava/net/MalformedURLException;

    const-string v1, "No valid URI scheme was provided"

    invoke-direct {v0, v1}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->a:Lorg/apache/http/impl/client/AbstractHttpClient;

    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->c:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->b:Lorg/apache/http/protocol/HttpContext;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/client/AbstractHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    iget-object v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v1, v2, v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->a(Lcom/loopj/android/http/ResponseHandlerInterface;Lorg/apache/http/HttpResponse;)V

    .line 156
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v1, v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->a(Lorg/apache/http/HttpResponse;)V

    .line 163
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    iget-object v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v1, v2, v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->b(Lcom/loopj/android/http/ResponseHandlerInterface;Lorg/apache/http/HttpResponse;)V

    goto :goto_0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 172
    .line 173
    const/4 v0, 0x0

    .line 174
    iget-object v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->a:Lorg/apache/http/impl/client/AbstractHttpClient;

    invoke-virtual {v2}, Lorg/apache/http/impl/client/AbstractHttpClient;->getHttpRequestRetryHandler()Lorg/apache/http/client/HttpRequestRetryHandler;

    move-result-object v3

    move v2, v1

    .line 176
    :cond_0
    :goto_0
    if-eqz v2, :cond_2

    .line 178
    :try_start_0
    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->c()V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 195
    :cond_1
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 184
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UnknownHostException exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 185
    iget v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    iget-object v4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->b:Lorg/apache/http/protocol/HttpContext;

    invoke-interface {v3, v2, v0, v4}, Lorg/apache/http/client/HttpRequestRetryHandler;->retryRequest(Ljava/io/IOException;ILorg/apache/http/protocol/HttpContext;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    move-object v6, v2

    move v2, v0

    move-object v0, v6

    .line 200
    :goto_2
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v4, :cond_0

    .line 201
    iget-object v4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    iget v5, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    invoke-interface {v4, v5}, Lcom/loopj/android/http/ResponseHandlerInterface;->b(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 204
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 206
    const-string v0, "AsyncHttpRequest"

    const-string v2, "Unhandled exception origin cause"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 211
    :cond_2
    throw v0

    .line 185
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 186
    :catch_2
    move-exception v2

    .line 190
    :try_start_2
    new-instance v0, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NPE in HttpClient: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 191
    iget v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    iget-object v4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->b:Lorg/apache/http/protocol/HttpContext;

    invoke-interface {v3, v0, v2, v4}, Lorg/apache/http/client/HttpRequestRetryHandler;->retryRequest(Ljava/io/IOException;ILorg/apache/http/protocol/HttpContext;)Z

    move-result v2

    goto :goto_2

    .line 192
    :catch_3
    move-exception v0

    .line 193
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 198
    iget v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/loopj/android/http/AsyncHttpRequest;->e:I

    iget-object v4, p0, Lcom/loopj/android/http/AsyncHttpRequest;->b:Lorg/apache/http/protocol/HttpContext;

    invoke-interface {v3, v0, v2, v4}, Lorg/apache/http/client/HttpRequestRetryHandler;->retryRequest(Ljava/io/IOException;ILorg/apache/http/protocol/HttpContext;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    goto :goto_2
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->g:Z

    if-nez v0, :cond_0

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->g:Z

    .line 224
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    :cond_0
    monitor-exit p0

    return-void

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lcom/loopj/android/http/AsyncHttpRequest;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->f:Z

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->e()V

    .line 218
    :cond_0
    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->f:Z

    return v0
.end method

.method public b(Lcom/loopj/android/http/AsyncHttpRequest;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 86
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-boolean v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->i:Z

    if-nez v0, :cond_2

    .line 92
    iput-boolean v3, p0, Lcom/loopj/android/http/AsyncHttpRequest;->i:Z

    .line 93
    invoke-virtual {p0, p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a(Lcom/loopj/android/http/AsyncHttpRequest;)V

    .line 96
    :cond_2
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->e()V

    .line 104
    :cond_3
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    :try_start_0
    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_1
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v0, :cond_4

    .line 123
    iget-object v0, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    invoke-interface {v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->f()V

    .line 126
    :cond_4
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p0, p0}, Lcom/loopj/android/http/AsyncHttpRequest;->b(Lcom/loopj/android/http/AsyncHttpRequest;)V

    .line 133
    iput-boolean v3, p0, Lcom/loopj/android/http/AsyncHttpRequest;->h:Z

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    invoke-virtual {p0}, Lcom/loopj/android/http/AsyncHttpRequest;->a()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    if-eqz v1, :cond_5

    .line 112
    iget-object v1, p0, Lcom/loopj/android/http/AsyncHttpRequest;->d:Lcom/loopj/android/http/ResponseHandlerInterface;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v4, v4, v0}, Lcom/loopj/android/http/ResponseHandlerInterface;->b(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    goto :goto_1

    .line 114
    :cond_5
    const-string v1, "AsyncHttpRequest"

    const-string v2, "makeRequestWithRetries returned error, but handler is null"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
