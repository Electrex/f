.class Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;
.super Landroid/text/method/NumberKeyListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/michaelnovakjr/numberpicker/NumberPicker;


# direct methods
.method private constructor <init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-direct {p0}, Landroid/text/method/NumberKeyListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;Lcom/michaelnovakjr/numberpicker/NumberPicker$1;)V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;-><init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;)V

    return-void
.end method

.method private a(Ljava/lang/String;C)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 536
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    move v1, v0

    .line 539
    :goto_0
    if-ge v0, v2, :cond_0

    .line 540
    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 541
    if-gez v3, :cond_1

    .line 546
    :cond_0
    return v1

    .line 543
    :cond_1
    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 544
    add-int/lit8 v1, v1, 0x1

    .line 545
    goto :goto_0
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/16 v5, 0x2d

    const/4 v4, 0x0

    .line 473
    invoke-super/range {p0 .. p6}, Landroid/text/method/NumberKeyListener;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 474
    if-nez v0, :cond_0

    .line 475
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 478
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4, v4, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    invoke-interface {p4, p6, v2}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 530
    :cond_1
    :goto_0
    return-object v0

    .line 485
    :cond_2
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 490
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_3

    .line 491
    if-eqz p5, :cond_3

    .line 492
    const-string v0, ""

    goto :goto_0

    .line 495
    :cond_3
    invoke-direct {p0, v1, v5}, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a(Ljava/lang/String;C)I

    move-result v2

    .line 497
    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    .line 498
    const-string v0, ""

    goto :goto_0

    .line 501
    :cond_4
    if-lez v2, :cond_5

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_5

    .line 502
    const-string v0, ""

    goto :goto_0

    .line 506
    :cond_5
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d(Lcom/michaelnovakjr/numberpicker/NumberPicker;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    .line 507
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 508
    if-lez v2, :cond_6

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_6

    .line 509
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g(Lcom/michaelnovakjr/numberpicker/NumberPicker;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v2, v1, :cond_1

    .line 510
    const-string v0, ""

    goto :goto_0

    .line 514
    :cond_6
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g(Lcom/michaelnovakjr/numberpicker/NumberPicker;)I

    move-result v1

    if-le v2, v1, :cond_1

    .line 515
    const-string v0, ""

    goto :goto_0

    .line 520
    :cond_7
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v2, v1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Lcom/michaelnovakjr/numberpicker/NumberPicker;Ljava/lang/String;)I

    move-result v1

    .line 527
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    iget v2, v2, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    if-le v1, v2, :cond_1

    .line 528
    const-string v0, ""

    goto :goto_0
.end method

.method protected getAcceptedChars()[C
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    invoke-static {}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->e()[C

    move-result-object v0

    .line 466
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f()[C

    move-result-object v0

    goto :goto_0
.end method

.method public getInputType()I
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-static {v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    const/16 v0, 0x1002

    .line 458
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
