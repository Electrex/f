.class public Lcom/michaelnovakjr/numberpicker/NumberPicker;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field public static final a:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

.field private static final r:[C

.field private static final s:[C


# instance fields
.field protected b:I

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;

.field protected g:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

.field protected h:Z

.field protected i:J

.field private final j:Landroid/os/Handler;

.field private final k:Ljava/lang/Runnable;

.field private final l:Landroid/widget/EditText;

.field private final m:Landroid/text/InputFilter;

.field private n:[Ljava/lang/String;

.field private o:I

.field private p:Z

.field private q:Z

.field private t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

.field private u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/michaelnovakjr/numberpicker/NumberPicker$1;

    invoke-direct {v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker$1;-><init>()V

    sput-object v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

    .line 416
    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->r:[C

    .line 420
    const/16 v0, 0xb

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->s:[C

    return-void

    .line 416
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data

    .line 420
    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x2ds
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 126
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    new-instance v0, Lcom/michaelnovakjr/numberpicker/NumberPicker$2;

    invoke-direct {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker$2;-><init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;)V

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->k:Ljava/lang/Runnable;

    .line 111
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->i:J

    .line 127
    invoke-virtual {p0, v3}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setOrientation(I)V

    .line 128
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 129
    sget v1, Lcom/cgollner/systemmonitor/lib/R$layout;->number_picker:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 130
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->j:Landroid/os/Handler;

    .line 131
    new-instance v1, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberPickerInputFilter;

    invoke-direct {v1, p0, v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberPickerInputFilter;-><init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;Lcom/michaelnovakjr/numberpicker/NumberPicker$1;)V

    .line 132
    new-instance v0, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;

    invoke-direct {v0, p0, v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker$NumberRangeKeyListener;-><init>(Lcom/michaelnovakjr/numberpicker/NumberPicker;Lcom/michaelnovakjr/numberpicker/NumberPicker$1;)V

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->m:Landroid/text/InputFilter;

    .line 133
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->increment:I

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    .line 134
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setNumberPicker(Lcom/michaelnovakjr/numberpicker/NumberPicker;)V

    .line 137
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->decrement:I

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    .line 138
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setNumberPicker(Lcom/michaelnovakjr/numberpicker/NumberPicker;)V

    .line 142
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->numpicker_input:I

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    .line 143
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 144
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 145
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    new-array v2, v3, [Landroid/text/InputFilter;

    aput-object v1, v2, v4

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 147
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-virtual {p0, v4}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setEnabled(Z)V

    .line 151
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 153
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_wrap:I

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h:Z

    .line 155
    sget v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_startRange:I

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 156
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_endRange:I

    const/16 v3, 0xc8

    invoke-virtual {v2, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 157
    sget v3, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_defaultValue:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 159
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 162
    if-ge v1, v0, :cond_1

    .line 167
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b(II)V

    .line 168
    invoke-static {v3, v1, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(III)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setCurrentInternal(I)V

    .line 169
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h()V

    .line 170
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 171
    return-void

    :cond_1
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 338
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/michaelnovakjr/numberpicker/NumberPicker;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 551
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    if-nez v1, :cond_2

    .line 552
    const-string v1, "-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 580
    :goto_0
    return v0

    .line 556
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 557
    :catch_0
    move-exception v0

    .line 558
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    goto :goto_0

    .line 561
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 564
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 565
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    aget-object v1, v1, v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566
    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 574
    :cond_3
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_0

    .line 575
    :catch_1
    move-exception v0

    .line 580
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 373
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 374
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 383
    :goto_0
    return-void

    .line 381
    :cond_0
    invoke-direct {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 342
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Ljava/lang/String;)I

    move-result v0

    .line 343
    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    iget v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    invoke-static {v0, v1, v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(III)I

    move-result v0

    .line 344
    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    if-eq v1, v0, :cond_0

    .line 345
    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    iput v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->e:I

    .line 346
    iput v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    .line 347
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a()V

    .line 349
    :cond_0
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 350
    return-void
.end method

.method static synthetic a(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->p:Z

    return v0
.end method

.method static synthetic b(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->j:Landroid/os/Handler;

    return-object v0
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 226
    if-ge p2, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "End value cannot be less than the start value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    iput p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    .line 228
    iput p2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    .line 229
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    if-ge v0, p1, :cond_2

    .line 230
    iput p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    .line 234
    :cond_1
    :goto_0
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->o:I

    .line 235
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h()V

    .line 236
    return-void

    .line 231
    :cond_2
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    if-le v0, p2, :cond_1

    .line 232
    iput p2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    goto :goto_0
.end method

.method static synthetic c(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/michaelnovakjr/numberpicker/NumberPicker;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->m:Landroid/text/InputFilter;

    return-object v0
.end method

.method static synthetic e()[C
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->s:[C

    return-object v0
.end method

.method static synthetic f(Lcom/michaelnovakjr/numberpicker/NumberPicker;)Z
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic f()[C
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->r:[C

    return-object v0
.end method

.method static synthetic g(Lcom/michaelnovakjr/numberpicker/NumberPicker;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->o:I

    return v0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x2

    .line 244
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    const/16 v0, 0x1002

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 247
    return-void
.end method

.method private setCurrentInternal(I)V
    .locals 2

    .prologue
    .line 261
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    if-le v0, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Current value cannot be less than the range start."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    if-ge v0, p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Current value cannot be greater than the range end."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_1
    iput p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    .line 264
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

    invoke-interface {v0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f:Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f:Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;

    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->e:I

    iget v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    invoke-interface {v0, p0, v1, v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;->a(Lcom/michaelnovakjr/numberpicker/NumberPicker;II)V

    .line 311
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1, p2}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b(II)V

    .line 198
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 199
    return-void
.end method

.method protected b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 320
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 322
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 324
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    iget v3, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    invoke-virtual {p0, v3}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    .line 328
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v0, v1, v0

    sub-int v0, v2, v0

    .line 329
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 334
    :goto_0
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 335
    return-void

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->n:[Ljava/lang/String;

    iget v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    iget v3, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    sub-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    goto :goto_0
.end method

.method protected b(I)V
    .locals 2

    .prologue
    .line 295
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    if-le p1, v0, :cond_1

    .line 296
    iget-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    .line 300
    :goto_0
    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    iput v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->e:I

    .line 301
    iput v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    .line 303
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a()V

    .line 304
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 305
    return-void

    .line 296
    :cond_0
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    goto :goto_0

    .line 297
    :cond_1
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    if-ge p1, v0, :cond_3

    .line 298
    iget-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->c:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b:I

    goto :goto_0

    :cond_3
    move v0, p1

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->p:Z

    .line 410
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->q:Z

    .line 414
    return-void
.end method

.method public getCurrent()I
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Landroid/view/View;)V

    .line 277
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 280
    :cond_0
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->increment:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 281
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b(I)V

    .line 285
    :cond_1
    :goto_0
    return-void

    .line 282
    :cond_2
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->decrement:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 283
    iget v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b(I)V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    .line 366
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Landroid/view/View;)V

    .line 369
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 358
    if-nez p2, :cond_0

    .line 359
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(Landroid/view/View;)V

    .line 361
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 395
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 396
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 397
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->increment:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 398
    iput-boolean v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->p:Z

    .line 399
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 405
    :cond_0
    :goto_0
    return v2

    .line 400
    :cond_1
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->decrement:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 401
    iput-boolean v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->q:Z

    .line 402
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setCurrent(I)V
    .locals 0

    .prologue
    .line 250
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setCurrentInternal(I)V

    .line 251
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 252
    return-void
.end method

.method public setCurrentAndNotify(I)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setCurrentInternal(I)V

    .line 256
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a()V

    .line 257
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->b()V

    .line 258
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 176
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->t:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setEnabled(Z)V

    .line 177
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->u:Lcom/michaelnovakjr/numberpicker/NumberPickerButton;

    invoke-virtual {v0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPickerButton;->setEnabled(Z)V

    .line 178
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 179
    return-void
.end method

.method public setFormatter(Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->g:Lcom/michaelnovakjr/numberpicker/NumberPicker$Formatter;

    .line 187
    return-void
.end method

.method public setOnChangeListener(Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->f:Lcom/michaelnovakjr/numberpicker/NumberPicker$OnChangedListener;

    .line 183
    return-void
.end method

.method public setSpeed(J)V
    .locals 1

    .prologue
    .line 271
    iput-wide p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->i:J

    .line 272
    return-void
.end method

.method public setWrap(Z)V
    .locals 0

    .prologue
    .line 207
    iput-boolean p1, p0, Lcom/michaelnovakjr/numberpicker/NumberPicker;->h:Z

    .line 208
    return-void
.end method
