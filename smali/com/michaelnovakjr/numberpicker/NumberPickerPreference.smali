.class public Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"


# instance fields
.field private a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    const v0, 0x1010091

    invoke-direct {p0, p1, p2, v0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    if-nez p2, :cond_0

    .line 44
    :goto_0
    return-void

    .line 26
    :cond_0
    sget-object v0, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 27
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_startRange:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b:I

    .line 28
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_endRange:I

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->c:I

    .line 29
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_defaultValue:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->d:I

    .line 30
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_summary_uni:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->e:Ljava/lang/String;

    .line 31
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->e:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 32
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->second:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->e:Ljava/lang/String;

    .line 34
    :cond_1
    sget v1, Lcom/cgollner/systemmonitor/lib/R$styleable;->numberpicker_summary_plural:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->f:Ljava/lang/String;

    .line 35
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->f:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 36
    sget v1, Lcom/cgollner/systemmonitor/lib/R$string;->seconds:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->f:Ljava/lang/String;

    .line 38
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    sget v0, Lcom/cgollner/systemmonitor/lib/R$layout;->pref_number_picker:I

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->setDialogLayoutResource(I)V

    .line 42
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a()V

    goto :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b()I

    move-result v0

    .line 48
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->e:Ljava/lang/String;

    .line 50
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 51
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->persistInt(I)Z

    move-result v0

    return v0
.end method

.method private b()I
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->d:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 64
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->pref_num_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/michaelnovakjr/numberpicker/NumberPicker;

    iput-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    .line 65
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    iget v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b:I

    iget v2, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->a(II)V

    .line 66
    iget-object v0, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->setCurrent(I)V

    .line 67
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 73
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->b()I

    move-result v0

    .line 74
    iget-object v1, p0, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a:Lcom/michaelnovakjr/numberpicker/NumberPicker;

    invoke-virtual {v1}, Lcom/michaelnovakjr/numberpicker/NumberPicker;->getCurrent()I

    move-result v1

    .line 76
    if-eqz p1, :cond_0

    if-eq v1, v0, :cond_0

    .line 77
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0, v1}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a(I)Z

    .line 79
    invoke-direct {p0}, Lcom/michaelnovakjr/numberpicker/NumberPickerPreference;->a()V

    .line 82
    :cond_0
    return-void
.end method
