.class Lcom/path/android/jobqueue/JobManager$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic b:I

.field final synthetic c:J

.field final synthetic d:Lcom/path/android/jobqueue/BaseJob;

.field final synthetic e:Lcom/path/android/jobqueue/AsyncAddCallback;

.field final synthetic f:Lcom/path/android/jobqueue/JobManager;


# direct methods
.method constructor <init>(Lcom/path/android/jobqueue/JobManager;JIJLcom/path/android/jobqueue/BaseJob;Lcom/path/android/jobqueue/AsyncAddCallback;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lcom/path/android/jobqueue/JobManager$4;->f:Lcom/path/android/jobqueue/JobManager;

    iput-wide p2, p0, Lcom/path/android/jobqueue/JobManager$4;->a:J

    iput p4, p0, Lcom/path/android/jobqueue/JobManager$4;->b:I

    iput-wide p5, p0, Lcom/path/android/jobqueue/JobManager$4;->c:J

    iput-object p7, p0, Lcom/path/android/jobqueue/JobManager$4;->d:Lcom/path/android/jobqueue/BaseJob;

    iput-object p8, p0, Lcom/path/android/jobqueue/JobManager$4;->e:Lcom/path/android/jobqueue/AsyncAddCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 564
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/path/android/jobqueue/JobManager$4;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 565
    iget-object v2, p0, Lcom/path/android/jobqueue/JobManager$4;->f:Lcom/path/android/jobqueue/JobManager;

    iget v3, p0, Lcom/path/android/jobqueue/JobManager$4;->b:I

    const-wide/16 v4, 0x0

    iget-wide v6, p0, Lcom/path/android/jobqueue/JobManager$4;->c:J

    sub-long v0, v6, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v4, p0, Lcom/path/android/jobqueue/JobManager$4;->d:Lcom/path/android/jobqueue/BaseJob;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/path/android/jobqueue/JobManager;->a(IJLcom/path/android/jobqueue/BaseJob;)J

    move-result-wide v0

    .line 566
    iget-object v2, p0, Lcom/path/android/jobqueue/JobManager$4;->e:Lcom/path/android/jobqueue/AsyncAddCallback;

    if-eqz v2, :cond_0

    .line 567
    iget-object v2, p0, Lcom/path/android/jobqueue/JobManager$4;->e:Lcom/path/android/jobqueue/AsyncAddCallback;

    invoke-interface {v2, v0, v1}, Lcom/path/android/jobqueue/AsyncAddCallback;->a(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 569
    :catch_0
    move-exception v0

    .line 570
    const-string v1, "addJobInBackground received an exception. job class: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/path/android/jobqueue/JobManager$4;->d:Lcom/path/android/jobqueue/BaseJob;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/path/android/jobqueue/log/JqLog;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
