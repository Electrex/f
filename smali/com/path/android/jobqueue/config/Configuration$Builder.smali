.class public final Lcom/path/android/jobqueue/config/Configuration$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/path/android/jobqueue/config/Configuration;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/path/android/jobqueue/config/Configuration;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/path/android/jobqueue/config/Configuration;-><init>(Lcom/path/android/jobqueue/config/Configuration$1;)V

    iput-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->b:Landroid/content/Context;

    .line 81
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/path/android/jobqueue/config/Configuration$Builder;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    invoke-static {v0, p1}, Lcom/path/android/jobqueue/config/Configuration;->a(Lcom/path/android/jobqueue/config/Configuration;Ljava/lang/String;)Ljava/lang/String;

    .line 91
    return-object p0
.end method

.method public a()Lcom/path/android/jobqueue/config/Configuration;
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    invoke-static {v0}, Lcom/path/android/jobqueue/config/Configuration;->a(Lcom/path/android/jobqueue/config/Configuration;)Lcom/path/android/jobqueue/QueueFactory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    new-instance v1, Lcom/path/android/jobqueue/JobManager$DefaultQueueFactory;

    invoke-direct {v1}, Lcom/path/android/jobqueue/JobManager$DefaultQueueFactory;-><init>()V

    invoke-static {v0, v1}, Lcom/path/android/jobqueue/config/Configuration;->a(Lcom/path/android/jobqueue/config/Configuration;Lcom/path/android/jobqueue/QueueFactory;)Lcom/path/android/jobqueue/QueueFactory;

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    invoke-static {v0}, Lcom/path/android/jobqueue/config/Configuration;->b(Lcom/path/android/jobqueue/config/Configuration;)Lcom/path/android/jobqueue/network/NetworkUtil;

    move-result-object v0

    if-nez v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    new-instance v1, Lcom/path/android/jobqueue/network/NetworkUtilImpl;

    iget-object v2, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/path/android/jobqueue/network/NetworkUtilImpl;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/path/android/jobqueue/config/Configuration;->a(Lcom/path/android/jobqueue/config/Configuration;Lcom/path/android/jobqueue/network/NetworkUtil;)Lcom/path/android/jobqueue/network/NetworkUtil;

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration$Builder;->a:Lcom/path/android/jobqueue/config/Configuration;

    return-object v0
.end method
