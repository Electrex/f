.class public Lcom/path/android/jobqueue/config/Configuration;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Lcom/path/android/jobqueue/QueueFactory;

.field private g:Lcom/path/android/jobqueue/di/DependencyInjector;

.field private h:Lcom/path/android/jobqueue/network/NetworkUtil;

.field private i:Lcom/path/android/jobqueue/log/CustomLogger;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "default_job_manager"

    iput-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->a:Ljava/lang/String;

    .line 26
    const/4 v0, 0x5

    iput v0, p0, Lcom/path/android/jobqueue/config/Configuration;->b:I

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/path/android/jobqueue/config/Configuration;->c:I

    .line 28
    const/16 v0, 0xf

    iput v0, p0, Lcom/path/android/jobqueue/config/Configuration;->d:I

    .line 29
    const/4 v0, 0x3

    iput v0, p0, Lcom/path/android/jobqueue/config/Configuration;->e:I

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/path/android/jobqueue/config/Configuration$1;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/path/android/jobqueue/config/Configuration;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/path/android/jobqueue/config/Configuration;)Lcom/path/android/jobqueue/QueueFactory;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->f:Lcom/path/android/jobqueue/QueueFactory;

    return-object v0
.end method

.method static synthetic a(Lcom/path/android/jobqueue/config/Configuration;Lcom/path/android/jobqueue/QueueFactory;)Lcom/path/android/jobqueue/QueueFactory;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/path/android/jobqueue/config/Configuration;->f:Lcom/path/android/jobqueue/QueueFactory;

    return-object p1
.end method

.method static synthetic a(Lcom/path/android/jobqueue/config/Configuration;Lcom/path/android/jobqueue/network/NetworkUtil;)Lcom/path/android/jobqueue/network/NetworkUtil;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/path/android/jobqueue/config/Configuration;->h:Lcom/path/android/jobqueue/network/NetworkUtil;

    return-object p1
.end method

.method static synthetic a(Lcom/path/android/jobqueue/config/Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/path/android/jobqueue/config/Configuration;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/path/android/jobqueue/config/Configuration;)Lcom/path/android/jobqueue/network/NetworkUtil;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->h:Lcom/path/android/jobqueue/network/NetworkUtil;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/path/android/jobqueue/QueueFactory;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->f:Lcom/path/android/jobqueue/QueueFactory;

    return-object v0
.end method

.method public c()Lcom/path/android/jobqueue/di/DependencyInjector;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->g:Lcom/path/android/jobqueue/di/DependencyInjector;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/path/android/jobqueue/config/Configuration;->d:I

    return v0
.end method

.method public e()Lcom/path/android/jobqueue/network/NetworkUtil;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->h:Lcom/path/android/jobqueue/network/NetworkUtil;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/path/android/jobqueue/config/Configuration;->b:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/path/android/jobqueue/config/Configuration;->c:I

    return v0
.end method

.method public h()Lcom/path/android/jobqueue/log/CustomLogger;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/path/android/jobqueue/config/Configuration;->i:Lcom/path/android/jobqueue/log/CustomLogger;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/path/android/jobqueue/config/Configuration;->e:I

    return v0
.end method
