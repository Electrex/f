.class public Lcom/path/android/jobqueue/log/JqLog;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/path/android/jobqueue/log/CustomLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lcom/path/android/jobqueue/log/JqLog$1;

    invoke-direct {v0}, Lcom/path/android/jobqueue/log/JqLog$1;-><init>()V

    sput-object v0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    return-void
.end method

.method public static a(Lcom/path/android/jobqueue/log/CustomLogger;)V
    .locals 0

    .prologue
    .line 30
    sput-object p0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    .line 31
    return-void
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    invoke-interface {v0, p0, p1}, Lcom/path/android/jobqueue/log/CustomLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method public static varargs a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    invoke-interface {v0, p0, p1, p2}, Lcom/path/android/jobqueue/log/CustomLogger;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    invoke-interface {v0}, Lcom/path/android/jobqueue/log/CustomLogger;->a()Z

    move-result v0

    return v0
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/path/android/jobqueue/log/JqLog;->a:Lcom/path/android/jobqueue/log/CustomLogger;

    invoke-interface {v0, p0, p1}, Lcom/path/android/jobqueue/log/CustomLogger;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    return-void
.end method
