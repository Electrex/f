.class public Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/path/android/jobqueue/JobQueue;


# instance fields
.field public final a:Ljava/util/Comparator;

.field private b:J

.field private c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

.field private final d:Ljava/lang/String;

.field private final e:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-wide/32 v0, -0x80000000

    iput-wide v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b:J

    .line 110
    new-instance v0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue$1;

    invoke-direct {v0, p0}, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue$1;-><init>(Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;)V

    iput-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->a:Ljava/util/Comparator;

    .line 17
    iput-object p3, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->d:Ljava/lang/String;

    .line 18
    iput-wide p1, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->e:J

    .line 19
    new-instance v0, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    .line 20
    return-void
.end method

.method static synthetic a(II)I
    .locals 1

    .prologue
    .line 9
    invoke-static {p0, p1}, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(JJ)I
    .locals 2

    .prologue
    .line 9
    invoke-static {p0, p1, p2, p3}, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b(JJ)I

    move-result v0

    return v0
.end method

.method private static b(II)I
    .locals 1

    .prologue
    .line 132
    if-le p0, p1, :cond_0

    .line 133
    const/4 v0, -0x1

    .line 138
    :goto_0
    return v0

    .line 135
    :cond_0
    if-le p1, p0, :cond_1

    .line 136
    const/4 v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(JJ)I
    .locals 2

    .prologue
    .line 142
    cmp-long v0, p0, p2

    if-lez v0, :cond_0

    .line 143
    const/4 v0, -0x1

    .line 148
    :goto_0
    return v0

    .line 145
    :cond_0
    cmp-long v0, p2, p0

    if-lez v0, :cond_1

    .line 146
    const/4 v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->a()I

    move-result v0

    return v0
.end method

.method public a(ZLjava/util/Collection;)I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0, p1, p2}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->b(ZLjava/util/Collection;)Lcom/path/android/jobqueue/nonPersistentQueue/CountWithGroupIdsResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/path/android/jobqueue/nonPersistentQueue/CountWithGroupIdsResult;->a()I

    move-result v0

    return v0
.end method

.method public declared-synchronized a(Lcom/path/android/jobqueue/JobHolder;)J
    .locals 4

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b:J

    .line 28
    iget-wide v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/path/android/jobqueue/JobHolder;->a(Ljava/lang/Long;)V

    .line 29
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0, p1}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->a(Lcom/path/android/jobqueue/JobHolder;)Z

    .line 30
    invoke-virtual {p1}, Lcom/path/android/jobqueue/JobHolder;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)Ljava/lang/Long;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 90
    iget-object v1, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v1, p1, v0}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->a(ZLjava/util/Collection;)Lcom/path/android/jobqueue/JobHolder;

    move-result-object v1

    .line 91
    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/path/android/jobqueue/JobHolder;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/path/android/jobqueue/JobHolder;)J
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c(Lcom/path/android/jobqueue/JobHolder;)V

    .line 39
    const-wide/high16 v0, -0x8000000000000000L

    invoke-virtual {p1, v0, v1}, Lcom/path/android/jobqueue/JobHolder;->a(J)V

    .line 40
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0, p1}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->a(Lcom/path/android/jobqueue/JobHolder;)Z

    .line 41
    invoke-virtual {p1}, Lcom/path/android/jobqueue/JobHolder;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(ZLjava/util/Collection;)Lcom/path/android/jobqueue/JobHolder;
    .locals 6

    .prologue
    .line 70
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0, p1, p2}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->a(ZLjava/util/Collection;)Lcom/path/android/jobqueue/JobHolder;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0}, Lcom/path/android/jobqueue/JobHolder;->g()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 75
    const/4 v0, 0x0

    .line 82
    :cond_0
    :goto_0
    return-object v0

    .line 77
    :cond_1
    iget-wide v2, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/path/android/jobqueue/JobHolder;->a(J)V

    .line 78
    invoke-virtual {v0}, Lcom/path/android/jobqueue/JobHolder;->d()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/path/android/jobqueue/JobHolder;->b(I)V

    .line 79
    iget-object v1, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v1, v0}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->b(Lcom/path/android/jobqueue/JobHolder;)Z

    goto :goto_0
.end method

.method public c(Lcom/path/android/jobqueue/JobHolder;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/path/android/jobqueue/nonPersistentQueue/NonPersistentPriorityQueue;->c:Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;

    invoke-virtual {v0, p1}, Lcom/path/android/jobqueue/nonPersistentQueue/NetworkAwarePriorityQueue;->b(Lcom/path/android/jobqueue/JobHolder;)Z

    .line 50
    return-void
.end method
