.class Lde/halfbit/tinybus/TinyBus$TinyBusImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lde/halfbit/tinybus/impl/ObjectsMeta$EventDispatchCallback;
.implements Lde/halfbit/tinybus/impl/Task$TaskCallbacks;
.implements Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;


# instance fields
.field final synthetic a:Lde/halfbit/tinybus/TinyBus;

.field private b:Ljava/lang/ref/WeakReference;

.field private c:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lde/halfbit/tinybus/TinyBus;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus$Wireable;

    .line 475
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus$Wireable;->b()V

    goto :goto_0

    .line 478
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 468
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->b:Ljava/lang/ref/WeakReference;

    .line 469
    return-void

    .line 468
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 445
    iget v0, p1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->b:I

    if-ne v0, v1, :cond_0

    .line 446
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    const/16 v1, 0xb

    invoke-static {v0, v1, p3}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    .line 447
    invoke-virtual {v0, p0}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/impl/Task$TaskCallbacks;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    .line 448
    iput-object p1, v0, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    .line 449
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lde/halfbit/tinybus/impl/Task;->g:Ljava/lang/ref/WeakReference;

    .line 451
    invoke-virtual {p0}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->d()Landroid/content/Context;

    move-result-object v1

    .line 452
    invoke-static {v1}, Lde/halfbit/tinybus/impl/TinyBusDepot;->a(Landroid/content/Context;)Lde/halfbit/tinybus/impl/TinyBusDepot;

    move-result-object v1

    invoke-virtual {v1}, Lde/halfbit/tinybus/impl/TinyBusDepot;->a()Lde/halfbit/tinybus/impl/workers/Dispatcher;

    move-result-object v1

    invoke-virtual {v1, v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 457
    :goto_0
    return-void

    .line 455
    :cond_0
    iget-object v0, p1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->a:Ljava/lang/reflect/Method;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x2

    iput v0, p1, Lde/halfbit/tinybus/impl/Task;->c:I

    .line 523
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 524
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-boolean v0, v0, Lde/halfbit/tinybus/TinyBus;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->a()V

    .line 525
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus$Wireable;

    .line 484
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus$Wireable;->c()V

    goto :goto_0

    .line 487
    :cond_0
    return-void
.end method

.method public b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 2

    .prologue
    .line 529
    monitor-enter p0

    .line 530
    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    const/4 v0, 0x2

    iput v0, p1, Lde/halfbit/tinybus/impl/Task;->c:I

    .line 532
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 534
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-boolean v0, v0, Lde/halfbit/tinybus/TinyBus;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->a()V

    .line 535
    :cond_0
    return-void

    .line 532
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    iget-object v0, v0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus$Wireable;

    .line 493
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus$Wireable;->a()V

    goto :goto_0

    .line 496
    :cond_0
    monitor-enter p0

    .line 497
    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 498
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a:Lde/halfbit/tinybus/TinyBus;

    invoke-static {v0}, Lde/halfbit/tinybus/TinyBus;->a(Lde/halfbit/tinybus/TinyBus;)Landroid/os/Handler;

    move-result-object v1

    .line 499
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 500
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/Task;

    .line 501
    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 502
    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/Task;->a()V

    goto :goto_1

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 504
    :cond_1
    :try_start_1
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 506
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 507
    return-void
.end method

.method public c(Lde/halfbit/tinybus/impl/Task;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 539
    iget-object v0, p1, Lde/halfbit/tinybus/impl/Task;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 540
    if-eqz v0, :cond_0

    .line 541
    iget-object v1, p1, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    iget-object v1, v1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->a:Ljava/lang/reflect/Method;

    .line 542
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    array-length v2, v2

    if-ne v2, v3, :cond_1

    .line 544
    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p1, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    aput-object v3, v2, v4

    iget-object v3, p1, Lde/halfbit/tinybus/impl/Task;->b:Lde/halfbit/tinybus/TinyBus;

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public d()Landroid/content/Context;
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 511
    :goto_0
    if-nez v0, :cond_1

    .line 512
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must create bus with TinyBus.from(Context) method to use this function."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    goto :goto_0

    .line 515
    :cond_1
    return-object v0
.end method
