.class public abstract Lde/halfbit/tinybus/TinyBus$Wireable;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lde/halfbit/tinybus/Bus;

.field protected b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus$Wireable;->a:Lde/halfbit/tinybus/Bus;

    .line 83
    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus$Wireable;->b:Landroid/content/Context;

    .line 84
    return-void
.end method

.method protected a(Lde/halfbit/tinybus/Bus;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lde/halfbit/tinybus/TinyBus$Wireable;->a:Lde/halfbit/tinybus/Bus;

    .line 78
    iput-object p2, p0, Lde/halfbit/tinybus/TinyBus$Wireable;->b:Landroid/content/Context;

    .line 79
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus$Wireable;->a:Lde/halfbit/tinybus/Bus;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must call super.onCreate(bus, context) method when overriding it."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    return-void
.end method
