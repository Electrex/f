.class public Lde/halfbit/tinybus/TinyBus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lde/halfbit/tinybus/Bus;


# static fields
.field private static final d:Ljava/util/HashMap;


# instance fields
.field final a:Lde/halfbit/tinybus/impl/TaskQueue;

.field b:Z

.field c:Ljava/util/ArrayList;

.field private final e:Ljava/util/HashMap;

.field private final f:Ljava/util/HashMap;

.field private final g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lde/halfbit/tinybus/TinyBus;-><init>(Landroid/content/Context;)V

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    .line 143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->f:Ljava/util/HashMap;

    .line 164
    new-instance v0, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-direct {v0, p0}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;-><init>(Lde/halfbit/tinybus/TinyBus;)V

    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    .line 165
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a(Landroid/content/Context;)V

    .line 167
    new-instance v0, Lde/halfbit/tinybus/impl/TaskQueue;

    invoke-direct {v0}, Lde/halfbit/tinybus/impl/TaskQueue;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->i:Ljava/lang/Thread;

    .line 170
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    .line 171
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lde/halfbit/tinybus/TinyBus;->h:Landroid/os/Handler;

    .line 172
    return-void

    .line 171
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    goto :goto_0
.end method

.method static synthetic a(Lde/halfbit/tinybus/TinyBus;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lde/halfbit/tinybus/TinyBus;->c()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;
    .locals 3

    .prologue
    .line 122
    const-class v1, Lde/halfbit/tinybus/TinyBus;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lde/halfbit/tinybus/impl/TinyBusDepot;->a(Landroid/content/Context;)Lde/halfbit/tinybus/impl/TinyBusDepot;

    move-result-object v2

    .line 123
    invoke-virtual {v2, p0}, Lde/halfbit/tinybus/impl/TinyBusDepot;->c(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;

    move-result-object v0

    .line 124
    if-nez v0, :cond_0

    .line 125
    invoke-virtual {v2, p0}, Lde/halfbit/tinybus/impl/TinyBusDepot;->b(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 127
    :cond_0
    monitor-exit v1

    return-object v0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/Exception;)Ljava/lang/RuntimeException;
    .locals 4

    .prologue
    .line 314
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 315
    check-cast p1, Ljava/lang/RuntimeException;

    .line 324
    :goto_0
    return-object p1

    .line 317
    :cond_0
    instance-of v0, p1, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v0, :cond_1

    .line 319
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 320
    const-string v1, "at"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    .line 321
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 322
    const-string v1, "tinybus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in @Subscriber method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". See stack trace for more details."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private c()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can only call post() from a background thread, if the thread, in which TinyBus was created, had a Looper. Solution: create TinyBus in MainThread or in another thread with Looper."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->h:Landroid/os/Handler;

    return-object v0
.end method

.method private c(Ljava/lang/Class;)Lde/halfbit/tinybus/TinyBus$Wireable;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 291
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 299
    :goto_0
    return-object v0

    .line 294
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus$Wireable;

    .line 295
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 299
    goto :goto_0
.end method

.method private e(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 303
    if-nez p1, :cond_0

    .line 304
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Object must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->i:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 307
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "You must call this method from the same thread, in which TinyBus was created. Created: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lde/halfbit/tinybus/TinyBus;->i:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", current thread: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 309
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lde/halfbit/tinybus/TinyBus$Wireable;
    .locals 3

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->e(Ljava/lang/Object;)V

    .line 270
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->d()Landroid/content/Context;

    move-result-object v0

    .line 272
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->c(Ljava/lang/Class;)Lde/halfbit/tinybus/TinyBus$Wireable;

    move-result-object v1

    .line 273
    if-eqz v1, :cond_2

    .line 275
    instance-of v2, v0, Landroid/app/Application;

    if-nez v2, :cond_0

    instance-of v0, v0, Landroid/app/Service;

    if-eqz v0, :cond_1

    .line 277
    :cond_0
    invoke-virtual {v1}, Lde/halfbit/tinybus/TinyBus$Wireable;->c()V

    .line 278
    invoke-virtual {v1}, Lde/halfbit/tinybus/TinyBus$Wireable;->a()V

    .line 281
    :cond_1
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 283
    :cond_2
    return-object v1
.end method

.method public a(Lde/halfbit/tinybus/TinyBus$Wireable;)Lde/halfbit/tinybus/TinyBus;
    .locals 2

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->e(Ljava/lang/Object;)V

    .line 250
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->d()Landroid/content/Context;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    .line 255
    :cond_0
    iget-object v1, p0, Lde/halfbit/tinybus/TinyBus;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, p0, v1}, Lde/halfbit/tinybus/TinyBus$Wireable;->a(Lde/halfbit/tinybus/Bus;Landroid/content/Context;)V

    .line 258
    invoke-virtual {p1}, Lde/halfbit/tinybus/TinyBus$Wireable;->d()V

    .line 260
    instance-of v1, v0, Landroid/app/Application;

    if-nez v1, :cond_1

    instance-of v0, v0, Landroid/app/Service;

    if-eqz v0, :cond_2

    .line 262
    :cond_1
    invoke-virtual {p1}, Lde/halfbit/tinybus/TinyBus$Wireable;->b()V

    .line 264
    :cond_2
    return-object p0
.end method

.method a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    .line 337
    :goto_0
    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/TaskQueue;->a()Lde/halfbit/tinybus/impl/Task;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 338
    iget-object v2, v1, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    .line 339
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 341
    iget v0, v1, Lde/halfbit/tinybus/impl/Task;->c:I

    packed-switch v0, :pswitch_data_0

    .line 384
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected task code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lde/halfbit/tinybus/impl/Task;->c:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :catchall_0
    move-exception v0

    iput-boolean v7, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    throw v0

    .line 344
    :pswitch_0
    :try_start_1
    sget-object v0, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/ObjectsMeta;

    .line 345
    if-nez v0, :cond_0

    .line 346
    new-instance v0, Lde/halfbit/tinybus/impl/ObjectsMeta;

    invoke-direct {v0, v2}, Lde/halfbit/tinybus/impl/ObjectsMeta;-><init>(Ljava/lang/Object;)V

    .line 347
    sget-object v4, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    :cond_0
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Lde/halfbit/tinybus/impl/ObjectsMeta;->c(Ljava/lang/Object;Ljava/util/HashMap;)V

    .line 350
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Lde/halfbit/tinybus/impl/ObjectsMeta;->b(Ljava/lang/Object;Ljava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    :try_start_2
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    sget-object v4, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    iget-object v5, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v0, v2, v3, v4, v5}, Lde/halfbit/tinybus/impl/ObjectsMeta;->a(Ljava/lang/Object;Ljava/util/HashMap;Ljava/util/HashMap;Lde/halfbit/tinybus/impl/ObjectsMeta$EventDispatchCallback;)V

    .line 353
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->f:Ljava/util/HashMap;

    sget-object v4, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    iget-object v5, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v0, v3, v2, v4, v5}, Lde/halfbit/tinybus/impl/ObjectsMeta;->a(Ljava/util/HashMap;Ljava/lang/Object;Ljava/util/HashMap;Lde/halfbit/tinybus/impl/ObjectsMeta$EventDispatchCallback;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386
    :cond_1
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Lde/halfbit/tinybus/impl/Task;->a()V

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 355
    invoke-direct {p0, v0}, Lde/halfbit/tinybus/TinyBus;->a(Ljava/lang/Exception;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 361
    :pswitch_1
    sget-object v0, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/ObjectsMeta;

    .line 362
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Lde/halfbit/tinybus/impl/ObjectsMeta;->d(Ljava/lang/Object;Ljava/util/HashMap;)V

    .line 363
    iget-object v3, p0, Lde/halfbit/tinybus/TinyBus;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Lde/halfbit/tinybus/impl/ObjectsMeta;->a(Ljava/lang/Object;Ljava/util/HashMap;)V

    goto :goto_1

    .line 368
    :pswitch_2
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 369
    if-eqz v0, :cond_1

    .line 372
    :try_start_4
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 373
    sget-object v0, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/ObjectsMeta;

    .line 374
    invoke-virtual {v0, v3}, Lde/halfbit/tinybus/impl/ObjectsMeta;->a(Ljava/lang/Class;)Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    move-result-object v0

    .line 375
    iget-object v6, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    invoke-virtual {v6, v0, v5, v2}, Lde/halfbit/tinybus/TinyBus$TinyBusImpl;->a(Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 377
    :catch_1
    move-exception v0

    .line 378
    :try_start_5
    invoke-direct {p0, v0}, Lde/halfbit/tinybus/TinyBus;->a(Ljava/lang/Exception;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 390
    :cond_2
    iput-boolean v7, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    .line 392
    return-void

    .line 341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->e(Ljava/lang/Object;)V

    .line 177
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    const/4 v1, 0x0

    invoke-static {p0, v1, p1}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 178
    iget-boolean v0, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lde/halfbit/tinybus/TinyBus;->a()V

    .line 179
    :cond_0
    return-void
.end method

.method public b()Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->e(Ljava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    const/4 v1, 0x1

    invoke-static {p0, v1, p1}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 185
    iget-boolean v0, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lde/halfbit/tinybus/TinyBus;->a()V

    .line 186
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->c(Ljava/lang/Class;)Lde/halfbit/tinybus/TinyBus$Wireable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 197
    if-nez p1, :cond_0

    .line 198
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Event must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->i:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 203
    const/4 v0, 0x2

    invoke-static {p0, v0, p1}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lde/halfbit/tinybus/TinyBus;->a:Lde/halfbit/tinybus/impl/TaskQueue;

    invoke-virtual {v1, v0}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 205
    iget-boolean v0, p0, Lde/halfbit/tinybus/TinyBus;->b:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lde/halfbit/tinybus/TinyBus;->a()V

    .line 216
    :cond_1
    :goto_0
    return-void

    .line 210
    :cond_2
    iget-object v0, p0, Lde/halfbit/tinybus/TinyBus;->i:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    const/16 v0, 0xa

    invoke-static {p0, v0, p1}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    iget-object v1, p0, Lde/halfbit/tinybus/TinyBus;->g:Lde/halfbit/tinybus/TinyBus$TinyBusImpl;

    .line 212
    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/Task;->a(Lde/halfbit/tinybus/impl/Task$TaskCallbacks;)Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    .line 213
    invoke-direct {p0}, Lde/halfbit/tinybus/TinyBus;->c()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lde/halfbit/tinybus/TinyBus;->e(Ljava/lang/Object;)V

    .line 191
    sget-object v0, Lde/halfbit/tinybus/TinyBus;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/ObjectsMeta;

    .line 192
    if-eqz v0, :cond_0

    iget-object v1, p0, Lde/halfbit/tinybus/TinyBus;->e:Ljava/util/HashMap;

    iget-object v2, p0, Lde/halfbit/tinybus/TinyBus;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1, v2}, Lde/halfbit/tinybus/impl/ObjectsMeta;->a(Ljava/lang/Object;Ljava/util/HashMap;Ljava/util/HashMap;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
