.class Lde/halfbit/tinybus/impl/Task$TaskPool;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I

.field private c:Lde/halfbit/tinybus/impl/Task;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput p1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->a:I

    .line 110
    return-void
.end method


# virtual methods
.method a()Lde/halfbit/tinybus/impl/Task;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->c:Lde/halfbit/tinybus/impl/Task;

    .line 114
    if-nez v0, :cond_0

    .line 115
    new-instance v0, Lde/halfbit/tinybus/impl/Task;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lde/halfbit/tinybus/impl/Task;-><init>(Lde/halfbit/tinybus/impl/Task$1;)V

    .line 120
    :goto_0
    return-object v0

    .line 117
    :cond_0
    iget-object v1, v0, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    iput-object v1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->c:Lde/halfbit/tinybus/impl/Task;

    .line 118
    iget v1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->b:I

    goto :goto_0
.end method

.method a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->b:I

    iget v1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->a:I

    if-ge v0, v1, :cond_0

    .line 125
    iget-object v0, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->c:Lde/halfbit/tinybus/impl/Task;

    iput-object v0, p1, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    .line 126
    iput-object p1, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->c:Lde/halfbit/tinybus/impl/Task;

    .line 127
    iget v0, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lde/halfbit/tinybus/impl/Task$TaskPool;->b:I

    .line 129
    :cond_0
    return-void
.end method
