.class public Lde/halfbit/tinybus/impl/Task;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final h:Lde/halfbit/tinybus/impl/Task$TaskPool;


# instance fields
.field public a:Lde/halfbit/tinybus/impl/Task;

.field public b:Lde/halfbit/tinybus/TinyBus;

.field public c:I

.field public d:Ljava/lang/Object;

.field public e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

.field public f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

.field public g:Ljava/lang/ref/WeakReference;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lde/halfbit/tinybus/impl/Task$TaskPool;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lde/halfbit/tinybus/impl/Task$TaskPool;-><init>(I)V

    sput-object v0, Lde/halfbit/tinybus/impl/Task;->h:Lde/halfbit/tinybus/impl/Task$TaskPool;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lde/halfbit/tinybus/impl/Task$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lde/halfbit/tinybus/impl/Task;-><init>()V

    return-void
.end method

.method public static a(Lde/halfbit/tinybus/TinyBus;ILjava/lang/Object;)Lde/halfbit/tinybus/impl/Task;
    .locals 2

    .prologue
    .line 58
    sget-object v1, Lde/halfbit/tinybus/impl/Task;->h:Lde/halfbit/tinybus/impl/Task$TaskPool;

    monitor-enter v1

    .line 59
    :try_start_0
    sget-object v0, Lde/halfbit/tinybus/impl/Task;->h:Lde/halfbit/tinybus/impl/Task$TaskPool;

    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/Task$TaskPool;->a()Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    .line 60
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    iput-object p0, v0, Lde/halfbit/tinybus/impl/Task;->b:Lde/halfbit/tinybus/TinyBus;

    .line 62
    iput p1, v0, Lde/halfbit/tinybus/impl/Task;->c:I

    .line 63
    iput-object p2, v0, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    .line 64
    const/4 v1, 0x0

    iput-object v1, v0, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    .line 65
    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lde/halfbit/tinybus/impl/Task$TaskCallbacks;)Lde/halfbit/tinybus/impl/Task;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lde/halfbit/tinybus/impl/Task;->e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

    .line 70
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Lde/halfbit/tinybus/impl/Task;->b:Lde/halfbit/tinybus/TinyBus;

    .line 75
    iput-object v0, p0, Lde/halfbit/tinybus/impl/Task;->d:Ljava/lang/Object;

    .line 76
    iput-object v0, p0, Lde/halfbit/tinybus/impl/Task;->e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

    .line 77
    sget-object v1, Lde/halfbit/tinybus/impl/Task;->h:Lde/halfbit/tinybus/impl/Task$TaskPool;

    monitor-enter v1

    .line 78
    :try_start_0
    sget-object v0, Lde/halfbit/tinybus/impl/Task;->h:Lde/halfbit/tinybus/impl/Task$TaskPool;

    invoke-virtual {v0, p0}, Lde/halfbit/tinybus/impl/Task$TaskPool;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 79
    monitor-exit v1

    .line 80
    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lde/halfbit/tinybus/impl/Task;->c:I

    sparse-switch v0, :sswitch_data_0

    .line 95
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lde/halfbit/tinybus/impl/Task;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :sswitch_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/Task;->e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/impl/Task$TaskCallbacks;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 97
    :goto_0
    return-void

    .line 91
    :sswitch_1
    iget-object v0, p0, Lde/halfbit/tinybus/impl/Task;->e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/impl/Task$TaskCallbacks;->b(Lde/halfbit/tinybus/impl/Task;)V

    goto :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
