.class public Lde/halfbit/tinybus/impl/TaskQueue;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lde/halfbit/tinybus/impl/Task;

.field protected b:Lde/halfbit/tinybus/impl/Task;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lde/halfbit/tinybus/impl/Task;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 19
    iget-object v1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    if-nez v1, :cond_0

    .line 27
    :goto_0
    return-object v0

    .line 22
    :cond_0
    iget-object v1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    .line 23
    iget-object v2, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    iget-object v2, v2, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    iput-object v2, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    .line 24
    iget-object v2, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    if-nez v2, :cond_1

    .line 25
    iput-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    :cond_1
    move-object v0, v1

    .line 27
    goto :goto_0
.end method

.method public a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    if-nez v0, :cond_0

    .line 11
    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    .line 16
    :goto_0
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    iput-object p1, v0, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    .line 14
    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    goto :goto_0
.end method

.method public b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    if-nez v0, :cond_0

    .line 33
    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->b:Lde/halfbit/tinybus/impl/Task;

    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    .line 38
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    iput-object v0, p1, Lde/halfbit/tinybus/impl/Task;->a:Lde/halfbit/tinybus/impl/Task;

    .line 36
    iput-object p1, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TaskQueue;->a:Lde/halfbit/tinybus/impl/Task;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
