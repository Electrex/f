.class public Lde/halfbit/tinybus/impl/TinyBusDepot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lde/halfbit/tinybus/impl/TinyBusDepot;


# instance fields
.field private c:Lde/halfbit/tinybus/impl/workers/Dispatcher;

.field private final d:Ljava/util/WeakHashMap;

.field private final e:Landroid/util/SparseArray;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lde/halfbit/tinybus/impl/TinyBusDepot;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lde/halfbit/tinybus/impl/TinyBusDepot;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    .line 62
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 56
    invoke-virtual {v0, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Context;)Lde/halfbit/tinybus/impl/TinyBusDepot;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lde/halfbit/tinybus/impl/TinyBusDepot;->b:Lde/halfbit/tinybus/impl/TinyBusDepot;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lde/halfbit/tinybus/impl/TinyBusDepot;

    invoke-direct {v0, p0}, Lde/halfbit/tinybus/impl/TinyBusDepot;-><init>(Landroid/content/Context;)V

    sput-object v0, Lde/halfbit/tinybus/impl/TinyBusDepot;->b:Lde/halfbit/tinybus/impl/TinyBusDepot;

    .line 49
    :cond_0
    sget-object v0, Lde/halfbit/tinybus/impl/TinyBusDepot;->b:Lde/halfbit/tinybus/impl/TinyBusDepot;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()Lde/halfbit/tinybus/impl/workers/Dispatcher;
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->c:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lde/halfbit/tinybus/impl/workers/Dispatcher;

    invoke-direct {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->c:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    .line 79
    :cond_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->c:Lde/halfbit/tinybus/impl/workers/Dispatcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lde/halfbit/tinybus/TinyBus;

    invoke-direct {v0, p1}, Lde/halfbit/tinybus/TinyBus;-><init>(Landroid/content/Context;)V

    .line 67
    iget-object v1, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-object v0
.end method

.method public c(Landroid/content/Context;)Lde/halfbit/tinybus/TinyBus;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    return-object v0
.end method

.method d(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    .line 159
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->b()Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;->b()V

    .line 163
    :cond_0
    return-void
.end method

.method e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    .line 168
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_1

    check-cast p1, Landroid/app/Activity;

    .line 169
    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 171
    :goto_0
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 172
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->b()Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;->c()V

    .line 182
    :cond_0
    return-void

    .line 169
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 111
    if-eqz p2, :cond_0

    .line 114
    const-string v0, "de.halfbit.tinybus.id"

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 115
    if-le v1, v2, :cond_0

    .line 116
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    .line 123
    iget-object v2, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->delete(I)V

    .line 124
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->b()Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;

    move-result-object v1

    invoke-interface {v1, p1}, Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;->a(Landroid/content/Context;)V

    .line 125
    iget-object v1, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lde/halfbit/tinybus/impl/TinyBusDepot;->e(Landroid/content/Context;)V

    .line 107
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    .line 140
    if-eqz v0, :cond_0

    .line 143
    iget v1, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->f:I

    .line 144
    iget-object v2, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 145
    const-string v0, "de.halfbit.tinybus.id"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/TinyBus;

    .line 85
    if-eqz v0, :cond_1

    .line 89
    iget-object v1, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v1

    .line 90
    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 91
    iget-object v2, p0, Lde/halfbit/tinybus/impl/TinyBusDepot;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 94
    :cond_0
    invoke-virtual {v0}, Lde/halfbit/tinybus/TinyBus;->b()Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lde/halfbit/tinybus/impl/TinyBusDepot$LifecycleCallbacks;->a()V

    .line 97
    :cond_1
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lde/halfbit/tinybus/impl/TinyBusDepot;->d(Landroid/content/Context;)V

    .line 102
    return-void
.end method
