.class Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lde/halfbit/tinybus/impl/workers/Dispatcher;)V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 150
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->a:Ljava/lang/ref/WeakReference;

    .line 151
    return-void
.end method


# virtual methods
.method a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 180
    return-void
.end method

.method b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 188
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/workers/Dispatcher;

    .line 155
    if-nez v0, :cond_0

    .line 156
    const/16 v1, 0x64

    iput v1, p1, Landroid/os/Message;->what:I

    .line 159
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 176
    :goto_0
    return-void

    .line 161
    :sswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lde/halfbit/tinybus/impl/Task;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c(Lde/halfbit/tinybus/impl/Task;)V

    goto :goto_0

    .line 166
    :sswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lde/halfbit/tinybus/impl/Task;

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->d(Lde/halfbit/tinybus/impl/Task;)V

    goto :goto_0

    .line 171
    :sswitch_2
    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b()V

    .line 172
    invoke-virtual {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_0

    .line 159
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x64 -> :sswitch_2
    .end sparse-switch
.end method
