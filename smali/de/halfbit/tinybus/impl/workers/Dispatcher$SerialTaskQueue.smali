.class Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;
.super Lde/halfbit/tinybus/impl/TaskQueue;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Lde/halfbit/tinybus/impl/TaskQueue;-><init>()V

    .line 201
    iput-object p1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->c:Ljava/lang/String;

    .line 202
    return-void
.end method


# virtual methods
.method public a()Lde/halfbit/tinybus/impl/Task;
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    .line 225
    invoke-super {p0}, Lde/halfbit/tinybus/impl/TaskQueue;->a()Lde/halfbit/tinybus/impl/Task;

    move-result-object v0

    return-object v0
.end method

.method public a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0, p1}, Lde/halfbit/tinybus/impl/TaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 219
    iget v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    .line 220
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 209
    iput-boolean p1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->d:Z

    .line 210
    return-void
.end method

.method public b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 230
    invoke-super {p0, p1}, Lde/halfbit/tinybus/impl/TaskQueue;->b(Lde/halfbit/tinybus/impl/Task;)V

    .line 231
    iget v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->e:I

    .line 232
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->d:Z

    return v0
.end method
