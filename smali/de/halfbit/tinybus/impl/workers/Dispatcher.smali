.class public Lde/halfbit/tinybus/impl/workers/Dispatcher;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

.field private final b:Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "tinybus-dispatcher"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 49
    new-instance v1, Lde/halfbit/tinybus/impl/workers/ThreadPool;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Lde/halfbit/tinybus/impl/workers/ThreadPool;-><init>(Lde/halfbit/tinybus/impl/workers/Dispatcher;I)V

    iput-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    .line 50
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c:Ljava/util/HashMap;

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->d:Ljava/util/ArrayList;

    .line 52
    new-instance v1, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;-><init>(Landroid/os/Looper;Lde/halfbit/tinybus/impl/workers/Dispatcher;)V

    iput-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b:Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

    .line 53
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a()V

    .line 113
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;

    .line 115
    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 122
    :goto_0
    if-nez v0, :cond_1

    .line 136
    :goto_1
    return-void

    .line 126
    :cond_1
    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->a()Lde/halfbit/tinybus/impl/Task;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    invoke-virtual {v2, v1}, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a(Lde/halfbit/tinybus/impl/Task;)Z

    move-result v2

    .line 128
    if-eqz v2, :cond_2

    .line 129
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->a(Z)V

    goto :goto_1

    .line 134
    :cond_2
    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->b(Lde/halfbit/tinybus/impl/Task;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b:Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

    invoke-virtual {v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "method accessed from wrong thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    return-void
.end method

.method public a(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b:Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 63
    return-void
.end method

.method b()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a()V

    .line 108
    return-void
.end method

.method b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b:Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$DispatcherHandler;->b(Lde/halfbit/tinybus/impl/Task;)V

    .line 71
    return-void
.end method

.method c(Lde/halfbit/tinybus/impl/Task;)V
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a()V

    .line 84
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    iget-object v1, v1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;

    .line 85
    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;

    iget-object v1, p1, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    iget-object v1, v1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;-><init>(Ljava/lang/String;)V

    .line 87
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c:Ljava/util/HashMap;

    iget-object v2, p1, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    iget-object v2, v2, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->a(Lde/halfbit/tinybus/impl/Task;)V

    .line 92
    invoke-direct {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c()V

    .line 93
    return-void
.end method

.method d(Lde/halfbit/tinybus/impl/Task;)V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a()V

    .line 99
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lde/halfbit/tinybus/impl/Task;->f:Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;

    iget-object v1, v1, Lde/halfbit/tinybus/impl/ObjectsMeta$EventCallback;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;

    .line 100
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher$SerialTaskQueue;->a(Z)V

    .line 102
    invoke-virtual {p1}, Lde/halfbit/tinybus/impl/Task;->a()V

    .line 103
    invoke-direct {p0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->c()V

    .line 104
    return-void
.end method
