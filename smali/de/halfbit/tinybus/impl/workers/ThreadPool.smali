.class Lde/halfbit/tinybus/impl/workers/ThreadPool;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lde/halfbit/tinybus/impl/workers/Dispatcher;

.field private final b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;


# direct methods
.method public constructor <init>(Lde/halfbit/tinybus/impl/workers/Dispatcher;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    .line 28
    new-array v0, p2, [Lde/halfbit/tinybus/impl/workers/WorkerThread;

    iput-object v0, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a()V

    .line 60
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    array-length v1, v0

    .line 61
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 62
    iget-object v2, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    aget-object v2, v2, v0

    .line 63
    if-eqz v2, :cond_0

    .line 64
    invoke-virtual {v2}, Lde/halfbit/tinybus/impl/workers/WorkerThread;->a()V

    .line 61
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    return-void
.end method

.method a(Lde/halfbit/tinybus/impl/Task;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    invoke-virtual {v1}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->a()V

    .line 35
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    array-length v2, v1

    move v1, v0

    .line 36
    :goto_0
    if-ge v1, v2, :cond_1

    .line 37
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    aget-object v0, v0, v1

    .line 38
    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lde/halfbit/tinybus/impl/workers/WorkerThread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tinybus-worker-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lde/halfbit/tinybus/impl/workers/WorkerThread;-><init>(Lde/halfbit/tinybus/impl/workers/ThreadPool;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Lde/halfbit/tinybus/impl/workers/WorkerThread;->start()V

    .line 41
    iget-object v3, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b:[Lde/halfbit/tinybus/impl/workers/WorkerThread;

    aput-object v0, v3, v1

    .line 44
    :cond_0
    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/workers/WorkerThread;->a(Lde/halfbit/tinybus/impl/Task;)Z

    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 50
    :cond_1
    return v0

    .line 36
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method b(Lde/halfbit/tinybus/impl/Task;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/ThreadPool;->a:Lde/halfbit/tinybus/impl/workers/Dispatcher;

    invoke-virtual {v0, p1}, Lde/halfbit/tinybus/impl/workers/Dispatcher;->b(Lde/halfbit/tinybus/impl/Task;)V

    .line 55
    return-void
.end method
