.class Lde/halfbit/tinybus/impl/workers/WorkerThread;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final c:Ljava/lang/Object;

.field private d:Lde/halfbit/tinybus/impl/Task;


# direct methods
.method public constructor <init>(Lde/halfbit/tinybus/impl/workers/ThreadPool;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    .line 37
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 53
    return-void
.end method

.method public a(Lde/halfbit/tinybus/impl/Task;)Z
    .locals 2

    .prologue
    .line 41
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x0

    monitor-exit v1

    .line 47
    :goto_0
    return v0

    .line 45
    :cond_0
    iput-object p1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    .line 46
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 47
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 57
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 59
    :goto_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :goto_1
    :try_start_0
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 65
    :try_start_1
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 74
    :try_start_4
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    iget-object v0, v0, Lde/halfbit/tinybus/impl/Task;->e:Lde/halfbit/tinybus/impl/Task$TaskCallbacks;

    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/impl/Task$TaskCallbacks;->c(Lde/halfbit/tinybus/impl/Task;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 80
    iget-object v0, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    .line 81
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    .line 83
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 84
    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    invoke-virtual {v1, v0}, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b(Lde/halfbit/tinybus/impl/Task;)V

    goto :goto_0

    .line 83
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 76
    :catch_1
    move-exception v0

    .line 77
    :try_start_7
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 80
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    .line 81
    iget-object v2, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 82
    const/4 v3, 0x0

    :try_start_8
    iput-object v3, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->d:Lde/halfbit/tinybus/impl/Task;

    .line 83
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 84
    iget-object v2, p0, Lde/halfbit/tinybus/impl/workers/WorkerThread;->a:Lde/halfbit/tinybus/impl/workers/ThreadPool;

    invoke-virtual {v2, v1}, Lde/halfbit/tinybus/impl/workers/ThreadPool;->b(Lde/halfbit/tinybus/impl/Task;)V

    .line 85
    throw v0

    .line 83
    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 87
    :cond_1
    return-void
.end method
