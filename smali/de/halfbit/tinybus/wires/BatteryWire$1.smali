.class Lde/halfbit/tinybus/wires/BatteryWire$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lde/halfbit/tinybus/wires/BatteryWire;


# direct methods
.method constructor <init>(Lde/halfbit/tinybus/wires/BatteryWire;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 73
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    new-instance v1, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    invoke-direct {v1, p2}, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;-><init>(Landroid/content/Intent;)V

    invoke-static {v0, v1}, Lde/halfbit/tinybus/wires/BatteryWire;->a(Lde/halfbit/tinybus/wires/BatteryWire;Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;)Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    .line 75
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-static {v0}, Lde/halfbit/tinybus/wires/BatteryWire;->b(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;

    move-result-object v0

    iget-object v1, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-static {v1}, Lde/halfbit/tinybus/wires/BatteryWire;->a(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-static {v0}, Lde/halfbit/tinybus/wires/BatteryWire;->c(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;

    move-result-object v0

    new-instance v1, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLowEvent;

    invoke-direct {v1}, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLowEvent;-><init>()V

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_2
    const-string v1, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$1;->a:Lde/halfbit/tinybus/wires/BatteryWire;

    invoke-static {v0}, Lde/halfbit/tinybus/wires/BatteryWire;->d(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;

    move-result-object v0

    new-instance v1, Lde/halfbit/tinybus/wires/BatteryWire$BatteryOkayEvent;

    invoke-direct {v1}, Lde/halfbit/tinybus/wires/BatteryWire$BatteryOkayEvent;-><init>()V

    invoke-interface {v0, v1}, Lde/halfbit/tinybus/Bus;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method
