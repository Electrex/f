.class public Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;

.field public final b:I


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;->a:Landroid/content/Intent;

    .line 23
    const-string v2, "scale"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 24
    if-nez v2, :cond_0

    .line 25
    iput v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;->b:I

    .line 37
    :goto_0
    return-void

    .line 28
    :cond_0
    const-string v3, "level"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 29
    mul-int/lit8 v3, v3, 0x64

    div-int v2, v3, v2

    .line 30
    if-gtz v2, :cond_1

    .line 35
    :goto_1
    iput v0, p0, Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;->b:I

    goto :goto_0

    .line 32
    :cond_1
    if-lt v2, v1, :cond_2

    move v0, v1

    .line 33
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1
.end method
