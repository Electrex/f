.class public Lde/halfbit/tinybus/wires/BatteryWire;
.super Lde/halfbit/tinybus/TinyBus$Wireable;
.source "SourceFile"


# instance fields
.field private final c:Landroid/content/IntentFilter;

.field private final d:Landroid/content/BroadcastReceiver;

.field private e:Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Lde/halfbit/tinybus/TinyBus$Wireable;-><init>()V

    .line 68
    new-instance v0, Lde/halfbit/tinybus/wires/BatteryWire$1;

    invoke-direct {v0, p0}, Lde/halfbit/tinybus/wires/BatteryWire$1;-><init>(Lde/halfbit/tinybus/wires/BatteryWire;)V

    iput-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->d:Landroid/content/BroadcastReceiver;

    .line 89
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->c:Landroid/content/IntentFilter;

    .line 90
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->c:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->c:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->c:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method static synthetic a(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->e:Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    return-object v0
.end method

.method static synthetic a(Lde/halfbit/tinybus/wires/BatteryWire;Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;)Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lde/halfbit/tinybus/wires/BatteryWire;->e:Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    return-object p1
.end method

.method static synthetic b(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->a:Lde/halfbit/tinybus/Bus;

    return-object v0
.end method

.method static synthetic c(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->a:Lde/halfbit/tinybus/Bus;

    return-object v0
.end method

.method static synthetic d(Lde/halfbit/tinybus/wires/BatteryWire;)Lde/halfbit/tinybus/Bus;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->a:Lde/halfbit/tinybus/Bus;

    return-object v0
.end method


# virtual methods
.method protected b()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->a:Lde/halfbit/tinybus/Bus;

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->a(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->b:Landroid/content/Context;

    iget-object v1, p0, Lde/halfbit/tinybus/wires/BatteryWire;->d:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lde/halfbit/tinybus/wires/BatteryWire;->c:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 99
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->a:Lde/halfbit/tinybus/Bus;

    invoke-interface {v0, p0}, Lde/halfbit/tinybus/Bus;->b(Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->b:Landroid/content/Context;

    iget-object v1, p0, Lde/halfbit/tinybus/wires/BatteryWire;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->e:Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    .line 106
    return-void
.end method

.method public e()Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;
    .locals 1
    .annotation runtime Lde/halfbit/tinybus/Produce;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lde/halfbit/tinybus/wires/BatteryWire;->e:Lde/halfbit/tinybus/wires/BatteryWire$BatteryLevelEvent;

    return-object v0
.end method
