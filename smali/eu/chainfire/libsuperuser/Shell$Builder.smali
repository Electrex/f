.class public Leu/chainfire/libsuperuser/Shell$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/util/List;

.field private f:Ljava/util/Map;

.field private g:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

.field private h:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput-object v1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->a:Landroid/os/Handler;

    .line 618
    const/4 v0, 0x1

    iput-boolean v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->b:Z

    .line 619
    const-string v0, "sh"

    iput-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->c:Ljava/lang/String;

    .line 620
    iput-boolean v2, p0, Leu/chainfire/libsuperuser/Shell$Builder;->d:Z

    .line 621
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->e:Ljava/util/List;

    .line 622
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->f:Ljava/util/Map;

    .line 623
    iput-object v1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->g:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

    .line 624
    iput-object v1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->h:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

    .line 625
    iput v2, p0, Leu/chainfire/libsuperuser/Shell$Builder;->i:I

    return-void
.end method

.method static synthetic a(Leu/chainfire/libsuperuser/Shell$Builder;)Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->b:Z

    return v0
.end method

.method static synthetic b(Leu/chainfire/libsuperuser/Shell$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Leu/chainfire/libsuperuser/Shell$Builder;)Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->d:Z

    return v0
.end method

.method static synthetic d(Leu/chainfire/libsuperuser/Shell$Builder;)Ljava/util/List;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Leu/chainfire/libsuperuser/Shell$Builder;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Leu/chainfire/libsuperuser/Shell$Builder;)Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->g:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

    return-object v0
.end method

.method static synthetic g(Leu/chainfire/libsuperuser/Shell$Builder;)Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->h:Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

    return-object v0
.end method

.method static synthetic h(Leu/chainfire/libsuperuser/Shell$Builder;)I
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->i:I

    return v0
.end method

.method static synthetic i(Leu/chainfire/libsuperuser/Shell$Builder;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Builder;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()Leu/chainfire/libsuperuser/Shell$Builder;
    .locals 1

    .prologue
    .line 688
    const-string v0, "su"

    invoke-virtual {p0, v0}, Leu/chainfire/libsuperuser/Shell$Builder;->a(Ljava/lang/String;)Leu/chainfire/libsuperuser/Shell$Builder;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Leu/chainfire/libsuperuser/Shell$Builder;
    .locals 0

    .prologue
    .line 873
    iput p1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->i:I

    .line 874
    return-object p0
.end method

.method public a(Ljava/lang/String;)Leu/chainfire/libsuperuser/Shell$Builder;
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->c:Ljava/lang/String;

    .line 670
    return-object p0
.end method

.method public a(Z)Leu/chainfire/libsuperuser/Shell$Builder;
    .locals 0

    .prologue
    .line 698
    iput-boolean p1, p0, Leu/chainfire/libsuperuser/Shell$Builder;->d:Z

    .line 699
    return-object p0
.end method

.method public a(Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;)Leu/chainfire/libsuperuser/Shell$Interactive;
    .locals 2

    .prologue
    .line 907
    new-instance v0, Leu/chainfire/libsuperuser/Shell$Interactive;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Leu/chainfire/libsuperuser/Shell$Interactive;-><init>(Leu/chainfire/libsuperuser/Shell$Builder;Leu/chainfire/libsuperuser/Shell$OnCommandResultListener;Leu/chainfire/libsuperuser/Shell$1;)V

    return-object v0
.end method

.method public b(Z)Leu/chainfire/libsuperuser/Shell$Builder;
    .locals 2

    .prologue
    .line 889
    const/4 v1, 0x6

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Leu/chainfire/libsuperuser/Debug;->a(IZ)V

    .line 890
    return-object p0

    .line 889
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
