.class Leu/chainfire/libsuperuser/Shell$Interactive$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;


# instance fields
.field final synthetic a:Leu/chainfire/libsuperuser/Shell$Interactive;


# direct methods
.method constructor <init>(Leu/chainfire/libsuperuser/Shell$Interactive;)V
    .locals 0

    .prologue
    .line 1488
    iput-object p1, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1491
    iget-object v1, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    monitor-enter v1

    .line 1492
    :try_start_0
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->d(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/Shell$Command;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1493
    monitor-exit v1

    .line 1509
    :goto_0
    return-void

    .line 1495
    :cond_0
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->d(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/Shell$Command;

    move-result-object v0

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$Command;->c(Leu/chainfire/libsuperuser/Shell$Command;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1497
    :try_start_1
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v2, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->d(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/Shell$Command;

    move-result-object v2

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Command;->c(Leu/chainfire/libsuperuser/Shell$Command;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->b(Leu/chainfire/libsuperuser/Shell$Interactive;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1501
    :goto_1
    :try_start_2
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v2, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->d(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/Shell$Command;

    move-result-object v2

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Command;->c(Leu/chainfire/libsuperuser/Shell$Command;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Leu/chainfire/libsuperuser/Shell$Interactive;Ljava/lang/String;)Ljava/lang/String;

    .line 1502
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v0}, Leu/chainfire/libsuperuser/Shell$Interactive;->e(Leu/chainfire/libsuperuser/Shell$Interactive;)V

    .line 1508
    :goto_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1504
    :cond_1
    :try_start_3
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v0, p1}, Leu/chainfire/libsuperuser/Shell$Interactive;->b(Leu/chainfire/libsuperuser/Shell$Interactive;Ljava/lang/String;)V

    .line 1505
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v2, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->f(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;

    move-result-object v2

    invoke-static {v0, p1, v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Leu/chainfire/libsuperuser/Shell$Interactive;Ljava/lang/String;Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;)V

    .line 1506
    iget-object v0, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    iget-object v2, p0, Leu/chainfire/libsuperuser/Shell$Interactive$5;->a:Leu/chainfire/libsuperuser/Shell$Interactive;

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->d(Leu/chainfire/libsuperuser/Shell$Interactive;)Leu/chainfire/libsuperuser/Shell$Command;

    move-result-object v2

    invoke-static {v2}, Leu/chainfire/libsuperuser/Shell$Command;->d(Leu/chainfire/libsuperuser/Shell$Command;)Leu/chainfire/libsuperuser/Shell$OnCommandLineListener;

    move-result-object v2

    invoke-static {v0, p1, v2}, Leu/chainfire/libsuperuser/Shell$Interactive;->a(Leu/chainfire/libsuperuser/Shell$Interactive;Ljava/lang/String;Leu/chainfire/libsuperuser/StreamGobbler$OnLineListener;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1499
    :catch_0
    move-exception v0

    goto :goto_1
.end method
