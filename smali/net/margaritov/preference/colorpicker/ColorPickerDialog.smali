.class public Lnet/margaritov/preference/colorpicker/ColorPickerDialog;
.super Landroid/app/Dialog;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;


# instance fields
.field private a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

.field private b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

.field private c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

.field private d:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-direct {p0, p2}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b(I)V

    .line 51
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 57
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c(I)V

    .line 59
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 65
    sget v1, Lcom/cgollner/systemmonitor/lib/R$layout;->dialog_color_picker:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 67
    invoke-virtual {p0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->setContentView(Landroid/view/View;)V

    .line 69
    sget v0, Lcom/cgollner/systemmonitor/lib/R$string;->dialog_color_picker:I

    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->setTitle(I)V

    .line 71
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->color_picker_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/margaritov/preference/colorpicker/ColorPickerView;

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    .line 72
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->old_color_panel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    .line 73
    sget v0, Lcom/cgollner/systemmonitor/lib/R$id;->new_color_panel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    .line 75
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v0}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    invoke-virtual {v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getDrawingOffset()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    invoke-virtual {v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getDrawingOffset()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 82
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v0, p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v0, p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    invoke-virtual {v0, p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setOnColorChangedListener(Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;)V

    .line 85
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->setColor(I)V

    .line 86
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(IZ)V

    .line 88
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->setColor(I)V

    .line 101
    return-void
.end method

.method public a(Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->d:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    .line 114
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    invoke-virtual {v0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setAlphaSliderVisible(Z)V

    .line 105
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/cgollner/systemmonitor/lib/R$id;->new_color_panel:I

    if-ne v0, v1, :cond_0

    .line 123
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->d:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->d:Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v1}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->getColor()I

    move-result v1

    invoke-interface {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;->a(I)V

    .line 127
    :cond_0
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->dismiss()V

    .line 128
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    const-string v1, "old_color"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->setColor(I)V

    .line 142
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a:Lnet/margaritov/preference/colorpicker/ColorPickerView;

    const-string v1, "new_color"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(IZ)V

    .line 143
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    const-string v1, "old_color"

    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->b:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v2}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    const-string v1, "new_color"

    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->c:Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;

    invoke-virtual {v2}, Lnet/margaritov/preference/colorpicker/ColorPickerPanelView;->getColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    return-object v0
.end method
