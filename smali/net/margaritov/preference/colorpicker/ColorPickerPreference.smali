.class public Lnet/margaritov/preference/colorpicker/ColorPickerPreference;
.super Landroid/preference/Preference;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;


# instance fields
.field a:Landroid/view/View;

.field b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

.field private c:I

.field private d:F

.field private e:Z


# direct methods
.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 92
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 94
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a:Landroid/view/View;

    const v2, 0x1020018

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 95
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 97
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v3

    iget v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->d:F

    const/high16 v5, 0x41000000    # 8.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 104
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 105
    if-lez v2, :cond_2

    .line 106
    invoke-virtual {v0, v6, v2}, Landroid/widget/LinearLayout;->removeViews(II)V

    .line 108
    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 109
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setMinimumWidth(I)V

    .line 110
    new-instance v0, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    const/high16 v2, 0x40a00000    # 5.0f

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {v0, v2}, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;-><init>(I)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private b()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 115
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->d:F

    const/high16 v1, 0x41f80000    # 31.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 116
    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->c:I

    .line 117
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 118
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 119
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 121
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_4

    move v2, v3

    .line 122
    :goto_1
    if-ge v2, v6, :cond_3

    .line 123
    if-le v3, v7, :cond_0

    if-le v2, v7, :cond_0

    add-int/lit8 v0, v5, -0x2

    if-ge v3, v0, :cond_0

    add-int/lit8 v0, v6, -0x2

    if-lt v2, v0, :cond_2

    :cond_0
    const v0, -0x777778

    .line 124
    :goto_2
    invoke-virtual {v4, v3, v2, v0}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 125
    if-eq v3, v2, :cond_1

    .line 126
    invoke-virtual {v4, v2, v3, v0}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 122
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 123
    goto :goto_2

    .line 121
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 131
    :cond_4
    return-object v4
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->isPersistent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->persistInt(I)Z

    .line 139
    :cond_0
    iput p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->c:I

    .line 140
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a()V

    .line 142
    :try_start_0
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->getOnPreferenceChangeListener()Landroid/preference/Preference$OnPreferenceChangeListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 154
    new-instance v0, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->c:I

    invoke-direct {v0, v1, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    .line 155
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {v0, p0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a(Lnet/margaritov/preference/colorpicker/ColorPickerDialog$OnColorChangedListener;)V

    .line 156
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->e:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->a(Z)V

    .line 159
    :cond_0
    if-eqz p1, :cond_1

    .line 160
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {v0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 162
    :cond_1
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {v0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->show()V

    .line 163
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 86
    iput-object p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a:Landroid/view/View;

    .line 87
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a()V

    .line 88
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    const/high16 v0, -0x1000000

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a(Landroid/os/Bundle;)V

    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 247
    if-eqz p1, :cond_0

    instance-of v0, p1, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;

    if-nez v0, :cond_1

    .line 249
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 256
    :goto_0
    return-void

    .line 253
    :cond_1
    check-cast p1, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;

    .line 254
    invoke-virtual {p1}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 255
    iget-object v0, p1, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;->a:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 235
    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 236
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {v0}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 242
    :goto_0
    return-object v0

    .line 240
    :cond_1
    new-instance v0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;

    invoke-direct {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 241
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->b:Lnet/margaritov/preference/colorpicker/ColorPickerDialog;

    invoke-virtual {v1}, Lnet/margaritov/preference/colorpicker/ColorPickerDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference$SavedState;->a:Landroid/os/Bundle;

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 72
    if-eqz p1, :cond_0

    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->c:I

    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->getPersistedInt(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerPreference;->a(I)V

    .line 73
    return-void

    .line 72
    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
