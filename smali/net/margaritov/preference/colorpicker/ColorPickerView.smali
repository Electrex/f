.class public Lnet/margaritov/preference/colorpicker/ColorPickerView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private A:I

.field private B:F

.field private C:Landroid/graphics/RectF;

.field private D:Landroid/graphics/RectF;

.field private E:Landroid/graphics/RectF;

.field private F:Landroid/graphics/RectF;

.field private G:Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

.field private H:Landroid/graphics/Point;

.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Paint;

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/graphics/Paint;

.field private m:Landroid/graphics/Paint;

.field private n:Landroid/graphics/Paint;

.field private o:Landroid/graphics/Shader;

.field private p:Landroid/graphics/Shader;

.field private q:Landroid/graphics/Shader;

.field private r:Landroid/graphics/Shader;

.field private s:I

.field private t:F

.field private u:F

.field private v:F

.field private w:Ljava/lang/String;

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 150
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    .line 62
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    .line 67
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    .line 71
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    .line 76
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    .line 79
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    .line 99
    const/16 v0, 0xff

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    .line 100
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    .line 101
    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    .line 102
    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    .line 105
    const v0, -0xe3e3e4

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->x:I

    .line 106
    const v0, -0x919192

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    .line 107
    iput-boolean v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    .line 113
    iput v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->A:I

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    .line 151
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a()V

    .line 152
    return-void
.end method

.method private a(II)I
    .locals 1

    .prologue
    .line 696
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-ne p1, v0, :cond_1

    .line 699
    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPrefferedWidth()I

    move-result p2

    goto :goto_0
.end method

.method private a(F)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 350
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->E:Landroid/graphics/RectF;

    .line 351
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 353
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 355
    mul-float v3, p1, v1

    const/high16 v4, 0x43b40000    # 360.0f

    div-float/2addr v3, v4

    sub-float/2addr v1, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v2, Landroid/graphics/Point;->y:I

    .line 356
    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Point;->x:I

    .line 358
    return-object v2
.end method

.method private a(FF)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 363
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->D:Landroid/graphics/RectF;

    .line 364
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 365
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 367
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 369
    mul-float/2addr v2, p1

    iget v4, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v3, Landroid/graphics/Point;->x:I

    .line 370
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p2

    mul-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v3, Landroid/graphics/Point;->y:I

    .line 372
    return-object v3
.end method

.method private a(I)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 377
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    .line 378
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 380
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 382
    int-to-float v3, p1

    mul-float/2addr v3, v1

    const/high16 v4, 0x437f0000    # 255.0f

    div-float/2addr v3, v4

    sub-float/2addr v1, v3

    iget v3, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v2, Landroid/graphics/Point;->x:I

    .line 383
    iget v0, v0, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Point;->y:I

    .line 385
    return-object v2
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 155
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    .line 156
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    .line 157
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    .line 158
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    .line 159
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    .line 160
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    .line 162
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c()F

    move-result v0

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    .line 164
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b()V

    .line 167
    invoke-virtual {p0, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setFocusable(Z)V

    .line 168
    invoke-virtual {p0, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setFocusableInTouchMode(Z)V

    .line 169
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 233
    iget-object v8, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->D:Landroid/graphics/RectF;

    .line 236
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 237
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v0, v8, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v3, v0

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v4, v0

    iget-object v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 240
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->o:Landroid/graphics/Shader;

    if-nez v0, :cond_0

    .line 241
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    const/4 v5, -0x1

    const/high16 v6, -0x1000000

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->o:Landroid/graphics/Shader;

    .line 245
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v6

    .line 247
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v3, v8, Landroid/graphics/RectF;->right:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    const/4 v5, -0x1

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->p:Landroid/graphics/Shader;

    .line 249
    new-instance v0, Landroid/graphics/ComposeShader;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->o:Landroid/graphics/Shader;

    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->p:Landroid/graphics/Shader;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/ComposeShader;-><init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/PorterDuff$Mode;)V

    .line 250
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->h:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 252
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 254
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    invoke-direct {p0, v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(FF)Landroid/graphics/Point;

    move-result-object v0

    .line 256
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 259
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    const v2, -0x222223

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    iget-object v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 262
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 611
    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    if-nez v2, :cond_0

    .line 647
    :goto_0
    return v1

    .line 615
    :cond_0
    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    .line 616
    iget-object v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 619
    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->E:Landroid/graphics/RectF;

    int-to-float v5, v2

    int-to-float v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 620
    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->A:I

    .line 622
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b(F)F

    move-result v1

    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    :goto_1
    move v1, v0

    .line 647
    goto :goto_0

    .line 626
    :cond_1
    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->D:Landroid/graphics/RectF;

    int-to-float v5, v2

    int-to-float v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 628
    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->A:I

    .line 630
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b(FF)[F

    move-result-object v2

    .line 632
    aget v1, v2, v1

    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    .line 633
    aget v1, v2, v0

    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    goto :goto_1

    .line 637
    :cond_2
    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 639
    const/4 v1, 0x2

    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->A:I

    .line 641
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b(I)I

    move-result v1

    iput v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private b(F)F
    .locals 4

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 426
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->E:Landroid/graphics/RectF;

    .line 428
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 430
    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    .line 431
    const/4 v0, 0x0

    .line 440
    :goto_0
    mul-float/2addr v0, v3

    div-float/2addr v0, v1

    sub-float v0, v3, v0

    return v0

    .line 433
    :cond_0
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    move v0, v1

    .line 434
    goto :goto_0

    .line 437
    :cond_1
    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float v0, p1, v0

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 445
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    .line 446
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    .line 448
    int-to-float v2, p1

    iget v3, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 449
    const/4 v0, 0x0

    .line 458
    :goto_0
    mul-int/lit16 v0, v0, 0xff

    div-int/2addr v0, v1

    rsub-int v0, v0, 0xff

    return v0

    .line 451
    :cond_0
    int-to-float v2, p1

    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    move v0, v1

    .line 452
    goto :goto_0

    .line 455
    :cond_1
    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method private b(II)I
    .locals 1

    .prologue
    .line 704
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-ne p1, v0, :cond_1

    .line 707
    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPrefferedHeight()I

    move-result p2

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x1

    .line 173
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->h:Landroid/graphics/Paint;

    .line 174
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    .line 175
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->j:Landroid/graphics/Paint;

    .line 176
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    .line 177
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->l:Landroid/graphics/Paint;

    .line 178
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    .line 179
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    .line 182
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 183
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 184
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 186
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->x:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 187
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 188
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 189
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 191
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    const v1, -0xe3e3e4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    const/high16 v1, 0x41600000    # 14.0f

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 193
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 194
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 195
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 198
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 266
    iget-object v8, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->E:Landroid/graphics/RectF;

    .line 269
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 270
    iget v0, v8, Landroid/graphics/RectF;->left:F

    sub-float v1, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->top:F

    sub-float v2, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->right:F

    add-float v3, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->q:Landroid/graphics/Shader;

    if-nez v0, :cond_0

    .line 278
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d()[I

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->q:Landroid/graphics/Shader;

    .line 279
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->j:Landroid/graphics/Paint;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->q:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 282
    :cond_0
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 284
    const/high16 v0, 0x40800000    # 4.0f

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v9

    .line 286
    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    invoke-direct {p0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(F)Landroid/graphics/Point;

    move-result-object v1

    .line 288
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 289
    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 290
    iget v3, v8, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 291
    iget v3, v1, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 292
    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->bottom:F

    .line 295
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v9, v9, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 297
    return-void
.end method

.method private b(FF)[F
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 391
    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->D:Landroid/graphics/RectF;

    .line 392
    const/4 v0, 0x2

    new-array v5, v0, [F

    .line 394
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 395
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 397
    iget v0, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    move v0, v1

    .line 407
    :goto_0
    iget v6, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v6, p2, v6

    if-gez v6, :cond_2

    .line 418
    :goto_1
    const/4 v4, 0x0

    div-float v2, v7, v2

    mul-float/2addr v0, v2

    aput v0, v5, v4

    .line 419
    const/4 v0, 0x1

    div-float v2, v7, v3

    mul-float/2addr v1, v2

    sub-float v1, v7, v1

    aput v1, v5, v0

    .line 421
    return-object v5

    .line 400
    :cond_0
    iget v0, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    move v0, v2

    .line 401
    goto :goto_0

    .line 404
    :cond_1
    iget v0, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, p1, v0

    goto :goto_0

    .line 410
    :cond_2
    iget v1, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, p2, v1

    if-lez v1, :cond_3

    move v1, v3

    .line 411
    goto :goto_1

    .line 414
    :cond_3
    iget v1, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, p2, v1

    goto :goto_1
.end method

.method private c()F
    .locals 3

    .prologue
    .line 201
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->d:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 202
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 204
    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    return v0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 301
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->G:Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    if-nez v0, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v8, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    .line 306
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 307
    iget v0, v8, Landroid/graphics/RectF;->left:F

    sub-float v1, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->top:F

    sub-float v2, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->right:F

    add-float v3, v0, v4

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 315
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->G:Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    invoke-virtual {v0, p1}, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 317
    const/4 v0, 0x3

    new-array v0, v0, [F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v1, v0, v6

    const/4 v1, 0x1

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    aput v2, v0, v1

    .line 318
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v5

    .line 319
    invoke-static {v6, v0}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v6

    .line 321
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v3, v8, Landroid/graphics/RectF;->right:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->r:Landroid/graphics/Shader;

    .line 325
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->r:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 327
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 329
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_2

    .line 330
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v3, v10

    add-float/2addr v2, v3

    iget-object v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 333
    :cond_2
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v10

    div-float/2addr v0, v9

    .line 335
    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    invoke-direct {p0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(I)Landroid/graphics/Point;

    move-result-object v1

    .line 337
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 338
    iget v3, v1, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 339
    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->right:F

    .line 340
    iget v0, v8, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    sub-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->top:F

    .line 341
    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e:F

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->bottom:F

    .line 343
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v9, v9, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private d()[I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 209
    const/16 v0, 0x169

    new-array v3, v0, [I

    .line 212
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    move v1, v2

    :goto_0
    if-ltz v0, :cond_0

    .line 213
    const/4 v4, 0x3

    new-array v4, v4, [F

    int-to-float v5, v0

    aput v5, v4, v2

    const/4 v5, 0x1

    aput v6, v4, v5

    const/4 v5, 0x2

    aput v6, v4, v5

    invoke-static {v4}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v4

    aput v4, v3, v1

    .line 212
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    :cond_0
    return-object v3
.end method

.method private e()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 754
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    .line 755
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    sub-float/2addr v0, v2

    .line 757
    iget-boolean v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v2, :cond_0

    .line 758
    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    add-float/2addr v2, v3

    sub-float/2addr v0, v2

    .line 761
    :cond_0
    iget v2, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v4

    .line 762
    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v4

    .line 763
    add-float v3, v1, v0

    .line 764
    add-float/2addr v0, v2

    .line 766
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v2, v1, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->D:Landroid/graphics/RectF;

    .line 767
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 770
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    .line 772
    iget v0, v1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    sub-float/2addr v0, v2

    add-float v2, v0, v6

    .line 773
    iget v0, v1, Landroid/graphics/RectF;->top:F

    add-float v3, v0, v6

    .line 774
    iget v0, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, v6

    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    add-float/2addr v0, v5

    :goto_0
    sub-float v0, v4, v0

    .line 775
    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v6

    .line 777
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v2, v3, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->E:Landroid/graphics/RectF;

    .line 778
    return-void

    .line 774
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 782
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-nez v0, :cond_0

    .line 801
    :goto_0
    return-void

    .line 784
    :cond_0
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    .line 786
    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v4

    .line 787
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    sub-float/2addr v2, v3

    add-float/2addr v2, v4

    .line 788
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    .line 789
    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v4

    .line 791
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v1, v2, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    .line 793
    new-instance v0, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-direct {v0, v1}, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;-><init>(I)V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->G:Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    .line 794
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->G:Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;

    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lnet/margaritov/preference/colorpicker/AlphaPatternDrawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method private getPrefferedHeight()I
    .locals 3

    .prologue
    .line 726
    const/high16 v0, 0x43480000    # 200.0f

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 728
    iget-boolean v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v1, :cond_0

    .line 729
    int-to-float v0, v0

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 732
    :cond_0
    return v0
.end method

.method private getPrefferedWidth()I
    .locals 3

    .prologue
    .line 713
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPrefferedHeight()I

    move-result v0

    .line 715
    iget-boolean v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v1, :cond_0

    .line 716
    int-to-float v0, v0

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 720
    :cond_0
    int-to-float v0, v0

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    add-float/2addr v0, v1

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a(IZ)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 853
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 854
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 855
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 856
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    .line 858
    new-array v4, v8, [F

    .line 860
    invoke-static {v1, v3, v2, v4}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 862
    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    .line 863
    aget v0, v4, v5

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    .line 864
    aget v0, v4, v6

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    .line 865
    aget v0, v4, v7

    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    .line 867
    if-eqz p2, :cond_0

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    if-eqz v0, :cond_0

    .line 868
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    new-array v2, v8, [F

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v3, v2, v5

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    aput v3, v2, v6

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    aput v3, v2, v7

    invoke-static {v1, v2}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v1

    invoke-interface {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;->a(I)V

    .line 871
    :cond_0
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    .line 872
    return-void
.end method

.method public getAlphaSliderText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    return-object v0
.end method

.method public getBorderColor()I
    .locals 1

    .prologue
    .line 826
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    return v0
.end method

.method public getColor()I
    .locals 4

    .prologue
    .line 834
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    aput v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    aput v3, v1, v2

    invoke-static {v0, v1}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v0

    return v0
.end method

.method public getDrawingOffset()F
    .locals 1

    .prologue
    .line 883
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    return v0
.end method

.method public getSliderTrackerColor()I
    .locals 1

    .prologue
    .line 920
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->x:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(Landroid/graphics/Canvas;)V

    .line 226
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b(Landroid/graphics/Canvas;)V

    .line 227
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 653
    .line 656
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 657
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 659
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 660
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 662
    invoke-direct {p0, v0, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(II)I

    move-result v2

    .line 663
    invoke-direct {p0, v1, v3}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b(II)I

    move-result v1

    .line 665
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-nez v0, :cond_2

    .line 667
    int-to-float v0, v2

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    sub-float/2addr v0, v3

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 670
    if-gt v0, v1, :cond_0

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getTag()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "landscape"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 672
    :cond_0
    int-to-float v0, v1

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->c:F

    add-float/2addr v0, v2

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    add-float/2addr v0, v2

    float-to-int v0, v0

    move v5, v1

    move v1, v0

    move v0, v5

    .line 692
    :goto_0
    invoke-virtual {p0, v1, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setMeasuredDimension(II)V

    .line 693
    return-void

    :cond_1
    move v1, v2

    .line 675
    goto :goto_0

    .line 680
    :cond_2
    int-to-float v0, v1

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    sub-float/2addr v0, v3

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 682
    if-le v0, v2, :cond_3

    .line 684
    int-to-float v0, v2

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a:F

    sub-float/2addr v0, v1

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->b:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v2

    goto :goto_0

    :cond_3
    move v5, v1

    move v1, v0

    move v0, v5

    .line 687
    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 739
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 741
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    .line 742
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 743
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    int-to-float v1, p1

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 744
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 745
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->C:Landroid/graphics/RectF;

    int-to-float v1, p2

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->B:F

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 747
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->e()V

    .line 748
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->f()V

    .line 749
    invoke-direct {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g()V

    .line 750
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 567
    .line 569
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v2

    .line 595
    :goto_0
    if-eqz v1, :cond_1

    .line 597
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    if-eqz v1, :cond_0

    .line 598
    iget-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    const/4 v4, 0x3

    new-array v4, v4, [F

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v5, v4, v2

    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    aput v2, v4, v0

    const/4 v2, 0x2

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    aput v5, v4, v2

    invoke-static {v3, v4}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v2

    invoke-interface {v1, v2}, Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;->a(I)V

    .line 601
    :cond_0
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    .line 606
    :goto_1
    return v0

    .line 573
    :pswitch_0
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v1, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    .line 575
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 581
    :pswitch_1
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 587
    :pswitch_2
    const/4 v1, 0x0

    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->H:Landroid/graphics/Point;

    .line 589
    invoke-direct {p0, p1}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 606
    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 569
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/high16 v8, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 466
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 467
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 472
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v7, 0x2

    if-ne v5, v7, :cond_0

    .line 474
    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->A:I

    packed-switch v5, :pswitch_data_0

    :cond_0
    move v0, v3

    .line 550
    :goto_0
    if-eqz v0, :cond_a

    .line 552
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    const/4 v2, 0x3

    new-array v2, v2, [F

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    aput v5, v2, v3

    iget v3, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    aput v3, v2, v4

    const/4 v3, 0x2

    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    aput v5, v2, v3

    invoke-static {v1, v2}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v1

    invoke-interface {v0, v1}, Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;->a(I)V

    .line 556
    :cond_1
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    move v0, v4

    .line 561
    :goto_1
    return v0

    .line 480
    :pswitch_0
    iget v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    const/high16 v7, 0x42480000    # 50.0f

    div-float/2addr v2, v7

    add-float/2addr v5, v2

    .line 481
    iget v2, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    const/high16 v7, 0x42480000    # 50.0f

    div-float/2addr v6, v7

    sub-float/2addr v2, v6

    .line 483
    cmpg-float v6, v5, v0

    if-gez v6, :cond_3

    move v5, v0

    .line 490
    :cond_2
    :goto_2
    cmpg-float v6, v2, v0

    if-gez v6, :cond_4

    .line 497
    :goto_3
    iput v5, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->u:F

    .line 498
    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->v:F

    move v0, v4

    .line 502
    goto :goto_0

    .line 486
    :cond_3
    cmpl-float v6, v5, v1

    if-lez v6, :cond_2

    move v5, v1

    .line 487
    goto :goto_2

    .line 493
    :cond_4
    cmpl-float v0, v2, v1

    if-lez v0, :cond_c

    move v0, v1

    .line 494
    goto :goto_3

    .line 506
    :pswitch_1
    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    mul-float v2, v6, v8

    sub-float/2addr v1, v2

    .line 508
    cmpg-float v2, v1, v0

    if-gez v2, :cond_5

    .line 515
    :goto_4
    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->t:F

    move v0, v4

    .line 519
    goto :goto_0

    .line 511
    :cond_5
    const/high16 v0, 0x43b40000    # 360.0f

    cmpl-float v0, v1, v0

    if-lez v0, :cond_b

    .line 512
    const/high16 v0, 0x43b40000    # 360.0f

    goto :goto_4

    .line 523
    :pswitch_2
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->F:Landroid/graphics/RectF;

    if-nez v0, :cond_7

    :cond_6
    move v0, v3

    .line 524
    goto :goto_0

    .line 528
    :cond_7
    iget v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    int-to-float v0, v0

    mul-float v1, v2, v8

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 530
    if-gez v0, :cond_9

    move v0, v3

    .line 537
    :cond_8
    :goto_5
    iput v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->s:I

    move v0, v4

    .line 540
    goto :goto_0

    .line 533
    :cond_9
    const/16 v1, 0xff

    if-le v0, v1, :cond_8

    .line 534
    const/16 v0, 0xff

    goto :goto_5

    .line 561
    :cond_a
    invoke-super {p0, p1}, Landroid/view/View;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :cond_b
    move v0, v1

    goto :goto_4

    :cond_c
    move v0, v2

    goto :goto_3

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAlphaSliderText(I)V
    .locals 1

    .prologue
    .line 929
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 930
    invoke-virtual {p0, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->setAlphaSliderText(Ljava/lang/String;)V

    .line 931
    return-void
.end method

.method public setAlphaSliderText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 939
    iput-object p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->w:Ljava/lang/String;

    .line 940
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    .line 941
    return-void
.end method

.method public setAlphaSliderVisible(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 893
    iget-boolean v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    if-eq v0, p1, :cond_0

    .line 894
    iput-boolean p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->z:Z

    .line 901
    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->o:Landroid/graphics/Shader;

    .line 902
    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->p:Landroid/graphics/Shader;

    .line 903
    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->q:Landroid/graphics/Shader;

    .line 904
    iput-object v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->r:Landroid/graphics/Shader;

    .line 906
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->requestLayout()V

    .line 909
    :cond_0
    return-void
.end method

.method public setBorderColor(I)V
    .locals 0

    .prologue
    .line 818
    iput p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->y:I

    .line 819
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    .line 820
    return-void
.end method

.method public setColor(I)V
    .locals 1

    .prologue
    .line 842
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->a(IZ)V

    .line 843
    return-void
.end method

.method public setOnColorChangedListener(Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->g:Lnet/margaritov/preference/colorpicker/ColorPickerView$OnColorChangedListener;

    .line 811
    return-void
.end method

.method public setSliderTrackerColor(I)V
    .locals 2

    .prologue
    .line 912
    iput p1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->x:I

    .line 914
    iget-object v0, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->k:Landroid/graphics/Paint;

    iget v1, p0, Lnet/margaritov/preference/colorpicker/ColorPickerView;->x:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 916
    invoke-virtual {p0}, Lnet/margaritov/preference/colorpicker/ColorPickerView;->invalidate()V

    .line 917
    return-void
.end method
