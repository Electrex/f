.class public interface abstract Lokio/BufferedSink;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokio/Sink;


# virtual methods
.method public abstract a(Lokio/Source;)J
.end method

.method public abstract b(Ljava/lang/String;)Lokio/BufferedSink;
.end method

.method public abstract b(Lokio/ByteString;)Lokio/BufferedSink;
.end method

.method public abstract c()Lokio/Buffer;
.end method

.method public abstract c([B)Lokio/BufferedSink;
.end method

.method public abstract c([BII)Lokio/BufferedSink;
.end method

.method public abstract e()Lokio/BufferedSink;
.end method

.method public abstract e(I)Lokio/BufferedSink;
.end method

.method public abstract f(I)Lokio/BufferedSink;
.end method

.method public abstract g(I)Lokio/BufferedSink;
.end method

.method public abstract i(J)Lokio/BufferedSink;
.end method

.method public abstract t()Lokio/BufferedSink;
.end method
